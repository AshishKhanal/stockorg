/**
* Unit test to prevents new active requirement to be created for document status other than Current.
*
* @author Dibya Shrestha
* @date 2015/07/15
* 
*/
@IsTest
public class SQX_Test_1328_CreatingActiveRequirement {
    
    static Boolean runAllTests = true,
                   run_givenCreateNewActiveRequirementForCurrentDocument_CreatedWithActivationDate = false,
                   run_givenCreateNewActiveRequirementForNotCurrentDocument_NotCreated = false;
    
    static final String ERR_MSG_CannotSetRequirementToActive = 'Cannot set Requirement to active for Controlled Documents that are not Pre-Release or Current.';
    
    /**
    * This test ensures new active requirement is created for document with status Current.
    */
    public testmethod static void givenCreateNewActiveRequirementForCurrentDocument_CreatedWithActivationDate() {
        if (!runAllTests && !run_givenCreateNewActiveRequirementForCurrentDocument_CreatedWithActivationDate) {
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser) {
            Database.SaveResult saveResult;
            
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            saveResult = jf.save();

            // creating required document with current status.
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // creating new active requirement
            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = false,
                Active__c = true
            );
            
            // ACT: add new active requirement
            saveResult = Database.insert(Req1, false);
            
            System.assert(saveResult.isSuccess() == true,
                'Expected new active requirement for document with Current status to be added.');
            
            Req1 = [SELECT Id, Activation_Date__c FROM SQX_Requirement__c WHERE Id = :Req1.Id];
            
            System.assert(Req1.Activation_Date__c != null,
                'Expected activation date to be set for newly added active requirement.');
        }
    }
    
    static Boolean hasError(String errorMessage, Database.SaveResult saveResult) {
        for (Database.Error err : saveResult.getErrors()) {
            if (err.getMessage().equals(errorMessage)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
    * This test ensures new active requirement is not created for document with status other than Current.
    */
    public testmethod static void givenCreateNewActiveRequirementForNotCurrentDocument_NotCreated() {
        if (!runAllTests && !run_givenCreateNewActiveRequirementForNotCurrentDocument_NotCreated) {
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser) {
            Database.SaveResult saveResult;
            String expectedError_CannotSetRequirementToActive = 'Expected error message "' + ERR_MSG_CannotSetRequirementToActive + '"';
            
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            saveResult = Database.insert(jf1, false);

            // creating required document with draft status.
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.doc.Auto_Release__c = false;
            cDoc1.save();
            
            // creating new active requirement
            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = false,
                Active__c = true
            );
            
            // ACT: add new active requirement
            saveResult = Database.insert(Req1, false);
            
            System.assert(saveResult.isSuccess() == false,
                'Expected new active requirement for document with Draft status not to be added.');
            
            System.assert(hasError(ERR_MSG_CannotSetRequirementToActive, saveResult), expectedError_CannotSetRequirementToActive);
            
            System.assert(saveResult.isSuccess() == false,
                'Expected new active requirement for document with In Approval status not to be added.');
            
            System.assert(hasError(ERR_MSG_CannotSetRequirementToActive, saveResult), expectedError_CannotSetRequirementToActive);
            
            // set document status as Approved
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_APPROVED).save();
            
            // ACT: add new active requirement
            saveResult = Database.insert(Req1, false);
            
            System.assert(saveResult.isSuccess() == false,
                'Expected new active requirement for document with Approved status not to be added.' + cDoc1);
            
            System.assert(hasError(ERR_MSG_CannotSetRequirementToActive, saveResult), expectedError_CannotSetRequirementToActive);
            
            // set document status as Obsolete
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            
            // ACT: add new active requirement
            saveResult = Database.insert(Req1, false);
            
            System.assert(saveResult.isSuccess() == false,
                'Expected new active requirement for document with Obsolete status not to be added.');
            
            System.assert(hasError(ERR_MSG_CannotSetRequirementToActive, saveResult), expectedError_CannotSetRequirementToActive);
        }
    }

}