/*
 * Unit test of Validation Rules for Supplier Deviation object
 */ 
@isTest
public class SQX_Test_SupplierDeviation_Validate_Rule {
    
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { standardUser, adminUser});
    }
    
    /*
    * Given: given SD Close record
    * When: user try to edit the record and void or close the same record
    * Then : throws the validation error message
    */
    public static testMethod void givenSDAlreadyClose_WhenEditAndCloseTheRecord_ThenThrowErrorMessage(){
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){ 
            
            //Arrange: Create SD Record
            SQX_Test_Supplier_Deviation sdClose = new SQX_Test_Supplier_Deviation().save();

            // completing SD
            sdClose.submit();
            sdClose.initiate();


            //Close the SD Record
            sdClose.close();

            SQX_BulkifiedBase.clearAllProcessedEntities();

            // Try to Edit the record once record is closed
            sdClose.clear();
            sdClose.sd.Reason_for_Deviation__c='supplier deviation';
            
            //Act:Try to update the record 
            List<Database.SaveResult> closeResult = new SQX_DB().continueOnError().op_update(
                                                    new List<SQX_Supplier_Deviation__c>{ sdClose.sd }, 
                                                    new List<SObjectField>{
                                                        SQX_Supplier_Deviation__c.Reason_for_Deviation__c
                                                    });
            
            //Assert: display Validation Error
            System.assertEquals(1, closeResult[0].getErrors().size(), 'Expected exactly one error to be thrown but got ' + closeResult[0].getErrors().size());
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_EDIT_AFTER_LOCK_VALIDATION_ERROR_MSG, closeResult[0].getErrors()[0].getMessage());
        }
    }
    
    /*
    * Given: given SD Void record
    * When: user try to edit the record and void or closed the same record
    * Then : throws the validation error message
    */
    public static testMethod void givenSDAlreadyVoid_WhenEditAndVoidTheRecord_ThenThrowErrorMessage(){

        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){ 

            //Arrange: Create SD Record
            SQX_Test_Supplier_Deviation sdVoid = new SQX_Test_Supplier_Deviation().save();
            
            //void the SD Record
            sdVoid.void();
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // Try to Edit the record once record is void
            sdVoid.clear();
            sdVoid.sd.Reason_for_Deviation__c='supplier deviation';
            
            //Act:Try to update the record 
            List<Database.SaveResult> voidResult = new SQX_DB().continueOnError().op_update(
                                                   new List<SQX_Supplier_Deviation__c>{ sdVoid.sd }, 
                                                   new List<SObjectField>{
                                                       SQX_Supplier_Deviation__c.Reason_for_Deviation__c
                                                   });
            
            //Assert: display Validation Error
            System.assertEquals(1, voidResult[0].getErrors().size(), 'Expected exactly one error to be thrown but got ' + voidResult[0].getErrors().size());
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_EDIT_AFTER_LOCK_VALIDATION_ERROR_MSG, voidResult[0].getErrors()[0].getMessage());
        }
    }

    /**
     * GIVEN : An SD
     * WHEN : Deviation Processes are inserted with Step -1, 0 and 1
     * THEN : Error message should thrown for Deviation Processes with step -1 and 0 but not for 1
     * @story : [SQX-6427]
     */
    @isTest
    public static  void givenSD_WhenDeviationProcessesAreInsertedWithVariousSteps_ThenInvalidRecordsShouldntBeSaved(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');

        System.runAs(adminUser){

            //Arrange: Create SD record
            SQX_Test_Supplier_Deviation sdRecord = new SQX_Test_Supplier_Deviation().save();
            List<SQX_Deviation_Process__c> processes = new SQX_Deviation_Process__c[]{
                    new SQX_Deviation_Process__c (SQX_Parent__c=sdRecord.sd.id,
                                                Step__c=-1,
                                                SQX_User__c=UserInfo.getUserId(),
                                                Due_Date__c=Date.today()
                                               ),
                    new SQX_Deviation_Process__c (SQX_Parent__c=sdRecord.sd.id,
                                                Step__c=0,
                                                SQX_User__c=UserInfo.getUserId(),
                                                Due_Date__c=Date.today()
                                                ),
                    new SQX_Deviation_Process__c (SQX_Parent__c=sdRecord.sd.id,
                                                Step__c=1,
                                                SQX_User__c=UserInfo.getUserId(),
                                                Due_Date__c=Date.today()
                                                )
                    };
            //Act :Try To insert the OnBoarding Step with Step number
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(processes, new List<Schema.SObjectField>{});

            //Assert: Verify the Step Number Validation Rule Error message
            System.assert(!result[0].isSuccess(),'Step number cant be less than one');
            System.assertEquals('Step should be greater than zero',result[0].getErrors()[0].getMessage());
            System.assert(!result[1].isSuccess(),'Step number cant be less than one');
            System.assertEquals('Step should be greater than zero', result[1].getErrors()[0].getMessage());
            System.assertEquals(true, result[2].isSuccess(),'Record wasnt saved successfully' + result[2]);
        }
    }

    /*
    * Given: given SD draft record
    * When: user try to extend the record
    * Then : throws the validation error message
    */
    public static testMethod void givenSDAlreadyDraft_WhenExtendTheRecord_ThenThrowErrorMessage(){

        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){ 

            //Arrange: Create SD Record
            SQX_Test_Supplier_Deviation sdDraft = new SQX_Test_Supplier_Deviation().save();
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // Try to extend the record
            sdDraft.clear();
            sdDraft.sd.Activity_Code__c = 'extend';
            
            //Act:Try to update the record 
            List<Database.SaveResult> draftResult = new SQX_DB().continueOnError().op_update(
                                                   new List<SQX_Supplier_Deviation__c>{ sdDraft.sd }, 
                                                   new List<SObjectField>{
                                                       SQX_Supplier_Deviation__c.Reason_for_Deviation__c
                                                   });
            
            //Assert: display Validation Error
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG, draftResult[0].getErrors()[0].getMessage());
        }
    }
    
    /*
    * Given: given SD Void record
    * When: user try to extend the record
    * Then : throws the validation error message
    */
    public static testMethod void givenSDAlreadyVoid_WhenExtendTheRecord_ThenThrowErrorMessage(){

        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){ 

            //Arrange: Create SD Record
            SQX_Test_Supplier_Deviation sdVoid = new SQX_Test_Supplier_Deviation().save();
            
            //void the SD Record
            sdVoid.void();
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // Try to extend the record
            sdVoid.clear();
            sdVoid.sd.Activity_Code__c = 'extend';
            
            //Act:Try to update the record 
            List<Database.SaveResult> voidResult = new SQX_DB().continueOnError().op_update(
                                                   new List<SQX_Supplier_Deviation__c>{ sdVoid.sd }, 
                                                   new List<SObjectField>{
                                                       SQX_Supplier_Deviation__c.Reason_for_Deviation__c
                                                   });
            
            //Assert: display Validation Error
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG, voidResult[0].getErrors()[0].getMessage());
        }
    }

    /*
    * Given: given SD close record
    * When: user try to extebd the record
    * Then : record extended
    */
    public static testMethod void givenSDCloseRecord_WhenExtendTheRecord_ThenIsExtended(){
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){ 
            
            //Arrange: Create SD Record
            SQX_Test_Supplier_Deviation sdClose = new SQX_Test_Supplier_Deviation().save();

            // completing SD
            sdClose.submit();
            sdClose.initiate();


            //Close the SD Record
            sdClose.close();
            System.assertEquals('close', sdClose.sd.Activity_Code__c);
            SQX_BulkifiedBase.clearAllProcessedEntities();

            sdClose.clear();
            sdClose.sd.Activity_Code__c = 'extend';
            
            //Act:Try to extend the record 
            List<Database.SaveResult> closeResult = new SQX_DB().continueOnError().op_update(
                                                    new List<SQX_Supplier_Deviation__c>{ sdClose.sd }, 
                                                    new List<SObjectField>{
                                                        SQX_Supplier_Deviation__c.Reason_for_Deviation__c
                                                    });
            
            //Assert: Ensured that closed record should be extended
            System.assertEquals(0, closeResult[0].getErrors().size(), 'Expected closed record should be extended.');
        }
    }

    /*
     * Given: Supplier Deviation record
     * When: Record is closed with null Result value
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierDeviation_WhenRecordIsClosedWithoutResultValue_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
    
        System.runAs(standardUser) {
            
            // Arrange: Create, Submit, Initiate and Close the Supplier Deviation record
            List<SQX_Supplier_Deviation__c> supplierDeviations = new List<SQX_Supplier_Deviation__c>();
            SQX_Test_Supplier_Deviation sd1 = new SQX_Test_Supplier_Deviation().save();
            SQX_Test_Supplier_Deviation sd2 = new SQX_Test_Supplier_Deviation().save();
            
            sd1.submit();
            sd1.initiate();
            sd1.sd.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE;
            supplierDeviations.add(sd1.sd);
            
            sd2.submit();
            sd2.initiate();
            sd2.sd.compliancequest__Result__c = null;
            sd2.sd.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE; 
            supplierDeviations.add(sd2.sd);
            
            //Act:Insert the record
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(supplierDeviations, new List<Schema.SObjectField>{});
            
            // Assert: Validation error
            System.assertEquals(true, result[0].isSuccess(), 'Supplier Deviation should be saved. ' + result[0].getErrors());
            System.assertEquals(false, result[1].isSuccess(), 'Supplier Deviation should not be saved. ' + result[1].getErrors());
            String expErrMsg = 'Supplier Deviation Result is required.';
            System.assert(SQX_Utilities.checkErrorMessage(result[1].getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result[1].getErrors());
        }
    }

      /**
      * Given: supplier deviation  record in open status,
      * When: deviation record is edited
      * Then: Validation error msg, 'Deviation Process step in open status cannot be edited.' is thrown.	
      */
     @isTest
    public static void givenDeviationStep_WhenUserEditRecord_ErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> {adminUser, standardUser});
        System.runAs(adminUser){
             //Arrange: Create policy task
             SQX_Test_Supplier_Deviation.createPolicyTasks(1, 'Task', standardUser, 1);
         }
         
         
         System.runAs(standardUser){  
             // Act: Create supplier deviation  record
             SQX_Test_Supplier_Deviation supDev = new SQX_Test_Supplier_Deviation().save();
             
             //Act: Submit the record
             supDev.submit();
             supDev.initiate();
 
             SQX_Deviation_Process__c devStep = [Select id, name, status__c, result__c , comment__c
                                                      from SQX_Deviation_Process__c where status__c =  :SQX_Steps_Trigger_Handler.STATUS_OPEN and
                                                      SQX_Parent__c = :supDev.sd.id limit 1];
             //Act: Any allowed field is edited
             devStep.Comment__c = 'Just a random comment.';
             List<Database.SaveResult> resultAgain =  new SQX_DB().continueOnError().op_update(new List<SQX_Deviation_Process__c> {devStep}, 
                                                                                               new List<Schema.SObjectField>{});
             //Assert: Make sure that fields are updated
             System.assertEquals(true, resultAgain[0].isSuccess());
             
             //Act: try updating field that is not allowed to be modified
             devStep.allowed_days__c = 5;
             List<Database.SaveResult> result =  new SQX_DB().continueOnError().op_update(new List<SQX_Deviation_Process__c> {devStep}, new List<Schema.SObjectField>{});
             
             //Assert: Validation error message is thrown
             System.assertEquals('Deviation Process step in open status cannot be edited.',result[0].getErrors()[0].getMessage());  
             
         } 
     }
    
    /**
     * Given: Supplier deviation  record with policy task
     * When: Assignee is blank in deviation process step
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierDeviation_WhenAssigneeIsBlankInDeviationProcess_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){
            //Act: Create supplier document record
            SQX_Test_Supplier_Deviation supDev = new SQX_Test_Supplier_Deviation().save();
            
            //Act: Submit the record
            supDev.submit();
            
            //Act: Set blank Assignee  
            SQX_Deviation_Process__c devStep = [SELECT Id,Status__c, SQX_User__c FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: supDev.sd.Id];
            devStep.Status__c = SQX_Supplier_Common_Values.STATUS_OPEN;
            devStep.SQX_User__c = null;
            Database.SaveResult result = Database.update(devStep, false);
            
            //Assert: Assignee field should not be empty
            String ExpErrMsg = 'Assignee cannot be blank.';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage(), 'Assignee must be set in deviation process.');
        }
    }
    /**
     * Given: Supplier Deviation with policy task 
     * When: Obsolete controlled document is referred to task
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierDeviation_WhenObsoleteDocumentReferred_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> {adminUser, standardUser});
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){    
            //Arrange: Create controlled document
            SQX_Test_Controlled_Document testControlledDocument = new SQX_Test_Controlled_Document();
            testControlledDocument.doc.Document_Status__c = SQX_Controlled_Document.STATUS_OBSOLETE;
            testControlledDocument.save();
        
            // Act: Create supplier document record
            SQX_Test_Supplier_Deviation supDev = new SQX_Test_Supplier_Deviation().save();
            
            //Act: Submit the record
            supDev.submit();
            
            //Act: Set obsolete document in step
            SQX_Deviation_Process__c devStep = [SELECT Id, Status__c, SQX_Controlled_Document__c FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: supDev.sd.Id];
            devStep.Status__c = SQX_Supplier_Common_Values.STATUS_OPEN;
            devStep.SQX_Controlled_Document__c = testControlledDocument.doc.Id;
            Database.SaveResult result = Database.update(devStep, false);
            
            //Assert: Obsolete controlled document should not be set in step
            String ExpErrMsg = 'Only Pre-Release and Current Controlled Documents can be referred.';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage());
        }
    }
    
    /**
     * Given: Supplier Deviation with policy task 
     * When: Voided Audit is used in deviation process step
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierDeviation_WhenVoidedAuditUsedInStep_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){    
            //Arrange: Create Audit record
            SQX_Test_Audit testAudit = new SQX_Test_Audit(adminUser).save();
            testAudit.audit.Status__c = SQX_Audit.STATUS_VOID;
            testAudit.save();
        
            // Act: Create supplier document record
            SQX_Test_Supplier_Deviation supDev = new SQX_Test_Supplier_Deviation().save();
            
            //Act: Submit the record
            supDev.submit();
            
            //Act: Set voided Audit in step
            SQX_Deviation_Process__c devStep = [SELECT Id, SQX_Audit_Number__c FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: supDev.sd.Id];
            devStep.SQX_Audit_Number__c = testAudit.audit.Id;
            Database.SaveResult result = Database.update(devStep, false);
            
            //Assert: Voided Audit should not be set in step
            String ExpErrMsg = 'Audit with void status cannot be used.';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage(), 'Voided Audit cannot be referred in step.');
        }
    }
}