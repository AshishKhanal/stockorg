/**
* This class contains all the statuses, required operations(trigger) for a finding.
*/
public with sharing class SQX_Finding{

    /* Status pick list field listing for use in code */

    /** Draft finding status, this is the default status of a finding. */
    public static final String STATUS_DRAFT = 'Draft';
    
    /** Open finding status, reached once finding is published */ 
    public static final String STATUS_OPEN = 'Open'; 

    /** Complete finding status, reached once all the requirements(response/submission) met */
    public static final String STATUS_COMPLETE = 'Complete';

    /** Closed status, terminal status of a finding.*/ 
    public static final String STATUS_CLOSED = 'Closed'; 

    /* Resolution of the finding */

    /** Effective resolution, means finding is effective */
    public static final String RESOLUTION_EFFECTIVE = 'Effective'; 

    /** Ineffective resolution, means what has been isn't good enough */
    public static final String RESOLUTION_INEFFECTIVE = 'Ineffective'; 

    /** Void resolution, used when a finding is to be cancelled/voided */
    public static final String RESOLUTION_VOID = 'Void';
    
    /* Record Type Strings */
    /** CAPA Finding record type's name */
    public static final String RECORD_TYPE_CAPA = 'CAPA Finding'; 

    /** Audit Finding record type's name */
    public static final String RECORD_TYPE_AUDIT = 'Audit Finding'; 
    
    /** Complaint based finding */
    public static final String RECORD_TYPE_COMPLAINT = 'Complaint Finding';

    /** Nonconformance based finding */
    public static final String RECORD_TYPE_NON_CONFORMANCE = 'NC Finding';
    public static final String NC_RECORD_TYPE_DEVELOPER_NAME = 'NC_Finding';
    public static final String AUDIT_RECORD_TYPE_DEVELOPER_NAME = 'Audit_Finding';
    public static final String COMPLAINT_RECORD_TYPE_DEVELOPER_NAME = 'Complaint_Finding';
    public static final String ISSUE_RECORD_TYPE_DEVELOPER_NAME = 'Issue_Finding';

    /**
     * Finding sources
     */
    public static final String FINDING_SOURCE_CAPA = 'CAPA',
                                FINDING_SOURCE_NC = 'NC',
                                FINDING_SOURCE_COMPLAINT = 'Complaint',
                                FINDING_SOURCE_AUDIT = 'Audit';
    
    /**
     * Finding stages
     */
    public static final String  STAGE_CAPA_LINKED = 'CAPA Linked',
                                STAGE_CAPA_LINK_APPROVAL = 'CAPA Link Approval',
                                STAGE_WAITING_FOR_CAPA = 'Waiting for CAPA',
                                STAGE_COMPLETE = 'Complete';
    
    /**
    * Initiates/Opens a finding. So that it can progress further.
    */
    public static Database.SaveResult[] initiate(List<SQX_Finding__c> findings){
        return initiateAndChangeOwner(findings, false, true);
    }
    
    /**
    * takes ownership and initiates a finding
    */
    public static Database.SaveResult[] takeOwnershipAndInitiate(List<SQX_Finding__c> findings){
        return initiateAndChangeOwner(findings, true, true);
    }

    /**
    * findings the ownership of the findings
    */
    public static Database.SaveResult[] takeOwnership(List<SQX_Finding__c> findings){
        return initiateAndChangeOwner(findings, true, false);
    }
    
    /**
    * This method initiates the finding and can optionally take the ownership of the finding. The ownership is assigned to
    * the running user.
    * @param findings the findings that have to be initiated
    * @param changeOwner If <code>true</code> will change the ownership of the change to running user, <code>false</code> nothing will change
    * @param openChange if <code>true</code> will open the findings, else it will leave the status as is
    * @return returns the save result of the operation
    */
    private static Database.SaveResult[] initiateAndChangeOwner(List<SQX_Finding__c> findings, boolean changeOwner, boolean openChange){
        for(SQX_Finding__c finding : findings){

            if(openChange) finding.Status__c = STATUS_OPEN;
            if(changeOwner) finding.OwnerId = UserInfo.getUserId();

        }
        
        List<SObjectField> changedFields = new List<SObjectField> { SQX_Finding__c.Status__c };
        if(changeOwner){
            //add Owner field to changed fields to check for FLS issues
            changedFields.add(SQX_Finding__c.OwnerId);
        }
        
        return new SQX_DB().op_update(findings, changedFields);
    }
    
    /**
    * This method closes the finding records and perform other actions that are necessary when closing finding records
    * @param findings records the list of findings that are to be closed
    */
    public static void close(List<SQX_Finding__c> findings){
        for(SQX_Finding__c finding : findings){
            finding.Status__c = STATUS_CLOSED;
        }
        
        new SQX_DB().op_update(findings, new List<SObjectField> { SQX_Finding__c.Status__c });
    }

    /**
    * This method escalates finding to capa if capa is provided, Else a new CAPA is created in 'Draft' status and Finding is escalated to this CAPA
    * @param findings records the list of findings that are to be closed
    */
    public static Map<Id,Id> escalateToCAPA(List<SQX_Finding__c> findings, String comment){
        Map<Id, Id> retNewCreatedCAPA = new Map<Id, Id>();
        
        if(!findings.isEmpty()){
     
            List<SQX_CAPA__c> insertCapa = new List<SQX_CAPA__c>();
            List<SQX_CAPA__c> capaToUpdate = new List<SQX_CAPA__c>();
            List<SQX_Finding_CAPA__c> insertFindingCAPA = new List<SQX_Finding_CAPA__c>();
            Map<Id, SQX_Finding__c> findingCapaMap = new Map<Id, SQX_Finding__c>();
            
            for(SQX_Finding__c finding: findings){

                // if capa exist relate it
                if(finding.Create_New_CAPA__c == true){

                    SQX_Finding_CAPA__c findingCAPA = new SQX_Finding_CAPA__c();

                    SQX_CAPA__c capa = createCAPAObjectFromFinding (finding);
                    insertCapa.add(capa);

                    // Link Finding to Capa 
                    findingCAPA.SQX_Finding__c = finding.Id;
                    findingCAPA.Resolves_by_CAPA__c = true;
                    findingCAPA.Comment__c = comment;

                    insertFindingCAPA.add(findingCAPA);
                }
                else {
                    findingCapaMap.put(finding.SQX_CAPA__c, finding);
                }    
            }

            // Create new CAPA
            new SQX_DB().op_insert(insertCapa, new List<SObjectField>{
                            SQX_CAPA__c.SQX_Finding__c,
                            SQX_CAPA__c.Issued_Date__c,
                            SQX_CAPA__c.Target_Due_Date__c});
            
            // Update FindingCAPA record after CAPA is created 
            for(SQX_CAPA__c capa : insertCapa){
                for(SQX_Finding_CAPA__c findingCAPA: insertFindingCAPA){
                    findingCAPA.SQX_CAPA__c = capa.Id;
                    retNewCreatedCAPA.put(findingCAPA.SQX_Finding__c, capa.Id);
                }
            }
            
            // Add passive refrence to CAPA if CAPA is mentioned
            if(findingCAPAMap.size() > 0){
               
                SQX_Finding_CAPA__c findingCAPA = new SQX_Finding_CAPA__c();
                List<SQX_CAPA__c> existingCAPAs = [SELECT Id,SQX_Finding__c FROM SQX_CAPA__c WHERE Id IN: findingCapaMap.keySet()];
                for(SQX_CAPA__c capa : existingCAPAs){
                    SQX_Finding__c finding = findingCapaMap.get(capa.Id);

                    findingCAPA.SQX_Finding__c = finding.Id;
                    findingCAPA.SQX_CAPA__c = capa.Id;
                    findingCAPA.Resolves_by_CAPA__c = true;
                    findingCAPA.Comment__c = comment;
                    
                    capa.SQX_Finding__c = finding.Id;
                    insertFindingCAPA.add(findingCAPA);
                    capaToUpdate.add(capa);
                }
            }

            new SQX_DB().op_insert(insertFindingCAPA, new List<SObjectField> { SQX_Finding_CAPA__c.SQX_CAPA__c, 
                                                                             SQX_Finding_CAPA__c.SQX_Finding__c,
                                                                             SQX_Finding_CAPA__c.Resolves_by_CAPA__c,
                                                                             SQX_Finding_CAPA__c.Comment__c 
                                                                         });

            /**
            * Without sharing has been used to cover the following scenario:
            * ------------------------------------------ 
            * Without sharing has been used to allow finding owner to Update Finding field of CAPA record, as finding owner might not have necessary access to update CAPA fields 
            * 
            */
            new SQX_DB().withoutSharing().op_update(capaToUpdate, new List<SObjectField> {SQX_CAPA__c.SQX_Finding__c });

        }
        
        return retNewCreatedCAPA;
    }

    /**
    * This method creates CAPA object from Finding 
    * @param findings records the list of findings whose values are copied to CAPA 
    */
    public static SQX_CAPA__c createCAPAObjectFromFinding (SQX_Finding__c finding){

        SQX_CAPA__c capa = new SQX_CAPA__c();

        capa.SQX_Finding__c = finding.Id;
        capa.Issued_Date__c = Date.today();
        capa.Target_Due_Date__c = Date.today() + 30 ;

        return capa;
    }
    
    /**
    * this class contains all the static methods that are designed to be executed as a task in trigger.
    * @author Pradhanta Bhandari
    * @date 2014/2/3
    */
    public with sharing class  Bulkified extends SQX_BulkifiedBase{

        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newFindings equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Finding__c> newFindings, Map<Id,SQX_Finding__c> oldMap){
        
            super(newFindings, oldMap);
        }
        
        /**
        * this trigger handler checks if a finding meets all requirement or not and creates a new task if it does
        * on finding meets requirement for closure create task
        * the requirement can be summarized as follows:
        * 
        * Pre-condition: No approval process required (Scenario - 1)
        * -------------------------------------------
        * Meets all of the requirements (Response, Containment, Investigation, Corrective Action, Preventive Action)
        *       -------> create a closure task
        *
        *
        * Pre-condition: Requires any approval besides effectiveness review only (Scenario - 2)
        * ---------------------------------------------------
        * Meets all of the requirements ----> create a closure task
        *
        * Pre-condition: Requires effectiveness review
        * --------------------------------------------
        * Add effectiveness review task
        * @author Pradhanta Bhandari
        * @date 2014/2/3
        * @lastmodified 2014/3/5 [Pradhanta] fixed a bug resulting, when finding was not completed only when containment was required
        * @story SQX-11
        */
        public void onFindingMeetsAllRequirementsCreateTask(List<SQX_Finding__c> newFindings, List<SQX_Finding__c> oldFindings) {

            Map<Id, SQX_Finding__c> findingsThatMeetCompletionRequirement = new Map<Id, SQX_Finding__c>((List<SQX_Finding__c>)new SQX_CompletionStrategy(newFindings).matchingStrategy());
            /*
            * finally check if any responses are pending
            */

            List<AggregateResult> incompleteResponses = new AccessAllRecords().getAllIncompleteResponses(findingsThatMeetCompletionRequirement.values());

            /*
            * Iterate through 
            * i. the list of incomplete responses and remove incomplete finding, those with pending responses
            */
            for(AggregateResult resp : incompleteResponses) {
                Id findingId = (Id)resp.get('FindingId');
                if(findingsThatMeetCompletionRequirement.containsKey(findingId)) {
                    findingsThatMeetCompletionRequirement.remove(findingId);
                }
            }

            /*
                i. Complete all findings that meet completion requirement
            */
            for(SQX_Finding__c finding: findingsThatMeetCompletionRequirement.values()){
                finding.Status__c = SQX_Finding.STATUS_COMPLETE;   
            }
        }


        /**
        * this trigger sets the Stage of Finding to 'Capa Linked' when CAPA is linked with Finding 
        */
        public Bulkified setCAPALinkedStage() {
            final String ACTION_NAME = 'setCAPALinkedStage';
            
            Rule filterCAPALinked = new Rule();
            filterCAPALinked.addRule(Schema.SQX_Finding__c.SQX_CAPA__c, RuleOperator.HasChanged);
            filterCAPALinked.addRule(Schema.SQX_Finding__c.SQX_CAPA__c, RuleOperator.NotEquals, null);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.Finding, ACTION_NAME)
                .applyFilter(filterCAPALinked, RuleCheckMethod.OnCreateAndEveryEdit);
            
            if (!filterCAPALinked.evaluationResult.isEmpty()) {
                this.addToProcessedRecordsFor(SQX.Finding, ACTION_NAME, filterCAPALinked.evaluationResult);
                
                for (SQX_Finding__c finding : (List<SQX_Finding__c>)filterCAPALinked.evaluationResult) {
                    finding.Stage__c = STAGE_CAPA_LINKED; 
                }
            }
            
            return this;
        }



        /**
        *  this trigger shares all the finding with required account role 
        * @author Pradhanta Bhandari
        * @date 2014/2/3
        */
        public void shareFindingRecordWithAccountRole(List<SQX_Finding__c> newFindings, List<SQX_Finding__c>oldFindings) {

            if(SQX_SwitchBoard.isEnabled(SObjectType.SQX_Finding__c, SQX_SwitchBoard.SHARE_FINDING_WITH_ACCOUNT)){

                // all finding whose status is changed
                List<SQX_Finding__c> recordsWithNewSharingRule = new List<SQX_Finding__c>();
                List<User> relatedUsers= [Select ContactId From  User Where ContactId in  (SELECT SQX_Investigator_Contact__c from SQX_Finding__c WHERE Id IN :newFindings)];
                Map<Id, User> users = new Map<Id, User>();
                for(User user : relatedUsers ){
                    users.put(user.ContactId, user);
                }

                integer index = -1;
                for(SQX_Finding__c finding : newFindings){
                    index++;
                    if(trigger.isUpdate){
                        SQX_Finding__c oldFinding = oldFindings.get(index);

                        
                        if(finding.Status__c == oldFinding.Status__c || finding.Status__c != SQX_Finding.STATUS_OPEN){
                            continue;
                        }
                    }
                    else{
                        if(finding.Status__c != SQX_Finding.STATUS_OPEN){
                            continue;
                        }
                    }
                    
                    if(finding.SQX_Investigator_Contact__c == null){
                        //if external contact is not defined this means
                        //new sharing rules must not be added
                        continue;
                    }
                    
                    User currentuser = users.get(finding.SQX_Investigator_Contact__c);
                    System.debug('CurrAcc xyz: ' + currentuser);

                    //dont add to list if partner account is not enabled
                    //parter account is not checked with IsPortalEnabled field because this field is available only when partner account is enabled. Without enabling portal field this field is not available
        
                    if(currentuser != null && currentuser.ContactId != null){
                        recordsWithNewSharingRule.add(finding);
                    }
                    
                    
                    
                }
                
                //managed sharing base resets all Managed_Sharing__c rules 
                //i.e. deletes old ones and adds new ones.
                SQX_Managed_Sharing_Base share = new SQX_Managed_Sharing_Base();
                
                share.shareRecordWithPartnerUsers(recordsWithNewSharingRule, 'SQX_Supplier_Account__c', 'Read', 
                       SQX_Finding__Share.RowCause.Managed_Sharing__c);
            }
        }


        /**
        * Shares audit finding with the members of audit team.
        */
        public void shareAuditFindingWithAuditTeamMembers(List<SQX_Finding__c> newFindings, List<SQX_Finding__c>oldFindings){
            Map<Id, SQX_Finding__c> findingById = new Map<Id, SQX_Finding__c> (newFindings);
            
            //get the audit record type id, this will be used to filter Audit Findings
            Id auditRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Finding, AUDIT_RECORD_TYPE_DEVELOPER_NAME);
            for(SQX_Finding__c record : findingById.values()){
                
                // if the record isn't an audit finding we don't want to share it with audit team, additionally if there isn't audit info we ignore the finding
                if(auditRecordTypeId != record.RecordTypeId || record.SQX_Audit__c == null) {
                    findingById.remove(record.Id);
                }
            }
            SQX_Audit_Team.shareRecordsWithAuditTeamMembers(findingById.values(), SQX_Finding__c.SQX_Audit__c, Schema.SQX_Finding__Share.RowCause.Managed_Sharing__c, true);
        }        
        
        
        /**
        * assign a audit primary contact as set in audit for an audit finding i.e. copy primary contact from audit and assign to finding [SQX-]
        * @author Prashan Vaidya
        * @date 2013/12/31
        * @story SQX-35
        */
        public void assignAuditPrimaryContactToAuditFinding(List<SQX_Finding__c> newFindings) {

            //1. Filter all the findings that are audit findings, based on the audit ID, this will allow for easier update later on.
            Map<Id, List<SQX_Finding__c>> allAuditFindings = new Map<Id, List<SQX_Finding__c>>();
            
            Id auditRecordType = SQX_Utilities.getRecordTypeIDFor(SQX.Finding, SQX_Finding.RECORD_TYPE_AUDIT);
            for(SQX_Finding__c finding : newFindings){

                if(finding.recordtypeid == auditRecordType){
                    if(finding.SQX_Audit__c == null){
                        finding.addError('Audit finding requires a audit'); //add a custom validation exception when audit is missing in audit finding.
                    }
                    else{
                        if(allAuditFindings.containsKey(finding.SQX_Audit__c) == false){
                            allAuditFindings.put(finding.SQX_Audit__c, new List<SQX_Finding__c>());
                        }

                        allAuditFindings.get(finding.SQX_Audit__c).add(finding);
                    }
                    
                }

            }

            //2. Query the server to get the primary contact and the account for all audits
            List<SQX_Audit__c> audits = [SELECT ID, Primary_Contact_Name__c, Account__c FROM SQX_Audit__c 
            WHERE Id IN : allAuditFindings.keySet()];

            //3. Copy the primary contact and account from audit to finding
            for(SQX_Audit__c audit : audits){
                List<SQX_Finding__c> findings = allAuditFindings.get(audit.Id);

                //for each finding with the audit update them
                for(SQX_Finding__c finding: findings){
                    finding.SQX_Investigator_Contact__c = audit.Primary_Contact_Name__c;
                    finding.SQX_Supplier_Account__c = audit.Account__c;
                }       
            } 
        }


        
        /**
        * used to update audit's number of findings, this is so because lookup fields don't support rollup. So it is simulating rollup of Lookup relationship
        * @author Prashan Vaidya
        * @date  2013/12/31
        * @story SQX-9
        */
        public void SQX_FindingsRollup(List<SQX_Finding__c> newFindings, List<SQX_Finding__c> oldFindings) {

            
            List<SQX_Finding__c> auditFindings = new List<SQX_Finding__c>();
            
            // modified objects whose parent records should be updated
            SQX_Finding__c[] objects = null;
            oldFindings = oldFindings == null ? new List<SQX_Finding__c>() : oldFindings ;
            Map<Id, SQX_Finding__c> oldFindingMap = new Map<Id, SQX_Finding__c>(oldFindings );

            if (Trigger.isDelete) {
                objects = oldFindings;
            } else {
                /*
                    Handle any filtering required, specially on Trigger.isUpdate event. If the rolled up fields
                    are not changed, then please make sure you skip the rollup operation.
                    We are not adding that for sake of similicity of this illustration.
                */ 
                objects = newFindings;
            }
            Set<Id> auditIdUnique = new Set<Id>();
            String auditIds = 'SQX_Audit__c IN (';
            
            for(SQX_Finding__c finding: objects){

                SQX_Finding__c oldFinding = oldFindingMap.get(finding.Id);
                if(finding.SQX_Audit__c != null && (Trigger.isDelete || oldFinding == null || oldFinding.SQX_Audit__c != finding.SQX_Audit__c)){
                    auditFindings.add(finding);
                    if(!auditIdUnique.contains(finding.SQX_Audit__c)) {
                        auditIdUnique.add(finding.SQX_Audit__c);
                    	auditIds = auditIds + '\'' + finding.SQX_Audit__c + '\',';
                    }
                }
            }
            
            if(auditFindings.size() == 0) {
                return;
            }
            auditIds = auditIds.substringBeforeLast(',');
            auditIds = auditIds + ')';

            /*
            First step is to create a context for LREngine, by specifying parent and child objects and
            lookup relationship field name
            */
            LREngine.Context ctx = new LREngine.Context(SQX_Audit__c.SobjectType, // parent object
            SQX_Finding__c.SobjectType,  // child object
            Schema.SObjectType.SQX_Finding__c.fields.SQX_Audit__c // relationship field name
            );
            ctx.detailWhereClause = auditIds;
            /*
            Next, one can add multiple rollup fields on the above relationship. 
            Here specify 
            2. The field to aggregate in child object
            1. The field to which aggregated value will be saved in master/parent object
            3. The aggregate operation to be done i.e. SUM, AVG, COUNT, MIN/MAX
            */
            ctx.add(
            new LREngine.RollupSummaryField(
            Schema.SObjectType.SQX_Audit__c.fields.Number_of_Findings__c,
            Schema.SObjectType.SQX_Finding__c.fields.always_1__c,
            LREngine.RollupOperation.COUNT
            )); 
            /*ctx.add(
                    new LREngine.RollupSummaryField(
                                                    Schema.SObjectType.Account.fields.SLAExpirationDate__c,
                                                    Schema.SObjectType.SQX_Finding__c.fields.CloseDate,
                                                    LREngine.RollupOperation.Max
                                                ));  */                                     

            /* 
            Calling rollup method returns in memory master objects with aggregated values in them. 
            Please note these master records are not persisted back, so that client gets a chance 
            to post process them after rollup
            */ 
            if(auditFindings.size() > 0){
                Sobject[] masters = LREngine.rollUp(ctx, auditFindings);    

                // Persiste the changes in master
                new SQX_DB().op_update(masters, new List<SObjectField>{
                    SQX_Audit__c.fields.Number_of_Findings__c
                });
                
            }
        }


        /**
        * this trigger handler assign default approvers(the finding owner) to a finding if one is null.
        * @author Pradhanta Bhandari
        * @date 2014/2/3
        * @story SQX-32
        */
        public void assignDefaultApprovers(List<SQX_Finding__c> newFindings) {

            
            //for each finding if approver is not 
            for(SQX_Finding__c finding: newFindings){
                
                if (finding.Investigation_Approval__c == true && finding.SQX_Investigation_Approver__c == null) {
                    String objectType = ''+(finding.OwnerID).getSobjectType();
                    //if owner is of type user, assign owner as approver, else assign current user as approver. Solves the issue, when owner is queue
                    if(objectType.equals('User')){
                        finding.SQX_Investigation_Approver__c = finding.OwnerID;
                    }
                    else{
                        finding.SQX_Investigation_Approver__c= UserInfo.getUserId();
                    }
                    
                }
                
                /*if(finding.SQX_Implementation_Approver__c == null){
                    finding.SQX_Implementation_Approver__c = finding.OwnerID;
                }
                
                /*if(finding.SQX_Effectiveness_Reviewer__c == null){
                    finding.SQX_Effectiveness_Reviewer__c = finding.OwnerID;
                }*/
            }
        }
        
        /**
        * transfers ownership to provided assignee user when finding is initiated
        */
        public Bulkified changeOwnershipToAssigneeWhenFindingIsInitiated() {
            final String ACTION_NAME = 'changeOwnershipToAssigneeWhenFindingIsInitiated';
            
            Rule openFindings = new Rule();
            openFindings.addTransitionRule(Schema.SQX_Finding__c.Status__c, RuleOperator.Transitioned, STATUS_OPEN, STATUS_DRAFT);
            
            this.resetView()
                .applyFilter(openFindings, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if (!openFindings.evaluationResult.isEmpty()) {
                for (SQX_Finding__c finding : (List<SQX_Finding__c>)openFindings.evaluationResult) {
                    if (finding.SQX_Assignee__c != null) {
                        // set ownership to provided assignee
                        finding.OwnerId = finding.SQX_Assignee__c;
                    }
                }
            }
            
            return this;
        }
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/13
    * @description this class is intended to contain any action that requires access to all records irrespective of sharing model.
    */
    public  without sharing class AccessAllRecords{


        /**
        * this function returns the list of all incomplete responses in the given findings
        * @author Pradhanta Bhandari
        * @date 2014/2/13
        * @param findingsThatMeetCompletionRequirement this parameter provides a list of all findings to be matched
        * @return returns a list of finding response along with the findings for given params.
        */
        public  List<AggregateResult> getAllIncompleteResponses(List<SQX_Finding__c> findingsThatMeetCompletionRequirement ){
            // all responses in draft with implementation
            // or anything that is in approval

            return [SELECT Count(Id), SQX_Finding__c FindingId
            FROM SQX_Finding_Response__c
            WHERE
            Status__c = : SQX_Finding_Response.STATUS_IN_APPROVAL
            AND 
            SQX_Finding__c IN : findingsThatMeetCompletionRequirement
            GROUP BY SQX_Finding__c ];
        }


    }

}

