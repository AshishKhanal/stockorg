/**
 * Base extension class used for closing and voiding calibration record with user sign-off details.
 */
public with sharing class SQX_Extension_Calibration extends SQX_Controller_Sign_Off{
    
    private SQX_Calibration__c mainRecord = null;
    private ApexPages.StandardController controller = null;
    private SQX_Equipment__c equipment = null;
    
    private Boolean isPageModeVoid = false;

    /**
     * Add default policies
     */
    protected override Map<String, SQX_CQ_Esig_Policies__c> getDefaultEsigPolicies() {
        Map<String, SQX_CQ_Esig_Policies__c> defaultEsigPolicies = new Map<String, SQX_CQ_Esig_Policies__c>
        {
            SQX_Calibration.PurposeOfSignature_CloseCalibration => this.POLICY_NO_COMMENT_REQUIRED,
            SQX_Calibration.PurposeOfSignature_VoidCalibration => this.POLICY_NO_COMMENT_REQUIRED
            
        };
        return defaultEsigPolicies;
    }

    /*
     * set purpose of sig for Sign Off Controller
     */
    private void setPurposeOfSig() {
        this.purposeOfSigMap.putAll( SQX_Calibration.purposeOfSigMap );
    }

    /**
     * default constructor to load esig policies for CQ Admin app
     */
    public SQX_Extension_Calibration() {
        super(null);
        setPurposeOfSig();
    }
    
    /**
     * constructor for the vf page.
     */
    public SQX_Extension_Calibration(ApexPages.StandardController controller){
        super(controller);
        
        mainRecord = (SQX_Calibration__c)controller.getRecord();
        this.controller = controller;
        
        String mode = ApexPages.currentPage().getParameters().get('mode');
        if (mode != null && mode == 'void') {
            isPageModeVoid = true;
        }
        
        if (isPageModeVoid == false && mainRecord.Calibration_Status__c == SQX_Calibration.STATUS_OPEN) {
            // set default new equipment status when closing open calibration as per final calibration result
            mainRecord.New_Equipment_Status__c = SQX_Calibration.getDetaultNewEquipmentStatus(mainRecord);
        }
        
        equipment = [SELECT Id, Equipment_Status__c, Requires_Periodic_Calibration__c, Last_Calibration_Date__c, Calibration_Interval__c, Next_Calibration_Date__c
                     FROM SQX_Equipment__c
                     WHERE Id =: mainRecord.SQX_Equipment__c];
        
        if (mainRecord.New_Calibration_Interval__c == null) {
            // copy calibration interval from equipment if null
            mainRecord.New_Calibration_Interval__c = equipment.Calibration_Interval__c;
        }
        
        if (isPageModeVoid == false && equipment.Requires_Periodic_Calibration__c != null && equipment.Requires_Periodic_Calibration__c == true
            && mainRecord.Next_Calibration_Date__c == null && mainRecord.New_Calibration_Interval__c != null) {
            // set next calibration date when equipment requires periodic calibration when closing calibration
            mainRecord.Next_Calibration_Date__c = mainRecord.Calibration_Date__c.addDays((Integer)mainRecord.New_Calibration_Interval__c);
        }

        // setting data for esig
        setPurposeOfSig();
        recordId = mainRecord.Id;
        actionName = isPageModeVoid ? SQX_Calibration.PurposeOfSignature_VoidCalibration : SQX_Calibration.PurposeOfSignature_CloseCalibration;
        purposeOfSignature = SQX_Calibration.purposeOfSigMap.containsKey(actionName) ? SQX_Calibration.purposeOfSigMap.get(actionName) : '';
    }
    
    /**
     * saves changes on a calibration record
     */
    public PageReference save() {
        PageReference returnUrl = null;
        SavePoint sp = null;
        String originalStatus = mainRecord.Calibration_Status__c;
        
        try {
            if (originalStatus != SQX_Calibration.STATUS_OPEN) {
                String errorMsg = '';
                if (isPageModeVoid) {
                    // error messages for void mode
                    errorMsg = Label.SQX_ERR_MSG_Calibration_Record_Already_Voided_Cannot_Void_Again;
                    
                    if (originalStatus == SQX_Calibration.STATUS_CLOSE) {
                        errorMsg = Label.SQX_ERR_MSG_Calibration_Record_Already_Closed_Cannot_Void;
                    }
                } else {
                    // error messages for close mode
                    errorMsg = Label.SQX_ERR_MSG_Calibration_Record_Already_Void_Cannot_Close;
                    
                    if (originalStatus == SQX_Calibration.STATUS_CLOSE) {
                        errorMsg = Label.SQX_ERR_MSG_Calibration_Record_Already_Close_Cannot_Close_Again;
                    }
                }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg));
            } else {
                // process calibration record that has open status only
                if ( hasValidCredentials() ) {
                    // set calibration sign-off fields and proper status
                    if ( isPageModeVoid == false ) {
                        // set next calibration date when closing calibration only
                        if (equipment.Requires_Periodic_Calibration__c != null && equipment.Requires_Periodic_Calibration__c == true) {
                            if (mainRecord.Next_Calibration_Date__c == null) {
                                // set next calibration date when null
                                mainRecord.Next_Calibration_Date__c = mainRecord.Calibration_Date__c.addDays((Integer)mainRecord.New_Calibration_Interval__c);
                            }
                        } else if (mainRecord.Next_Calibration_Date__c != null) {
                            // set next calibration date to null since equipment does not require calibration
                            mainRecord.Next_Calibration_Date__c = null;
                        }
                    }
                    mainRecord.Calibration_Status__c = (isPageModeVoid ? SQX_Calibration.STATUS_VOID : SQX_Calibration.STATUS_CLOSE);
                    mainRecord.Sign_Off_By__c = getUserDetails();
                    mainRecord.Sign_Off_Date__c = System.now();
                    mainRecord.Sign_Off_Comment__c = RecordActivityComment;
                    
                    // set savepoint
                    sp = Database.setSavePoint();
                    
                    // any user can close the calibration record who may or may not have enough permission
                    Database.SaveResult sr = new SQX_DB().withoutSharing().continueOnError().op_update(new List<SQX_Calibration__c>{ mainRecord }, new List<Schema.SObjectField>{}).get(0);
                    
                    if (isPageModeVoid == false && sr.isSuccess()) {
                        SQX_Equipment__c orgEquipment = equipment.clone(true, true, true, true); // clone equipment to compare for record activity
                        
                        // update equipment record for close mode
                        equipment.Last_Calibration_Date__c = mainRecord.Calibration_Date__c;
                        equipment.Calibration_Interval__c = mainRecord.New_Calibration_Interval__c;
                        equipment.Next_Calibration_Date__c = mainRecord.Next_Calibration_Date__c;
                        equipment.Equipment_Status__c = mainRecord.New_Equipment_Status__c;
                        
                        // the user closing the calibration record needs to update the equipment record with the copied values and he/she may or may not have enough permission
                        sr = new SQX_DB().withoutSharing().continueOnError().op_update(new List<SQX_Equipment__c>{ equipment }, new List<Schema.SObjectField>{}).get(0);
                        
                        if (sr.isSuccess()) {
                            // get equipment change list for tracking record activity
                            List<String> equipmentChangelist = SQX_Equipment.getChangeList(orgEquipment, equipment, new List<Schema.SObjectField>{
                                                                                                                        SQX_Equipment__c.fields.Equipment_Status__c,
                                                                                                                        SQX_Equipment__c.fields.Calibration_Interval__c,
                                                                                                                        SQX_Equipment__c.fields.Last_Calibration_Date__c,
                                                                                                                        SQX_Equipment__c.fields.Next_Calibration_Date__c
                            });
                            
                            if (equipmentChangelist.size() > 0) {
                                // insert record activity for equipment
                                String equipmentRecordActivityComment = String.join(equipmentChangelist, '\n') + '\n' + RecordActivityComment;
                                SQX_Record_History.insertRecordHistory(string.valueOf(SQX_Equipment__c.SObjectType), equipment.Id, (String)equipment.Id, equipmentRecordActivityComment, purposeOfSigMap.get(SQX_Calibration.PurposeOfSignature_CloseCalibration), SQX_Calibration.PurposeOfSignature_CloseCalibration);
                            }
                        }
                    }
                    
                    if (sr.isSuccess()) {
                        // set valid returnUrl using current page url when exists
                        PageReference defRetURL = new ApexPages.StandardController(mainRecord).View();
                        returnUrl = SQX_Utilities.getValidReturnUrl(null, defRetURL);
                    } else {
                        // show errors on page
                        for(Database.Error error : sr.getErrors()){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error.getMessage()));
                        }
                        
                        // rollback savepoint
                        Database.rollback(sp);
                    }
                } else {
                    // show login error
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_INVALID_USERNAME_PASSWORD));
                }
            }
        } catch (Exception ex) {
            // ignore error cause these will have been added by the dml exceptions
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            
            // rollback savepoint
            if (sp != null) {
                Database.rollback(sp);
            }
        }
        
        if (returnUrl == null) {
            // when error occurred while saving, revert calibration status and sign-off fields
            mainRecord.Calibration_Status__c = originalStatus;
            mainRecord.Sign_Off_By__c = null;
            mainRecord.Sign_Off_Date__c = null;
            mainRecord.Sign_Off_Comment__c = null;
        }
        
        return returnUrl;
    }
}