/**
 * Controller class for Lightning Component SQX_Picklist_Selector.
 */
public with sharing class SQX_Picklist_Provider {
    
    /**
     * picklist value category list
     */
    public static final String CATEGORY_COMPLAINT_CONTACT_TYPE = 'Complaint Contact Type',
                                CATEGORY_COMPLAINT_ORIGIN  = 'Complaint Origin';

    /**
     * This method returns the list of Picklist Value records for given category and(or) parent.
     *
     * @param category category for the required Picklist Value records that need to be fetched
     * @param parentFilter Parent value for the required Picklist Value records that needs to be fetched
     */
    @AuraEnabled(cacheable=true)
    public static list<SQX_Picklist_Value__c> getPicklistItems(String category, String parentFilter) {
        List<SQX_Picklist_Value__c> picklistValues = new list<SQX_Picklist_Value__c>();
        picklistValues.add(new SQX_Picklist_Value__c(Name= Label.CQ_UI_Dropdown_None,Value__c='')); 
        
        SQX_DynamicQuery.Filter combinedFilter = new SQX_DynamicQuery.Filter();
        SQX_DynamicQuery.Filter filter1 = new SQX_DynamicQuery.Filter();
        SQX_DynamicQuery.Filter filter2 = new SQX_DynamicQuery.Filter();
        SQX_DynamicQuery.Filter filter3 = new SQX_DynamicQuery.Filter();
        combinedFilter.logic = SQX_DynamicQuery.FILTER_LOGIC_AND;
        combinedFilter.filters.add(filter1);
        
        filter1.operator = SQX_DynamicQuery.FILTER_OPERATOR_EQUALS;
        filter1.field = '' + SQX_Picklist_Value__c.Category__c;
        filter1.value = category;

        filter3.operator = SQX_DynamicQuery.FILTER_OPERATOR_EQUALS;
        filter3.field = '' + SQX_Picklist_Value__c.Active__c;
        filter3.value = true;
        combinedFilter.filters.add(filter3);
        
        if (String.isNotEmpty(parentFilter)){
            filter2.operator = SQX_DynamicQuery.FILTER_OPERATOR_EQUALS;
            filter2.field = '' + SQX_Picklist_Value__c.Parent__c;
            filter2.value = parentFilter;
            combinedFilter.filters.add(filter2);
        }
        
        picklistValues.addAll((List<SQX_Picklist_Value__c>) SQX_DynamicQuery.getallFields(1, 500, SQX_Picklist_Value__c.SObjectType.getDescribe(), new Set<Schema.SObjectField>{},
                    combinedFilter, new List<Schema.SObjectField>{Schema.SQX_Picklist_Value__c.Name}, true));
        return picklistValues;
    }
}