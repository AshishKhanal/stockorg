/**
 * This class contains the logic related to sample tracking
 */
public with sharing class SQX_Sample_Tracking {
    
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        
        public Bulkified() {
            
        }
        
        /**
         * The constructor for handling the trigger activities
         * @param newSampleTrackings the sample trackings that have been newly added/updated
         * @param oldMap the map of old values of the sample tracking
         */
        public Bulkified(List<SQX_Sample_Tracking__c> newSampleTrackings, Map<Id, SQX_Sample_Tracking__c> oldMap) {
            super(newSampleTrackings, oldMap);
        }
        
        /**
         * Method to prevent deletion of sample tracking if parent complaint is locked
         */
        public Bulkified preventDeletionOfLockedRecords(){
            
            this.resetView();
            
            Map<Id, SQX_Sample_Tracking__c> sampleTrackings = new Map<Id, SQX_Sample_Tracking__c>((List<SQX_Sample_Tracking__c>)this.view);
            
            for(SQX_Sample_Tracking__c sampleTracking : [SELECT Id FROM SQX_Sample_Tracking__c WHERE Id IN : sampleTrackings.keySet() AND SQX_Complaint__r.Is_Locked__c =: true]) {
                sampleTrackings.get(sampleTracking.Id).addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
            }
            
            return this;
        }
        
    }
    

}