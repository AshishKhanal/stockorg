/*
 * Unit test for transfering nsi part and service to linked account supplier part and supplier service when NSI is closed
 */ 

@isTest
public class SQX_Test_6150_TransferNSIPartsAndService {
    
    @testSetup
    public static void commonSetup(){
        
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, assigneeUser, adminUser});
        
        SQX_Test_NSI nsi;
        SQX_Part__c part;
        SQX_Standard_Service__c stdServiceName;
        
        System.runAs(adminUser) {
            part = SQX_Test_Part.insertPart(null, new User(Id = UserInfo.getUserId()), true, '');
            stdServiceName = new SQX_Standard_Service__c(name = 'StandardServiceKP');
            insert stdServiceName;
            nsi = new SQX_Test_NSI().save();
            
            SQX_New_Supplier_Introduction__c nsiRecord=[SELECT Status__c,Record_stage__C FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            nsiRecord.SQX_Part__c = part.id;
            nsiRecord.SQX_Standard_Service__c = stdServiceName.id;
                        
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c> { nsiRecord },  new List<Schema.SObjectField>{});
        }
        
    }
    /**
     * Given: Any NSI record with blank account and part / standard_service is not empty
     * When : NSI record is closed with approved result 
     * Then : Account record is created with supplier part and supplier services having part and services referencing to
     *        nsi records part and standard services.
     */ 
    @isTest
    public static void givenNSIRecord_WhenClosedWithResultApproved_ThenTransferNSIPartsAndServicesToAccountSupplierPartsAndServices(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            //Arrange: Get NSI record 
            SQX_New_Supplier_Introduction__c nsiRecord = [SELECT Id FROM SQX_New_Supplier_Introduction__c];
            nsiRecord.status__c = SQX_NSI.STATUS_CLOSED;
            nsiRecord.record_stage__c = SQX_NSI.STAGE_CLOSED;
            nsiRecord.result__c = SQX_NSI.NSI_RESULT_APPROVED;
            
            System.assertEquals(true, [Select id from SQX_Supplier_Service__c].size() == 0);
            System.assertEquals(true, [Select id from SQX_Supplier_Part__c].size() == 0);
            System.assertEquals(true, [Select id from Account].size() == 0);
            
            //Arrange: Close NSI record with approevd result 
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsiRecord }, new List<SObjectField>{});
            System.assertEquals(1, [Select id from Account].size(), 'Expected one account record to be inserted when NSI is closed with approved result');
            assertThatSupplierPartAndServicesAreTransferredToAccountWhenNSIIsClosed();
        }
    }
    
    /**
     * Given: Any NSI record with blank account and part / standard_service is not empty
     * When : NSI record is closed with conditionally approved result 
     * Then : Account record is created with supplier part and supplier services having part and services referencing to
     *        nsi records part and standard services.
     */ 
    @isTest
    public static void givenNSIRecord_WhenClosedWithResultConditionallyApproved_ThenTransferNSIPartsAndServicesToAccountSupplierPartsAndServices(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            SQX_New_Supplier_Introduction__c nsiRecord = [SELECT Id FROM SQX_New_Supplier_Introduction__c];
            nsiRecord.status__c = SQX_NSI.STATUS_CLOSED;
            nsiRecord.record_stage__c = SQX_NSI.STAGE_CLOSED;
            nsiRecord.Task_Name__c = 'TaskName';
            nsiRecord.SQX_Task_Assignee__c = adminUser.Id;
            nsiRecord.result__c = SQX_NSI.NSI_RESULT_CONDITIONALLY_APPROVED;
            
            System.assertEquals(true, [Select id,account__c,Standard_Service__c from SQX_Supplier_Service__c].size() == 0);
            System.assertEquals(true, [Select id,account__c,part__c from SQX_Supplier_Part__c].size() == 0);
            
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsiRecord }, new List<SObjectField>{});
            assertThatSupplierPartAndServicesAreTransferredToAccountWhenNSIIsClosed();
            
        }
    }
    
    /**
     *Make sure that supplier part and service is transferred to account when NSI is closed
     */ 
    public static void assertThatSupplierPartAndServicesAreTransferredToAccountWhenNSIIsClosed() {
            List<SQX_Supplier_Service__c> supplierServices = [Select id,account__c,Standard_Service__c from SQX_Supplier_Service__c];
            List<SQX_Supplier_Part__c> supplierParts = [Select id,account__c,part__c from SQX_Supplier_Part__c];
            System.assertEquals(true, supplierServices.size() == 1,'Expected one supplier service record to be inserted when NSI is closed with conditionally approved result');
            System.assertEquals(true, supplierParts.size() == 1, 'Expected one supplier service record to be inserted when NSI is closed with conditionally approved result');
        
            SQX_New_Supplier_Introduction__c closedNSIRecord = [SELECT Id, SQX_Account__c, SQX_Part__c, SQX_Standard_Service__c,Supplier_Service__c,Supplier_Part__c FROM SQX_New_Supplier_Introduction__c];
            Id linkedAccountId = closedNSIRecord.SQX_Account__c;
            System.assertEquals(linkedAccountId, supplierParts[0].account__c,'Expected Supplier part to be created and connected to NSILinkedAccount');
            System.assertEquals(linkedAccountId, supplierServices[0].account__c,'Expected Supplier service to be created and connected to NSILinkedAccount');
            System.assertEquals(closedNSIRecord.SQX_Part__c, supplierParts[0].part__c,'Expected NSI part reference and supplier part  reference to be same');
            System.assertEquals(closedNSIRecord.SQX_Standard_Service__c, supplierServices[0].Standard_Service__c,'Expected NSI standard service reference and supplier service reference to be same');

            //Assert: Make sure that supplier_service field and supplier_part fields in nsi have references to newly created supplier part and service
            System.assertEquals(true, closedNSIRecord.Supplier_Service__c != null, 'Expected SupplierService reference  to be present in NSI.');
            System.assertEquals(true, closedNSIRecord.Supplier_Part__c != null, 'Expected SupplierPart reference  to be present in NSI.');
            System.assertEquals(closedNSIRecord.Supplier_Part__c, supplierParts[0].id);
            System.assertEquals(closedNSIRecord.Supplier_Service__c, supplierServices[0].id);
            
    }
}