/**
* tests for Auto Number (SQX_Aut_Number__c) object
*/
@isTest
public class SQX_Test_2610_AutoNumber {
    
    static final String AutoNumName1 = 'Custom Object 1',
                        AutoNumName2 = 'Object';
    
    @testSetup
    static void commonSetup() {
        // add required users
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, 'adminUser');
        
        System.runas(adminUser) {
            // add required auto number records
            List<SQX_Auto_Number__c> autoNums = new List<SQX_Auto_Number__c>();
            autoNums.add(new SQX_Auto_Number__c(
                Name = AutoNumName1,
                Next_Number__c = 0,
                Number_Format__c = 'CO1-{1}-{2}',
                Numeric_Format__c = '000000'
            ));
            autoNums.add(new SQX_Auto_Number__c(
                Name = AutoNumName2,
                Next_Number__c = 0,
                Number_Format__c = 'OBJ-{1}',
                Numeric_Format__c = '00000'
            ));
            insert autoNums;
        }
    }
    
    /**
    * verifies duplicate name is not saved
    * verifies error message for validation rule "Auto_Number_Name_Should_Be_Unique"
    * verifies updating name with case different does not give duplicate error
    */
    static testmethod void givenDuplicateAutoNumberName_Error() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')) {
            // create required duplicate record with different case auto number name
            SQX_Auto_Number__c dup1 = new SQX_Auto_Number__c(
                Name = AutoNumName1.toUpperCase(), // used different case for duplicate record
                Next_Number__c = 10,
                Number_Format__c = 'DUP-{1}',
                Numeric_Format__c = '00'
            );
            
            // ACT: insert duplicate record
            Database.SaveResult sr = Database.insert(dup1, false);
            
            String duplicateNameErrMsg = 'Auto Number Name is already present in the System.';
            System.assertEquals(false, sr.isSuccess(), 'Insert record with duplicate auto number name is expected to fail.');
            System.assert(SQX_Utilities.checkErrorMessage(sr.getErrors(), duplicateNameErrMsg), 'Duplicate error message is expected.');
            
            
            // read record from db and set duplicate name
            SQX_Auto_Number__c an1 = [SELECT Id, Name FROM SQX_Auto_Number__c WHERE Name =: AutoNumName1];
            an1.Name = AutoNumName2;
            
            // ACT: update record with duplicate name
            sr = Database.update(an1, false);
            
            System.assertEquals(false, sr.isSuccess(), 'Update record with duplicate auto number name is expected to fail.');
            System.assert(SQX_Utilities.checkErrorMessage(sr.getErrors(), duplicateNameErrMsg), 'Duplicate error message is expected.' + sr);
            
            // ACT: update same name with different case
            an1.Name = AutoNumName1.toLowerCase();
            sr = Database.update(an1, false);
            
            System.assertEquals(true, sr.isSuccess(), 'Update record for auto number name with case different is expected to succeed.');
        }
    }
    
    /**
    * verifies next number cannot be less than zero
    * verifies error message for validation rule "Prevent_Negative_Next_Number"
    */
    static testmethod void givenNextNumberLessThanZero_Error() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')) {
            // create auto number with next number less than zero
            SQX_Auto_Number__c an1 = new SQX_Auto_Number__c(
                Name = 'Auto Scheme',
                Next_Number__c = -10
            );
            
            // ACT: save record
            Database.SaveResult sr = Database.insert(an1, false);
            
            String expErrMsg = 'Next Number cannot be less than 0.';
            System.assertEquals(false, sr.isSuccess(), 'Save record with next number less than zero is expected to fail.');
            System.assert(SQX_Utilities.checkErrorMessage(sr.getErrors(), expErrMsg), 'Proper error message is expected.');
        }
    }
    
    /**
    * verifies numeric format characters to be only zero
    * verifies error message for validation rule "Validate_Numeric_Format_Characters"
    */
    static testmethod void givenInvalidNumericFormat_Error() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')) {
            List<SQX_Auto_Number__c> autoNums = new List<SQX_Auto_Number__c>();
            
            // add requried auto numbers invalid numeric format
            autoNums.add(new SQX_Auto_Number__c(
                Name = 'Scheme 1',
                Next_Number__c = 1,
                Numeric_Format__c = '000ASDF'
            ));
            autoNums.add(new SQX_Auto_Number__c(
                Name = 'Scheme 2',
                Next_Number__c = 1,
                Numeric_Format__c = 'a'
            ));
            autoNums.add(new SQX_Auto_Number__c(
                Name = 'Scheme 3',
                Next_Number__c = 1,
                Numeric_Format__c = '-000'
            ));
            autoNums.add(new SQX_Auto_Number__c(
                Name = 'Scheme 4',
                Next_Number__c = 1,
                Numeric_Format__c = '000.'
            ));
            autoNums.add(new SQX_Auto_Number__c(
                Name = 'Scheme 5',
                Next_Number__c = 1,
                Numeric_Format__c = '0 00'
            ));
            
            // ACT: save records with invalid numeric format
            Database.SaveResult[] srs = Database.insert(autoNums, false);
            
            String expErrMsg = 'You cannot use characters other than 0 (zero) in Numeric Format.';
            for (Database.SaveResult sr : srs) {
                System.assertEquals(false, sr.isSuccess(), 'Save record with invalid numeric format is expected to fail.');
                System.assert(SQX_Utilities.checkErrorMessage(sr.getErrors(), expErrMsg), 'Proper error message is expected.');
            }
        }
    }
    
}