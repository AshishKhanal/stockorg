/**
* test for owner change action of a complaint to ensure the owner of the investigation of the finding, and PHR are changed to complaint owner.
* tests for error messages using custom labels:
*   - SQX_ERR_MSG_WHEN_CREATING_COMPLAINT_INVESTIGATION_AND_PRODUCT_HISTORY_REVIEW
*   - SQX_ERR_MSG_WHEN_SETTING_COMPLAINT_OWNER_TO_FINDING_AND_PRODUCT_HISTORY_REVIEW
*   - SQX_ERR_MSG_WHEN_SETTING_COMPLAINT_OWNER_TO_INVESTIGATION
*/
@isTest
public class SQX_Test_2137_Complaint_OwnerChange {
    
    // final static String VALID_COMPLAINT_QUEUE = 'Test_2137_VALID_COMPLAINT_QUEUE',
    //                     INVALID_COMPLAINT_QUEUE_WITHOUT_INV = 'Test_2137_QUEUE_WITHOUT_INV',
    //                     INVALID_COMPLAINT_QUEUE_WITHOUT_PHR = 'Test_2137_QUEUE_WITHOUT_PHR',
    //                     INVALID_COMPLAINT_QUEUE_WITHOUT_INV_AND_PHR = 'Test_2137_QUEUE_WITHOUT_INV_AND_PHR';
    
    // @testSetup
    // public static void commonSetup() {
    //     // add required users
    //     UserRole role = SQX_Test_Account_Factory.createRole();
    //     User admin1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'admin1');
    //     User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'user1');
    //     User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'user2');
        
    //     // add required queues
    //     Group validQueue = new Group(Name = VALID_COMPLAINT_QUEUE, Type = 'Queue'),
    //         noInvQueue = new Group(Name = INVALID_COMPLAINT_QUEUE_WITHOUT_INV, Type = 'Queue'),
    //         noPHRQueue = new Group(Name = INVALID_COMPLAINT_QUEUE_WITHOUT_PHR, Type = 'Queue'),
    //         noInvPHRQueue = new Group(Name = INVALID_COMPLAINT_QUEUE_WITHOUT_INV_AND_PHR, Type = 'Queue');
    //     insert new List<Group>{ validQueue, noInvQueue, noPHRQueue, noInvPHRQueue };
        
    //     // add queue members
    //     List<GroupMember> queueMembers = new List<GroupMember>();
    //     queueMembers.add(new GroupMember(GroupId = validQueue.Id, UserOrGroupId = user1.Id));
    //     queueMembers.add(new GroupMember(GroupId = noInvQueue.Id, UserOrGroupId = user1.Id));
    //     queueMembers.add(new GroupMember(GroupId = noPHRQueue.Id, UserOrGroupId = user1.Id));
    //     queueMembers.add(new GroupMember(GroupId = noInvPHRQueue.Id, UserOrGroupId = user1.Id));
    //     insert queueMembers;
        
    //     // associate complaint, investigation, phr to queue
    //     String complaintObjType = Schema.SObjectType.SQX_Complaint__c.getName(),
    //         invObjType = Schema.SObjectType.SQX_Investigation__c.getName(),
    //         phrObjType = Schema.SObjectType.SQX_Product_History_Review__c.getName();
    //     List<QueueSObject> queueObjs = new List<QueueSObject>();
    //     // valid queue objects
    //     queueObjs.add(new QueueSObject(QueueId = validQueue.Id, SObjectType = complaintObjType));
    //     queueObjs.add(new QueueSObject(QueueId = validQueue.Id, SObjectType = invObjType));
    //     queueObjs.add(new QueueSObject(QueueId = validQueue.Id, SObjectType = phrObjType));
    //     // no INV queue objects
    //     queueObjs.add(new QueueSObject(QueueId = noInvQueue.Id, SObjectType = complaintObjType));
    //     queueObjs.add(new QueueSObject(QueueId = noInvQueue.Id, SObjectType = phrObjType));
    //     // no PHR queue objects
    //     queueObjs.add(new QueueSObject(QueueId = noPHRQueue.Id, SObjectType = complaintObjType));
    //     queueObjs.add(new QueueSObject(QueueId = noPHRQueue.Id, SObjectType = invObjType));
    //     // no Inv and PHR queue objects
    //     queueObjs.add(new QueueSObject(QueueId = noInvPHRQueue.Id, SObjectType = complaintObjType));
    //     insert queueObjs;
        
    //     // add required departments
    //     System.runas(admin1) {
    //         insert new SQX_Department__c( Name = VALID_COMPLAINT_QUEUE, Queue_Id__c = validQueue.Id );
    //     }
    // }
    
    // /**
    // * test for owner change action of a complaint to ensure the owner of the investigation of the finding, and PHR are changed to complaint owner.
    // */
    // public static testmethod void givenComplaintChangeOwner_PHRAndInestigationOwnerUpdated() {
    //     // get users
    //     Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
    //     User admin1 = userMap.get('admin1'),
    //         user1 = userMap.get('user1'),
    //         user2 = userMap.get('user2');
        
    //     // get queue
    //     Group queue1 = [SELECT Id, Name FROM Group WHERE Name = :VALID_COMPLAINT_QUEUE AND Type = 'Queue'];
        
    //     // get department
    //     SQX_Department__c dept1 = [SELECT Id, Name FROM SQX_Department__c WHERE Name = :VALID_COMPLAINT_QUEUE];
        
    //     System.runas(user1) {
    //         // add required complaint
    //         SQX_Test_Complaint complaint1 = new SQX_Test_Complaint(null, admin1);
    //         complaint1.complaint.SQX_Department__c = dept1.Id;
    //         complaint1.save();
            
    //         SQX_BulkifiedBase.clearAllProcessedEntities();
            
    //         // submit the complaint to set owner to queue
    //         Map<String, String> paramsSubmit = new Map<String, String>{ 'nextAction' => 'submit' };
    //         SQX_Extension_Complaint.processChangeSetWithAction(complaint1.complaint.Id, null, null, paramsSubmit, null);

    //         SQX_BulkifiedBase.clearAllProcessedEntities();
            
    //         // ACT: initiate the complaint to add investigation and phr
    //         Map<String, String> paramsInitiate = new Map<String, String>{ 'nextAction' => 'initiate' };
    //         SQX_Extension_Complaint.processChangeSetWithAction(complaint1.complaint.Id, null, null, paramsInitiate, null);
            
    //         // get investigation
    //         SQX_Investigation__c inv = [SELECT Id, OwnerId, SQX_Finding__r.OwnerId FROM SQX_Investigation__c WHERE SQX_Complaint__c = :complaint1.complaint.Id];
            
    //         // get phr
    //         SQX_Product_History_Review__c phr = [SELECT Id, OwnerId FROM SQX_Product_History_Review__c WHERE SQX_Complaint__c = :complaint1.complaint.Id];
            
    //         System.assertEquals(queue1.Id, inv.OwnerId, 'Finding investigation owner is expected to be changed to queue.');
    //         System.assertEquals(queue1.Id, phr.OwnerId, 'PHR owner is expected to be changed to queue.');
            
            
    //         SQX_BulkifiedBase.clearAllProcessedEntities();
            
    //         // complete investigation
    //         inv.Status__c = SQX_Investigation.STATUS_PUBLISHED;
    //         update inv;
            
    //         SQX_BulkifiedBase.clearAllProcessedEntities();
            
    //         // ACT: change complaint owner to some user
    //         complaint1.complaint.OwnerId = user2.Id;
    //         update complaint1.complaint;
            
    //         // get investigation
    //         inv = [SELECT Id, OwnerId FROM SQX_Investigation__c WHERE Id = :inv.Id];
            
    //         // get phr
    //         phr = [SELECT Id, OwnerId FROM SQX_Product_History_Review__c WHERE SQX_Complaint__c = :complaint1.complaint.Id];
            
    //         System.assertEquals(user2.Id, inv.OwnerId, 'Finding investigation owner is expected to be changed to complaint owner.');
    //         System.assertEquals(user2.Id, phr.OwnerId, 'PHR owner is expected to be changed to complaint owner.');
    //     }
    // }
    
    // /**
    // * tests for error messages using custom labels:
    // *   - SQX_ERR_MSG_WHEN_SETTING_COMPLAINT_OWNER_TO_FINDING_AND_PRODUCT_HISTORY_REVIEW
    // *   - SQX_ERR_MSG_WHEN_SETTING_COMPLAINT_OWNER_TO_INVESTIGATION
    // */
    // public static testmethod void givenComplaintChangeOwnerWithInvalidQueue_VerifyErrorMessages() {
    //     // get users
    //     Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
    //     User admin1 = userMap.get('admin1'),
    //         user1 = userMap.get('user1');
        
    //     // get queues
    //     Group noPHRQueue = [SELECT Id, Name FROM Group WHERE Name = :INVALID_COMPLAINT_QUEUE_WITHOUT_PHR AND Type = 'Queue'];
    //     Group noInvQueue = [SELECT Id, Name FROM Group WHERE Name = :INVALID_COMPLAINT_QUEUE_WITHOUT_INV AND Type = 'Queue'];
        
    //     System.runas(user1) {
    //         Database.SaveResult res1;
    //         String customLabelMsg;
            
    //         // add required complaints
    //         SQX_Test_Complaint complaint1 = new SQX_Test_Complaint(null, admin1).setStatus(SQX_Complaint.STATUS_OPEN).save();
            
    //         SQX_BulkifiedBase.clearAllProcessedEntities();
            
    //         // ACT: Change owner to queue not associated with phr
    //         complaint1.complaint.OwnerId = noPHRQueue.Id;
    //         res1 = Database.update(complaint1.complaint, false);
            
    //         customLabelMsg = Label.SQX_ERR_MSG_WHEN_SETTING_COMPLAINT_OWNER_TO_FINDING_AND_PRODUCT_HISTORY_REVIEW.split('Actual error')[0]; // testing for custom label's starting texts
    //         System.assertEquals(true, SQX_Utilities.checkErrorMessageContainingTexts(res1.getErrors(), 'Queue not associated'),
    //             'Expected "...Queue not associated..." error when queue not associated with PHR is used as complaint owner.');
    //         System.assertEquals(true, SQX_Utilities.checkErrorMessageContainingTexts(res1.getErrors(), customLabelMsg),
    //             'Expected "' + customLabelMsg + '" error when queue not associated with PHR is used as complaint owner.');
            
            
    //         // ACT: Change owner to queue not associated with investigation
    //         complaint1.complaint.OwnerId = noInvQueue.Id;
    //         res1 = Database.update(complaint1.complaint, false);

    //         customLabelMsg = Label.SQX_ERR_MSG_WHEN_SETTING_COMPLAINT_OWNER_TO_INVESTIGATION.split('Actual error')[0]; // testing for custom label's starting texts
    //         System.assertEquals(true, SQX_Utilities.checkErrorMessageContainingTexts(res1.getErrors(), 'Queue not associated'),
    //             'Expected "...Queue not associated..." error when queue not associated with Investigation is used as complaint owner.');
    //     }
    // }
    
    // /**
    // * tests for error messages using custom label SQX_ERR_MSG_WHEN_CREATING_COMPLAINT_INVESTIGATION_AND_PRODUCT_HISTORY_REVIEW
    // */
    // public static testmethod void giveInitiateComplaintWithInvalidQueue_VerifyErrorMessage() {
    //     // get users
    //     Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
    //     User admin1 = userMap.get('admin1'),
    //         user1 = userMap.get('user1');
        
    //     // get queues
    //     Group noInvPHRQueue = [SELECT Id, Name FROM Group WHERE Name = :INVALID_COMPLAINT_QUEUE_WITHOUT_INV_AND_PHR AND Type = 'Queue'];
        
    //     System.runas(user1) {
    //         Database.SaveResult res1;
    //         String customLabelMsg;
            
    //         // add required complaints
    //         SQX_Test_Complaint complaint1 = new SQX_Test_Complaint(null, admin1).save();
            
    //         SQX_BulkifiedBase.clearAllProcessedEntities();
            
    //         // ACT: initiate complaint using owner as queue not associated to investigation and PHR
    //         complaint1.setStatus(SQX_Complaint.STATUS_OPEN);
    //         complaint1.complaint.OwnerId = noInvPHRQueue.Id;
    //         res1 = Database.update(complaint1.complaint, false);
            
    //         customLabelMsg = Label.SQX_ERR_MSG_WHEN_CREATING_COMPLAINT_INVESTIGATION_AND_PRODUCT_HISTORY_REVIEW.split('Actual error')[0]; // testing for custom label's starting texts
    //         System.assertEquals(true, SQX_Utilities.checkErrorMessageContainingTexts(res1.getErrors(), 'Queue not associated'),
    //             'Expected "...Queue not associated..." error when queue not associated with Investigation and PHR is used as complaint owner upon initiation.'
    //             + res1 + [SELECT Id, OwnerId FROM SQX_Investigation__c WHERE SQX_Complaint__c = :complaint1.complaint.Id]);
    //         System.assertEquals(true, SQX_Utilities.checkErrorMessageContainingTexts(res1.getErrors(), customLabelMsg),
    //             'Expected "' + customLabelMsg + '" error when queue not associated with Investigation and PHR is used as complaint owner upon initiation.');
    //     }
    // }
}