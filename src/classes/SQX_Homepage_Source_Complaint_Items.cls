/**
* This class acts as an item source for Complaint related items
* @author  Anuj Bhandari
* @date    07-04-2017
*/
public with sharing class SQX_Homepage_Source_Complaint_Items extends SQX_Homepage_ItemSource {


    private String module = SQX_Homepage_Constants.MODULE_TYPE_COMPLAINT;

    private User loggedInUser;

    /**
     *   Constructor method
     *   @param filter - instance of SQX_Homepage_Filter class that defines the filters to be applied across records
     */
    public SQX_Homepage_Source_Complaint_Items() {
        super();
        loggedInUser = SQX_Homepage_Helper.getLoggedInUserProfile();
    }

    /**
    *   Method returns all the sobject types and the corresponding fields being queried in this class(by this source)
    */
    protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        return new Map<SObjectType, List<SObjectField>>{
            SQX_Complaint__c.SObjectType => new List<SObjectField> 
            {
                SQX_Complaint__c.Name,
                SQX_Complaint__c.Complaint_Title__c,
                SQX_Complaint__c.Status__c,
                SQX_Complaint__c.CreatedDate
            },
            User.SObjectType => new List<SObjectField>
            {
                User.Name,
                User.SmallPhotoUrl
            }
        };
    }

    /**
     *   returns a list of homepage items for the current user related to Complaint module
     */
    protected override List <SObject> getUserRecords() {

        List<String> compStatuses = new List<String>
        {
            SQX_Complaint.STATUS_DRAFT,
            SQX_Complaint.STATUS_OPEN
        };

        List<String> compStages = new List<String>
        {
            SQX_Complaint.STAGE_DRAFT,
            SQX_Complaint.STAGE_TRIAGE
        };

        List<SQX_Complaint__c> complaintItems = [SELECT Id,
                                                        Name,
                                                        Complaint_Title__c,
                                                        Status__c,
                                                        Record_Stage__c,
                                                        CreatedDate
                                                        FROM SQX_Complaint__c
                                                        WHERE
                                                            Status__c IN : compStatuses
                                                                        AND
                                                            Record_Stage__c IN : compStages
                                                                        AND
                                                            OwnerID IN: SQX_Homepage_Helper.getAllIdsForLoggedInUser()
                                                    ];

        return complaintItems;
    }

    /**
     *   Method returns a SQX_Homepage_Item type item from the given sobject record
     *   @param item - Record to be converted to home page item
     */
    protected override SQX_Homepage_Item getHomePageItemFromRecord(SObject item) {
        return getComplaintItem((SQX_Complaint__c) item);
    }

    /**
     *   Method returns a SQX_Homepage_Item item from the given complaint record
     *   @param item - Record to be converted to home page item
     */
    private SQX_Homepage_Item getComplaintItem(SQX_Complaint__c item) {
        SQX_Homepage_Item complaintItem;

        if (item.Status__c == SQX_Complaint.STATUS_OPEN) {
            complaintItem = getOpenComplaintItem(item);

        } else if (item.Status__c == SQX_Complaint.STATUS_DRAFT) {
            
            complaintItem = getDraftComplaintItem(item);

            if(item.Record_Stage__c == SQX_Complaint.STAGE_TRIAGE){
                complaintItem = getTriageComplaintItem(item);
            }
        }

        return complaintItem;
    }


    /**
     *   Method returns a Open Complaint type SQX_Homepage_Item item from the given complaint record
     *   @param item - Record to be converted to home page item
     */
    private SQX_Homepage_Item getOpenComplaintItem(SQX_Complaint__c item) {

        SQX_Homepage_Item openComplaintItem = new SQX_Homepage_Item();

        openComplaintItem.itemId = (Id) item.Id;

        // set action type
        openComplaintItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_OPEN_RECORDS;

        openComplaintItem.createdDate = Date.valueOf(item.CreatedDate);

        // set module type
        openComplaintItem.moduleType = this.module;

        // set actions for the current item
        openComplaintItem.actions = getViewAction(openComplaintItem.itemId);

        // set feed text for the current item
        openComplaintItem.feedText = getOpenComplaintFeedText(item);

        // set user details of the user related to this item
        openComplaintItem.creator = loggedInUser;

        return openComplaintItem;
    }

    /**
     *   Method returns a Draft Complaint type SQX_Homepage_Item item from the given complaint
     *   @param item - Record to be converted to home page item
     */
    private SQX_Homepage_Item getDraftComplaintItem(SQX_Complaint__c item) {

        SQX_Homepage_Item draftComplaintItem = new SQX_Homepage_Item();

        draftComplaintItem.itemId = (Id) item.Id;

        // set action type
        draftComplaintItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS;

        draftComplaintItem.createdDate = Date.valueOf(item.CreatedDate);

        // set module type
        draftComplaintItem.moduleType = this.module;

        // set actions for the current item
        draftComplaintItem.actions = getViewAction(draftComplaintItem.itemId);

        // set feed text for the current item
        draftComplaintItem.feedText = getDraftComplaintFeedText(item);

        // set user details of the user related to this item
        draftComplaintItem.creator = loggedInUser;

        return draftComplaintItem;
    }


    /**
     *   Method returns a Completed Complaint type SQX_Homepage_Item item from the given complaint
     *   @param item - Record to be converted to home page item
     */
    private SQX_Homepage_Item getTriageComplaintItem(SQX_Complaint__c item) {

        SQX_Homepage_Item triageComplaintItem = new SQX_Homepage_Item();

        triageComplaintItem.itemId = (Id) item.Id;

        // set action type
        triageComplaintItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;

        triageComplaintItem.createdDate = Date.valueOf(item.CreatedDate);

        // set module type
        triageComplaintItem.moduleType = this.module;

        // set actions for the current item
        triageComplaintItem.actions = getViewAction(triageComplaintItem.itemId, SQX_Homepage_Constants.ACTION_INITIATE);

        // set feed text for the current item
        triageComplaintItem.feedText = getTriageComplaintFeedText(item);

        // set user details of the user related to this item
        triageComplaintItem.creator = loggedInUser;

        return triageComplaintItem;
    }

    /**
    *   Returns the view action type for Complaint type
    *   @param id - id of the record to be viewed
    */
    private List<SQX_Homepage_Item_Action> getViewAction(String id){
        return getViewAction(id, SQX_Homepage_Constants.ACTION_VIEW);
    }


    /**
     *   Returns the specified action for Complaint item
     *   @param id - id of the record for which action is to be set
     *   @param actionName - name of the action to be set
     */
    private List <SQX_Homepage_Item_Action> getViewAction(String id, String actionName) {

        // adding action - View
        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = actionName;

        PageReference pr = new PageReference('/' + id);
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_VIEW;

        action.supportsBulk = false;

        return new List <SQX_Homepage_Item_Action> {
            action
        };
    }


    /**
     *   Method returns the open Complaint type feed text for the given item
     */
    private String getOpenComplaintFeedText(SQX_Complaint__c item) {
        return getComplaintFeedText(item, SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS);
    }

    /**
     *   Method returns the draft Complaint type feed text for the given item
     */
    private String getDraftComplaintFeedText(SQX_Complaint__c item) {
        return getComplaintFeedText(item, SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS);
    }


    /**
     *   Method returns the Triage Complaint type feed text for the given item
     */
    private String getTriageComplaintFeedText(SQX_Complaint__c item) {
        return getComplaintFeedText(item, SQX_Homepage_Constants.FEED_TEMPLATE_TRIAGE_ITEMS);
    }

    private String getComplaintFeedText(SQX_Complaint__c item, String template)
    {
        return String.format(template, new String[] {
            item.Name,
            String.isBlank(item.Complaint_Title__c) ? '' : item.Complaint_Title__c
        });
    }

}
