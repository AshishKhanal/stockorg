/**
* This batch class is responsible to process all incoming material transaction records with the status 'Pending'.
* @story   SQX-2810 Inspection: Process Receipt
* @author  Anuj Bhandari
* @version 1.0
* @since   2017-01-10
*/
global with sharing class SQX_Material_Transaction_Job_Processor implements Database.Batchable <sObject>{
    public class invalidDataException extends Exception{}
    //Default batch size of 5
    global Integer BATCH_SZE = 5;
    //Instance specific configurable variable for next schedule in minutes
    global Integer SCHEDULE_AFTER_MIN = 2;
    //Name of the batch job
    public static string JOB_NAME = 'CQ-Material Transaction Batch Job Processor';
    //this flag can be used in tests to disable rescheduling
    //for normal usage this value is always true, for tests it is set to false but can be changed
    public boolean RESCHEDULE = !Test.isRunningTest();
    
    /**
    * Returns the query that will fetch the all material transaction records that need processing.
    * The Pending records with "Transaction_Code__c" value as "INCOMING_RECEIPT" are subjected for further processing.
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
            [SELECT Id,
                    Name,
                    External_Reference__c,
                    Line_Number__c,
                    Lot_Number__c,
                    Inspection_Required__c,
                    SQX_Part_Number__c,    
                    Part_Rev__c,
                    PO_WO_Number__c,
                    SQX_Process__c,
                    Quantity__c,
                    Supplier__c,
                    Part_Number__c,
                    Process__c,
                    Division__c,
                    Business_Unit__c,
                    Region__c,
                    Site__c,
                    Supplier_Name__c, 
                    Status__c,
                    Unit_of_Measure__c
             FROM SQX_Material_Transaction__c
             WHERE Transaction_Code__c =:SQX_Material_Transaction.CODE_TRANSACTION_INCOMING_RECEIPT
             AND Status__c= :SQX_Material_Transaction.STATUS_PENDING
             Order By LastModifiedDate ASC]);
    }
    
    /**
    * batch method that creates the receipt for the material transaction incoming receipts
    */
    global void execute(Database.BatchableContext bc,List<sObject> scope){
        SQX_Material_Transaction mtProcessor = new SQX_Material_Transaction(scope);
        mtProcessor.processMaterialTransaction();
    }

    /**
    * This method is called by the batch executor to perform cleanups. In case of CQ this method reschedules the Transaction Batch Job Processing Staging 
    * every 2 minutes
    */
    global void finish(Database.BatchableContext bc){
        //schedule next loop.
        if (RESCHEDULE ){
            System.scheduleBatch(new SQX_Material_Transaction_Job_Processor(), JOB_NAME, SCHEDULE_AFTER_MIN , BATCH_SZE);
        }
    }  
}