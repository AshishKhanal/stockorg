/**
 * class to perform actions for Supplier Interaction
 */
public with sharing class SQX_Supplier_Interaction {
    // interaction reason pick list
    public static final String INTERACTION_REASON_DOC_RENEW = 'Document Update/Renewal';
    
    /**
     * Bulkified class to handle the actions performed by the trigger by bulkifying data
     */
    public with sharing class Bulkified extends SQX_Supplier_Trigger_Handler {
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newRecs equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Supplier_Interaction__c> newRecs, Map<Id, SQX_Supplier_Interaction__c> oldMap) {
            
            super(newRecs, oldMap);
        }
        
        /**
        * Method executes all actions that are run in before Trigger context
        * a. [setSubmissionInfo]: sets some additional information when record is being created from supplier doc for requesting renew of supplier doc
        */
        protected override void onBefore() {
            if (Trigger.isInsert) {
                setSubmissionInfo();
            }
        }
        
        /**
         * Method executes all actions that are run in After Trigger context
         * a. [processClosedRecordsWithAcceptedResult]: create/update supplier documents for document request steps and link all files to account.
         * b. [createInteractionStepIfInteractionHasSupplierDoc]: create Supplier Interacton Step when Supplier Interaction is created from request renew.
         */
        protected override void onAfter() {
            if (Trigger.isUpdate) {
                processClosedRecordsWithAcceptedResult();
            } else if (Trigger.isInsert) {
                createInteractionStepIfInteractionHasSupplierDoc();
            }
        }
        
        /**
        * creates/updates supplier document from document request steps when closed with accepted result
        * links files to account when closed with accepted result
        */
        private void processClosedRecordsWithAcceptedResult() {
            final String ACTION_NAME = 'processClosedRecordsWithAcceptedResult';
            
            Rule closedRule = new Rule();
            closedRule.addRule(Schema.SQX_Supplier_Interaction__c.Status__c, RuleOperator.Equals, SQX_Supplier_Common_Values.STATUS_CLOSED);
            closedRule.addRule(Schema.SQX_Supplier_Interaction__c.Result__c, RuleOperator.NotEquals, SQX_Supplier_Common_Values.RESULT_REJECTED);
            
            resetView();
            removeProcessedRecordsFor(SQX.SupplierInteraction, ACTION_NAME);
            applyFilter(closedRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if (!view.isEmpty()) {
                addToProcessedRecordsFor(SQX.SupplierInteraction, ACTION_NAME, view);
                
                Map<Id, SQX_Supplier_Interaction_Step__c> steps = new Map<Id, SQX_Supplier_Interaction_Step__c>([
                    SELECT Id,
                        SQX_Parent__r.Part_Name__c,
                        Name,
                        RecordTypeId,
                        Document_Name__c,
                        Issue_Date__c,
                        Expiration_Date__c,
                        Due_Date__c,
                        Comment__c,
                        Needs_Periodic_Update__c,
                        Notify_Before_Expiration__c,
                        SQX_Controlled_Document__c,
                        SQX_User__c,
                        SQX_Parent__c,
                        SQX_Parent__r.SQX_Account__c,
                        SQX_Parent__r.SQX_Part__c,
                        SQX_Parent__r.SQX_Standard_Service__c,
                        SQX_Parent__r.Scope__c,
                        SQX_Supplier_Document__c
                    FROM SQX_Supplier_Interaction_Step__c
                    WHERE SQX_Parent__c IN :view
                        AND Applicable__c = true
                        AND Archived__c = false
                ]);
                
                if (!steps.isEmpty()) {
                    Id rtDocReq = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierInteractionStep, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST);
                    
                    List<SQX_Certification__c> supDocsToInsert = new List<SQX_Certification__c>();
                    List<SQX_Certification__c> supDocsToUpdate = new List<SQX_Certification__c>();
                    Map<Id, SQX_Certification__c> stepSupDocMap = new Map<Id, SQX_Certification__c>();
                    List<SQX_Supplier_Interaction_Step__c> interactionStepToUpdate = new List<SQX_Supplier_Interaction_Step__c>();
                    
                    for (SQX_Supplier_Interaction_Step__c step : steps.values()) {
                        if (step.RecordTypeId == rtDocReq) {
                            // create supplier document object and transfer values
                            SQX_Certification__c sd = new SQX_Certification__c(
                                Part_Name__c = step.SQX_Parent__r.Part_Name__c,
                                Document_Name__c = step.Document_Name__c,
                                Description__c = step.SQX_Parent__r.Scope__c,
                                Issue_Date__c = step.Issue_Date__c,
                                Expire_Date__c = step.Expiration_Date__c,
                                Needs_Periodic_Update__c = step.Needs_Periodic_Update__c,
                                Notify_Before_Expiration__c = step.Notify_Before_Expiration__c,
                                SQX_Controlled_Document__c = step.SQX_Controlled_Document__c,
                                Account__c = step.SQX_Parent__r.SQX_Account__c,
                                SQX_Part__c = step.SQX_Parent__r.SQX_Part__c,
                                SQX_Standard_Service__c = step.SQX_Parent__r.SQX_Standard_Service__c
                            );
                            
                            // set supplier document id if exists or renewal document request
                            if (step.SQX_Supplier_Document__c != null) {
                                sd.Id = step.SQX_Supplier_Document__c;
                                
                                supDocsToUpdate.add(sd);
                            }
                            else {
                                supDocsToInsert.add(sd);
                            }
                            stepSupDocMap.put(step.Id, sd);
                        }
                    }
                    
                    /**
                    * WITHOUT SHARING USED
                    * --------------------
                    * User processing supplier interaction may not have edit permission on the related account
                    */
                    SQX_DB dbWithoutSharing = new SQX_DB().withoutSharing();
                    // Account__c field of Supplier Document object is required so skipped for update
                    List<SObjectField> supDocFields = new List<SObjectField>{
                        SQX_Certification__c.Document_Name__c,
                        SQX_Certification__c.Issue_Date__c,
                        SQX_Certification__c.Expire_Date__c,
                        SQX_Certification__c.SQX_Controlled_Document__c
                    };
                    // insert new supplier documents
                    dbWithoutSharing.op_insert(supDocsToInsert, supDocFields);
                    // update existing supplier documents
                    dbWithoutSharing.op_update(supDocsToUpdate, supDocFields);
                    
                    Set<Id> stepIds = steps.keySet();
                    Map<String, ContentDocumentLink> newFileLinks = new Map<String, ContentDocumentLink>();
                    Set<Id> sharedContentDocumentIds = new Set<Id>();
                    Set<Id> sharedlinkedEntityIds = new Set<Id>();
                    
                    for (ContentDocumentLink lnk : [SELECT ContentDocumentId, LinkedEntityId
                                                    FROM ContentDocumentLink
                                                    WHERE LinkedEntityId IN :stepIds]) {
                        SQX_Supplier_Interaction_Step__c step = steps.get(lnk.LinkedEntityId);
                        Id contentDocId = lnk.ContentDocumentId;
                        Id accId = step.SQX_Parent__r.SQX_Account__c;
                                      
                        sharedContentDocumentIds.add(contentDocId);
                        sharedlinkedEntityIds.add(accId);
                        // add content document link for related account
                        ContentDocumentLink accLink = new ContentDocumentLink(
                            ContentDocumentId = contentDocId,
                            LinkedEntityId = accId,
                            ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_VIEWER,
                            Visibility = SQX_ContentDocument.CONTENTDOCUMENTLINK_VISIBILITY_ALLUSERS
                        );
                        newFileLinks.put(contentDocId + '' + accId, accLink);
                        
                        SQX_Certification__c sd = stepSupDocMap.get(step.Id);
                        if (sd != null) {
                            sharedlinkedEntityIds.add(sd.Id);
                            // add content document link for related supplier document
                            ContentDocumentLink sdLnk = accLink.clone();
                            sdLnk.LinkedEntityId = sd.Id;
                            
                            // add to insert list
                            newFileLinks.put(contentDocId + '' + sd.Id, sdLnk);
                        }
                    }
                    
                    // remove already linked contents for accounts and supplier documents from insert list
                    if (!sharedContentDocumentIds.isEmpty() && !sharedlinkedEntityIds.isEmpty()) {
                        for(ContentDocumentLink docLink : [SELECT ContentDocumentId, LinkedEntityId
                                                           FROM ContentDocumentLink
                                                           WHERE ContentDocumentId IN :sharedContentDocumentIds
                                                           AND LinkedEntityId IN :sharedlinkedEntityIds]) {
                                                               // remove content document links from insert list for already linked contents
                                                               newFileLinks.remove(docLink.ContentDocumentId + '' + docLink.LinkedEntityId);
                                                           }
                    }
                    
                    // insert new content document links
                    SQX_DB db = new SQX_DB();
                    Database.SaveResult[] srs = db.op_insert(newFileLinks.values(), new List<SObjectField>{
                        ContentDocumentLink.ContentDocumentId,
                        ContentDocumentLink.LinkedEntityId,
                        ContentDocumentLink.ShareType,
                        ContentDocumentLink.Visibility
                    });
                    
                    // update step record with new or updated supplier document id
                    for (Id stepId : stepSupDocMap.keySet()) {
                        interactionStepToUpdate.add(new SQX_Supplier_Interaction_Step__c(Id = stepId, SQX_New_Or_Updated_Supplier_Document__c = stepSupDocMap.get(stepId).Id));
                    }
                    
                    // update steps with new or updated supplier document link
                    db.op_update(interactionStepToUpdate, new List<SObjectField>{ SQX_Supplier_Interaction_Step__c.SQX_New_Or_Updated_Supplier_Document__c });
                }
            }
        }
        
        /**
         * This method sets interaction reason, stage, submitted , submitted by and date while interaction is created from supplier doc as requesting for renew
         */
        private void setSubmissionInfo() {
            Rule closedRule = new Rule();
            closedRule.addRule(Schema.SQX_Supplier_Interaction__c.SQX_Supplier_Document__c, RuleOperator.HasChanged);
            closedRule.addRule(Schema.SQX_Supplier_Interaction__c.SQX_Supplier_Document__c, RuleOperator.NotEquals, null);
            resetView();
            applyFilter(closedRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            for (SQX_Supplier_Interaction__c si : (List<SQX_Supplier_Interaction__c>)view) {
                si.Record_Stage__c = SQX_Supplier_Common_Values.STAGE_TRIAGE;
                si.Submitted_By__c = UserInfo.getUserId();
                si.Submitted_Date__c = DateTime.now();
            }
        }
        
        /**
         * Creates Supplier Interaction Step record if Supplier Interaction has Supplier Document value which tells that the Interaction Record is created for renew
         */
        private void createInteractionStepIfInteractionHasSupplierDoc() {
            Rule closedRule = new Rule();
            closedRule.addRule(Schema.SQX_Supplier_Interaction__c.SQX_Supplier_Document__c, RuleOperator.HasChanged);
            closedRule.addRule(Schema.SQX_Supplier_Interaction__c.SQX_Supplier_Document__c, RuleOperator.NotEquals, null);

            resetView();
            applyFilter(closedRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if (!view.isEmpty()) {
                List<SQX_Supplier_Interaction_Step__c> stepsToCreate = new List<SQX_Supplier_Interaction_Step__c>();
                Set<Id> supplierDocIds = getIdsForField(view, Schema.SQX_Supplier_Interaction__c.SQX_Supplier_Document__c);
                Map<Id, SQX_Certification__c> sdocMap = new Map<Id, SQX_Certification__c>([SELECT Id, Document_Name__c, SQX_Controlled_Document__c, Needs_Periodic_Update__c, Notify_Before_Expiration__c FROM SQX_Certification__c WHERE Id IN :supplierDocIds]);
                
                for (SQX_Supplier_Interaction__c si : (List<SQX_Supplier_Interaction__c>)view) {
                    SQX_Certification__c sdoc = sdocMap.get(si.SQX_Supplier_Document__c);
                    
                    if (sdoc != null) {
                        stepsToCreate.add(new SQX_Supplier_Interaction_Step__c(
                            SQX_Parent__c = si.Id,
                            Document_Name__c = sdoc.Document_Name__c,
                            Step__c = 1,
                            Name = sdoc.Document_Name__c,
                            SQX_User__c = si.SQX_Supplier_Liaison__c,
                            Due_Date__c = si.Renew_Due_Date__c,
                            SQX_Controlled_Document__c = si.SQX_Renew_Controlled_Document__c,
                            SQX_Supplier_Document__c  = sdoc.Id,
                            Needs_Periodic_Update__c = sdoc.Needs_Periodic_Update__c,
                            Notify_Before_Expiration__c = sdoc.Notify_Before_Expiration__c,
                            RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierInteractionStep, SQX_Supplier_Common_Values.RT_TYPE_DOCUMENT_REQUEST)
                        ));
                    }
                }
                
                // insert step
                new SQX_DB().op_insert(stepsToCreate,new List<Schema.SObjectField> {
                                                    Schema.SQX_Supplier_Interaction__c.SQX_Supplier_Contact__c,
                                                    Schema.SQX_Supplier_Interaction__c.Interaction_Reason__c,
                                                    Schema.SQX_Supplier_Interaction__c.SQX_Part__c,
                                                    Schema.SQX_Supplier_Interaction__c.SQX_Standard_Service__c,
                                                    Schema.SQX_Supplier_Interaction__c.SQX_Supplier_Liaison__c,
                                                    Schema.SQX_Supplier_Interaction__c.SQX_Account__c,
                                                    Schema.SQX_Supplier_Interaction__c.Record_Stage__c});
            }
        }
    }
}