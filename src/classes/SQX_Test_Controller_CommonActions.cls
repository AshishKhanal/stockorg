@IsTest
public class SQX_Test_Controller_CommonActions{
    
  
    public static testmethod void givenAnInvestigationCannotBeRevisedIfInDraft(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            SQX_Investigation__c investigation = SQX_Test_Investigation.prepareMockObject();
            insert investigation;
            
            PageReference pageRef = Page.SQX_CommonActions;
            Test.setCurrentPage(pageRef);
            
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.OBJECT_ID_GET_KEY, investigation.Id);
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.ACTION_GET_KEY, SQX_Controller_CommonActions.ACTION_REVISE_INVESTIGATION);
            
            SQX_Controller_CommonActions controller = new SQX_Controller_CommonActions();
            controller.performAction();
            
            List<SQX_Investigation__c> investigations = [SELECT ID, Revision__c
                                                    FROM SQX_Investigation__c
                                                       WHERE SQX_Original_Investigation__c = : investigation.ID];
            
            System.assert(investigations.size() == 0, 'Expected revised investigation not be created, but found one');
        }

    }
    
    
    /**
    *   Setup: Create a finding as draft
    *   Action: Send publish get command to the controller with the finding's ID
    *   Expected: The finding gets published
    *
    * @author Sagar Shrestha
    * @date 2014/5/12
    * 
    */
    public static testmethod void sendPublishCommand_FindingIsOpen(){  

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            //setup
            SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                               .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                               .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                               .setStatus(SQX_Finding.STATUS_DRAFT)
                               .save();       

            PageReference pageRef = Page.SQX_CommonActions;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.OBJECT_ID_GET_KEY, finding.finding.Id); 
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.ACTION_GET_KEY, SQX_Controller_CommonActions.ACTION_PUBLISH_RESPONSE);  

            SQX_Controller_CommonActions commonActions= new SQX_Controller_CommonActions();
            commonActions.performAction();  

            finding.synchronize();              
            
            //assert that default approver is the owner
            System.assert(finding.finding.Status__c==SQX_Finding.STATUS_OPEN,
                 'Expected finding to be published '+finding.finding);      

            commonActions.performAction(); 
            
            System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO),
                 'Expected error to be thrown '); 
        }
    }

    /**
    *   Setup: Create a investigation as published approved
    *   Action: Send revise get command to the controller with the investigation's ID
    *   Expected: The investigation gets revised, call to get redirect URL gets the new Investigations Id
    *
    * @author Pradhanta Bhandari
    * @date 2014/5/12
    * 
    */
    public static testmethod void givenAnInvestigationCanBeRevisedIfApproved(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            SQX_Investigation__c investigation = SQX_Test_Investigation.prepareMockObject();
            investigation.Status__c = SQX_Investigation.STATUS_PUBLISHED;
            investigation.Approval_Status__c = SQX_Investigation.APPROVAL_STATUS_APPROVED;
            insert investigation;
            
            PageReference pageRef = Page.SQX_CommonActions;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.OBJECT_ID_GET_KEY, investigation.ID); 
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.ACTION_GET_KEY, SQX_Controller_CommonActions.ACTION_REVISE_INVESTIGATION);  
     
            SQX_Controller_CommonActions controller = new SQX_Controller_CommonActions();
            controller.performAction();
            
            List<SQX_Investigation__c> investigations = [SELECT ID, Revision__c
                                                    FROM SQX_Investigation__c
                                                       WHERE SQX_Original_Investigation__c = : investigation.ID];
            
            System.assert(investigations.size() > 0, 'Expected revised investigation to be created, but found none');

            Id redirectToId= controller.getRedirectToId();

            System.assert(investigations[0].Id == redirectToId, 'Expected this '+redirectToId+ ' with this '+investigations);


            controller.cancelAction();
        }
        //revision number to be ignored for now, since we are not mananging it
        //System.assert(investigations.get(0).Revision__c > investigation.Revision__c, 'Expected revised investigation to be newer than old investigation');
    }

    /**
    *   Setup: Create a response as draft
    *   Action: Send publish get command to the controller with the Response's ID
    *   Expected: The response gets published
    *
    * @author Sagar Shrestha
    * @date 2014/5/13
    * 
    */
    public static testmethod void givenResponseCanBePublishedIfInDraft(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            Account account = SQX_Test_Account_Factory.createAccount();
            Contact primaryContact = SQX_Test_Account_Factory.createContact(account);
            User primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER );

             SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                               .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                               .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                               .setStatus(SQX_Finding.STATUS_OPEN)
                               .save();
            
            SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(finding.finding).save();
            
            PageReference pageRef = Page.SQX_CommonActions;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.OBJECT_ID_GET_KEY, response.response.ID); 
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.ACTION_GET_KEY, SQX_Controller_CommonActions.ACTION_PUBLISH_RESPONSE);  
     
            SQX_Controller_CommonActions controller = new SQX_Controller_CommonActions();
            controller.performAction();
            
            response.response = [SELECT Status__c
                                    FROM SQX_Finding_Response__c
                                       WHERE Id = : response.response.ID];

            System.assert(response.response.Status__c == SQX_Finding_Response.STATUS_PUBLISHED, 
                'Expected the investigation to get published'+response);
        }


        //revision number to be ignored for now, since we are not mananging it
        //System.assert(investigations.get(0).Revision__c > investigation.Revision__c, 'Expected revised investigation to be newer than old investigation');
    }

     /**
    *   Setup: Create a CAPA as draft
    *   Action: Send publish get command to the controller with CAPA's ID
    *   Expected: The CAPA gets published
    *
    * @author Sagar Shrestha
    * @date 2014/5/12
    * 
    */
    public static testmethod void sendPublishCommand_CAPAIsOpen(){  

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            SQX_Test_CAPA_Utility capa=  new SQX_Test_CAPA_Utility(true, newUser, newUser).save();    

            PageReference pageRef = Page.SQX_CommonActions;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.OBJECT_ID_GET_KEY, capa.CAPA.Id); 
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.ACTION_GET_KEY, SQX_Controller_CommonActions.ACTION_PUBLISH_RESPONSE);  

            SQX_Controller_CommonActions commonActions= new SQX_Controller_CommonActions();
            commonActions.performAction();  

            capa.CAPA= [SELECT Status__c from SQX_CAPA__c where Id = : capa.CAPA.Id] ;             
            
            
            System.assert(capa.CAPA.Status__c==SQX_CAPA.STATUS_OPEN,
                 'Expected CAPA to be published '+capa.CAPA + ApexPages.getMessages());

            commonActions.performAction(); 

            //send same CAPA twice, expected to get error
            System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO),
                 'Expected error to be thrown '); 

            //send invalid Id, expected to get error
            try{
                ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.OBJECT_ID_GET_KEY, '1234fg');
                commonActions.performAction(); 
                System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO),
                     'Expected error to be thrown ');
            }
            catch(Exception ex){

            }



            //send invalid objectid, expected to get error
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.OBJECT_ID_GET_KEY, UserInfo.getUserId());
            commonActions.performAction(); 

            System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO),
                 'Expected error to be thrown ');

            //send invalid action with valid object, expected to get errors
            SQX_Test_CAPA_Utility anotherCapa= new SQX_Test_CAPA_Utility()
                                                .save(); 
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.OBJECT_ID_GET_KEY, anotherCapa.CAPA.Id); 
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.ACTION_GET_KEY, 'invalidAction'); 

            System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO),
                 'Expected error to be thrown '); 
        }
    }

     /**
    *   Test for deleted objects
    *
    * @author Sagar Shrestha
    * @date 2014/5/12
    * 
    */
    public static testmethod void testForDeletedObjects(){  
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            SQX_Test_CAPA_Utility capa= new SQX_Test_CAPA_Utility()
                                                .save();    

            PageReference pageRef = Page.SQX_CommonActions;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.OBJECT_ID_GET_KEY, capa.CAPA.Id); 
            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.ACTION_GET_KEY, SQX_Controller_CommonActions.ACTION_PUBLISH_RESPONSE); 

            delete capa.CAPA; 

            //send deleted CAPA
            SQX_Controller_CommonActions commonActions= new SQX_Controller_CommonActions();
            commonActions.performAction();                      
      
            System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO),
                 'Expected error to be thrown when deleted CAPA is sent for action'); 

            SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                               .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                               .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                               .setStatus(SQX_Finding.STATUS_DRAFT)
                               .save(); 

            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.OBJECT_ID_GET_KEY, finding.finding.Id); 
            delete finding.finding;
            commonActions.performAction();

            System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO),
                 'Expected error to be thrown when deleted finding is sent for action');

            Account account = SQX_Test_Account_Factory.createAccount();
            Contact primaryContact = SQX_Test_Account_Factory.createContact(account);
            User primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER );

            SQX_Test_Finding findingr = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                               .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                               .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                               .setStatus(SQX_Finding.STATUS_OPEN)
                               .save();
            
            SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(findingr.finding).save();

            ApexPages.currentPage().getParameters().put(SQX_Controller_CommonActions.OBJECT_ID_GET_KEY, response.response.Id);
            delete response.response;
            commonActions.performAction();

            System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO),
                 'Expected error to be thrown when deleted response is sent for action');

        }




    }
}