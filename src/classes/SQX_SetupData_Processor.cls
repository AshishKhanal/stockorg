/**
* Batch processor for migrating all Users to Personnel,
* User Job Function records related to migrating users to Personnel Job Function,
* and Document Training records related migrating users to Personnel Document Training
* Migration user may or may not have the permission on all records while migrating records.
*/
global with sharing class SQX_SetupData_Processor {
    
    final static List<String> CQStaticResourceNames = new List<String> { 'SetupData', 'SetupData_6_4', 'SetupData_7_0', 'SetupData_8_0', 'SetupData_9_0' };
    
    /**
    * internal method to process provided CQ Static Resources containing setup data
    */
    static void execute(List<String> names) {
        System.debug(String.format('SQX_SetupData_Processor.execute() starting limit, cpu time: {0}, soql: {1}/{2}, dml: {3}/{4}', 
                                    new String[]{ 
                                        '' + Limits.getCpuTime(), 
                                        '' + Limits.getQueries(),
                                        '' + Limits.getLimitQueries(),
                                        '' + Limits.getDMLStatements(),
                                        '' + Limits.getLimitDMLStatements()
                                    })
        );
        
        for (StaticResource setupData : [SELECT Body, Name FROM StaticResource WHERE Name IN :names AND NamespacePrefix = :SQX.NSPrefix.substringBefore('_')]) {
            System.debug('Processiing CQ Static Resource: ' + setupData.Name);
            
            //get contents of seupdata
            String setupDataJSONString = setupData.Body.toString(),
                processedSetupDataJSON = '',
                setupDatas = '';

            // process specific static resource content before upserting data if needed
            if (setupData.Name == 'SetupData') {
                Id simpleQuestionRecordType = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.TaskQuestion, 'Simple_Question');
                Id dynamicQuestionRecordType = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.TaskQuestion, 'Dynamic_Question');
                
                //replace placeholder of question record type with actual record type ids
                processedSetupDataJSON = setupDataJSONString.replace('SimpleQuestionRecordType', simpleQuestionRecordType)
                                                            .replace('DynamicQuestionRecordType', dynamicQuestionRecordType);
            }

            if (String.isBlank(processedSetupDataJSON)) {
                setupDatas = '{"changeSet":' + setupDataJSONString + '}';
            }
            else {
                setupDatas = '{"changeSet":' + processedSetupDataJSON + '}';
            }


            //upsert all records
            SQX_Upserter.UpsertRecords(setupDatas );
        }
        
        System.debug(String.format('SQX_SetupData_Processor.execute() ending limit, cpu time: {0}, soql: {1}/{2}, dml: {3}/{4}', 
                                    new String[]{ 
                                        '' + Limits.getCpuTime(), 
                                        '' + Limits.getQueries(),
                                        '' + Limits.getLimitQueries(),
                                        '' + Limits.getDMLStatements(),
                                        '' + Limits.getLimitDMLStatements()
                                    })
        );
    }
    
    /**
    * executes all CQ static resources containing setup data
    */
    global static void execute() {
        execute(CQStaticResourceNames);
    }
    
    /**
    * executes provided CQ static resource name containing setup data
    */
    global static void execute(String cqStaticResourceName) {
        execute(new List<String>{ cqStaticResourceName });
    }
    
}