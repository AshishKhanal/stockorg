/**
* This test ensures the following things:
* a. given an assignee is added to an action plan it is transferred to the created action
*/
@IsTest
public class SQX_Test_1684_AssigneeAddition {

    /**
    * Ensures a. for Audit
    * This test ensures that audit action plans assignee is copied to action
    */
    public static testmethod void givenActionPlanIsApproved_ActionIsCreatedWithAssigneeForAudit(){
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);

        System.runAs(standardUser){
            //ARRANGE:
            //       Create an open audit with an audit finding.
            SQX_Test_Audit audit = new SQX_Test_Audit()
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();

            SQX_Test_Finding finding = audit.addAuditFinding()
                                            .setRequired(true, false, true, true, true)
                                            .setApprovals(false, false, false)
                                            .save();

            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().AddDays(10);
            audit.setStatus(SQX_Audit.STATUS_COMPLETE)
                 .save();


            //ACT:
            //     Submit a response with investigation to the finding. This will be approved directly
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();

            // added audit response
            changeSet.addChanged('AuditResponse-1', 
                                     new SQX_Audit_Response__c(
                                           SQX_Audit__c= audit.audit.Id,
                                           Audit_Response_Summary__c ='Audit Response for finding 1'));

            // added finding response 1
            changeSet.addChanged('responseForFinding1-1', 
                                    new SQX_Finding_Response__c(
                                           SQX_Finding__c = finding.finding.Id,
                                           Response_Summary__c = 'Response for audit')
                                )
                        .addRelation(SQX_Finding_Response__c.SQX_Audit_Response__c, 'AuditResponse-1');

           
            //added  investigation for finding 1
            changeSet.addChanged('Investigation-1',
                                    new SQX_Investigation__c(
                                            SQX_Finding__c = finding.finding.Id,
                                            Investigation_Summary__c = 'Investigation for finding1'));

            //added response inlusion of type investigation
            changeSet.addChanged('Inclusion-1',
                                    new SQX_Response_Inclusion__c(Type__c = 'Investigation'))
                        .addRelation(SQX_Response_Inclusion__c.SQX_Investigation__c, 'Investigation-1')
                        .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding1-1');

            changeSet.addChanged('Action-Plan-1', 
                                    new SQX_Action_Plan__c(
                                            SQX_User__c = standardUser.Id,
                                            Plan_Number__c = '1',
                                            Plan_Type__c = 'Corrective',
                                            Description__c = 'Hello World',
                                            Due_Date__c = Date.today()
                                        ))
                        .addRelation(SQX_Action_Plan__c.SQX_Investigation__c, 'Investigation-1');

            changeSet.addChanged('Action-Plan-2', 
                                    new SQX_Action_Plan__c(
                                            SQX_User__c = adminUser.Id,
                                            Plan_Number__c = '2',
                                            Plan_Type__c = 'Corrective',
                                            Description__c = 'Hello World',
                                            Due_Date__c = Date.today()
                                        ))
                        .addRelation(SQX_Action_Plan__c.SQX_Investigation__c, 'Investigation-1');

            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'submitresponse');
            params.put('comment', 'Mock comment');
            params.put('purposeOfSignature', 'Mock purposeOfSignature');

            System.assertEquals(SQX_Extension_UI.OK_STATUS,
                SQX_Extension_UI.submitResponse(audit.audit.Id, '{"changeSet": []}', changeSet.toJSON(), false , null, params ), 'Expected to successfully submit response');



            //ASSERT:
            //      Ensure that newly created action has the assignee copied 

            List<SQX_Action__c> actions = [SELECT Id, SQX_User__c, Plan_Number__c FROM SQX_Action__c WHERE SQX_Audit__c = : audit.audit.Id ORDER BY Plan_Number__c ASC ];
            System.assertEquals(2, actions.size()); //ensure that all action plan is changed into action, secondary assertion

            System.assertEquals(standardUser.Id, actions[0].SQX_User__c); //Primary assertion: Ensure that the assignee was correctly copied
            System.assertEquals(adminUser.Id, actions[1].SQX_User__c); //Primary assertion: Ensure that the assignee was correctly copied

        }
  
    }


    /**
    * Ensures a. for CAPA
    * This test ensures that capa action plans assignee is copied to action
    */
    public static testmethod void givenActionPlanIsApproved_ActionIsCreatedWithAssigneeForCAPA(){
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);

        System.runAs(standardUser){
            //ARRANGE:
            //       Create an open capa with an capa finding.
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility(true, standardUser, standardUser);
            capa.capa.Investigation_Approval__c = false;
            capa.save();

            new SQX_Extension_CAPA_Supplier(new ApexPages.StandardController(capa.capa)).initializeTemporaryStorage();


            //ACT:
            //     Submit a response with investigation to the finding. This will be approved directly
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();

           
            // added finding response 1
            changeSet.addChanged('responseForFinding1-1', 
                                    new SQX_Finding_Response__c(
                                           SQX_CAPA__c = capa.capa.Id,
                                           Response_Summary__c = 'Response for audit')
                                );

           
            //added  investigation for finding 1
            changeSet.addChanged('Investigation-1',
                                    new SQX_Investigation__c(
                                            SQX_CAPA__c = capa.capa.Id,
                                            Investigation_Summary__c = 'Investigation for finding1'));

            //added response inlusion of type investigation
            changeSet.addChanged('Inclusion-1',
                                    new SQX_Response_Inclusion__c(Type__c = 'Investigation'))
                        .addRelation(SQX_Response_Inclusion__c.SQX_Investigation__c, 'Investigation-1')
                        .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding1-1');

            changeSet.addChanged('Action-Plan-1', 
                                    new SQX_Action_Plan__c(
                                            SQX_User__c = standardUser.Id,
                                            Plan_Number__c = '1',
                                            Plan_Type__c = 'Corrective',
                                            Description__c = 'Hello World',
                                            Due_Date__c = Date.today()
                                        ))
                        .addRelation(SQX_Action_Plan__c.SQX_Investigation__c, 'Investigation-1');

            changeSet.addChanged('Action-Plan-2', 
                                    new SQX_Action_Plan__c(
                                            SQX_User__c = adminUser.Id,
                                            Plan_Number__c = '2',
                                            Plan_Type__c = 'Corrective',
                                            Description__c = 'Hello World',
                                            Due_Date__c = Date.today()
                                        ))
                        .addRelation(SQX_Action_Plan__c.SQX_Investigation__c, 'Investigation-1');

            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'submitresponse');
            params.put('comment', 'Mock comment');
            params.put('purposeOfSignature', 'Mock purposeOfSignature');

            System.assertEquals(SQX_Extension_UI.OK_STATUS,
                SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', changeSet.toJSON(), false , null, params ), 'Expected to successfully submit response');



            //ASSERT:
            //      Ensure that newly created action has the assignee copied 

            List<SQX_Action__c> actions = [SELECT Id, SQX_User__c, Plan_Number__c FROM SQX_Action__c WHERE SQX_CAPA__c = : capa.capa.Id ORDER BY Plan_Number__c ASC ];
            System.assertEquals(2, actions.size()); //ensure that all action plan is changed into action, secondary assertion

            System.assertEquals(standardUser.Id, actions[0].SQX_User__c); //Primary assertion: Ensure that the assignee was correctly copied
            System.assertEquals(adminUser.Id, actions[1].SQX_User__c); //Primary assertion: Ensure that the assignee was correctly copied

        }
  
    }
}