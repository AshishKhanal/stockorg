/**
* Unit tests for creating new User Job Function Object using SQX_New_User_Job_Function VF page and extension SQX_Extension_User_Job_Function
*/
@IsTest
public class SQX_Test_1310_User_Job_Function {
    // removed tests for obsolete object SQX_User_Job_Function__c
    // TODO: remove this file while cleaning up
}