/**
* This class checks When a document is created through Change Order, 
* document job function should be supplemented by what is in the Change Order impacted Job Functions
*/ 
@isTest
public class SQX_Test_2451_Impacted_Job_Function {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        System.runAs(adminUser){
            Id impactedJFId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_JOB_FUNCTION);

            // Create job function
            List<SQX_Job_Function__c> jobInfo=new List<SQX_Job_Function__c>();
            SQX_Job_Function__c job1 = new SQX_Job_Function__c();
            job1.Name = 'Dev';
            jobInfo.add(job1);
            SQX_Job_Function__c job2 = new SQX_Job_Function__c();
            job2.Name = 'QC';
            jobInfo.add(job2);
            SQX_Job_Function__c job3 = new SQX_Job_Function__c();
            job3.Name = 'QT';
            jobInfo.add(job3);
            new SQX_DB().op_insert(jobInfo,  new List<SObjectField>());
            // Create change order and impacts
            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().save();
            SQX_Impact__c impt1 =new SQX_Impact__c();
            impt1.SQX_Job_Function__C = job1.Id;
            impt1.Description_of_Impact__c = 'test desc.';
            impt1.SQX_Change_Order__c = changeOrder.Id;
            impt1.RecordTypeId = impactedJFId;
            new SQX_DB().op_insert(new List<SQX_Impact__c>{ impt1 },  new List<SObjectField>());
            
            // Create implementation with new document
            SQX_Implementation__c implementation =new SQX_Implementation__c();
            implementation.SQX_Change_Order__c= changeOrder.Id;
            implementation.Type__c ='New Document';
            implementation.Title__c='NewDoc1'; 
            implementation.Description__c='test from CO';
            new SQX_DB().op_insert(new List<SQX_Implementation__c>{ implementation },  new List<SObjectField>());
            
            // Set implementation param
            Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
            ApexPages.currentPage().getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPLEMENTATION, implementation.Id);
            
            // create new document through change order
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.updateContent(true, 'Content-1.txt');
            cDoc1.doc.Document_Number__c = 'CDOC_1_NUMBER';
            cDoc1.doc.SQX_Change_Order__c = changeOrder.Id;
            cDoc1.save();

            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            cDoc1.save();

            // revise document through change order
            SQX_Test_Change_Order changeOrder1 = new SQX_Test_Change_Order().save();
            SQX_Impact__c impt2 =new SQX_Impact__c();
            impt2.SQX_Job_Function__C = job2.Id;
            impt2.Description_of_Impact__c = 'test desc.';
            impt2.SQX_Change_Order__c = changeOrder1.Id;
            impt2.RecordTypeId = impactedJFId;
            new SQX_DB().op_insert(new List<SQX_Impact__c>{ impt2 },  new List<SObjectField>());
            
            // Create implementation with revise document
            SQX_Implementation__c implementation1 =new SQX_Implementation__c();
            implementation1.SQX_Change_Order__c = changeOrder1.Id;
            implementation1.Type__c ='Revise Document';
            implementation1.Title__c = 'NewDoc1[Rev]'; 
            implementation1.Description__c='test from CO';
            implementation1.SQX_Controlled_Document__c = cDoc1.doc.Id;
            new SQX_DB().op_insert(new List<SQX_Implementation__c>{ implementation1 },  new List<SObjectField>());

            // Set implementation param
            Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
            ApexPages.currentPage().getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPLEMENTATION, implementation1.Id);
            
            // Create revise document through change order
            SQX_Test_Controlled_Document revisedDoc;
            revisedDoc = cDoc1.revise();
            revisedDoc.doc.SQX_Change_Order__c = changeOrder1.Id;
            revisedDoc.save();
        }
    }
    
    /**
    * Given: given change order impacts job functions
    * When : when controlled document create new document
    * Then : copy impacted job functions
    */
    testMethod static void givenCOImpactedJobFunction_WhenDocumentAddedThroughCO_ThenCopyCOImpactedJobFunctionInDocumentRequirement(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            // Arrange: Create Job function and change order
            SQX_Job_Function__c job1 = [SELECT Id FROM SQX_Job_Function__c WHERE Name ='Dev' LIMIT 1 ];
            SQX_Impact__c impt1 = [SELECT SQX_Job_Function__C FROM SQX_Impact__c WHERE SQX_Job_Function__C =:job1.Id LIMIT 1];
            SQX_Controlled_Document__c doc=[SELECT Id FROM SQX_Controlled_Document__c LIMIT 1];

            // Assert: Ensure that change order impacted job function copied in controlled document requirement
            List<SQX_Requirement__c> reqInfo=[SELECT SQX_Controlled_Document__c,Skip_Revision_Training__c,SQX_Job_Function__c 
                                              FROM SQX_Requirement__c WHERE SQX_Controlled_Document__c=:doc.Id];
            for(SQX_Requirement__c rq : reqInfo){
                //Assert : Ensure that impact job function equals to document requirement job function
                System.assertEquals(impt1.SQX_Job_function__c, rq.SQX_Job_function__c);
                //Assert : Ensure that controlled document id equals to doc requirement id
                System.assertEquals(doc.Id, rq.SQX_Controlled_Document__c);
                //Assert : Ensure that doc requirement skip revision traning should be false
                System.assertEquals(false, rq.Skip_Revision_Training__c);
            }
        }
    }

    /**
    * Given: given change order impacts job functions
    * When : when revise a document through change order
    * Then : copy impacted job functions
    */
    testMethod static void givenCOImpactedJobFunction_WhenDocumentReviseThroughCO_ThenCopyCOImpactedJobFunctionInDocumentRequirement(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){ 
            // Arrange: Create change order and impacts
            SQX_Controlled_Document__c revisedDoc=[SELECT Id FROM SQX_Controlled_Document__c LIMIT 1 OFFSET 1];
            SQX_Job_Function__c job1 = [SELECT Id FROM SQX_Job_Function__c WHERE Name ='Dev' LIMIT 1];
            SQX_Impact__c impt1 = [SELECT SQX_Job_Function__C FROM SQX_Impact__c WHERE SQX_Job_Function__C =:job1.Id LIMIT 1];
            SQX_Job_Function__c job2 = [SELECT Id FROM SQX_Job_Function__c WHERE Name ='QC' LIMIT 1];

            // Assert: Ensure that change order impacted job function copied in controlled document requirement
            List<SQX_Requirement__c> reqInfo1=[SELECT SQX_Controlled_Document__c,Skip_Revision_Training__c,SQX_Job_function__c FROM SQX_Requirement__c 
                                                WHERE SQX_Controlled_Document__c=:revisedDoc.Id];
            // Assert : Ensure that created two requirements
            System.assertEquals(2, reqInfo1.size());
            for(SQX_Requirement__c rq1 : reqInfo1){
                // Assert : Ensure that revision doc id equals to revision doc requirement id
                System.assertEquals(revisedDoc.Id, rq1.SQX_Controlled_Document__c);
                // Assert : Ensure that doc requirement skip revision traning should be false
                System.assertEquals(false, rq1.Skip_Revision_Training__c);
            }
        }
    }

    /**
    * Given: given new change order impacts job function 
    * When : when add a impact on existing change order
    * Then : copy impacted job functions
    */
    testMethod static void givenNewCOImpactedJobFunction_WhenAddImpactOnExistingChangeOrder_ThenCopyCOImpactedJobFunctionInDocumentRequirement(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){ 
            SQX_Controlled_Document__c doc=[SELECT Id FROM SQX_Controlled_Document__c LIMIT 1];
            SQX_Job_Function__c job1 = [SELECT Id FROM SQX_Job_Function__c WHERE Name ='Dev' LIMIT 1];
            SQX_Job_Function__c job2 = [SELECT Id FROM SQX_Job_Function__c WHERE Name ='QC' LIMIT 1];
            SQX_Impact__c impt1 = [SELECT SQX_Job_Function__C FROM SQX_Impact__c WHERE SQX_Job_Function__C =:job1.Id LIMIT 1];
            SQX_Change_Order__c changeOrder = [SELECT Id FROM SQX_Change_Order__c LIMIT 1];
            // Arrange: Create new impact
            SQX_Impact__c impt3 =new SQX_Impact__c();
            impt3.SQX_Job_Function__C = job2.Id;
            impt3.Description_of_Impact__c = 'test desc.';
            impt3.SQX_Change_Order__c = changeOrder.Id;
            impt3.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_JOB_FUNCTION);
            
            // Act: when add new impact in existing change order
            new SQX_DB().op_insert(new List<SQX_Impact__c>{ impt3 },  new List<SObjectField>());
            List<SQX_Requirement__c> reqInfo3=[SELECT SQX_Controlled_Document__c,Skip_Revision_Training__c,SQX_Job_function__c 
                                               FROM SQX_Requirement__c WHERE  SQX_Controlled_Document__c =: doc.Id];
            
            // Assert:- Ensured that two requirement created
            System.assertEquals(2, reqInfo3.size());
        }
    }

    /**
    * Given: given change order impact job function
    * When : when update impact job function in existing change order
    * Then : copy impacted job functions
    */
    testMethod static void givenCOImpactedJobFunction_WhenUpdateImpactOnExistingChangeOrder_ThenCopyCOImpactedJobFunctionInDocumentRequirement(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){    
            SQX_Controlled_Document__c doc= [SELECT Id FROM SQX_Controlled_Document__c LIMIT 1];
            SQX_Job_Function__c job1 = [SELECT Id FROM SQX_Job_Function__c WHERE Name ='Dev' LIMIT 1];
            SQX_Impact__c impt1 = [SELECT SQX_Job_Function__C FROM SQX_Impact__c WHERE SQX_Job_Function__C =:job1.Id LIMIT 1];
            // Arrange: updated impact
            SQX_Job_Function__c job3 = [SELECT Id FROM SQX_Job_Function__c WHERE Name ='QT' LIMIT 1];
            SQX_Impact__c uptImpt = [SELECT Id,SQX_Job_Function__C,Description_of_Impact__c,SQX_Change_Order__c 
                                     FROM SQX_Impact__c WHERE ID=:impt1.Id ];
            uptImpt.SQX_Job_Function__C = job3.Id;
            uptImpt.Description_of_Impact__c = 'test desc 123';
            
            // Act: when update existing change order impact, if it is not exists in doc requirements then it added on doc requirements
            new SQX_DB().op_update(new List<SQX_Impact__c>{ uptImpt },  new List<SObjectField>());
            List<SQX_Requirement__c> reqInfo4=[SELECT SQX_Controlled_Document__c,Skip_Revision_Training__c,SQX_Job_function__r.Name 
                                               FROM SQX_Requirement__c WHERE SQX_Controlled_Document__c=:doc.Id ];
            
            // Assert:- Ensured that three(one existing requirements + one updated impact) requirement created 
            System.assertEquals(2,reqInfo4.size());
        }
    }

    /**
    * Given: given duplicate impact job function
    * When : when add impact job function in existing change order
    * Then : not to be copied
    */
    testMethod static void givenDuplicateImpactedJobFunction_WhenAddImpactOnExistingChangeOrder_ThenNotToBeCopied(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){  
            SQX_Controlled_Document__c doc=[SELECT Id FROM SQX_Controlled_Document__c LIMIT 1];
            SQX_Job_Function__c job1 = [SELECT Id FROM SQX_Job_Function__c WHERE Name ='Dev' LIMIT 1];
            SQX_Impact__c impt1 = [SELECT SQX_Job_Function__C FROM SQX_Impact__c WHERE SQX_Job_Function__C =:job1.Id LIMIT 1];
            // Arrange: Create duplicate impact
            SQX_Change_Order__c changeOrder = [SELECT Id FROM SQX_Change_Order__c LIMIT 1];
            SQX_Impact__c impt4 =new SQX_Impact__c();
            impt4.SQX_Job_Function__C = job1.Id;
            impt4.Description_of_Impact__c = 'test desc.';
            impt4.SQX_Change_Order__c = changeOrder.Id;
            
            // Act: when duplicate impact insert
            new SQX_DB().op_insert(new List<SQX_Impact__c>{ impt4 },  new List<SObjectField>());
            List<SQX_Requirement__c> reqInfo5=[SELECT SQX_Controlled_Document__c,Skip_Revision_Training__c,SQX_Job_function__c 
                                               FROM SQX_Requirement__c WHERE  SQX_Controlled_Document__c =: doc.Id];
            
            // Assert: Ensure that 1 requirements, duplicate not created
            System.assertEquals(1, reqInfo5.size());
        }
    }
}