/**
 * extension class for handling VF page actions
 */
global with sharing class SQX_Extension_Inspection extends SQX_Controller_Sign_Off {
    
    private SQX_Inspection__c mainRecord            = null; // inspection record in which action is to be preformed
    private ApexPages.StandardController controller = null; // controller of the extension

    /**
     * static string of param passed in the URL
     */
    public static final String PARAM_ID     = 'id',
                               PARAM_MODE   = 'mode';

    /**
     * sets the label for the button
     */
    public String ButtonLabel           { get; set; }
    
    /**
     * sets approve/reject policy as per inspection review esig policy
     */
    private SQX_CQ_Esig_Policies__c reviewPolicy(){        
        List<SQX_CQ_Esig_Policies__c> policy = [SELECT Id, Required__c, Comment__c FROM SQX_CQ_Esig_Policies__c WHERE Action__c =: SQX_Inspection.PurposeOfSignature_RevieiwingInspection];
        if(policy.size() > 0){
            return new SQX_CQ_Esig_Policies__c(Required__c  = policy.get(0).Required__c, Comment__c = policy.get(0).Comment__c);
        }else{
            SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance();
            return new SQX_CQ_Esig_Policies__c(Required__c  = instance.Enabled__c, Comment__c = true);
        }
    }
    
    /*
     * set purpose of sig for Sign Off Controller
     */
    private void setPurposeOfSig() {
        // SQX-3841: We are adding static policy for closing inspection and rejecting inspection because it is only
        // being used internally. We wouldn't want it to appear in esig setting page and copy eisg policy from review inspection
        SQX_CQ_Esig_Policies__c reviewPolicy = reviewPolicy();
        addStaticPolicy(SQX_Inspection.PurposeOfSignature_ClosingInspection, reviewPolicy);
        addStaticPolicy(SQX_Inspection.PurposeOfSignature_RejectingInspection, reviewPolicy);
        this.purposeOfSigMap.putAll(SQX_Inspection.purposeOfSigMap);
    }
    
    /**
     * constructor method of the class to check esig status and assign main record and controller
     */
    global SQX_Extension_Inspection(ApexPages.StandardController controller){

        super(controller, new List<SObjectField> {Schema.SQX_Inspection__c.Previous_Status__c, 
                                                    Schema.SQX_Inspection__c.Review_Required__c,
                                                    Schema.SQX_Inspection__c.SQX_Completed_By__c,
                                                    Schema.SQX_Inspection__c.Failed_Characteristics__c,
                                                    Schema.SQX_Inspection__c.Passed_Characteristics__c,
                                                    Schema.SQX_Inspection__c.Skipped_Characteristics__c,
                                                    Schema.SQX_Inspection__c.Total_Characteristics__c,
                                                    Schema.SQX_Inspection__c.Status__c,
                                                    Schema.SQX_Inspection__c.Approval_Status__c,
                                                    Schema.SQX_Inspection__c.OwnerId,
                                                    Schema.SQX_Inspection__c.SQX_Closure_Reviewer__c,
                                                    Schema.SQX_Inspection__c.Inspection_Result__c
                                                    });
        
        mainRecord = (SQX_Inspection__c) controller.getRecord();

        setPurposeOfSig();
        // gets the action type from the URL
        if(ApexPages.currentPage() != null){
            recordId = mainRecord.Id;
            actionName = ApexPages.currentPage().getParameters().get(PARAM_MODE);
            purposeOfSignature = SQX_Inspection.purposeOfSigMap.containsKey(actionName) ? SQX_Inspection.purposeOfSigMap.get(actionName) : '';
            
            // if the inspection is completed result is calculated
            if(actionName == SQX_Inspection.PurposeOfSignature_CompletingInspection){
                if(mainRecord.Failed_Characteristics__c > 0) {
                    mainRecord.Inspection_Result__c = SQX_Inspection.INSPECTION_RESULT_FAIL;
                } else if(mainRecord.Passed_Characteristics__c + mainRecord.Skipped_Characteristics__c == mainRecord.Total_Characteristics__c){
                    mainRecord.Inspection_Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
                }
            }
        }
    }

    /**
     * method to check if the sign-off is required or not
     * @return boolean value to check if sign-off is required
     */
    global Boolean getCanPerformAction(){
        Boolean needsSignOff = false;
        SQX_Inspection inspection = new SQX_Inspection(mainRecord);
        
        if(actionName == SQX_Inspection.PurposeOfSignature_InitiatingInspection){
            ButtonLabel = Label.CQ_UI_Take_Ownership_And_Initiate;
            needsSignOff = inspection.canInitiateInspection();
        } else if(actionName == SQX_Inspection.PurposeOfSignature_CompletingInspection){
            ButtonLabel = Label.CQ_UI_Flow_Complete;
            needsSignOff = inspection.canCompleteInspection();
        } else if(actionName == SQX_Inspection.PurposeOfSignature_VoidingInspection){
            ButtonLabel = Label.CQ_UI_Void;
            needsSignOff = inspection.canVoidInspection();
        } else if(actionName == SQX_Inspection.PurposeOfSignature_ReopeningInspection){
            ButtonLabel = Label.CQ_UI_Reopen;
            needsSignOff = inspection.canReopenInspection();
        } else if(actionName == SQX_Inspection.PurposeOfSignature_ClosingInspection){
            ButtonLabel = Label.CQ_UI_Close;
            needsSignOff = inspection.canCloseInspection();
        } else if(actionName == SQX_Inspection.PurposeOfSignature_RejectingInspection){
            ButtonLabel = Label.CQ_UI_Reject;
            needsSignOff = inspection.canCloseInspection();
        } else if(actionName == SQX_Inspection.Action_Review_Inspection){
            needsSignOff = inspection.canCloseInspection();
        }else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_PREVENT_CHANGING_INSPECTION));
        }

        if(!needsSignOff){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, inspection.pageErrorMessage));
        }
        return needsSignOff;
    }
    
    /**
     * method to validate esig and add record activity and main record
     * @return url to redirect the page or throws error
     */
    global PageReference saveRecord(){
        PageReference ref = null;
        Database.SaveResult result = null;

        SQX_Inspection inspection = new SQX_Inspection(mainRecord);
        
        if(hasValidCredentials()){
            try{
                if(actionName == SQX_Inspection.PurposeOfSignature_InitiatingInspection){
                    result = inspection.initiateInspection(RecordActivityComment);
                } 
                else if(actionName == SQX_Inspection.PurposeOfSignature_CompletingInspection){
                    result = inspection.completeInspection(RecordActivityComment);
                } 
                else if(actionName == SQX_Inspection.PurposeOfSignature_ClosingInspection){
                    result = inspection.closeInspection(RecordActivityComment);
                } 
                else if(actionName == SQX_Inspection.PurposeOfSignature_VoidingInspection){
                    result = inspection.voidInspection(RecordActivityComment);
                } 
                else if(actionName == SQX_Inspection.PurposeOfSignature_ReopeningInspection){
                    result = inspection.reopenInspection(RecordActivityComment);
                } 
                else if(actionName == SQX_Inspection.PurposeOfSignature_RejectingInspection){
                    result = inspection.rejectInspection(RecordActivityComment);
                } 
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_PREVENT_CHANGING_INSPECTION));
                }

                if(result != null && result.isSuccess()){
                    ref = new ApexPages.StandardController(mainRecord).view();
                }
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, inspection.pageErrorMessage));
                }
            }catch(Exception ex){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_INVALID_USERNAME_PASSWORD));
        }
        return ref;
    }
    
    /**
     * saves the inspection details changed from the inspection detail vf page
     * @return redirect to main record if the save is successful
     */
    global PageReference saveInspectionDetails(){
        PageReference returnTo = null;
        SavePoint sp = Database.setSavePoint();

        try {
            if(mainRecord.SQX_Inspection_Details__r.size() > 0){
                // FLS Bypassed because VF page will automatically take care of FLS check
                new SQX_DB().op_update(mainRecord.SQX_Inspection_Details__r, new List<SObjectField> {});
            }

            returnTo = new ApexPages.StandardController(mainRecord).view();
        }
        catch (Exception ex) {
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }

        
        return returnTo;
    }

    /**
     * approves the inspection sent for review
     */
    global PageReference approveRecord(){
        return approveRejectRecord(SQX_Inspection.PurposeOfSignature_ClosingInspection);
    }

    /**
     * rejects the inspection sent for review
     */
    global PageReference rejectRecord(){
        return approveRejectRecord(SQX_Inspection.PurposeOfSignature_RejectingInspection);
    }

    /**
     * performs approval and rejection of inspection review
     * @param approvalParam action to approve or reject inspection
     * @return redirection url to inspection
     */
    public PageReference approveRejectRecord(String approvalParam){
        String oldActionName = actionName;
        PageReference ref = null;
        try {
            actionName =  approvalParam;
            ref = saveRecord();
        } finally {
            actionName = oldActionName;
        }
        return ref;
        
    }
}