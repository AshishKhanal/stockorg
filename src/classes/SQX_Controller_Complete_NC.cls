/**
* This class is used to return all the Complete NC for which the user is NC Coordinator.
*/

public with sharing class SQX_Controller_Complete_NC{
    
    /**
    * returns the list of Nonconformance Complete NC for the currently logged in user's account 
    */
    public List<SQX_Nonconformance__c> getCompleteNCs() {
        return [
            SELECT Title__c,Status__c,Type_Of_Issue__c,Age__c,Occurrence_Date__c,Department__c,Disposition_Approval__c,
            Name, OwnerId, Owner.Name,SystemModstamp FROM SQX_Nonconformance__c 
            WHERE Status__c =: SQX_NC.STATUS_COMPLETE AND OwnerId = : UserInfo.getUserId() ORDER BY Name DESC];
    }
}