/**
* @description - Helper class for Homepage implementation
* @date - 07-04-2017
*/
public with sharing class SQX_Homepage_Helper {

    private static Id loggedInUserId = UserInfo.getUserId();

    /*
        Holds an instance of current logged in user with profile info
    */
    private static User loggedInUser;

    /**
    *   Returns user profile of the logged in user
    */
    public static User getLoggedInUserProfile(){

        if( loggedInUser == null ){
            loggedInUser = [SELECT Id, Name, SmallPhotoUrl FROM User WHERE id =: loggedInUserId];
        }
        return loggedInUser;
    }


    /*
        Holds a set of queue ids the current user is a part of
    */
    private static Set<Id> loggedInUserIds;


    /**
    * returns a set of Ids of queue the current user is a part of + the current user Id
    */
    public static Set<Id> getAllIdsForLoggedInUser(){

        if( loggedInUserIds == null ){
            loggedInUserIds = SQX_Utilities.getQueueIdsForUser(loggedInUserId);
            loggedInUserIds.add(loggedInUserId);
        }
        return loggedInUserIds;
    }
}