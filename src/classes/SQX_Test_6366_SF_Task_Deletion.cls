/**
 * @description : Class for testing prevention from deleting SF task if its parent is not in complete or void status 
 * @author : Sanjay Maharjan 
 * @date : 2018/8/28
 * @story : 6366
*/
@isTest
public class SQX_Test_6366_SF_Task_Deletion {
    
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
    * GIVEN : SF Task whose parent NSI is not in closed or void status
    * WHEN  : SF Task is deleted
    * THEN  : Error message is thrown
    */
    public static testmethod void givenSFTask_WhenParentNSIIsNotClosed_ThenSFTaskCannotBeDeleted(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        SQX_Test_NSI.addUserToQueue(new List<User> {adminUser});
        
        System.runAs(adminUser){
            // ARRANGE : NSI is created and policy task is created
            SQX_Test_NSI.createPolicyTasks(1, 'Task', adminUser, 1);
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();

            // ACT : NSI is submitted
            nsi.submit();
            
            // ACT : NSI is initiated
            nsi.initiate();
            
            List<Task> tasks = [SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id];
            
            // ACT : Delete task
            Database.DeleteResult result = new SQX_DB().continueOnError().op_delete(tasks)[0];
            
            String errorMessage = Label.SQX_ERR_MSG_WHEN_DELETING_TASK_BEFORE_COMPLETION;
            
            // ASSERT : Error message should be thrown
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), errorMessage), 'Expected error message : ' + errorMessage + ' but actual error message : ' + result.getErrors());
            
            // ACT : NSI is closed
            nsi.close();
            
            // ASSERT : Ensure tasks are deleted
            System.assertEquals(0, [SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id].size());
            
        }
        
    }

    /**
    * GIVEN : SF Task whose parent SD is not in closed or void status
    * WHEN  : SF Task is deleted
    * THEN  : Error message is thrown
    */
    public static testmethod void givenSFTask_WhenParentSDIsNotClosed_ThenSFTaskCannotBeDeleted(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> {adminUser});
        
        System.runAs(adminUser){
            // ARRANGE : SD is created and policy task is created
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, 'Task', adminUser, 1);
            SQX_Test_Supplier_Deviation sd = new SQX_Test_Supplier_Deviation().save();

            // ACT : SD is submitted
            sd.submit();
            
            // ACT : SD is initiated
            sd.initiate();
            
            List<Task> tasks = [SELECT Id FROM Task WHERE WhatId =: sd.sd.Id];
            
            // ACT : Delete task
            Database.DeleteResult result = new SQX_DB().continueOnError().op_delete(tasks)[0];
            
            String errorMessage = Label.SQX_ERR_MSG_WHEN_DELETING_TASK_BEFORE_COMPLETION;
            
            // ASSERT : Error message should be thrown
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), errorMessage), 'Expected error message : ' + errorMessage + ' but actual error message : ' + result.getErrors());
            
            // ACT : SD is closed
            sd.close();
            
            // ASSERT : Ensure tasks are deleted
            System.assertEquals(0, [SELECT Id FROM Task WHERE WhatId =: sd.sd.Id].size());
            
        }
        
    }


}