/**
* Controller to compare contents between controlled documents
*/
public with sharing class SQX_Controller_Document_Compare {
    
    /**
    * constants
    */
    public static final String PARAM_BASE_DOC_ID = 'origin',
                               PARAM_COMPARE_WITH_DOC_ID = 'recordId',
                               DOCUMENT_COMPARE_PACKAGE_NAMESPACE = 'compliancequest',
                               DOCUMENT_COMPARE_PACKAGE_COMPARE_PAGENAME = 'CQDE_Document_Compare';
    
    /**
    * Holds url param value of the base controlled document to compare
    */
    @TestVisible
    private String prmBaseDocId { get; set; }
    
    /**
    * Holds url param value of the controlled document to compare with
    */
    @TestVisible
    private String prmCompareWithDocId { get; set; }
    
    /**
    * constructor
    */
    public SQX_Controller_Document_Compare() {
        // read ids to compare from url
        prmBaseDocId = ApexPages.currentPage().getParameters().get(PARAM_BASE_DOC_ID);
        prmCompareWithDocId = ApexPages.currentPage().getParameters().get(PARAM_COMPARE_WITH_DOC_ID);
    }
    
    /**
    * returns true when document compare package is available in the org and has CQDC_Document_Compare page
    */
    @TestVisible
    public static Boolean isDocComparePackagePageAvailable {
        get {
            if (isDocComparePackagePageAvailable == null) {
                isDocComparePackagePageAvailable = [SELECT NamespacePrefix, Name FROM ApexPage
                                                    WHERE NamespacePrefix = :DOCUMENT_COMPARE_PACKAGE_NAMESPACE
                                                        AND Name = :DOCUMENT_COMPARE_PACKAGE_COMPARE_PAGENAME].size() == 1;
            }
            return isDocComparePackagePageAvailable;
        }
        private set;
    }
    
    /**
    * redirects to document compare page of addon if available
    */
    public PageReference redirectToAddonPage() {
        PageReference pr = null;
        
        if (isDocComparePackagePageAvailable) {
            pr = new PageReference('/apex/' + DOCUMENT_COMPARE_PACKAGE_NAMESPACE + '__' + DOCUMENT_COMPARE_PACKAGE_COMPARE_PAGENAME);
            if (String.isNotBlank(prmBaseDocId)) {
                pr.getParameters().put(PARAM_BASE_DOC_ID, prmBaseDocId);
            }
            if (String.isNotBlank(prmCompareWithDocId)) {
                pr.getParameters().put(PARAM_COMPARE_WITH_DOC_ID, prmCompareWithDocId);
            }
        }
        
        return pr;
    }
}