/**
* This class acts as an item source for Inspection related items
*/
public with sharing class SQX_Homepage_Source_Inspection_Items extends SQX_Homepage_ItemSource {


    private String module = SQX_Homepage_Constants.MODULE_TYPE_INSPECTION;

    private User loggedInUser;

    /**
    *   Constructor method
    */
    public SQX_Homepage_Source_Inspection_Items() {
        super();
        loggedInUser = SQX_Homepage_Helper.getLoggedInUserProfile();
    }

    /**
    *   Method returns all the sobject types and the corresponding fields being queried in this class(by this source)
    */
    protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        return new Map<SObjectType, List<SObjectField>>{
            SQX_Inspection__c.SObjectType => new List<SObjectField> 
            {
                SQX_Inspection__c.CreatedDate,
                SQX_Inspection__c.Inspection_Type__c,
                SQX_Inspection__c.Name,
                SQX_Inspection__c.Rev__c,
                SQX_Inspection__c.Status__c,
                SQX_Inspection__c.SQX_Part__c,
                SQX_Inspection__c.SQX_Process__c
            },
            SQX_Part__c.SObjectType => new List<SObjectField>
            {
                SQX_Part__c.Name,
                SQX_Part__c.Part_Number__c
            },
            SQX_Standard_Service__c.SObjectType => new List<SObjectField>
            {
                SQX_Standard_Service__c.Name
            },
            User.SObjectType => new List<SObjectField>
            {
                User.Name,
                User.SmallPhotoUrl
            }
        };
    }

    /**
    *   returns a list of homepage items for the current user related to Complaint module
    */
    protected override List <SObject> getUserRecords() {

        List<String> validStatuses = new List<String>
        {
            SQX_Inspection.INSPECTION_STATUS_DRAFT,
            SQX_Inspection.INSPECTION_STATUS_OPEN
        };

        List<SQX_Inspection__c> inspectionItems = [SELECT Id, Name, CreatedDate, Status__c, Rev__c, Inspection_Type__c,
                                                        SQX_Part__r.Name, SQX_Part__r.Part_Number__c,
                                                        SQX_Process__r.Name
                                                        FROM SQX_Inspection__c
                                                        WHERE
                                                            Status__c IN: validStatuses
                                                                        AND
                                                            OwnerID IN: SQX_Homepage_Helper.getAllIdsForLoggedInUser()
                                                    ];

        return inspectionItems;
    }

    /**
    *   Method returns a SQX_Homepage_Item type item from the given sobject record
    *   @param item - Record to be converted to home page item
    */
    protected override SQX_Homepage_Item getHomePageItemFromRecord(SObject item) {

        SQX_Homepage_Item hpItem = new SQX_Homepage_Item();

        hpItem.itemId = (Id) item.Id;

        hpItem.createdDate = Date.valueOf(item.get('CreatedDate'));

        hpItem.moduleType = this.module;
        
        hpItem.actions = new List<SQX_Homepage_Item_Action> { getViewAction(hpItem.itemId) };

        hpItem.creator = loggedInUser;

        String itemStatus = (String) item.get('Status__c');
        if(itemStatus == SQX_Inspection.INSPECTION_STATUS_DRAFT)
        {

            hpItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS;

            hpItem.feedText = getDraftInspectionFeedText((SQX_Inspection__c) item);

        }
        else if(itemStatus == SQX_Inspection.INSPECTION_STATUS_OPEN)
        {

            hpItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_OPEN_RECORDS;

            hpItem.feedText = getOpenInspectionFeedText((SQX_Inspection__c) item);
        }

        return hpItem;
    }

 
    /**
    *   Returns the view action type for Inspection
    *   @param id - id of the record for which action is to be set
    */
    private SQX_Homepage_Item_Action getViewAction(Id insId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_VIEW;

        PageReference pr = new PageReference('/' + insId);
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_VIEW;

        action.supportsBulk = false;

        return action;
    }


    /**
    *   Method returns the open Inspection type feed text for the given item
    *   @param item inspection item
    */
    private String getOpenInspectionFeedText(SQX_Inspection__c item) {
        return getInspectionFeedText(item, SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS);
    }

    /**
    *   Method returns the draft Inspection type feed text for the given item
    *   @param item inspection item
    */
    private String getDraftInspectionFeedText(SQX_Inspection__c item) {
        return getInspectionFeedText(item, SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS);
    }


    /**
    *   Method returns formatted feed text for inspection items
    *   @param item inspection item
    *   @param template feed template
    */
    private String getInspectionFeedText(SQX_Inspection__c item, String template)
    {
        String itemDetail = '';
        if(item.Inspection_Type__c == SQX_Inspection.INSPECTION_TYPE_PRODUCT)
        {
            // show part number, rev and part name
            itemDetail = item.SQX_Part__r.Part_Number__c + ' '
                         + (String.isBlank(item.Rev__c) ? ' - ' : '[' + item.Rev__c + '] - ' )
                         + item.SQX_Part__r.Name;
        }
        else
        {
            // show process name
            itemDetail = item.SQX_Process__r.Name;
        }

        return String.format(template, new String[] {
            item.Name,
            itemDetail
        });
    }

}