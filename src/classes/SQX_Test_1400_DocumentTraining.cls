/**
* Unit tests for preventing addition of a document training for a template document.
*
* @author Dibya Shrestha
* @date 2015/08/13
* 
*/
@IsTest
public class SQX_Test_1400_DocumentTraining {
    
    static Boolean runAllTests = true,
        run_givenAddDocumentTrainingForTemplateDocument_DocumentTrainingNotAdded = false;
    
    /**
    * This test ensures that a document training is not added for a template document.
    */
    public static testmethod void givenAddDocumentTrainingForTemplateDocument_DocumentTrainingNotAdded(){
        if (!runAllTests && !run_givenAddDocumentTrainingForTemplateDocument_DocumentTrainingNotAdded){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser) {
            // add required template document
            SQX_Test_Controlled_Document testDoc = new SQX_Test_Controlled_Document();
            testDoc.doc.RecordTypeId = SQX_Controlled_Document.getTemplateDocTypeId();
            testDoc.save();
            testDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Document_Training__c dt = new SQX_Document_Training__c(
                SQX_User__c = standardUser.Id,
                SQX_Controlled_Document__c = testDoc.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today()
            );
            
            // ACT: add document training for template document
            Database.SaveResult saveResult = Database.insert(dt, false);
            
            System.assert(saveResult.isSuccess() == false,
                'Expected document training not to be saved for template document.');
            
            // check expected error message
            String errorMessage = 'You cannot add document trainings for template documents.';
            Boolean hasExpectedErrorMessage = false;
            for (Database.Error err : saveResult.getErrors()) {
                if (err.getMessage().equals(errorMessage)) {
                    hasExpectedErrorMessage = true;
                    break;
                }
            }
            
            System.assert(hasExpectedErrorMessage,
                'Expected error message to be "' + errorMessage + '".');
        }
    }
}