/*
 * test to displays the list of all the Personnel who are trained or fulfilled on all the requirements defined in the job function
 * author: Shailesh Maharjan
 * date: 2016/01/04
 * story: SQX-1606
 **/
@isTest
public class SQX_Test_1606_List_PT_On_Job_Function{
    static Boolean runAllTests = true,
                   run_givenTrainingList = false;

     /**
    * This test ensures that completed personnel document training records are listed for a job function as expected.
    */

    public testmethod static void givenTrainingList(){
        if (!runAllTests && !run_givenTrainingList) {
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        SQX_Personnel_Document_Training__c DocT1;
        
        System.runas(standardUser) {
        
            // add required personnel record
            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.save();
        
            // add required personnel record
            SQX_Test_Personnel personnel2 = new SQX_Test_Personnel();
            personnel2.save();

            //Creating Controlled Doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
                cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document().save();
                cDoc2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            SQX_Test_Controlled_Document cDoc3 = new SQX_Test_Controlled_Document().save();
                cDoc3.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for PJF test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jfResult = jf.save();
            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function for PJF test 2' );
            jf = new SQX_Test_Job_Function(jf2);
            jfResult = jf.save();

            // creating required Requirement
            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = true
            );
            
            Database.SaveResult saveResult1 = Database.insert(Req1, false);

            // creating required Requirement
            SQX_Requirement__c Req2 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc2.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = true
            );
            
            saveResult1 = Database.insert(Req2, false);

            // creating required Requirement
            SQX_Requirement__c Req3 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc3.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = true
            );
            
            saveResult1 = Database.insert(Req3, false);
            
            Test.startTest();

            // creating required Personnel Job Function
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            );
            
            saveResult1 = Database.insert(pjf1, false);

            // creating required Personnel Job Function
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = personnel2.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            );
            
            saveResult1 = Database.insert(pjf2, false);

            // creating required Personnel Job Function
            SQX_Personnel_Job_Function__c pjf3 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Job_Function__c = jf2.Id,
                Active__c = true
            );
            
            saveResult1 = Database.insert(pjf3, false);
            
            // reading auto generated personnel document training list
            List<SQX_Personnel_Document_Training__c> pdts = [SELECT Id, SQX_Personnel__c, SQX_Controlled_Document__c, Status__c, Completion_Date__c
                                                             FROM SQX_Personnel_Document_Training__c];
            
            SQX_Personnel_Document_Training__c pdtDoc1;
            SQX_Personnel_Document_Training__c pdtDoc2;
            SQX_Personnel_Document_Training__c pdtDoc3;
            for (SQX_Personnel_Document_Training__c pdt : pdts) {
                if (pdt.SQX_Personnel__c == personnel.mainRecord.Id && pdt.SQX_Controlled_Document__c == cDoc1.doc.Id)
                    pdtDoc1 = pdt;
                if (pdt.SQX_Personnel__c == personnel2.mainRecord.Id && pdt.SQX_Controlled_Document__c == cDoc2.doc.Id)
                    pdtDoc2 = pdt;
                if (pdt.SQX_Personnel__c == personnel.mainRecord.Id && pdt.SQX_Controlled_Document__c == cDoc3.doc.Id)
                    pdtDoc3 = pdt;
            }
            
            // completing personnel document training for cDoc1
            pdtDoc1.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            pdtDoc1.Completion_Date__c = Date.today();
            saveResult1 = Database.update(pdtDoc1, false);
            
            // completing personnel document training for cDoc3
            pdtDoc3.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            pdtDoc3.Completion_Date__c = Date.today();
            saveResult1 = Database.update(pdtDoc3, false);
            
            // ACT: get required list as per set on controller
            ApexPages.StandardController controller = new ApexPages.StandardController(jf1);
            SQX_Extension_Trained_PSN_On_JobFunction extension = new SQX_Extension_Trained_PSN_On_JobFunction(controller);
            List<SQX_Extension_Trained_PSN_On_JobFunction.TrainedPersonnel_Wrapper> docList = extension.trainedPersonnelList;

            //Assert: Comparing size of the document 
            System.assertEquals(1, docList.size(),
                'Expected 1 trained personnel to be listed.');
            
            System.assertEquals(personnel.mainRecord.Id, docList[0].personnelId,
                'Expected trained personnel to be proper.');
            
            System.assertEquals(1, docList[0].documentTrainings.size(),
                'Expected 1 document training record to be listed.');
            
            System.assertEquals(pdtDoc1.Id, docList[0].documentTrainings[0].Id,
                'Expected document training record to be proper.');
            
            Test.stopTest();
        }
    }
}