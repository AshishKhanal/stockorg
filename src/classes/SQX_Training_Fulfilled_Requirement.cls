/**
* This class will contain the common methods and static strings required for the Training Fulfilled Requirement
*/
public with sharing class SQX_Training_Fulfilled_Requirement {

    /**
    * Bulkified class for Training Fulfilled Requirement
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase {
    
        public Bulkified() {
        }
        
        public Bulkified(List<SQX_Training_Fulfilled_Requirement__c> newList , Map<Id, SQX_Training_Fulfilled_Requirement__c> oldMap) {
            super(newList, oldMap);
        }

        /**
        * When the Training Session is Locked,
        * Training Fulfilled Requirement cannot be manipulated.
        * @author Sudeep Maharjan
        * @date 2017/04/24
        * @story [SQX-3347]
        */
        public Bulkified trainingFulfilledRequirementCannotbeManipulatedAfterTrainingSessionIsLocked(){
            this.resetView();
            List<SQX_Training_Fulfilled_Requirement__c> allTrainingFulfilledRequirements = (List<SQX_Training_Fulfilled_Requirement__c>) this.view;
            
            if(allTrainingFulfilledRequirements.size() > 0){
                Map<Id, SQX_Training_Session__c> sessionsMap = new Map<Id, SQX_Training_Session__c>([
                    SELECT Id, Is_Locked__c
                    FROM SQX_Training_Session__c
                    WHERE Id IN :getIdsForField(allTrainingFulfilledRequirements, Schema.SQX_Training_Fulfilled_Requirement__c.SQX_Training_Session__c)
                ]);
                
                for(SQX_Training_Fulfilled_Requirement__c trainingFulfilledRequirement: allTrainingFulfilledRequirements){
                    if (sessionsMap.get(trainingFulfilledRequirement.SQX_Training_Session__c).Is_Locked__c == true) {
                        trainingFulfilledRequirement.addError(Label.SQX_ERR_TFR_CANNOT_BE_MANIPULATED_AFTER_TRAINING_IS_COMPLETE_VOID);
                    }
                }
            }
            return this;
        }
    }
}