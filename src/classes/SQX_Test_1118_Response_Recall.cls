/*
 * description: test for capa, nc, audit recall
 */
@IsTest
public class SQX_Test_1118_Response_Recall{

    @testSetup static void setupTestData(){
        //add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser1');
    }

    private static Boolean runAllTests = true,
                            run_capaResponse_ApprovalRequired_CapaCanBeRecalled = false,
                            run_ncResponse_ApprovalRequired_NCCanBeRecalled = false,
                            run_auditResponse_ApprovalRequired_ResponseCanBeRecalled = false;
    
    /**
    * helper funtion to setup Audit, finding and finding response
    * @param actionApproval set whether action approval is required or not
    **/                      
    public static SQX_Test_Audit arrangeForAuditResponse(User adminUser, boolean investigationApproval, user responseSubmitter){
            /*
            * Arrange : Create Audit with investigation apprval required
            *           Add a finding with investigation and containment required
            *           Add another finding with containment required
            *           Create audit response with investigation and containment for first finding and containment for second finding
            */
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
            audit.setStatus(SQX_Audit.STATUS_COMPLETE);
            audit.setPolicy(investigationApproval, false, false);//investigation aproval required, action post approval not required, action pre approval not required
            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.save();
            SQX_Test_Finding auditFinding1 =  audit.addAuditFinding()
                                                .setRequired(true, false, true, false, false) //response, containment, and investigation required
                                                .setApprovals(investigationApproval, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN)
                                                .save();

            System.runas(responseSubmitter) {
                SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();

                // added audit response
                changeSet.addChanged('AuditResponse-1', 
                                         new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 1'));

                // added finding response 1
                changeSet.addChanged('responseForFinding1-1', 
                                        new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding1.finding.Id,
                                               Response_Summary__c = 'Response for audit')
                                    )
                            .addRelation(SQX_Finding_Response__c.SQX_Audit_Response__c, 'AuditResponse-1');


                // added containment in finding 1
                changeSet.addChanged('Containment-1',
                                        new SQX_Containment__c(
                                                SQX_Finding__c = auditFinding1.finding.Id,
                                                 Containment_Summary__c = 'Containment for finding1',
                                                 Completion_Date__c = Date.today()) );


                //added  investigation for finding 1
                changeSet.addChanged('Investigation-1',
                                        new SQX_Investigation__c(
                                                SQX_Finding__c = auditFinding1.finding.Id,
                                                Investigation_Summary__c = 'Investigation for finding1'));

                //added response inlusion of type containment
                changeSet.addChanged('Inclusion-1',
                                        new SQX_Response_Inclusion__c(Type__c = 'Containment'))
                            .addRelation(SQX_Response_Inclusion__c.SQX_Containment__c, 'Containment-1')
                            .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding1-1');

                //added response inlusion of type investigation
                changeSet.addChanged('Inclusion-2',
                                        new SQX_Response_Inclusion__c(Type__c = 'Investigation'))
                            .addRelation(SQX_Response_Inclusion__c.SQX_Investigation__c, 'Investigation-1')
                            .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding1-1');


                Map<String, String> params = new Map<String, String>();
                params.put('nextAction', 'submitresponse');
                params.put('comment', 'Mock comment');
                params.put('purposeOfSignature', 'Mock purposeOfSignature');

                /*
                * Act : Submit audit response for audit
                */
                System.assertEquals(SQX_Extension_UI.OK_STATUS,
                    SQX_Extension_UI.submitResponse(audit.audit.Id, '{"changeSet": []}', changeSet.toJSON(), false , null, params ), 'Expected to successfully submit reponse');


                List<SQX_Audit_Response__c> auditResponse1 = [Select Id from SQX_Audit_Response__c 
                                                                where SQX_Audit__c= :audit.audit.Id];
                auditResponse1 = [Select Status__c, Approval_Status__c from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id];

                /*
                * Assert : Response should be approved 
                */
                System.assertEquals(SQX_Audit_Response.STATUS_IN_APPROVAL, auditResponse1[0].Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.STATUS_IN_APPROVAL+ ' but found ' +auditResponse1[0].Status__c);
                System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_PENDING, auditResponse1[0].Approval_Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_PENDING+ ' but found ' +auditResponse1[0].Approval_Status__c);

                
            }                                   
            

            return audit;
    }


    /**
    *   Setup: create CAPA, save 
    *   Action: send response, recall response; 
    *   Expected: response should have approval status='Recalled'
    *
    * @author Anish Shrestha
    * @date 2015/09/21
    * 
    */
    public testmethod static void capaResponse_ApprovalRequired_CapaCanBeRecalled(){

        if(!runAllTests && !run_capaResponse_ApprovalRequired_CapaCanBeRecalled)
            return;

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User capaOwner = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        DateTime startTime = DateTime.now();
        
        System.runas(capaOwner){
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility().save();
            capa.capa.Status__c = SQX_CAPA.STATUS_OPEN;
            capa.save();

            /* construct a response and submit for approval */
            System.runas(capa.Finding.primaryContactUser){
                ApexPages.StandardController sc = new ApexPages.StandardController(capa.Capa);
                SQX_Extension_CAPA_Supplier controller = new SQX_Extension_CAPA_Supplier(sc);
            
                controller.initializeTemporaryStorage();
                SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();

                // added finding response 1
                changeSet.addChanged('responseForFinding1-1', 
                                        new SQX_Finding_Response__c(
                                               SQX_CAPA__c = capa.capa.Id,
                                               Response_Summary__c = 'Response for capa')
                                    );


                // added containment in finding 1
                changeSet.addChanged('Containment-1',
                                        new SQX_Containment__c(
                                                SQX_CAPA__c = capa.capa.Id,
                                                 Containment_Summary__c = 'Containment for finding1',
                                                 Completion_Date__c = Date.today()) );


                //added  investigation for finding 1
                changeSet.addChanged('Investigation-1',
                                        new SQX_Investigation__c(
                                                SQX_CAPA__c = capa.capa.Id,
                                                Investigation_Summary__c = 'Investigation for finding1'));

                //added response inlusion of type containment
                changeSet.addChanged('Inclusion-1',
                                        new SQX_Response_Inclusion__c(Type__c = 'Containment'))
                            .addRelation(SQX_Response_Inclusion__c.SQX_Containment__c, 'Containment-1')
                            .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding1-1');

                //added response inlusion of type investigation
                changeSet.addChanged('Inclusion-2',
                                        new SQX_Response_Inclusion__c(Type__c = 'Investigation'))
                            .addRelation(SQX_Response_Inclusion__c.SQX_Investigation__c, 'Investigation-1')
                            .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding1-1');


                Map<String, String> params = new Map<String, String>();
                params.put('nextAction', 'submitresponse');
                params.put('comment', 'Mock comment');
                params.put('purposeOfSignature', 'Mock purposeOfSignature');

                /*
                * Act : Submit capa response for capa
                */
                System.assertEquals(SQX_Extension_UI.OK_STATUS,
                    SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', changeSet.toJSON(), false , null, params ), 'Expected to successfully submit reponse');


                List<SQX_Finding_Response__c> findingResponse = [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_CAPA__c = : capa.capa.Id];

                System.assertEquals(1, findingResponse.size());
            
                /*
                * Act : recall the response 
                */
                params = new Map<String, String>();
                params.put('nextAction', 'Recall');
                params.put('recordID', findingResponse[0].Id);
                Id capaId = SQX_Extension_CAPA.processChangeSetWithAction(capa.capa.Id, '{"changeSet": []}', null, params, null);

                System.assertEquals(SQX.Capa, string.valueOf(capaId.getsObjectType()),  
                'Expected to successfully Approve '+ capaId);

                findingResponse = [Select Status__c, Approval_Status__c from SQX_Finding_Response__c where SQX_CAPA__c = :capa.capa.Id];


                //Assert: Expected the response to be recalled and published
                System.assertEquals(SQX_Finding_Response.STATUS_PUBLISHED, findingResponse[0].Status__c,  
                    'Expected Status to be '+ SQX_Finding_Response.STATUS_PUBLISHED+ ' but found ' +findingResponse[0].Status__c);
                System.assertEquals(SQX_Finding_Response.APPROVAL_STATUS_RECALLED, findingResponse[0].Approval_Status__c,  
                    'Expected Status to be '+ SQX_Finding_Response.APPROVAL_STATUS_RECALLED+ ' but found ' +findingResponse[0].Approval_Status__c);

            }

        
        }


    }

    /**
    *   Setup: create Audit, save 
    *   Action: send response, recall response; 
    *   Expected: response should have approval status='Recalled'
    *
    * @author Anish Shrestha
    * @date 2015/09/21
    * 
    */
    public static testmethod void auditResponse_ApprovalRequired_ResponseCanBeRecalled(){

            if(!runAllTests && !run_auditResponse_ApprovalRequired_ResponseCanBeRecalled){
                return;
            }  
            /* construct a response */
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User stdUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

            System.runas(stdUser){
                /*
                * Arange : add audit, add findings, and submit response and action to complete all tasks and approve audit response
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true,responseSubmitter);//action approval not required

                /*
                * Assert: Must have only one audit response
                */
                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                //audit = arrangeAuditResponseWithAction(audit, responseSubmitter, SQX_Finding_Response.RECORD_TYPE_RESPONSE_WITHOUT_FINDING);
                System.runas(adminUser){
                    List<SQX_Audit_Response__c> auditResponse = [Select Id from SQX_Audit_Response__c 
                                                                    where SQX_Audit__c= :audit.audit.Id];

                    /*
                    * Act : approve the audit response 
                    */
                    Map<String, String> params = new Map<String, String>();
                    params.put('nextAction', 'Recall');
                    params.put('recordID', auditResponse[0].Id);
                    Id auditId = SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, '{"changeSet": []}', null, params, null);

                    System.assertEquals(SQX.Audit, string.valueOf(auditId.getsObjectType()),  
                    'Expected to successfully Approve '+ auditId);

                    auditResponse = [Select Status__c, Approval_Status__c from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id];

                     //Assert: Expected the response to be recalled and published
                    System.assertEquals(SQX_Audit_Response.STATUS_COMPLETED, auditResponse[0].Status__c,  
                        'Expected Status to be '+ SQX_Audit_Response.STATUS_COMPLETED+ ' but found ' +auditResponse[0].Status__c);
                    System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_RECALLED, auditResponse[0].Approval_Status__c,  
                        'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_RECALLED+ ' but found ' +auditResponse[0].Approval_Status__c);

                }
                
            }
    }
}