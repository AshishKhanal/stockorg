/**
 * Controller class for Audit Excel Helper page
 */
public with sharing class SQX_Controller_Audit_Excel_Helper {

    /**
     *  Method returns the Spreadsheet Customer Script Layout Page reference
     */
    public PageReference GetSpreadsheetCuScriptLayoutPage()
    {
        return new PageReference(SQX_Utilities.getPageName('_Cu_Script_Layout', 'SQX_Audit_Spreadsheet'));
    }
    
}