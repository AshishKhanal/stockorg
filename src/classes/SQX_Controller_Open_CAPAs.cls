/**
* This class is used to return all the open CAPA for which the user is CAPA Coordinator.
*/
public with sharing class SQX_Controller_Open_CAPAs{
    
    /**
    * returns the list of CAPA open capa for the currently logged in user's account 
    */
    public List<SQX_CAPA__c> getOpenCAPAs() {

            return [
            
            SELECT Acknowledgement_Completion_Date__c,Acknowledgement_Due_Date__c,Age__c,Business_Unit__c,Title__c,CAPA_Type__c,
            Closed_Date__c,Closure_Comment__c,Complete_Date__c,CreatedById,CreatedDate,
            Date_Opened__c,Department__c,Division__c,Effectiveness_Monitoring_Days__c,Effectiveness_Verification_Plan__c,
            External_Reference_Number__c,Has_Acknowledgement__c,Id,IsDeleted,Issued_Date__c,LastActivityDate,LastModifiedById,
            LastModifiedDate,LastReferencedDate,LastViewedDate,Name,Needs_Acknowledgement__c,Needs_Effectiveness_Review__c,
            Operation__c,OwnerId,Post_Approval_Required__c,Pre_Approval_Required__c,Production_Line__c,Rating__c,Resolution__c,
            Site__c,SQX_Account__c,SQX_Action_Approver__c,SQX_CAPA_Sponsor__c,SQX_Effectiveness_Reviewer__c,
            SQX_External_Contact__c,SQX_Finding__c,Status__c,SystemModstamp,Target_Due_Date__c FROM SQX_CAPA__c
                   WHERE Status__c = 'Open' AND SQX_CAPA_Coordinator__c = : UserInfo.getUserId() AND Partner_enabled__c = false
                   ORDER BY Name DESC];
        
    }

}