/**
* Unit tests for capturing Target Review Date in Document Review
*/
@IsTest
public class SQX_Test_1793_DocReview_TargetReviewDate {

    /**
    * Tests to ensure target review date is set from next review date of controlled document
    * when document reviw is created.
    */
    public static testmethod void givenDocumentReviewCreated_TargetReviewDateIsSet() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser1) {
            // add required current status controlled document with next review date
            Date nextReviewDate = System.today() + 1;
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document();
            cDoc.doc.Next_Review_Date__c = nextReviewDate;
            cDoc.save();
            
            cDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // arrange document review create page
            PageReference docReviewCreatePage = Page.SQX_Document_Review_Editor;
            docReviewCreatePage.getParameters().put('controlleddocid', cDoc.doc.Id);
            
            Test.setCurrentPage(docReviewCreatePage);
            SQX_Document_Review__c rev1 = new SQX_Document_Review__c( Controlled_Document__c = cDoc.doc.Id );
            ApexPages.StandardController controller = new ApexPages.StandardController(rev1);
            SQX_Extension_Document_Review ext = new SQX_Extension_Document_Review(controller);
            rev1.Review_Decision__c = 'Continue Use';
            rev1.Performed_By__c = UserInfo.getUserId();
            rev1.Next_Review_Date__c = System.today() + 90;
            
            // ACT: create new document review
            ext.SubmitReview();
            
            rev1 = [SELECT Id, Target_Review_Date__c FROM SQX_Document_Review__c WHERE Id = :rev1.Id];
            
            System.assertEquals(nextReviewDate, rev1.Target_Review_Date__c);
        }
    }
}