/**
* tests for SQX_Extension_Requirement methods
*/
@IsTest
public class SQX_Test_Extension_Requirement {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User admin1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, 'admin1');
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'user1');
        
        SQX_Job_Function__c jf1;
        
        System.runAs(admin1) {
            // add required job functions
            insert new SQX_Job_Function__c( Name = 'JF1', Active__c = true);
        }
    }
    
    /*
    * This returns Map of Job Function 
    */
    static Map<String, SQX_Job_Function__c> getJobFunctions() {
        List<SQX_Job_Function__c> jfs = [SELECT Id, Name FROM SQX_Job_Function__c];
        
        Map<String, SQX_Job_Function__c> jfMap = new Map<String, SQX_Job_Function__c>();
        for (SQX_Job_Function__c jf : jfs) {
            jfMap.put(jf.Name, jf);
        }
        
        return jfMap;
    }


	/**
    * tests save method of SQX_Extension_Requirement extension
    */
    public testmethod static void givenRequirementWithNecessaryFields_whenSaveIsCalled_thenRequirementIsSavedAndRedirectedToNewRequirementPage() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        
        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf1 = jfs.get('JF1');

        System.runas(user1) {
            
            //Arrange: Add required Controlled Document
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document().save();
            cDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            //Arrange: Add Requirement
            SQX_Requirement__c req = new SQX_Requirement__c(SQX_Controlled_Document__c = cDoc.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 2, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Days_to_Complete_Refresher__c = 1,
                                                             Days_in_Advance_To_Start_Refresher__c = 1,
                                                             Training_Program_Step__c = 1,
                                                             Active__c = false);
            
            Test.setCurrentPage(Page.SQX_Requirement_Create);
            ApexPages.currentPage().getParameters().put('jobfunction', jf1.Id);
            ApexPages.StandardController stdController = new ApexPages.StandardController(req);
            SQX_Extension_Requirement ext = new SQX_Extension_Requirement(stdController);
            
            // ACT: Call save method using extension
            PageReference pr = ext.save();
            
            List<SQX_Requirement__c> newRequirement = [SELECT Id FROM SQX_Requirement__c WHERE SQX_Job_Function__c = :jf1.Id AND SQX_Controlled_Document__c = :cdoc.doc.Id];
            
            //Assert: Requirement must be created
            System.assertEquals(1, newRequirement.size(), 'Expected 1 requirement to be created with passed parameters.');
            
            //Assert: Expected Requirement to be saved
            System.assert(pr != null,
                'Expected requirement to be saved using passed parameters.');
            
            //Assert: Should redirect to Created requirement page.
            System.assert(pr.getURL().contains(new ApexPages.StandardController(new SQX_Requirement__c(Id = newRequirement[0].Id)).view().getURL()),
                'Expected main record VF page URL to be returned.' + pr.getURL());
            
        }
    }
    
    /**
    * tests saveAndNew method of SQX_Extension_Requirement extension
    */
    public testmethod static void givenRequirementWithNecessaryFields_whenSaveAndNewIsCalled_thenRequirementIsSavedAndRedirectedToCreateRequirementPage() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        
        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf1 = jfs.get('JF1');

        System.runas(user1) {
            
            //Arrange: Add required Controlled Document 
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document().save();
            cDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            //Arrange: Add Requirement
            SQX_Requirement__c req = new SQX_Requirement__c(SQX_Controlled_Document__c = cDoc.doc.Id,
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 2, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Days_to_Complete_Refresher__c = 1,
                                                             Days_in_Advance_To_Start_Refresher__c = 1,
                                                             Training_Program_Step__c = 1,
                                                             Active__c = false);
            
            Test.setCurrentPage(Page.SQX_Requirement_Create);
            ApexPages.currentPage().getParameters().put('jobfunction', jf1.Id);
            ApexPages.StandardController stdController = new ApexPages.StandardController(req);
            SQX_Extension_Requirement ext = new SQX_Extension_Requirement(stdController);
            
            // ACT: call saveAndNew method using extension
            PageReference pr = ext.saveAndNew();
            
            List<SQX_Requirement__c> newRequirement = [SELECT Id FROM SQX_Requirement__c WHERE SQX_Job_Function__c = :jf1.Id AND SQX_Controlled_Document__c = :cdoc.doc.Id];
            
            //Assert: Expected Requirement to be created 
            System.assertEquals(1, newRequirement.size(), 'Expected 1 requirement to be created with passed parameters.');
            
            //Assert: Expected Requirement to be saved
            System.assert(pr != null,
                'Expected requirement to be saved using passed parameters.');
            
            //Assert: Should redirect to New Requirement page
            System.assert(pr.getURL().contains(Page.SQX_Requirement_Create.getURL()),
                'Expected SQX_Requirement_Create VF page URL to be returned.');
            
        }
    }
}