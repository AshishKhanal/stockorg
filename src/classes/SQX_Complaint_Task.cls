/**
* Class enclosing the business logic and trigger logic for complaint task
* Encloses the logic for
* a. Creating task for each complaint task
* b. deleting task for complaint task which aren't completed
*/
public with sharing class SQX_Complaint_Task {
    
    /* Statuses pick list field listing for use in code  */
    public static final String STATUS_DRAFT = 'Draft',
                               STATUS_OPEN = 'Open',
                               STATUS_COMPLETE = 'Complete';

    /**
    * Creates a SF task corresponding to the list of complaint tasks that are being sent to this method
    * @param complaintTasks the list of complaint task whose SF task are to be created
    * @return returns the list of database save result for SF tasks created for the passed complaint task. The list will have same index of passed complaint tasks.
    */
    public List<Database.SaveResult> createTaskFor(List<SQX_Complaint_Task__c> complaintTasks){

        List<Task> tasksToAdd = new List<Task>();

        for(SQX_Complaint_Task__c complaintTask : complaintTasks){
            Date dueDate = complaintTask.Due_Date__c;
            if(complaintTask.Allowed_Days__c != null){
                dueDate = Date.today() + (Integer)complaintTask.Allowed_Days__c;
            }
            tasksToAdd.add(SQX_Task_Template.createTaskFor(new SQX_Complaint__c(Id = complaintTask.SQX_Complaint__c), complaintTask.SQX_User__c, new Map<String, SObject> {
                            'Task' => complaintTask
                        }, dueDate, SQX_Task_Template.TaskType.ComplaintOpened));
        }

        return new SQX_DB().op_insert(tasksToAdd, new List<Schema.SObjectField> { });
    }


    /**
    * This method will find each of the SF tasks associated with the complaint task that are not completed and delete them.
    * @param complaintTasks the list of complaint task whose related Tasks are to be deleted.
    * @return returns the list of databse delete result. The result isn't quite usable because user will have hard time identifying the task that was deleted for the complaint task. TODO: change the return type to Map of list of delete result, currently this isn't necessary.
    */
    public List<Database.DeleteResult> deleteTasksOf(List<SQX_Complaint_Task__c> complaintTasks){
        Set<Id> childWhatIds = new SQX_BulkifiedBase().getIdsForField(complaintTasks, SQX_Complaint_Task__c.Id);
        Set<Id> whatIds = new SQX_BulkifiedBase().getIdsForField(complaintTasks, SQX_Complaint_Task__c.SQX_Complaint__c);
        List<Task> tasksToDelete = [SELECT Id, Status FROM Task WHERE Child_What_Id__c IN: childWhatIds AND WhatId IN: whatIds AND Status != : SQX_Task.getClosedStatuses()];

        /*
        * WITHOUT SHARING Used
        * ----------------------
        *
        * Standard users won't be able to delete system created task assigned/owned by other user.
        * So this will allow the standard user to delete the system created task.
        */
        return new SQX_DB().withoutSharing().op_delete(tasksToDelete);
    }

    /**
     * method to complete complaint tasks when related phr, investigation, decision tree or script execution are created
     * @param complaintIds complaint for which complaint tasks are created
     * @param cqTaskIds CQ Task Ids from which complaint task is created
     */
    public static void completeComplaintTasks(Set<Id> complaintIds, Set<Id> cqTaskIds){
        List<SQX_Complaint_Task__c> complaintTaskList = new List<SQX_Complaint_Task__c>();
        complaintTaskList = [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c IN :complaintIds AND SQX_Task__c IN :cqTaskIds];
        for(SQX_Complaint_Task__c complaintTask : complaintTaskList){
            complaintTask.Status__c = SQX_Supplier_Common_Values.STATUS_COMPLETE;
        }

        /*
        * WITHOUT SHARING Used
        * ----------------------
        *
        * Since complaint task is child of complaint so the owner of the complaint task will be owner of the complaint
        * But sf task may be completed by assignee who do not have edit permission to complaint so to update complaint task it is used
        */
        new SQX_DB().withoutSharing().op_update(complaintTaskList, new List<Schema.SObjectField>{SQX_Complaint_Task__c.Status__c});
    }

    /**
    * This class handles the trigger events related to complaint task object and performs various actiosn
    */
    public with sharing class Bulkified extends SQX_Steps_Trigger_Handler {
        
        
        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newNCs equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Complaint_Task__c> newComplaints, Map<Id,SQX_Complaint_Task__c> oldMap){
        
            super(newComplaints, oldMap);
        }

        /**
         * After insert/update/delete/undelete, add or delete sharing rules for assignee [addDeleteSharingRulesOnComplaint]
         */
        protected override void onAfter() {
            if(Trigger.isInsert) {
                addDeleteSharingRulesOnComplaint();
                createEvaluationRecords();
            }
            else if(Trigger.isUpdate) {
                addDeleteSharingRulesOnComplaint();
                createEvaluationRecords();
                preventCompletionWithoutCompletingEvaluationRecords();
            }
            else if(Trigger.isDelete) {
                addDeleteSharingRulesOnComplaint();
            }
            else if(Trigger.isUndelete) {
                addDeleteSharingRulesOnComplaint();
            }
        }

        /**
         * method to create phr and investigation from task.
         */
        public Bulkified createEvaluationRecords(){
            final String ACTION_NAME = 'createEvaluationRecords';

            Rule applicableTaskRule = new Rule();
            Boolean trueValue = true;

            applicableTaskRule.addRule(Schema.SQX_Complaint_Task__c.Applicable__c, RuleOperator.equals, (Object) trueValue);
            applicableTaskRule.addRule(Schema.SQX_Complaint_Task__c.SQX_Task__c, RuleOperator.NotEquals, null);
            applicableTaskRule.addRule(Schema.SQX_Complaint_Task__c.Status__c, RuleOperator.equals, SQX_Complaint.STATUS_OPEN);

            this.resetView()
                .removeProcessedRecordsFor(SQX.ComplaintTask, ACTION_NAME)
                .applyFilter(applicableTaskRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Complaint_Task__c> complaintTaskList = (List<SQX_Complaint_Task__c>) applicableTaskRule.evaluationResult;
            
            if(complaintTaskList.size() > 0){
                SQX_Custom_Settings_Public__c cqSetting = SQX_Custom_Settings_Public__c.getInstance();
                if(cqSetting.Create_Complaint_Evaluation_Records__c){
                    
                    String complaintInvestigationRecordId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Investigation, SQX_Investigation.RECORD_TYPE_COMPLAINT_INVESTIGATION);
                    Set<Id> cqTaskIds = getIdsForField(complaintTaskList, SQX_Complaint_Task__c.SQX_Task__c);
                    Set<Id> complaintIds = getIdsForField(complaintTaskList, SQX_Complaint_Task__c.SQX_Complaint__c);
                    Map<Id, SQX_Task__c> cqTaskMap = new Map<Id, SQX_Task__c>([SELECT Id, Task_Type__c FROM SQX_Task__c WHERE Id = :cqTaskIds]);
                    Map<Id, SQX_Complaint__c> complaintMap = new Map<Id, SQX_Complaint__c>([SELECT Id, OwnerId FROM SQX_Complaint__c WHERE Id = :complaintIds]);
                    
                    List<SQX_Product_History_Review__c> phrToInsert = new List<SQX_Product_History_Review__c>();
                    List<SQX_Investigation__c> investigationToInsert = new List<SQX_Investigation__c>();

                    for(SQX_Complaint_Task__c complaintTask : complaintTaskList){
                        if(cqTaskMap.get(complaintTask.SQX_Task__c).Task_Type__c == SQX_Task.TYPE_INVESTIGATION){
                            
                            SQX_Investigation__c investigation = new SQX_Investigation__c();
                            investigation.SQX_Complaint__c = complaintTask.SQX_Complaint__c;
                            investigation.Status__c = SQX_Investigation.STATUS_DRAFT;
                            investigation.OwnerId = complaintMap.get(complaintTask.SQX_Complaint__c).OwnerId;
                            investigation.SQX_Task__c = complaintTask.SQX_Task__c;
                            investigation.RecordTypeId = complaintInvestigationRecordId;
                            investigationToInsert.add(investigation);

                        } else if (cqTaskMap.get(complaintTask.SQX_Task__c).Task_Type__c == SQX_Task.TYPE_PHR){

                            SQX_Product_History_Review__c productHistoryReview = new SQX_Product_History_Review__c();
                            productHistoryReview.SQX_Complaint__c = complaintTask.SQX_Complaint__c;
                            productHistoryReview.Status__c = SQX_Product_History_Review.STATUS_DRAFT;
                            productHistoryReview.OwnerId = complaintMap.get(complaintTask.SQX_Complaint__c).OwnerId;
                            productHistoryReview.SQX_Task__c = complaintTask.SQX_Task__c;
                            phrToInsert.add(productHistoryReview);
                        }
                    }

                    if(investigationToInsert.size() > 0){
                        new SQX_DB().op_insert(investigationToInsert, new List<Schema.SObjectField>{});
                    }

                    if(phrToInsert.size() > 0){
                        new SQX_DB().op_insert(phrToInsert, new List<Schema.SObjectField>{});
                    }
                }
            }
            return this;
        }

        /**
         * method to prevent phr, investigation, decision tree, script execution's complaint task completion
         */
        public Bulkified preventCompletionWithoutCompletingEvaluationRecords(){
            final String ACTION_NAME = 'preventCompletionWithoutCompletingEvaluationRecords';

            Rule applicableTaskRule = new Rule();
            Boolean trueValue = true;

            applicableTaskRule.addRule(Schema.SQX_Complaint_Task__c.SQX_Task__c, RuleOperator.NotEquals, null);
            applicableTaskRule.addRule(Schema.SQX_Complaint_Task__c.Status__c, RuleOperator.equals, SQX_Complaint.STATUS_COMPLETE);
            applicableTaskRule.addRule(Schema.SQX_Complaint_Task__c.Task_Type__c, RuleOperator.NotEquals, SQX_Task.TYPE_SAMPLE_REQUEST);

            this.resetView()
                .removeProcessedRecordsFor(SQX.ComplaintTask, ACTION_NAME)
                .applyFilter(applicableTaskRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Complaint_Task__c> complaintTaskList = (List<SQX_Complaint_Task__c>) applicableTaskRule.evaluationResult;
            
            if(complaintTaskList.size() > 0){
                Set<Id> complaintIds = getIdsForField(complaintTaskList, SQX_Complaint_Task__c.SQX_Complaint__c);

                List<SQX_Complaint__c> complaintMap = [SELECT Id, 
                                                        (SELECT Id, SQX_Task__c FROM SQX_Investigations__r WHERE Status__c != :SQX_Investigation.STATUS_PUBLISHED AND Status__c != :SQX_Investigation.STATUS_VOID),
                                                        (SELECT Id, SQX_Task__c FROM SQX_Product_History_Reviews__r WHERE Status__c != :SQX_Product_History_Review.STATUS_COMPLETE),
                                                        (SELECT Id, SQX_Task__c FROM SQX_Decision_Trees__r),
                                                        (SELECT Id, SQX_CQ_Task__c FROM SQX_Script_Executions__r) 
                                                        FROM SQX_Complaint__c WHERE Id = :complaintIds];

                Set<String> complaintTaskUniqueMap = new Set<String>();
                for(SQX_Complaint__c complaint : complaintMap){
                    for(SQX_Investigation__c inv : complaint.SQX_Investigations__r){
                        complaintTaskUniqueMap.add(complaint.Id + '' + inv.SQX_Task__c);
                    }
                    for(SQX_Product_History_Review__c phr : complaint.SQX_Product_History_Reviews__r){
                        complaintTaskUniqueMap.add(complaint.Id  + '' + phr.SQX_Task__c);
                    }
                    for(SQX_Decision_Tree__c dt : complaint.SQX_Decision_Trees__r){
                        complaintTaskUniqueMap.add(complaint.Id  + '' + dt.SQX_Task__c);
                    }
                    for(SQX_Script_Execution__c se : complaint.SQX_Script_Executions__r){
                        complaintTaskUniqueMap.add(complaint.Id  + '' + se.SQX_CQ_Task__c);
                    }
                }
                                                        
                for(SQX_Complaint_Task__c complaintTask : complaintTaskList){
                    if(complaintTask.Task_Type__c == SQX_Task.TYPE_PHR || complaintTask.Task_Type__c ==  SQX_Task.TYPE_INVESTIGATION){
                        if(complaintTaskUniqueMap.contains(complaintTask.SQX_Complaint__c + '' + complaintTask.SQX_Task__c)){
                            complaintTask.addError(Label.SQX_ERR_MSG_Prevent_Complaint_Task_Completion_for_PHR_and_Investigation);
                        }
                    } else if(complaintTask.Task_Type__c == SQX_Task.TYPE_DECISION_TREE || complaintTask.Task_Type__c ==  SQX_Task.TYPE_SCRIPT){
                        if(!complaintTaskUniqueMap.contains(complaintTask.SQX_Complaint__c + '' + complaintTask.SQX_Task__c)){
                            complaintTask.addError(Label.SQX_ERR_MSG_Prevent_Complaint_Task_Completion_for_DT_and_Script);
                        }
                    }
                }
            }

            return this;
        }

        /**
        * add or delete sharing rules on complaint and its related Finding, Investigation and PHR records to its task assignees when assignee is added/changed/deleted/undeleted
        * Note: sharing rules are added to complaints with status Open or Closure Rejected
        */
        public Bulkified addDeleteSharingRulesOnComplaint() {
            this.resetView();
            
            Map<Id, Set<Id>> addUserShares = new Map<Id, Set<Id>>(),
                delUserShares = new Map<Id, Set<Id>>();
            
            for (SQX_Complaint_Task__c tsk : (List<SQX_Complaint_Task__c>)this.view) {
                if (trigger.isUpdate) {
                    SQX_Complaint_Task__c oldTsk = (SQX_Complaint_Task__c)oldValues.get(tsk.Id);
                    
                    if (tsk.SQX_User__c != oldTsk.SQX_User__c) {
                        // add sharing rules for new assignee
                        if (addUserShares.containsKey(tsk.SQX_Complaint__c) && tsk.SQX_User__c  != null) {
                            addUserShares.get(tsk.SQX_Complaint__c).add(tsk.SQX_User__c);
                        } else if(tsk.SQX_User__c  != null){
                            addUserShares.put(tsk.SQX_Complaint__c, new Set<Id>{ tsk.SQX_User__c });
                        }
                        // delete sharing rules for old assignee
                        if (delUserShares.containsKey(oldTsk.SQX_Complaint__c) && oldTsk.SQX_User__c != null) {
                            delUserShares.get(oldTsk.SQX_Complaint__c).add(oldTsk.SQX_User__c);
                        } else if(oldTsk.SQX_User__c != null){
                            delUserShares.put(oldTsk.SQX_Complaint__c, new Set<Id>{ oldTsk.SQX_User__c });
                        }
                    }
                } else if (trigger.isDelete) {
                    // delete sharing rules for deleted assignee
                    if (delUserShares.containsKey(tsk.SQX_Complaint__c) && tsk.SQX_User__c != null) {
                        delUserShares.get(tsk.SQX_Complaint__c).add(tsk.SQX_User__c);
                    } else if(tsk.SQX_User__c != null){
                        delUserShares.put(tsk.SQX_Complaint__c, new Set<Id>{ tsk.SQX_User__c });
                    }
                } else {
                    // add sharing rules for new or undeleted assignee
                    if (addUserShares.containsKey(tsk.SQX_Complaint__c) && tsk.SQX_User__c != null) {
                        addUserShares.get(tsk.SQX_Complaint__c).add(tsk.SQX_User__c);
                    } else if(tsk.SQX_User__c != null){
                        addUserShares.put(tsk.SQX_Complaint__c, new Set<Id>{ tsk.SQX_User__c });
                    }
                }
            }
            
            if(!addUserShares.isEmpty() || !delUserShares.isEmpty()) {
                SQX_Complaint.addSharingRulesOnComplaint(null, addUserShares, delUserShares);
            }
            
            return this;
        }

        /**
         * This method is to prevent deletion of record if parent is locked
         */
        public Bulkified preventDeletionOfLockedRecords(){
            final String ACTION_NAME = 'preventDeletionOfLockedRecords';
            this.resetView();
            List<SQX_Complaint_Task__c> complaintTaskList = (List<SQX_Complaint_Task__c>) this.view;
            if(complaintTaskList.size() > 0){
                for(SQX_Complaint_Task__c record : complaintTaskList) {
                   if((Boolean) record.Is_Parent_Locked__c || (Boolean) record.Is_Locked__c) {
                        record.addError(Label.SQX_ERR_MSG_ONCE_LOCKED_NO_DELETE);
                    }
                }
            } 
            return this;
        }
    
    }

}