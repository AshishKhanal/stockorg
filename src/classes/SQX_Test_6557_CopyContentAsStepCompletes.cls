/*
 * Unit test to make sure that content is transferred to onboarding step when assigneeuser who does not have access to library completes step.
 */ 

@isTest
public class SQX_Test_6557_CopyContentAsStepCompletes {
    
    static final String DRAFT_LIB_TYPE = 'Draft',
        RELEASE_LIB_TYPE = 'Release';
    
    @testSetup
    public static void commonSetup() {
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        SQX_Test_NSI.addUserToQueue(new List<User>{standardUser, adminUser});
        
        setUpLibraryAccess(SQX_Test_Account_Factory.getUsers());
        createControlDoc('Title1 for conDoc1');
        User assigneeUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser2');
        User assigneeUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser1');
        
        System.runAs(adminUser) {
            //Arrange: Create tasks, part and standard service
            SQX_Test_NSI.createPolicyTasks(1, SQX_Task.TASK_TYPE_DOC_REQUEST, assigneeUser1, 1);
            SQX_Test_NSI.createPolicyTasks(1, SQX_Task.TASK_TYPE_DOC_REQUEST, assigneeUser2, 2); 
            SQX_Test_NSI.createPolicyTasks(1, SQX_Task.TASK_TYPE_DOC_REQUEST, assigneeUser2, 3); 
            
            List<SQX_Task__c> taskRecords = [Select id, sqx_controlled_document__c from SQX_Task__c];
            SQX_Controlled_Document__c contDoc = [Select id, Document_Status__c from SQX_Controlled_Document__c];
            
            for(SQX_Task__c taskRecord : taskRecords) {
                taskRecord.sqx_controlled_document__c = contDoc.id;
            }
            update taskRecords;
            
            System.assertEquals(3, taskRecords.size());
            System.assertEquals('Current', contDoc.Document_Status__c, 'Expected doc to be in current stage');
            
        }  
    }
    
    /**
    * Set library permission for user
    */
    static void setUpLibraryAccess(Map<String, User> userMap) {
        List<Id> userList = new List<Id>();
        
        for(String username : userMap.keySet()) {
            userList.add(userMap.get(username).id);
        }
        
        SQX_Test_Utilities.createLibraries(userList);
    }
    
    /**
    * Create control document
    */
    static void createControlDoc(String title) {
        System.runAs(SQX_Test_Account_Factory.getUsers().get('standardUser')) {
            SQX_Test_Controlled_Document conDoc1 = new SQX_Test_Controlled_Document().updateContent(true, 'Doc1-for-ConDoc1.txt');
            
            conDoc1.doc.Title__c = title;
            conDoc1.doc.Draft_Vault__c = SQX_Test_Utilities.getLibraryId(DRAFT_LIB_TYPE);
            conDoc1.doc.Release_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);
            conDoc1.save();
            
            conDoc1.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_APPROVED;
            conDoc1.doc.Effective_Date__c = Date.Today();
            conDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            conDoc1.save();
            
        }
        
    }
    
    /**
     * Given: A NSI Record
     * When: User with no access to library completes record
     * Then: Record completion is allowed, and no error is thrown.
     */ 
    /*
    @isTest
    public static void givenNSIRecord_WhenUserWithNoAccessToLibraryTriesToCompleteTask_NoErrorIsThrown() {
        SQX_Test_NSI nsiTestRecord;
        Map<String, User> userMap= SQX_Test_Account_Factory.getUsers();
        SQX_Controlled_Document__c contDoc = [Select id, valid_content_reference__c from SQX_Controlled_Document__c];
        User stdUser = userMap.get('standardUser');
        User assigneeUser1 = userMap.get('assigneeUser1');
        User assigneeUser2 = userMap.get('assigneeUser2');
        String description = '';
        
        //Act: submit nsi record
        System.runAs(stdUser) {
            nsiTestRecord = new SQX_Test_NSI().save();
            nsiTestRecord.submit();
            nsiTestRecord.initiate();
            
            //Assert: Make sure content is linked to nsi
            System.assertEquals(3, [Select id from SQX_Onboarding_Step__c].size());
            
        }
        
        System.runAs(assigneeUser1) {
            //Arrange: Get task1 record
            Task task1 = [Select id, child_what_id__c from task where ownerId = :assigneeUser1.id];
            
            //Act: assigneeuser with no permission to library tries to complete-go task record
            SQX_Onboarding_Step__c onboardingRecord1 = [Select id, ContentDocId__c from SQX_Onboarding_Step__c where id = :task1.child_what_id__c];
            
            //Assert: Make sure content is present in onboarding record
            List<ContentDocumentLink> cdLink = [Select ContentDocumentId from ContentDocumentLink where linkedEntityId = :onboardingRecord1.id ];
            System.assertEquals(1, cdLink.size());
            update task1;
        }
        
        System.runAs(assigneeUser2) {
            //Arrange: Get task2 record
            
            Test.startTest();
            SQX_BulkifiedBase.clearAllProcessedEntities();
            Task task2 = [Select id,child_what_id__c from task where ownerId = :assigneeUser2.id and Status NOT IN : SQX_Task.getClosedStatuses()];
            
            //Act: assigneeuser with no permission to library tries to complete-go task record
            SQX_Onboarding_Step__c onboardingRecord2 = [Select id, ContentDocId__c from SQX_Onboarding_Step__c where id = :task2.child_what_id__c];
            //Assert: Make sure content is present in onboarding record
            System.assertEquals(1, [Select ContentDocumentId from ContentDocumentLink where linkedEntityId = :onboardingRecord2.id and contentdocumentId = :onboardingRecord2.ContentDocId__c].size());
            update task2;
           
            SQX_BulkifiedBase.clearAllProcessedEntities();
            Task task3 = [Select id,child_what_id__c from task where ownerId = :assigneeUser2.id and Status NOT IN : SQX_Task.getClosedStatuses()];
            
            //Act: assigneeuser with no permission to library tries to complete-go task record
            SQX_Onboarding_Step__c onboardingRecord3 = [Select id, ContentDocId__c from SQX_Onboarding_Step__c where id = :task3.child_what_id__c];
            
            //Assert: Make sure content is present in onboarding record
            System.assertEquals(1, [Select ContentDocumentId from ContentDocumentLink where linkedEntityId = :onboardingRecord3.id and contentdocumentId = :onboardingRecord3.ContentDocId__c].size());
            update task3;
            Test.stopTest();
        } 
        
    }*/
    
    /**
     * Given: A NSI Record
     * When: User updates formToUse in draft step
     * Then: old content link should be removed
     */ 
    /*
    @isTest
    public static void givenNSIRecord_WhenDraftStepRecordFormToUseIsUpdate_NewContentIsAddedAndOldRemovedFromStep() {
        Map<String, User> userMap= SQX_Test_Account_Factory.getUsers();
        SQX_Controlled_Document__c contDoc = [Select id, valid_content_reference__c from SQX_Controlled_Document__c];
        
        User stdUser = userMap.get('standardUser');
        User assigneeUser1 = userMap.get('assigneeUser1');
        
        SQX_Test_NSI nsiTestRecord;
        createControlDoc('Title2 for conDoc2');
        SQX_Controlled_Document__c contDoc2 = [Select id, valid_content_reference__c from SQX_Controlled_Document__c where title__c = 'Title2 for conDoc2'];
        
        System.runAs(stdUser) {
            //Arrange: Create NSI Records
            nsiTestRecord = new SQX_Test_NSI().save();
            nsiTestRecord.submit();
            nsiTestRecord.initiate();

            SQX_Onboarding_Step__c onboardingStep2 = [SELECT Id, 
                                 Step__c, 
                                 SQX_Parent__c, 
                                 Name,
                                 Document_Name__c,
                                 SQX_User__c,
                                 Description__c,
                                 Due_Date__c,
                                 Allowed_Days__c,
                                 RecordTypeId,  
                                 SQX_Controlled_Document__c 
                            FROM SQX_Onboarding_Step__c WHERE SQX_Controlled_Document__c != null  LIMIT 1];
            onboardingStep2.Step__c = 2;
            onboardingStep2.Id = null;
            insert onboardingStep2;
            
            SQX_Onboarding_Step__c onboardingRecord1 = [Select id, ContentDocId__c, status__c from SQX_Onboarding_Step__c limit 1];
            //Act: Update form to Use in open step record
            ContentDocumentLink initialLink = [Select linkedentityId, contentDocumentId from contentDocumentLink where linkedentityId = :onboardingStep2.id];
            onboardingStep2.SQX_Controlled_Document__c = contDoc2.id;
            SQX_BulkifiedBase.clearAllProcessedEntities();
            System.assertEquals(1, [SELECT Id FROM ContentVersion WHERE ContentDocumentId = : initialLink.ContentDocumentId].size());
            update onboardingStep2;
            
            //Assert: Make sure new version is released since the content has been modified as CQ Record can't be modified
            ContentDocumentLink finalLink = [Select linkedentityId, contentDocumentId from contentDocumentLink where linkedentityId = :onboardingStep2.id];
            System.assertEquals(initialLink.LinkedEntityId, onboardingStep2.id);
            System.assertEquals(initialLink.LinkedEntityId, finalLink.LinkedEntityId);
            System.assertEquals(initialLink.contentDocumentId, finalLink.contentDocumentId);
            System.assertEquals(2, [SELECT Id FROM ContentVersion WHERE ContentDocumentId = : finalLink.ContentDocumentId].size());
            
        }
        
    }*/
}