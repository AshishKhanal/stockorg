/*
 * This trigger performs following actions:
 * a. before update         : calculates the result for the inspection based on the inspection details [calculateResultOnInspectionCompletion]
 * b. after insert/update   : created outbound material transaction record based on the custom settings on inspection initiation and closure [createOutboundTransactionForInspection]
 * c. before insert/update  : set the dates in inspection as per the inspection status and approval status [setDatesWhenInspectionStatusIsChanged]
 * d. before update         : set the dates in inspection as per the approval status [setDatesWhenApprovalStatusIsChanged]
 * e. before update         : close inspection directly if closure review is not required [closeInspectionOnCompletionIfReviewNotRequired]
 * f. after update          : create record activity for inspection when the inspection is closed directly without review [createRecordActivityOnInspectionClosure]
 * g. before update         : create NC from the failed inspection [createNCOnInspectionClosure]
 * h. before update         : set the lock flag to true which prevents change of Inspection when the inspection is closed or void [lockRecordAfterInspectionIsVoidOrClosed]
 * i. before update         : creates sf task for the inspection on inspection completion [createTaskOnInspectionCompletion]
 * j. before update         : completes sf task on inspection closure or rejection [completeTaskOnInspectionClosureOrRejection] 
 * k. before update         : creates sf task for the inspection on closure reviewer change[createTaskOnClosureReviewerChange]
 * l. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Inspection_Trigger on SQX_Inspection__c (before update, before insert, after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    SQX_Inspection.Bulkified processor = new SQX_Inspection.Bulkified(trigger.new, trigger.oldMap);
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            processor.calculateResultOnInspectionCompletion()
                    .closeInspectionOnCompletionIfReviewNotRequired()
                    .setDatesWhenInspectionStatusIsChanged()
                    .setDatesWhenApprovalStatusIsChanged()
                    .createTaskOnInspectionCompletion()
                    .createTaskOnClosureReviewerChange()
                    .completeTaskOnInspectionClosureOrRejection()
                    .createNCOnInspectionClosure()
                    .lockRecordAfterInspectionIsVoidOrClosed();
        } else if(Trigger.isInsert){
            processor.setDatesWhenInspectionStatusIsChanged();
        }
    } else if(Trigger.isAfter){
        if(Trigger.isInsert){
            processor.createOutboundTransactionForInspection();
        } else if(Trigger.isUpdate){
            processor.createOutboundTransactionForInspection()
                     .createRecordActivityOnInspectionClosure()
                     
                     .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}