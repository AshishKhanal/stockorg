/*
 * This trigger performs following actions:
 * a. before insert/update : prevents duplication of association of the active controlled documents [ensureControlledDocHaveUniqueAssociation]
 * b. before insert/update/delete : prevent modification of association when its related inspection criteria is in approval [preventModificationOfAssociationOnApproval]
 * c. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Association_Trigger on SQX_Association__c (before update, before insert, before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isBefore){
        if(Trigger.isUpdate || Trigger.isInsert){
             new SQX_Association.Bulkified(trigger.new, trigger.oldMap)
                                .ensureControlledDocHaveUniqueAssociation()
                                .preventModificationOfAssociationOnApproval();
        } else if(Trigger.isDelete){
             new SQX_Association.Bulkified(trigger.old, trigger.newMap).preventModificationOfAssociationOnApproval();
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}