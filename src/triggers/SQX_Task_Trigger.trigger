/**
* trigger to perform actions for SF Task
* a. before delete : method to prevent task from deletion before being completed [preventDeleteIfNotCompleted]
* b. after  update : method to prevent task from completion for supplier type [completeRelatedTasks]
*/
trigger SQX_Task_Trigger on Task (before delete, after  update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    if(trigger.isBefore){
        if(trigger.isDelete){
            new SQX_Task.Bulkified(trigger.old, trigger.newMap).preventDeleteIfNotCompleted();
        }
    } else {
        if (Trigger.isUpdate){
            new SQX_Task.Bulkified(trigger.new, trigger.oldMap)
                        .completeRelatedTasks();
        }
    }
}