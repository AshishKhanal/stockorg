/**
* Trigger to perform various workflow based on assessment question
* @author Paras Biswhakarma
* 
* a. before insert/update sets Unique Name field with Assessment Id and question number [ensureCombinationIsUnique()]
* b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Assessment_Question_Trigger on SQX_Assessment_Question__c (before insert, before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (Trigger.isBefore) {
        new SQX_Assessment_Question.Bulkified(Trigger.new, Trigger.oldMap)
                                   .ensureCombinationIsUnique(new Schema.SObjectField[]{
                                                                    Schema.SQX_Assessment_Question__c.SQX_Assessment__c,
                                                                    Schema.SQX_Assessment_Question__c.Question_Number__c
                                                              },
                                                              Schema.SQX_Assessment_Question__c.Uniqueness_Constraint__c);
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}