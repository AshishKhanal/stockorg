/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Audit_Team_Trigger on SQX_Audit_Team__c (before delete, before insert, before update, after delete, after update, after insert) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isBefore){
    
        if(Trigger.isDelete){
            /*
            * prevent deletion of an audit team member, who is the owner of the audit.
            */
            new SQX_Audit_Team.Bulkified(Trigger.old, Trigger.newMap).ensureThatAuditOwnerCantBeRemovedFromAuditTeam();
        }
        else if(Trigger.isInsert || Trigger.isUpdate){
            SQX_Audit_Team.Bulkified auditTeamBulkifiedObj = new SQX_Audit_Team.Bulkified(Trigger.new, Trigger.oldMap);

            if (Trigger.isUpdate) {
                auditTeamBulkifiedObj.updateSharingAccessLevelOnTeamRoleChange(); // update the managed sharing when role of team changes
            }

            /*
            * ensure that the combination of owner and audit is unique.
            */
            auditTeamBulkifiedObj.shareAuditAndFindingWithAuditTeamMembers()
               .ensureCombinationIsUnique(new Schema.SObjectField[] { Schema.SQX_Audit_Team__c.SQX_User__c,
                Schema.SQX_Audit_Team__c.SQX_Audit__c }, Schema.SQX_Audit_Team__c.Uniqueness_Constraint__c);
        }

    }
    else if(Trigger.isAfter){
        /*
        * remove sharing rules for any team member that is removed.
        */
        if(Trigger.isDelete){
            new SQX_Audit_Team.Bulkified(Trigger.old, Trigger.newMap)
            .removeSharingRulesForDeletedMembers()
            .deleteSharingRulesFromFinding();
        }
        else if(Trigger.isUpdate){
            /*
            * any update removes sharing rule for old team member
            */
            new SQX_Audit_Team.Bulkified(Trigger.new, Trigger.oldMap).removeOldSharingRulesWhenUpdated()
            .updateEventRelationForAudit() //updating team members for audit eventrelation
            
            .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
        else if(Trigger.isInsert){
            new SQX_Audit_Team.Bulkified(Trigger.new, Trigger.oldMap)
            .addEventRelationForAudit(); //adding members for audit eventrelation
        }
    }
}