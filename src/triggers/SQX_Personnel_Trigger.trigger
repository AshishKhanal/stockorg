/**
* Tasks performed:
* a. after insert / update [activatePersonnelJobFunctionWhenPersonnelActivated]: activate new personnel job functions when personnel is activated
* b. after insert / update [deactivateActivePersonnelJobFunctionWhenPersonnelDeactivated]: deactivate active personnel job functions when personnel is deactivated
* c. after insert / update [shareRecordWithActiveRelatedUser]: adds read access sharing rule for active related user and deletes sharing for old related user
* d. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Personnel_Trigger on SQX_Personnel__c (after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (trigger.isAfter) {
        if (trigger.isInsert || trigger.isUpdate) {
            SQX_Personnel.Bulkified bulkified = new SQX_Personnel.Bulkified(trigger.new, trigger.oldMap)
                .activatePersonnelJobFunctionWhenPersonnelActivated()
                .deactivateActivePersonnelJobFunctionWhenPersonnelDeactivated()
                .shareRecordWithActiveRelatedUser();
            
            if (Trigger.isUpdate) {
                bulkified.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
            }
        }
    }
}