/**
* This trigger handles the following business logs for decision tree object.
* a. Before Insert: On creation of decision tree sets the user (Run By) and run on date values in each record. [captureCreatorOfDecisionTree]
* b. Before Delete: On deletion of decision tree check whether parent record is locked and if locked then stop deletion. [preventDeletionOfLockedRecords] 
* c. After Insert: Void all the previously generated reporting requirements that are 'Pending' [voidPreviouslyGeneratedReportingRequirement]
* d. After Insert : Complete all related decision tree task
* e. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Decision_Tree_Trigger on SQX_Decision_Tree__c (before insert, after insert, before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        if(trigger.isInsert){
            new SQX_Decision_Tree.Bulkified(trigger.new, trigger.oldMap).captureCreatorOfDecisionTree();
        } else if(trigger.isDelete){
            new SQX_Decision_Tree.Bulkified(trigger.old, null).preventDeletionOfLockedRecords();
        }
    }
    else {
        if(trigger.isInsert){
            new SQX_Decision_Tree.Bulkified(trigger.new, trigger.oldMap).voidPreviouslyGeneratedReportingRequirement()
                                                                        .completeComplaintTask();
        }
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}