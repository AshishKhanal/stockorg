/**
 * This trigger performs following operations
 * a. before insert/update/delete : prevent adding or deleting ContentDocumentLink for locked NSI/SD/Onboarding/Deviation Process [preventContentUploadAndDeletionOnLockedRecord]
 */
trigger SQX_ContentDocumentLink_Trigger on ContentDocumentLink (before insert, before update, before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    SQX_ContentDocumentLink.Bulkified processor = null;
    if (Trigger.isDelete) {
        processor = new SQX_ContentDocumentLink.Bulkified(trigger.old, trigger.newMap);
        processor.preventContentUploadAndDeletionOnLockedRecord();
    } else {
        SQX_ContentDocumentLink.Bulkified processor = new SQX_ContentDocumentLink.Bulkified(trigger.new, trigger.oldMap);   
        processor.preventContentUploadAndDeletionOnLockedRecord();
    }
}