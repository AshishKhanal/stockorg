/*
* Trigger methods description
* a. after insert/update: Creates Training Fulfilled Requirement for Related Controlled Document[createTrainingFulfilledRequirementForRelatedControlledDocument]
* b. after update [processRostersOnCompletionOrClosure]:
*       - Clear Training Session link from incomplete roster's document training and create document training for completed roster personnel when completing training session
*       - Signoff Trainer of completed or no assessment personnel when closing training session[signOffDocumentTrainingWhenClosingTrainingSession]
* c. after update: Remove pending document training created during training session completion, when complete session is voided or session is reopened [resetPendingDocumentTrainingsWhenVoided]
* d. before update: Lock training session record when completed or closed or voided [lockTrainingSession]
* e. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Training_Session_Trigger on SQX_Training_Session__c (before update, after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (trigger.isBefore) {
        if (trigger.isUpdate) {
            new SQX_Training_Session.Bulkified(trigger.new, trigger.oldMap)
                .lockTrainingSession();
        }
    }
    else{
        if (trigger.isInsert) {
            new SQX_Training_Session.Bulkified(trigger.new, trigger.oldMap)
                .createTrainingFulfilledRequirementForRelatedControlledDocument(); 
        }
        else if(trigger.isUpdate){
            new SQX_Training_Session.Bulkified(trigger.new, trigger.oldMap)
                .createTrainingFulfilledRequirementForRelatedControlledDocument()    
                .processRostersOnCompletionOrClosure()
                .resetPendingDocumentTrainingsWhenVoided()
                
                .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}