/**
*   @author : Piyush Subedi
*   @date : 2016/12/1
*   @description : The trigger perfoms the following workflows 
*                   a. Before delete : prevents users from deleting a group which has been initiated from a controlled document record [preventDeletion]
*/
trigger SQX_Collaboration_Group_Trigger on CollaborationGroup (before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isDelete){
        new SQX_Collaboration_Group.Bulkified(Trigger.old, Trigger.newMap).preventDeletion();
    }
}