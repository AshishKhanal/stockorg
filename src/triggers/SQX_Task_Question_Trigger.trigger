/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Task_Question_Trigger on SQX_Task_Question__c (after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}