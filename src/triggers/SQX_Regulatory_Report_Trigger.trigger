/**
 * The following workflows are done
 * a. Before insert/update: The assignee is set to running user if no value has been provided and report is locked if voided/completed. [setDefaultAssigneeAndLock]
 * b. Before Insert: Create SF task assigned to report owner for each regulatory reporting requirement. [createReportingTasks]
 * c. Before Delete: On deletion of regulatory report check whether parent record is locked and if locked then stop deletion. [preventDeletionOfLockedRecords] 
 * c. Before Delete: Method is to prevent deletion of record. [preventDeletionOfRegulatoryReports]
 * d. After Update: Close task Associated with regulatory report [closeTaskAssociatedWithReports]
*  e. Before update : Set status based on activity code [setStatusAndStageBasedOnActivityCode]
*  f. After update : Clear activity code if it is not blank [clearActivityCode]
 * g. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 * h. Before Update: evaluates the record and if the activity code is blank [evaluateSaveActivity]
 * i. Before Insert/Update [updateAlertDateAndDueDate]: update alert date and due date
 */
trigger SQX_Regulatory_Report_Trigger on SQX_Regulatory_Report__c (before insert, before update, after insert, after update, before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            new SQX_Regulatory_Report.Bulkified(Trigger.new, Trigger.oldMap)
                                     .setStatusAndStageBasedOnActivityCode()
                                     .evaluateSaveActivity()
                                     .setDefaultAssigneeAndLock();
        }
        else if(Trigger.isInsert){
            new SQX_Regulatory_Report.Bulkified(Trigger.new, Trigger.oldMap)
                                     .setDefaultAssigneeAndLock()        
                                     .createReportingTasks();
        }
        else if(Trigger.isDelete){
            new SQX_Regulatory_Report.Bulkified(Trigger.old, null).preventDeletionOfLockedRecords()
                                                                    .preventDeletionOfRegulatoryReports();
        }
    }
    else if(Trigger.isAfter){
        if(Trigger.isUpdate){
            new SQX_Regulatory_Report.Bulkified(Trigger.new, Trigger.oldMap).closeTaskAssociatedWithReports()
                                                                            .clearActivityCode()

                                                                            .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}