/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Supplier_Service_Trigger on SQX_Supplier_Service__c (before update, before insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore ){
        if(trigger.isUpdate || trigger.isInsert){
            new SQX_BulkifiedBase(trigger.New, trigger.oldMap).ensureCombinationIsUnique(
                new Schema.SObjectField[]{Schema.SQX_Supplier_Service__c.Account__c, Schema.SQX_Supplier_Service__c.Standard_Service__c},
                Schema.SQX_Supplier_Service__c.Uniqueness_Constraint__c
            );
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }

}