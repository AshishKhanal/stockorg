/*
* Trigger methods description
* a. after insert/update [setPendingDocumentTrainingCountInControlledDocumentForInsertUpdate] : sets the number of pending document training in controlled document
* b. after delete/undelete [setPendingDocumentTrainingCountInControlledDocumentForDeleteUndelete] : sets the number of pending document training in controlled document
* c. before insert [setPersonnelTrainingManagerAsDefaultTrainerForPendingStatus] : sets default trainer for document training
* d. before delete [preventRecordDeletion] : prevents document trainings with active job functions and history records
* e. after update [syncRelatedPersonnelDocumentJobFunction] : syncs training status and due date of related PDJFs
* f. after update [signOffRecordWhenLinkedWithScormPersonnelAssesmentRecord] sign off records linked with personnel assement
* g. after insert/update [signOffRelatedDocumentsWhenCourseIsSignedOff] : signs off related documents/fulfilled requirements of Course type controlled documents when the PDT associated with the course is signed off
* h. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Personnel_Document_Training_Trigger on SQX_Personnel_Document_Training__c (before insert, before delete, after insert, after update, after delete, after undelete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (trigger.isBefore) {
        
        if (trigger.isInsert) {
             new SQX_Personnel_Document_Training.Bulkified(trigger.new, trigger.oldMap)
                     .setPersonnelTrainingManagerAsDefaultTrainerForPendingStatus();
        }
        
        if (trigger.isDelete) {
             new SQX_Personnel_Document_Training.Bulkified(trigger.old, trigger.newMap)
                     .preventRecordDeletion();
        }
        
    }

    else {
        if(trigger.isInsert || trigger.isUpdate){
            SQX_Personnel_Document_Training.Bulkified processor = new SQX_Personnel_Document_Training.Bulkified(trigger.new, trigger.oldMap);
            
            processor.setPendingDocumentTrainingCountInControlledDocumentForInsertUpdate()
                     .signOffRelatedDocumentsWhenCourseIsSignedOff();
            
            if (trigger.isUpdate) {
                processor.syncRelatedPersonnelDocumentJobFunction()
                         .signOffRecordWhenLinkedWithScormPersonnelAssesmentRecord()
                         
                         .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
            }
        }
        
        else if(trigger.isDelete){
             new SQX_Personnel_Document_Training.Bulkified(trigger.old, trigger.newMap)
                     .setPendingDocumentTrainingCountInControlledDocumentForDeleteUndelete();
        }
        
        else if(trigger.isUndelete){
             new SQX_Personnel_Document_Training.Bulkified(trigger.new, trigger.oldMap)
                     .setPendingDocumentTrainingCountInControlledDocumentForDeleteUndelete();
        }
    }
}