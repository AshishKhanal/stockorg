/**
 * This trigger performs following functions using apex method mentioned below
 * a. after insert / after update : prevents defect defective quantity greater than impacted product lot quantity [preventDefectiveQtyGreaterThanLotQty]
 * b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_NC_Defect_Trigger on SQX_NC_Defect__c (after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isAfter){
        SQX_NC_Defect.Bulkified processor = new SQX_NC_Defect.Bulkified(trigger.new, trigger.oldMap);
        if(Trigger.isInsert || Trigger.isUpdate){
            processor.preventDefectiveQtyGreaterThanLotQty();
        }
        if (Trigger.isUpdate) {
            processor.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}