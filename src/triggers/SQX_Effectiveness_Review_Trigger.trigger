/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Effectiveness_Review_Trigger on SQX_Effectiveness_Review__c (before insert, after update, after insert, before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
  
  SQX_Effectiveness_Review.Bulkified effReviews = new SQX_Effectiveness_Review.Bulkified(trigger.New, trigger.Old);
   
  if(trigger.isAfter){
      if(trigger.isInsert){
          //create task for individual reviews 
          effReviews.createTaskForReviews();
                    
        
      }
      else if(trigger.isUpdate){
          effReviews.completeAllTasks();
      }
      
      //close all related findings if the review has been flagged as effective or ineffective
      effReviews.filterEffectiveOrInEffectiveReviews(trigger.isUpdate)
                .closeRelatedCapas()
                .persistAllRecords();
                
      //create new review for all rescheduled reviews       
      effReviews.createEffectivenessReviewForScheduledReviews();
      
      
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
  }
  if(trigger.isBefore){
      if(trigger.isDelete){
          //prevent deletion if parent is locked or the record is locked by itself.
         new SQX_BulkifiedBase(trigger.old, null)
             .preventChangesIfLocked(SQX_Effectiveness_Review__c.Locked__c,
                                     SQX_Effectiveness_Review__c.Is_Locked__c,
                                     Label.SQX_ERR_MSG_ONCE_LOCKED_NO_DELETE,
                                     true);
      }
    }   
  
  effReviews.persistAllRecords();

}