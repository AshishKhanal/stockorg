/**
 * a. before delete : removes link of primary Investigation from Complaint and Associated Item [removeLinkOfInvestigation]
 * b. After Update: [addModifiedLongTextFieldHistory] captures modified long text field history
 * c. Before insert: set concatenated value of Complaint, Investigation and Associated Item in Unique_Investigation field 
 */
trigger SQX_Linked_Investigation_Trigger on SQX_Linked_Investigation__c (before insert, before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (Trigger.isBefore) {
        if (trigger.isInsert){
            new SQX_Linked_Investigation.Bulkified(Trigger.new, Trigger.newMap).setUniqueInvestigation();
        }
        if (Trigger.isDelete) {
            new SQX_Linked_Investigation.Bulkified(Trigger.old, Trigger.newMap).removeLinkOfInvestigation();
        }
    }
    if (Trigger.isAfter && Trigger.isUpdate) {
        new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
    }
}