/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_CAPA_Trigger on SQX_CAPA__c (before insert, before update, after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    SQX_CAPA.Bulkified triggerProcessor = null;
    
        
    if(trigger.isBefore){
        new SQX_CAPA.Bulkified(trigger.new, trigger.oldMap).performBeforeUpdates()
                                                            .updateIsPartner()
                                                            .completeCAPAOnAllPolicy()
                                                            .preventCAPAFromClosure();
    }
    else if(trigger.isAfter){
        
        triggerProcessor = new SQX_CAPA.Bulkified(trigger.new, trigger.oldMap);
        if((trigger.isUpdate)){ 
            triggerProcessor.createEffectivenessReviewAndTask()
                            .shareCapaWithAccountRole()
                            .closeRelatedTasksVoidUnsubmittedResponses_OnClosureOfCAPA()
                            .completeFindingWhenCAPAisCompleted()
                            .updateFindingWhenCAPAisClosed()
                            
                            .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
        else if(trigger.isInsert){
            triggerProcessor.createEffectivenessReviewAndTask()
                            .shareCapaWithAccountRole()
                            .closeRelatedTasksVoidUnsubmittedResponses_OnClosureOfCAPA();
        }
    }
}