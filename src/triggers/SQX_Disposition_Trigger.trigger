/**
* Trigger to handle various actions when disposition object is saved (inserted/updated) and deleted.
*
* The trigger performs the following workflows
* a. Before delete : Prevents deletion of completed and approved dispositions [preventDeletionOfCompletedDisposition]
* b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Disposition_Trigger on SQX_Disposition__c (after insert, after update, before insert, before update, before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
        if(trigger.isInsert || trigger.isUpdate){

            //sqx-783 once disposition is completed update the total qty to dispose in the impacted product object that is related to 
            //the disposition
            new SQX_Disposition.Bulkified(Trigger.new, Trigger.oldMap)
                                .updateDisposedQtyWhenDispositionIsComplete();
        }
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
    else {

        //SQX-783 find the Impacted Product that matches the selected Part + Lot combination for the given NC-Finding and 
        //set the lookup field (SQX_Impacted_Product__c) to that item
        //additionally, set an error on the object if no such Impacted Product (Part/Lot) is found.
        if(trigger.isUpdate || trigger.isInsert){
            new SQX_Disposition.Bulkified(Trigger.new, Trigger.oldMap)
                                .setTheImpactedProductToCombinationOfPartLot();
        }
        else if(trigger.isDelete){
            new SQX_Disposition.Bulkified(Trigger.old, Trigger.newMap)
                                .preventDeletionOfCompletedDisposition();
        }
    }
}