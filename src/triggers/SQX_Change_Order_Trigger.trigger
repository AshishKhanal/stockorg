/**
* This trigger handler performs various actions on Change Order based on the workflow. The list summarizes the flows
* i.   Before Update: Transfers the ownership of a submitted Change Order based on the queue name [transferTheOwnershipOfTheRecordToQueueOnTriage]
* ii.  after insert/update: copy/populate change order approvals for a change order with referenced approval matrix [copyApprovalMatrixStepsWhenOpen]
* iii. before update: validate change order approvals for empty users/approvers and steps exceeding 5 approvers while submitted for approval [checkAnyEmptyApproverOrStepExceedingMaximumApproversWhenSubmittingForApproval]
* iv.  before update: sets last approval status value when submitted approval request is recalled [setApprovalStatusToLastValueWhenRecalled]
* v.   before update: copies/sets all 5 change order approver fields from change order approvals for current approval step when in approval [setApproversForCurrentApprovalStep]
* vi.  before update: checks if change is complete if true sets the status to complete [checkIfChangeIsComplete]
* vii. after update: locks the implemenetation to prevent changes when CO is send for approval [lockImplementationsWhenInApproval]
* viii. after update: changes the implemenation from plan to action when the CO is approved [approveImplementationsWhenApproved]
* ix.  after update : unlocks the implementation to allow changes when the CO is recalled [unlockImplementationsWhenRejectingOrRecalling]
* x.   Before update: when in pre-release release immediately if effective date is today [checkForImmediateRelease]
* xi.  After update:  notify CQ aware module to take necessary action on changes to CO [completeRelatedCQAwareTasks]
* x.   After update: synchronize sharing rules when change orders owner changes 
*      i.e. add sharing rules for new owner in CO's implementation and remove for old owner [syncSharingRulesOnOwnerChange]
* xi. After Update : sharing implementation with  wner when CO owner is different than change order owner [changeImplementationOwnerAsChangeOwner]
* xii. After Update : adding Changing Owner to the change order record activity log [logChangingOwnerInRecordActivity]
* xiii. before update [checkWhenSubmittedForImplementationApproval]: when co is submitted for implementation approval, checks whether all actions / plans are complete or not
* xiv.  after update [approveInChangeApprovalDocuments]: approve controlled documents with approval status "In Change Approval" when CO implementation approved
* Xv .  before update [setRecordSubmittedFieldsAndUpdateStateAndStage] when user submit the change order record than check activity code of submit and update status ,state and submited by and submitted date 
* Xvi . brefore update [setRecordTakeOwnershipAndInitiate] when user take ownership and initiate than status and stage is  updated
* Xvii . before update [checkIfActionPlanSelectedThanReplanRecord] check action paln is selected or not if selected than status,stage and approval status is updated 
* XViii. Before update : set record status and stage as per activity code [setStatusAndStageBasedOnActivityCode]
* xviv.  after update [recordActivity]: inserts record activities based on the type of activity(activity code) being performed on the records
* xvv.   before  update [preventDeletionOnClosureAndVoid]:prevent deletion when change order record is void or closed
* xvvi.  before update [lockRecordAfterChangeOrderIsVoidOrClosed]:lock the change order when record is void or closed
* xvvii. before update [unlockRecordAfterChangeOrderIsReopen]:unlock the change order when record is reopen
* xvviii. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Change_Order_Trigger on SQX_Change_Order__c (before update, after insert, after update,before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            new SQX_Change_Order.Bulkified(trigger.new, trigger.oldMap).setStatusAndStageBasedOnActivityCode()
                                                                       .setRecordSubmittedFieldsAndUpdateStateAndStage()
                                                                       .transferTheOwnershipOfTheRecordToQueueOnTriage()
                                                                       .setRecordTakeOwnershipAndInitiate()
                                                                       .checkAnyEmptyApproverOrStepExceedingMaximumApproversWhenSubmittingForApproval()
                                                                       .setApprovalStatusToLastValueWhenRecalled()
                                                                       .setApproversForCurrentApprovalStep()
                                                                       .checkIfChangeIsComplete()
                                                                       .checkForImmediateRelease()
                                                                       .checkIfActionPlanSelectedThanReplanRecord()
                                                                       .lockRecordAfterChangeOrderIsVoidOrClosed()
                                                                       .unlockRecordAfterChangeOrderIsReopen()
                                                                       .checkWhenSubmittedForImplementationApproval();
        }
         if(trigger.isDelete){
             new SQX_Change_Order.Bulkified(trigger.old, trigger.newMap).preventDeletionOnClosureAndVoid();
        }
    }
    else {
        if (trigger.isInsert) {
            new SQX_Change_Order.Bulkified(trigger.new, trigger.oldMap).copyApprovalMatrixStepsWhenOpen();
        }

        else if(Trigger.isUpdate){
            SQX_Change_Order.Bulkified bulkified= new SQX_Change_Order.Bulkified(Trigger.new, Trigger.oldMap);
                                                               bulkified.copyApprovalMatrixStepsWhenOpen()
                                                                        .lockImplementationsWhenInApproval()
                                                                        .changeImplementationOwnerAsChangeOwner()
                                                                        .approveImplementationsWhenApproved()
                                                                        .unlockImplementationsWhenRejectingOrRecalling()
                                                                        .syncSharingRulesOnOwnerChange()
                                                                        .logChangingOwnerInRecordActivity()
                                                                        .approveInChangeApprovalDocuments()
                                                                        .recordActivity()
                                                                        .completeRelatedCQAwareTasks();
            bulkified.clearActivityCode()
                     
                     .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}