/**
* This trigger handler performs various actions on Implementation based on the workflow. The list summarizes the flows
* i.    Before Update/Insert: Creates the SF task when the action is created [createSFTasks]
* ii.   After Update: Update the SF task when the action is updated [updateSFTasks]
* iii.  After Update: Update the SF task when the action due date or assignee is changed [syncSFTasks]
* iv.   After Insert/Update: share the record with owner when the assignee is different than main record owner [shareRecordWithOwner]
* v.    Before Insert/Update: update the SF task when the task assignee or due date is changed [syncActionOwner]
* vi.   After Update: completes the change order when all the actions are complete [completeChangeOrderWhenAllActionsComplete]
* vii.  After Update: set change order on controlled doc that are being referred by implementation [setChangeOrderOnDocs]
* viii. before delete: prevent deletion when parent record is locked [preventDeletionOnClosureAndVoid]
* viv. After Insert/Update: Ensure change record have edit access or not [ensureChangeRecordHaveEditAccess]
* vv.   After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
* @date 2016-02-16
*/
trigger SQX_Implementation_Trigger on SQX_Implementation__c (before insert, before update,
after insert, after update,before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
        if(trigger.isUpdate){
            new SQX_Implementation.Bulkified(trigger.new, trigger.oldMap)
                                         .updateSFTasks()
                                         .syncSFTasks()
                                         .shareRecordWithOwner()
                                         .completeChangeOrderWhenAllActionsComplete()
                                         .setChangeOrderOnDocs()
                                         
                                         .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
        else if(trigger.isInsert){
            new SQX_Implementation.Bulkified(trigger.new, trigger.oldMap)
                                         .ensureChangeRecordHaveEditAccess()
                                         .shareRecordWithOwner();
            
        }
    }
    else{
        if(trigger.isUpdate || trigger.isInsert){
            new SQX_Implementation.Bulkified(trigger.new, trigger.oldMap)
                                         .syncActionOwner()
                                         .createSFTasks();
        }

        if(trigger.isDelete){
             new SQX_Implementation.Bulkified(trigger.old, trigger.newMap).preventDeletionOnClosureAndVoid();
        }
        
    }
}