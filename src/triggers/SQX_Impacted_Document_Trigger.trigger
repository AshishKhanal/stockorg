/**
 * This trigger executes following actions:
 * 1. after insert/ after update: creates new task when assignee is changed and completes old assignee task [updateTaskForImpactedDocument]
 * 2. before insert/ before update: completes task when impacted document is completed [completeTaskForImpactedDocument]
 * 3. before insert/ before update: completes impacted document if the selected controlled document is already in current status [completeImpactedDocument]
 * 4. after delete / after update / after insert: updates CAPA to check if CAPA can be completed if all the impacted documents are completed [updateCAPA]
 * 5. before delete : delete task if the related impacted document is deleted [deleteRelatedTasks]
 * 6. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */

trigger SQX_Impacted_Document_Trigger on SQX_Impacted_Document__c (before insert, after insert, before update, after update, before delete, after delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isBefore){
        if(Trigger.isInsert || Trigger.isUpdate){
            SQX_Impacted_Document.Bulkified processor = new SQX_Impacted_Document.Bulkified(trigger.new, trigger.oldMap);
            processor.completeTaskForImpactedDocument()
                .completeImpactedDocument();
        } else if (Trigger.isDelete){
            new SQX_Impacted_Document.Bulkified(trigger.old, trigger.newMap).deleteRelatedTasks(); 
        } 
        
    } else {
        if (Trigger.isDelete){
            new SQX_Impacted_Document.Bulkified(trigger.old, trigger.newMap).updateCAPA(); 
        } 
        else if(Trigger.isInsert || Trigger.isUpdate){
            new SQX_Impacted_Document.Bulkified(trigger.new, trigger.oldMap).updateTaskForImpactedDocument()
                .updateCAPA(); 
            
            if (Trigger.isUpdate) {
                new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
            }
        }
    } 
}