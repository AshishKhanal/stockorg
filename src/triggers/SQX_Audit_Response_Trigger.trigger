/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Audit_Response_Trigger on SQX_Audit_Response__c (before insert, before update, before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
        system.debug('#SQX_Audit_Response__c# : Running finding response after trigger');
        if(trigger.isUpdate){
            new SQX_Audit_Response.Bulkified(Trigger.new, Trigger.oldMap)
                    .onPublishingPercolateToRelated()
                    
                    .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
    else if(trigger.isBefore){
        if(trigger.isInsert || trigger.isUpdate){
            new SQX_Audit_Response.Bulkified(Trigger.new, Trigger.oldMap).assignApproverToAuditResponse().onSubmissionAddSubmittedInformation().onPublishedAddPublishedDate();
        }
        else if(trigger.isDelete){
            new SQX_Audit_Response.Bulkified(Trigger.old, Trigger.newMap).preventPublishedResponsesFromDeletion();
        }
    }
}