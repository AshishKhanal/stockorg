/**
*   @author : Piyush Subedi
*   @date : 2016/11/7
*   @description : The trigger performs the following workflows
*                   a. Before insert : adds a post in the group regarding the addition of member [insertMemberAddedFeed]
*                   b. Before delete : adds a post in the group regarding the removal of member [insertMemberRemovedFeed]
*/
trigger SQX_Collaboration_Group_Member_Trigger on CollaborationGroupMember (before insert, before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isBefore){
        if(Trigger.isInsert)
            new SQX_Collaboration_Group_Member.Bulkified(Trigger.new, Trigger.oldMap).insertMemberAddedFeed();
        else if(Trigger.isDelete)
            new SQX_Collaboration_Group_Member.Bulkified(Trigger.old, Trigger.newMap).insertMemberRemovedFeed();
    }
}