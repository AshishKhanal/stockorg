/*
 * trigger for updating records in Audit_Criteria
 * author: Saroj Dangol
 * date: 2019/03/29
 * story: SQX-1093
 **/
trigger SQX_Audit_Criteria_Trigger on SQX_Audit_Criteria__c (before insert) {
   // removed logic for obsolete object 
}