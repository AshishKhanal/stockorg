/**
* trigger performs following tasks
* a. after insert: adds phr answers to the newly created phr. [addProductHistoryReviewAnswer]
* b. before update: updates phr completion date upon phr completion [setCompletionDateOnPHRCompletion]
* c. after update : completed related SF tasks
* d. before delete : check if related complaint is locked or not before deleting [preventDeletionOfPHROfLockedRecord]
* e. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
* @author Anish Shretha 
* @date 2016-01-05
* @story [SQX-915]
*/
trigger SQX_Product_History_Review_Trigger on SQX_Product_History_Review__c(after insert, before update, after update, before insert, before delete){
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
        if(trigger.isInsert){
            new SQX_Product_History_Review.Bulkified(Trigger.new, Trigger.oldMap).addTaskQuestionToProductHistoryAnswer();
        }
        else if(trigger.isUpdate){
            new SQX_Product_History_Review.Bulkified(Trigger.new, Trigger.oldMap)
                    .completeComplaintTask()
                    
                    .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }

    }
    else{
        if(trigger.isUpdate){
            new SQX_Product_History_Review.Bulkified(Trigger.new, Trigger.oldMap).setCompletionDateOnPHRCompletion();
        } else if(trigger.isDelete) {
            new SQX_Product_History_Review.Bulkified(Trigger.old, Trigger.newMap).preventDeletionOfPHROfLockedRecord();
        }
    }
}