/**
* This trigger performs various actions required by SQX_Calibration
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Calibration_Trigger on SQX_Calibration__c (before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        if(trigger.isDelete){
            new SQX_Calibration.Bulkified(trigger.old, trigger.newMap)
                         .preventDeletionOnClouserAndVoid(); //prevent Clouser or Voided Calibration record from deletion
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}