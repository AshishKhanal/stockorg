/**
* Trigger for Medwatch object
*/
trigger SQX_Medwatch_Trigger on SQX_Medwatch__c (before update, after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;


    SQX_Medwatch_eSubmitter.SQX_Medwatch_Trigger_Handler handler = new SQX_Medwatch_eSubmitter.SQX_Medwatch_Trigger_Handler(Trigger.new, Trigger.oldMap);

    handler.execute();

    if(Trigger.isAfter && Trigger.isUpdate) {
        handler.addModifiedLongTextFieldHistory(); // execute at last; captures modified long text field history
    }
}