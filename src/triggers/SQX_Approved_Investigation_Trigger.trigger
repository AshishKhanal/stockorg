/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Approved_Investigation_Trigger on SQX_Approved_Investigation__c (before insert, before update,after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        if(trigger.isUpdate || trigger.isInsert){
            new SQX_BulkifiedBase(trigger.New, trigger.oldMap).ensureCombinationIsUnique(
                /* unique combination of finding + investigation */
                new Schema.SObjectField[]{Schema.SQX_Approved_Investigation__c.SQX_Finding__c, 
                Schema.SQX_Approved_Investigation__c.SQX_Investigation__c},
                
                /* copy to field */
                Schema.SQX_Approved_Investigation__c.Uniqueness_Constraint__c
            );
        }
    }
    else if(trigger.isAfter){
        if (Trigger.isInsert) {
            //create approved rootcauses
            new SQX_Approved_Investigation.Bulkified(trigger.New, trigger.oldMap)
                .copyApprovedInvestigationRootCauses();
        }
        
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}