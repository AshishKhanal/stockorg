/**
* Trigger to check invalid Queue Id in a record under Department.
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Department_Trigger on SQX_Department__c (before insert, before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
     if(Trigger.isBefore){
          if(trigger.isInsert || trigger.isUpdate){
            new SQX_Department.Bulkified(trigger.new, trigger.oldMap)
                .ensureQueueIdIsValid();
        }
      }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}