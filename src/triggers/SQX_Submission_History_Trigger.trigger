/**
 * The following workflows are done
 * a. Before Delete: On deletion of submission history check whether parent record is locked and if locked then stop deletion. [preventDeletionOfLockedRecords]
 * b. Before insert/update : Update the Status based on the Acknolwedgement status [updateSubmissionStatusWhenAcknowledgementIsReceived]
 * c. After insert/udpate : Complete the related Regulatory Report once it's submission record is complete [completeReportOnSubmissionCompletion]
 * d. After insert/update : Remove the link to Submission History from Regulatory Report after Submission fails [unlinkFailedSubmissionFromReport]
 * e. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Submission_History_Trigger on compliancequest__SQX_Submission_History__c (before delete, before insert, before update, after insert, after update) {

    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    if (Trigger.isBefore) {
        if (Trigger.isDelete) {
            new SQX_Submission_History.Bulkified(Trigger.old, null).preventDeletionOfLockedRecords();
        } else {
            new SQX_Submission_History.Bulkified(Trigger.new, Trigger.oldMap).updateSubmissionStatusWhenAcknowledgementIsReceived();
        }
    } else {
        SQX_Submission_History.Bulkified bulkified = new SQX_Submission_History.Bulkified(Trigger.new, Trigger.oldMap).completeReportOnSubmissionCompletion();
        
        if (Trigger.isUpdate) {
            bulkified.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }

}