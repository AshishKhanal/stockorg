/*
 * This trigger performs following actions:
 * a. before insert/update : prevents duplication of Training Session Roster [ensureCombinationIsUnique]
 * b. before delete : roster cannot be manipulated after training is complete or void [rosterCannotbeManipulatedAfterTrainingIsCompleteOrVoid]
 * c. after delete : deletes document training reference to training session when roster is deleted [deleteTrainingRelationWhenDeletingRoster]
 * d. before insert/update : validates personnel before adding to Training session roster [validatePersonnelWithIncompletePreRequisiteTrainings]
 * e. after update : generate document training or complete document training for rosters when activity code changes [processDocumentTraining]
 * f. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Training_Session_Roster_Trigger on SQX_Training_Session_Roster__c (before insert, before update, before delete, after delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isBefore){
        if(Trigger.isUpdate || Trigger.isInsert){
            new SQX_Training_Session_Roster.Bulkified(trigger.new, trigger.oldMap)
                .validatePersonnelWithIncompletePreRequisiteTrainings()
                .ensureCombinationIsUnique(
                    new Schema.SObjectField[]{Schema.SQX_Training_Session_Roster__c.SQX_Training_Session__c, 
                        Schema.SQX_Training_Session_Roster__c.SQX_Personnel__c},
                    Schema.SQX_Training_Session_Roster__c.Uniqueness_Constraint__c);
        }
        else if(Trigger.isDelete){
            new SQX_Training_Session_Roster.Bulkified(trigger.old, trigger.newMap)
                .rosterCannotbeManipulatedAfterTrainingIsCompleteOrVoid();
        }
    }
    else{
        if(Trigger.isDelete){
            new SQX_Training_Session_Roster.Bulkified(trigger.old, trigger.newMap)
                .deleteTrainingRelationWhenDeletingRoster();
        }
        else if (Trigger.isUpdate) {
            new SQX_Training_Session_Roster.Bulkified(trigger.new, trigger.oldMap)
                .processDocumentTraining()
                
                .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}