/*
 * This trigger performs following actions:
 * a. before insert / update : calculates the result for the inspection based on USL and LSL and compare it with observed value and set inspection detail result [calculateTolerance]
 * b. before insert / update / delete : prevent the modification of inspection detail after inspection is completed [preventModificationOfInspectionDetailAfterInspectionCompletion]
 * c. before insert / update : calculates equipment calibration based on the equipment selection on the inspection detail [checkEquipmentCalibration]
 * d. before delete : prevent deletion of inspection detail copied from inspection criteria  [preventDeletionOfCopiedInspectionDetail]
 * e. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Inspection_Detail_Trigger on SQX_Inspection_Detail__c (before update, before insert, before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isBefore){
        if(Trigger.isInsert || Trigger.isUpdate){
            new SQX_Inspection_Detail.Bulkified(trigger.new, trigger.oldMap)
                                    .preventModificationOfInspectionDetailAfterInspectionCompletion()
                                    .calculateTolerance()
                                    .checkEquipmentCalibration();
        }
        else if(Trigger.isDelete){
            new SQX_Inspection_Detail.Bulkified(trigger.old, trigger.newMap)
                                     .preventModificationOfInspectionDetailAfterInspectionCompletion()
                                     .preventDeletionOfCopiedInspectionDetail();
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}