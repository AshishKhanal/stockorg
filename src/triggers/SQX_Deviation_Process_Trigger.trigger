/**
 * The trigger performs the actions that are necessary for 'Step' type modules
 * a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Deviation_Process_Trigger on SQX_Deviation_Process__c (before delete, after update, before update, before insert, after insert) {
    
    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    SQX_Steps_Trigger_Handler wrapperProcess;

    if(Trigger.isDelete) {
        wrapperProcess = new SQX_Steps_Trigger_Handler(Trigger.old, Trigger.newMap);
    } else {
        wrapperProcess = new SQX_Steps_Trigger_Handler(Trigger.new, Trigger.oldMap);
    }

    wrapperProcess.execute();

    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            wrapperProcess.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}