/*
* Trigger methods description
* a. after insert/update [linkFindingWithCapaWhenResolvesByCapaIsChecked] : links Finding with Capa when ResolvesByCAPA is checked
* b. after update [unlinkFindingWithCapaWhenSolvedByCapaIsUnchecked] : unlinks Finding with Capa when Solved By Capa is Unchecked
* c. after delete [clearFindingCAPAFieldWhenReferenceIsDeleted] : clear Finding CAPA field When Reference Is Deleted
* d. before delete [preventFindingCAPADeletionIfFindingIsInCAPALinkApprovalStage] : prevent FindingCAPA Reference Deletion if finding is in 
* e. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Finding_Capa_Trigger on SQX_Finding_CAPA__c (after insert, after update, after delete, before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
        if(trigger.isInsert){
            new SQX_Finding_Capa.Bulkified(trigger.new, trigger.oldMap)
                                .linkFindingWithCapaWhenResolvesByCapaIsChecked();
        }
        else if(trigger.isUpdate){  
         
            new SQX_Finding_Capa.Bulkified(trigger.new, trigger.oldMap)
                                .linkFindingWithCapaWhenResolvesByCapaIsChecked()
                                .unlinkFindingWithCapaWhenResolvesByCapaIsUnchecked()
                                
                                .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
        else if(trigger.isDelete){
            new SQX_Finding_Capa.Bulkified(trigger.old, trigger.newMap)
                                .clearFindingCAPAFieldWhenReferenceIsDeleted();
        }
    }
    else{
        if(trigger.isDelete){
            new SQX_Finding_Capa.Bulkified(trigger.old, trigger.newMap)
                                .preventFindingCAPADeletionIfFindingIsInCAPALinkApprovalStage();
        }
    }
}