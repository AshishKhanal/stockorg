/**
* a. before update : Prevent content associated with SCORM assessment to be archived [preventAssessmentFromGettingArchived]
* b. before update: Prevent controlled content from getting archived directly [preventControlledDocFromGettingArchivedDirectly]
* c. before delete : Prevent content associated with SCORM assessment to be deleted [preventControlledDocFromGettingDeleted]
*                    Prevent content related to controlled document from getting deleted [preventControlledDocFromGettingDeleted]
*                    Prevent collaboration content from being deleted when collaboration is still active [preventControlledDocFromGettingDeleted]
*                    Prevent content associated with nsi task to be deleted [preventControlledDocFromGettingDeleted]
*/
trigger SQX_ContentDocument_Trigger on ContentDocument (before delete, before update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    if(trigger.isBefore){
        
        if(trigger.isDelete){
            new SQX_ContentDocument.Bulkified(trigger.old, trigger.newMap).preventCQDocumentGettingDeleted();
        }
        else if(trigger.isUpdate){
            new SQX_ContentDocument.Bulkified(trigger.new, trigger.oldMap).preventControlledDocFromGettingArchivedDirectly()
                                                                            .preventAssessmentFromGettingArchived();
        }
    }
}