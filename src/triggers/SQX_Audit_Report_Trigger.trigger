/**
* The trigger performs the following workflows
* a. After Update : updates Audit Stage to 'Respond/Ready For Closure' when Audit Report is Complete [updateAuditAfterAuditReportIsApproved]
* b. After Update : updates Audit Stage to 'Finding Approval' when Audit Report is sent for Approval[updateAuditStageWhenAuditReportIsSentForApproval]
* c. After Update : revert Audit Stage to 'In Progress' when Audit Report is rejected [updateAuditwhenAuditReportIsRejected]
* d. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/

trigger SQX_Audit_Report_Trigger on SQX_Audit_Report__c (after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
        if(trigger.isUpdate){
            new SQX_Audit_Report.Bulkified(trigger.new, trigger.oldMap)
                        .updateAuditAfterAuditReportIsApproved()
                        .updateAuditStageWhenAuditReportIsSentForApproval()
                        .updateAuditwhenAuditReportIsRejected()
                        
                        .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}