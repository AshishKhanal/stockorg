/**
* Trigger methods:
* a. After Update [syncPersonnelRecords]: syncs related personnel record of an updated user record
* Note: we are not performing long text field history for user object
*/
trigger SQX_User_Trigger on User (after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (trigger.isAfter) {
        if (trigger.isUpdate) {
            new SQX_User.Bulkified(trigger.new, trigger.oldMap)
                .syncPersonnelRecords(); // syncs related personnel record of an updated user record
        }
    }
}