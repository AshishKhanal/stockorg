/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
/**
* SQX-65
* 2. Given that: A Finding is not Closed 
* When: There are existing Impacted Parts in the Finding and I look up to add a Part that is existing in the Finding Impacted Products list 
* Then: The system throws an error stating that the Product already is present in the Impacted Products list
* preventImpactedPartFromBeingDuplicated 
*/
trigger SQX_Impacted_Product_Trigger on SQX_Impacted_Product__c (before insert, before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        if(trigger.isUpdate || trigger.isInsert){
            new SQX_Impacted_Product.Bulkified(trigger.New, trigger.oldMap)
                .ensureCombinationIsUnique(
                new Schema.SObjectField[]{Schema.SQX_Impacted_Product__c.SQX_Finding__c, 
                                            Schema.SQX_Impacted_Product__c.SQX_Impacted_Part__c, 
                                            Schema.SQX_Impacted_Product__c.Lot_Number__c, 
                                            Schema.SQX_Impacted_Product__c.Sublot_Number__c}, //added sublot number as mentioned in SQX-1640
                Schema.SQX_Impacted_Product__c.Uniqueness_Constraint__c
            );
            //2014/10/20 modified to ensure that the combination of Finding, Part and Lot are unique.
            
            //WARNING:
            //This finding + part + lot combination is being used in disposition to find the impacted product quickly
            //using SOQL query. Please ensure that changes over here are reflected there too. BTW unit tests will fail
            //if you dont :-D

        }
    }
    else{
    	if(trigger.isUpdate){
    		new SQX_Impacted_Product.Bulkified(trigger.New, trigger.oldMap)
                                    .updateDispositionsIfImpactedProductIsUpdated()
                                    
                                    .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
    	}
    }
    
    
}