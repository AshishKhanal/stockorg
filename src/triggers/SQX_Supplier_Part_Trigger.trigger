/**
* single trigger for supplier part dispatching calls to respective classes
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Supplier_Part_Trigger on SQX_Supplier_Part__c (before insert, before update, before delete, after insert,
after update, after delete, after undelete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate) ){
    
        
        new SQX_Supplier_Part.Bulkified(trigger.New, trigger.oldMap).ensureCombinationIsUnique(
            new Schema.SObjectField[]{Schema.SQX_Supplier_Part__c.Account__c, Schema.SQX_Supplier_Part__c.Part__c,Schema.SQX_Supplier_Part__c.Part_Text__c}, //the combination of fields
            Schema.SQX_Supplier_Part__c.Uniqueness_Constraint__c //the field that is unique
        );
    }
    if(trigger.isAfter){
        if(trigger.isUpdate){
            new SQX_Supplier_Part.Bulkified(Trigger.new, trigger.oldMap)
                                 .AfterUpdateSupplierPartActiveFieldUpdateReasonForChangeOnAccount()
                                 
                                 .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }

}