/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Resp_Inclusion_Approval_Trigger on SQX_Resp_Inclusion_Approval__c (before insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore && trigger.isInsert){
        new SQX_Resp_Inclusion_Approval.Bulkified(Trigger.new, Trigger.oldMap).setApproverInformation();
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}