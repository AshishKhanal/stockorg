/**
* @author Pradhanta Bhandari
* @date 2014/2/20
* @description implementation action trigger handler, that dispatches to respective code
* a.before update[assignAuditeeAndCoordinatorAsActionOwner]: Assigns auditee as the owner of the implementation while creating implementation if assignee field is left empty in Audit
*  and assigns coordinator as the owner of the implementation while creating implementation if assignee field is left empty in Capa
* b.after insert/update[shareAuditActionWithAuditTeamMembers]: Assigns edit access of the implemenations to all audit team members while creating or updating implemenation.
* c. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Action_Trigger on SQX_Action__c (before insert, before update, before delete,
after insert, after update, after delete, after undelete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
        if(trigger.isUpdate || trigger.isInsert){
            new SQX_Implementation_Action.Bulkified(trigger.new, trigger.oldMap)
                .shareRecordWithMainRecordOwner() // shares action to the main record (Audit or CAPA) owner
                .shareAuditActionWithAuditTeamMembers(); //shares action with the team members of the audit
        }
        
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
    else if(trigger.isBefore){
        if(trigger.isDelete){
            new SQX_BulkifiedBase(Trigger.old, null)
                                .preventChangesIfLocked(SQX_Action__c.Delete_Locked__c, null
                                            , Label.SQX_ERR_MSG_ONCE_LOCKED_NO_DELETE, true);
            
        }
        else if(trigger.isUpdate || trigger.isInsert){
            new SQX_Implementation_Action.Bulkified(trigger.new, trigger.oldMap)
                .assignAuditeeAndCoordinatorAsActionOwner()
                .syncRecordOwnerWithAssignee(); // sets assignee as owner of the action if assignee set is diferrent from record owner
        }   
    }
}