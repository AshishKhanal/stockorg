/**
* This trigger performs various actions required by SQX_Audit_Trigger
* a. before insert/update [setAndSyncAuditLeadAuditor]:
*       - sets default value for lead auditor for new audit plan with owner
*       - sets lead auditor as owner when audit plan is approved or initiated
*       - syncs owner value with lead auditor when owner is changed and audits not in plan
* b. after update - auto closes audit if Auto Close is checked  [autoCloseAuditifAutoCloseIsChecked]
*                 - create followup audit when audit is closed [createFollowupAuditWhenClosed]
*                 - complete related sf task when audit(created through nsi) is closed or voided or complete [completeSFTaskOnAuditCloseOrVoidOrComplete]
*                 - add Owner to Team if Owner is changed or Audit has passed planing state[updateTeamMemberOnOwnerChangeAndAuditPlanned]
* c. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Audit_Trigger on SQX_Audit__c (before update, after insert,after update, before delete, before insert) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
        if(trigger.isInsert){
            new SQX_Audit.Bulkified(trigger.new, trigger.oldMap)
                         .changeLastAuditDateOnAccountIfHigher() //last audit date account are updated if the account's audit date is new.
                         .linkEventToAudit() // link the new created audit to its event
                         .addOwnerToTeamMember()
                         .initiateFindingOnceAuditIsComplete() //initiate the finding once audit reaches complete status 
                         .addAuditChecklistToAuditWhenScheduled();
        }
        else if(trigger.isUpdate){
            new SQX_Audit.Bulkified(trigger.new, trigger.oldMap)
                         .changeLastAuditDateOnAccountIfHigher() //last audit date account are updated if the account's audit date is new.
                         .linkEventToAudit()
                         .updateTeamMemberOnOwnerChangeAndAuditPlanned()
                         .initiateFindingOnceAuditIsComplete() //initiate the finding once audit reaches complete status 
                         .addAuditChecklistToAuditWhenScheduled()
                         .updateAuditEventCalendar() // updating the event of triggered audit
                         .removeAuditEventCalendar()
                         .updateRelatedRecordOwner()
                         .autoCloseAuditifAutoCloseIsChecked()// auto closes audit if Auto Close is checked
                         .createFollowupAuditWhenClosed()
                         .completeSFTaskOnAuditCloseOrVoidOrComplete()
                         
                         .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
    else{
        if(trigger.isUpdate){
            new SQX_Audit.Bulkified(trigger.new, trigger.oldMap)
                        .addAuditEventinCalendar()
                        .setAndSyncAuditLeadAuditor()
                        .closeAllFindingOnAuditClosure()
                        .preventAuditCloseWithOpenActions();
        }
        else if(trigger.isDelete){
            new SQX_Audit.Bulkified(trigger.old, trigger.newMap)
                         .preventAuditWithAuditProgramInApprovalFromBeingDeleted();
        }
        else if(trigger.isInsert) {
            new SQX_Audit.Bulkified(trigger.new, trigger.oldMap)
                         .setAndSyncAuditLeadAuditor()
                         .addAuditEventinCalendar(); // adding event for created audit
        }
    }

}