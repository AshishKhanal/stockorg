/**
* This trigger helps route actions upon DML requests on Non-Conformance object.
* a. Before Insert/Update: Update fields in NC based on criteria example setting default approvers, setting completion date [performBeforeUpdates]
* b. Before Update: Complete an NC if all policy is met [completeNCOnAllPolicy]
* c. Before Update: If NC isn't ready prevent it from closure [preventNCFromClosure]
* d. After Insert/Update : If NC is voided then recalled all pending responses [recallResponsesWhenNCIsCompletedThroughNcVoid]
* e. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_NC_Trigger on SQX_Nonconformance__c (before update, before insert, after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        if(trigger.isUpdate){
            new SQX_NC.Bulkified(Trigger.new, Trigger.oldMap).performBeforeUpdates()
                                                             .completeNCOnAllPolicy()
                                                             .preventNCFromClosure();
        }
        else if(trigger.isInsert){
            new SQX_NC.Bulkified(Trigger.new, Trigger.oldMap).performBeforeUpdates();
        }
        
    } else if(trigger.isAfter) {
        SQX_NC.Bulkified bulkified = new SQX_NC.Bulkified(Trigger.new, Trigger.oldMap).recallResponsesWhenNCIsCompletedThroughNcVoid();
        
        if (Trigger.isUpdate) {
            bulkified.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}