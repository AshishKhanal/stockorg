/**
* this trigger performs the following activities on Controlled Document Approval object:
* a. Before Insert/Update: It copies the step number as text to use in approval matrix approval logic [setStepAsTextValue]
* b. Before Insert/Update: It checks whether the user is valid for the selected job function [checkIfUserHasValidUserFromJobFunction]
* c. Before Insert/Update/Delete: It checks whether doc approval can be created/updated/deleted [preventApprovalCUDOnceItIsSentForApprovalOrApproved]
* d. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Controlled_Document_Approval_Trigger on SQX_Controlled_Document_Approval__c (before insert, before update, before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (trigger.isBefore) {
        if (trigger.isInsert || trigger.isUpdate) {
            new SQX_Controlled_Document_Approval.Bulkified(trigger.new, trigger.oldMap).setStepAsTextValue()
            																	.checkIfUserHasValidUserFromJobFunction()
            																	.preventApprovalCUDOnceItIsSentForApprovalOrApproved();
        } else{
            new SQX_Controlled_Document_Approval.Bulkified(trigger.old, trigger.newMap).preventApprovalCUDOnceItIsSentForApprovalOrApproved();
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}