/**
* Trigger to perform various workflow based on audit program story
* @author Pradhanta Bhandari
* 
* a. before insert/update [setUniqueName]: sets Unique Name field with Name.
* b. after update [setRelatedAuditsToPlanAndProgramApproval]: sets related audit's Stage to Program Approval when Program is submitted for approval and Stage to Plan if rejected or recalled.
* c. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Audit_Program_Trigger on SQX_Audit_Program__c (before insert, before update, after update, before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isBefore){

        if(trigger.isInsert){
            new SQX_Audit_Program.Bulkified(trigger.new, trigger.oldMap)
                .ensureCalendarIdIsValid()
                .setUniqueName();
        }
        else if(trigger.isUpdate){
            //based on SQX-1136
            //ensures that only audit program with no 'In Process' Audit can't be voided
            //ensures that only audit program with 'Complete' Audit can be closed
            
            new SQX_Audit_Program.Bulkified(trigger.new, trigger.oldMap)
                .ensureOnlyAuditsWithoutInProcessCanBeVoided()
                .ensureOnlyAuditProgramsWithCompleteAuditCanBeClosed()
                .ensureCalendarIdIsValid()
                .setUniqueName();
        }
        else{
            new SQX_Audit_Program.Bulkified(trigger.old, trigger.newMap)
                .preventChangesIfLocked(Schema.SQX_Audit_Program__c.fields.Is_Locked__c, null,
                Label.SQX_ERR_MSG_ONCE_LOCKED_NO_DELETE, true);
        }
    }
    else{
        //based on SQX-1136
        //after updating the record, modify the planned audits included in the audit program to 'Schedule'
        //void all audits included in the audit program if the program is voided.
        new SQX_Audit_Program.Bulkified(trigger.new, trigger.oldMap)
            .scheduleAllPlannedAuditsWhenApproved()
            .voidAllPlannedAuditsForTheAuditProgram()
            .setRelatedAuditsToPlanAndProgramApproval()
            
            .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
    }
}