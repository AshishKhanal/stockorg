/*
* Trigger methods description
* a. before insert/update/delete : Prevents modification of training fulfilled requirement after training session is locked [trainingFulfilledRequirementCannotbeManipulatedAfterTrainingSessionIsLocked]
* b. before insert/update: Sets Uniqueness Constraint of Training Fulfilled Requirements [setUniquenessConstraintValue]
* c. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Training_Fulfilled_Requirement_Trigger on SQX_Training_Fulfilled_Requirement__c (before insert, before update, before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (trigger.isBefore) {
        if (trigger.isInsert || trigger.isUpdate) {
            new SQX_Training_Fulfilled_Requirement.Bulkified(trigger.New, trigger.oldMap)
                .trainingFulfilledRequirementCannotbeManipulatedAfterTrainingSessionIsLocked()
                .ensureCombinationIsUnique(
                // Unique Combination of Training Session and Controlled Document 
                new Schema.SObjectField[]{Schema.SQX_Training_Fulfilled_Requirement__c.SQX_Training_Session__c, 
                    Schema.SQX_Training_Fulfilled_Requirement__c.SQX_Controlled_Document__c},
                    Schema.SQX_Training_Fulfilled_Requirement__c.Uniqueness_Constraint__c
                );  
        }
        else if (trigger.isDelete){
            new SQX_Training_Fulfilled_Requirement.Bulkified(trigger.old, trigger.newMap)
                .trainingFulfilledRequirementCannotbeManipulatedAfterTrainingSessionIsLocked();
        }
    }  
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}

