/**
* Trigger methods description
* a. before insert [setAssessmentIdField] : update assesment id when activated
* b. before insert [generatePersonnelDocumentTrainings] : generates document trainings for activated job functions
* c. before update [createRefresherDocumentTrainings] : generates document trainings for activated refresher PDJFs
* d. before insert/update [updatePDJFValueswhenTrainingIsAddedOrUpdated] : updates PDJF Values when training is added or updated
* e. before insert/update [evaluateIsOverdueFlagOnEveryStatusChange] : Evaluate is overdue flag on Training Status change 
* f. before insert/update [updateRefresherQueueDateWhenNextRefreshDateChanges] : update Refresher queue date when Next refresher date is changed
* g. after update [evaluatePersonnelDocumentTrainings] : reevaluate document trainings for deactivated job functions
* h. after insert/update [updatePDJFwhenTrainingIsCompleted] : create new PDJF and archive PDJF when Document Training is completed
* i. after insert/update [generateNextStepTrainingsOnCompletion] : Generate pdts for next step when current step pdts are completed.
* j. after insert/update [generateTrainingCertificateOnCompletion] : Generate training certificate when pdt/pdjf is completed
* k. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Personnel_Document_Job_Function_Trigger on SQX_Personnel_Document_Job_Function__c (before insert, before update, after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (trigger.isBefore) {
        
        if (trigger.isInsert || trigger.isUpdate) {
            SQX_Personnel_Document_Job_Function.Bulkified bulkified = new SQX_Personnel_Document_Job_Function.Bulkified(trigger.new, trigger.oldMap);
            
            if (trigger.isInsert) {
                bulkified.setAssessmentIdField()
                         .generatePersonnelDocumentTrainings();
            }
            else if (trigger.isUpdate) {
                bulkified.createRefresherDocumentTrainings();
            }
            
            bulkified.updatePDJFValueswhenTrainingIsAddedOrUpdated()
                     .evaluateIsOverdueFlagOnEveryStatusChange()
                     .updateRefresherQueueDateWhenNextRefreshDateChanges();
        }
        
    }
    else {
        
        if (trigger.isInsert || trigger.isUpdate) {
            SQX_Personnel_Document_Job_Function.Bulkified bulkified = new SQX_Personnel_Document_Job_Function.Bulkified(trigger.new, trigger.oldMap);
            
            if (trigger.isUpdate) {
                bulkified.evaluatePersonnelDocumentTrainings();
            }
            
            bulkified.updatePDJFwhenTrainingIsCompleted()
                     .generateNextStepTrainingsOnCompletion()
                     .generateTrainingCertificateOnCompletion();
            
            if (Trigger.isUpdate) {
                bulkified.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
            }
        }
        
    }
}