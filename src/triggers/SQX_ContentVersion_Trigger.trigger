/**
* The trigger performs the following things
* a. After Insert: Prevents document upload if the controlled document isn't in draft. [preventDocUpload]
* b. After Insert/ Before Update: Ensure 1-1 relation between controlled document and its content 
*    i.e. a single content refers to controlled doc [preventMultipleContentForSingleDocument]
* c. Before Update: Prevents changes to the record type and controlled document reference once set. [preventChangesToControlledDoc]
* d. Before Update: For controlled content prevent the title or description from going out of sync [preventTitleorDescriptionUpdate]
* e. After Insert/Update: If the Content has been synced update related controlled document [synchronizeContent]
* f. After Insert : If content is inserted to SCORM Assessment, make sure the assessment is not locked, if locked thrown an error [preventContentUploadOnceAssessmenIsLocked]
* g. After Insert : Perform additional actions like 'checkInSecondaryContent, removeRenditionLog and submitForApproval' if listed in content version [performAdditionalActions]
* h. Before Insert [preventUpdateOfSubmissionHistoryAttachments]:new versions of submission history attachments cannot be uploaded once submission history record has been created. But new files can be added as attachments
*/
trigger SQX_ContentVersion_Trigger on ContentVersion (before insert, before update, after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
        if(trigger.isInsert){
            new SQX_ContentVersion.Bulkified(trigger.new, trigger.oldMap)
                                    .preventMultipleContentForSingleDocument()
                                    .preventDocUpload()
                                    .synchronizeContent()
                                    .postFeedWhenUploadNewVersion()
                                    .preventContentUploadOnceAssessmenIsLocked()
                                    .performAdditionalActions()
                .associateContentVersionWithSubmissionHistory();
        }
        else if (trigger.isUpdate){
            new SQX_ContentVersion.Bulkified(trigger.new, trigger.oldMap)
                                  .synchronizeContent();
        }
    }
    else if(trigger.isBefore){
        if(trigger.isUpdate){
            new SQX_ContentVersion.Bulkified(trigger.new, trigger.oldMap)
                                    .preventMultipleContentForSingleDocument()
                                    .preventChangesToControlledDoc()
                                    .preventTitleorDescriptionUpdate(); //do not allow title or description to be changed directly from content document
        }
         if(trigger.isInsert){
            new SQX_ContentVersion.Bulkified(trigger.new, trigger.oldMap).preventUpdateOfSubmissionHistoryAttachments(); 
        }
    }      
}