/**
* Trigger to perform various workflow based on assessment
* @author Paras Biswhakarma
* 
* a. before insert/update sets Unique Name field with Name [ensureCombinationIsUnique()]. 
* b. before update prevent the assessment from being obsolete [preventAssessmentFromBeingObsoleted()]
* c. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Assessment_Trigger on SQX_Assessment__c (before insert, before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    SQX_Assessment.Bulkified processor = new SQX_Assessment.Bulkified(trigger.new, trigger.oldMap);
    
    if (Trigger.isBefore) {
        processor.ensureCombinationIsUnique(new Schema.SObjectField[] {Schema.SQX_Assessment__c.Name}, Schema.SQX_Assessment__c.Uniqueness_Constraint__c);
        
        if (Trigger.isUpdate) {
            processor.preventAssessmentFromBeingObsoleted(); //prevent assessment from obsolete
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            processor.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}