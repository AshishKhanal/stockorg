/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
* b. Before Delete [preventPHRChecklistDeletion] to prevent product history checklist records
*/
trigger SQX_Product_History_Answer_Trigger on SQX_Product_History_Answer__c (after update, before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }else{
        if(trigger.isDelete) {
            new SQX_Product_History_Answer.Bulkified(Trigger.old, Trigger.newMap).preventPHRChecklistDeletion();
        }
    }
}