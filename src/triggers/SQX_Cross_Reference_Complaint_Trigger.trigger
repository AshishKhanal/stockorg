/**
 * The following workflows are done
 * a. Before Delete: On deletion of related complaint check whether parent record is locked and if locked then stop deletion. [preventDeletionOfLockedRecords] 
 * b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Cross_Reference_Complaint_Trigger on compliancequest__SQX_Cross_Reference_Complaint__c (before delete, after update) {
    
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (Trigger.isBefore) {
        if (Trigger.isDelete) {
            new SQX_Cross_Reference_Complaint.Bulkified(Trigger.old, null).preventDeletionOfLockedRecords();
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }

}