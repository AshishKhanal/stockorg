/**
* this trigger performs the following activities on Equipment Event Schedule object:
* a. Before Insert/Update: It sets the next due date for a recurring schedule based on some calculation, if it isn't already set [setCalculatedNextDueDateWhenNotSet]
* b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Equipment_Event_Schedule_Trigger on SQX_Equipment_Event_Schedule__c (before insert, before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            new SQX_Equipment_Event_Schedule.Bulkified(trigger.new, trigger.oldMap)
                .setCalculatedNextDueDateWhenNotSet(); // sets calculated next due date value for a recurring schedule with null next due date
        }
        
        if (trigger.isUpdate) {
            new SQX_Equipment_Event_Schedule.Bulkified(trigger.new, trigger.oldMap)
                .setCalculatedNextDueDateWhenNotSet() // sets calculated next due date value for an active recurring schedule
                .createOrUpdateTask(); // create task when queued and depends on next due date, so recommended to call this method at the end
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}