/**
* Trigger for Canada Report object
*/
trigger SQX_Canada_Report_Trigger on SQX_Canada_Report__c (before update, after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    SQX_Canada_Report_eSubmitter.SQX_Canada_Report_Trigger_Handler handler = new SQX_Canada_Report_eSubmitter.SQX_Canada_Report_Trigger_Handler(Trigger.new, Trigger.oldMap);

    handler.execute();

    if(Trigger.isAfter && Trigger.isUpdate) {
        handler.addModifiedLongTextFieldHistory(); // execute at last; captures modified long text field history
    }
}