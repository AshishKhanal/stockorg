/**
* Tasks performed:
* a. before insert: prevents requirement creation on document with no edit permission on the related document [preventCreationOnDocumentWithoutEditPermission()]
* b. before delete: prevents deletion on once activated Requirement [preventDeletionOnOnceActivatedRecord()]
* c. after insert/update: creates or updates training when activated [createOrUpdatePersonnelDocumentTrainingWhenActivated()]
* d. after update: updates training when deactivated [updatePersonnelDocumentTrainingWhenDeactivated()]
* e. before insert/update: set training batch job status when activated or deactivated if batch job is enabled [queueTrainingBatchJob]
* f. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Requirement_Trigger on SQX_Requirement__c(before insert, before update, before delete, after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (trigger.isBefore) {
        
        if (trigger.isInsert) {
            new SQX_Requirement.Bulkified(trigger.new, trigger.oldMap).preventCreationOnDocumentWithoutEditPermission()
                                                                      .queueTrainingBatchJob();
        }
        else if (trigger.isUpdate) {
            new SQX_Requirement.Bulkified(trigger.new, trigger.oldMap).queueTrainingBatchJob();
        }
        else if (trigger.isDelete) {
            new SQX_Requirement.Bulkified(trigger.old, trigger.newMap).preventDeletionOnOnceActivatedRecord();
        }
    
    }
    else if (trigger.isAfter) {
        
        if (trigger.isInsert) {
            new SQX_Requirement.Bulkified(trigger.new, trigger.oldMap)
                .createOrUpdatePersonnelDocumentTrainingWhenActivated();
                
        }
        else if (trigger.isUpdate) {
            new SQX_Requirement.Bulkified(trigger.new, trigger.oldMap)
                .createOrUpdatePersonnelDocumentTrainingWhenActivated()
                .updatePersonnelDocumentTrainingWhenDeactivated()
                
                .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    
    }
    
}