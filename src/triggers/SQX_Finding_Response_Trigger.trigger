/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Finding_Response_Trigger on SQX_Finding_Response__c (before insert, before update, before delete,
after insert, after update, after delete, after undelete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
        system.debug('#SQX_Finding_Response__c# : Running finding response after trigger');
        if(trigger.isUpdate){
            system.debug('Is an update call');
            new SQX_Finding_Response.Bulkified(Trigger.new, Trigger.oldMap).onPublishOrApprovalPercolateToRelated()
                                    
                                    .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
        else if(trigger.isInsert){
            new SQX_Finding_Response.Bulkified(Trigger.new, Trigger.oldMap).onPublishOrApprovalPercolateToRelated();
            
        }
    }
    else if(trigger.isBefore){
        if(trigger.isInsert){
            new SQX_Finding_Response.Bulkified(Trigger.new, Trigger.oldMap).onSubmissionFillDefaultApprovers().performActionsOnBefore();
        }
        else if(trigger.isUpdate){
            new SQX_Finding_Response.Bulkified(Trigger.new, Trigger.oldMap).onSubmissionFillDefaultApprovers().performActionsOnBefore();
        }
        else if(trigger.isDelete){
            new SQX_Finding_Response.Bulkified(Trigger.old, Trigger.newMap).preventPublishedResponsesFromDeletion();
        }
    }
}