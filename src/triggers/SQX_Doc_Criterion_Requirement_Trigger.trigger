/**
* @author Sudeep Maharjan
* @date 2016/09/12
* @description Trigger to handle followig actions on criteria document requirement
* a. Before Delete : Prevents deletion of Criterion Requirement Document if Parent Audit criteria document is Locked 
*                    [preventDeletionOfCriteriaRequirementDocumentWithCurrentStatus]
* b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Doc_Criterion_Requirement_Trigger on SQX_Doc_Criterion_Requirement__c (before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore && trigger.isDelete){
        
        new SQX_Doc_Criterion_Requirement.Bulkified(trigger.old, trigger.newMap).preventDeletionOfCriteriaRequirementDocumentWithCurrentStatus(); 
        
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}