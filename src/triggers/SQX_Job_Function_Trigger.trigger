/**
 * This trigger performs the following :
 * a. Before insert/update : If Job function(Training Program) is active or is deactivated, put activation date or deactivation date, activate related PJF and DJF or deactivate related PJF and DJF respectively [updateActivationAndDeactivationDate].
 * b. After update  : Activate djf and pjf when its parent jf is activated [activatePJFsAndRequirements].
 * c. After update  : Deactivate djf and pjf when its parent jf is deactivated [deactivatePJFsAndRequirements].
 * d. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Job_Function_Trigger on SQX_Job_Function__c (before insert, before update, after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    SQX_Job_Function.Bulkified processor = new SQX_Job_Function.Bulkified(Trigger.new, Trigger.oldMap);
    if (Trigger.isBefore) {
        processor.updateActivationAndDeactivationDate(); // update activation and deactivation date of jf
    } else {
        if (Trigger.isUpdate) {
            processor.activatePJFsAndRequirements() // activate pjf and djf;
                     .deactivatePJFsAndRequirements() // deactivate pjf and djf
                     
                     .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}