/**
* The trigger performs the following workflow:
* a. Before Insert/Update : Checks whether the combination of Supplier Deviation and Finding is unique [ensureCombinationIsUnique]
* b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*    
* @author : Dil Bahadur Thapa
* @date : 09/06/2018
* @story :[SQX-6310]
*/
trigger SQX_Supplier_Deviation_Finding_Trigger on compliancequest__SQX_Supplier_Deviation_Finding__c (before insert, before update, after update) {
    
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        if(trigger.isInsert || trigger.isUpdate){
            new SQX_BulkifiedBase(trigger.New, trigger.oldMap).ensureCombinationIsUnique(
                new Schema.SObjectField[]{Schema.SQX_Supplier_Deviation_Finding__c.SQX_Supplier_Deviation__c, Schema.SQX_Supplier_Deviation_Finding__c.SQX_Finding__c}, //the combination of fields
                Schema.SQX_Supplier_Deviation_Finding__c.Uniqueness_Constraint__c //the field that is unique
            );
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}