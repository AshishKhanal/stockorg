/**
* trigger to perform actions for Supplier Interaction
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Supplier_Interaction_Trigger on SQX_Supplier_Interaction__c (before insert, after insert, before delete, after update, before update) {

    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    SQX_Supplier_Interaction.Bulkified handler;
    
    if(Trigger.isDelete) {
        handler = new SQX_Supplier_Interaction.Bulkified(Trigger.old, Trigger.newMap);
    } else {
        handler = new SQX_Supplier_Interaction.Bulkified(Trigger.new, Trigger.oldMap);
    }
    handler.execute();
    
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            handler.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}