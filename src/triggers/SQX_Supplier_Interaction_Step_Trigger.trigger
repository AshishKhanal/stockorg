/**
* trigger to perform actions for Supplier Interaction Steps
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Supplier_Interaction_Step_Trigger on SQX_Supplier_Interaction_Step__c (after update, before delete, before update, after insert, before insert) {

    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    SQX_Steps_Trigger_Handler handler;

    if(Trigger.isDelete) {
        handler = new SQX_Steps_Trigger_Handler(Trigger.old, Trigger.newMap);
    } else {
        handler = new SQX_Steps_Trigger_Handler(Trigger.new, Trigger.oldMap);
    }

    handler.execute();
    
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            handler.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}