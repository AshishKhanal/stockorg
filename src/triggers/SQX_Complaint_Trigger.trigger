/**
* Trigger to perform actions of Complaint
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Complaint_Trigger on SQX_Complaint__c (before insert, before update, after insert, after update, before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    SQX_Steps_Trigger_Handler stepsTriggerHandler;
    if(Trigger.isDelete) {
        stepsTriggerHandler = new SQX_Steps_Trigger_Handler(Trigger.old, Trigger.newMap);
    } else {
        stepsTriggerHandler = new SQX_Steps_Trigger_Handler(Trigger.new, Trigger.oldMap);
    }
    stepsTriggerHandler.setParentAndChildFields(SQX_Complaint_Task__c.SQX_Complaint__c);
    
    SQX_Complaint.Bulkified wrapperProcess = Trigger.isDelete ? new SQX_Complaint.Bulkified(Trigger.old, Trigger.newMap) : new SQX_Complaint.Bulkified(Trigger.new, Trigger.oldMap);
    wrapperProcess.execute();
    
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            wrapperProcess.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }

}