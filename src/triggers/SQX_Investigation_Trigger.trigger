/**
 * This trigger performs various actions on Investigation records.
 * a. after update - [syncComplaintConclusionCodeWithPrimaryInvestigationOnClose] Syncs Conclusion Code of Complaint with Primary Diagnostic of closed investigation.
                   - [removeAllLinkedInvestigationOnVoid] Updates Complaint Conclusion Code with Primary Investigation's Defect Code if investigation is closed.
                   - [onPublishingApproveActionPlanAndCreateActions]
                   - [completeComplaintTask]
                   - [addModifiedLongTextFieldHistory] captures modified long text field history
 * b. before update - [updateInvestigationOnComplaint]
 * c. before insert - [onCreationDefaultAccountAndContact]
 * d. after insert - [createLinkedInvestigationAfterInvestigationIsCreated] Creates Linked Investigation after Investigation is inserted.
 * e. Before delete [preventDeletionOfLockedRecords] prevent to delete locked records
 * f. before update - [preventUpdateOnPrimaryInvestigaton] Prevents primary investigation from updating if the update is on the field part, part-family or defectcode
 */
trigger SQX_Investigation_Trigger on SQX_Investigation__c (after insert, after update, before insert, before update, before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    SQX_Investigation.Bulkified triggerProcessor;
    
    if( trigger.isUpdate){
        triggerProcessor = new SQX_Investigation.Bulkified(Trigger.new, Trigger.oldMap);
    	if(trigger.isAfter){
        	triggerProcessor.onPublishingApproveActionPlanAndCreateActions()
                            .completeComplaintTask()
                            .syncComplaintConclusionCodeWithPrimaryInvestigationOnClose()
                            .removeAllLinkedInvestigationOnVoid()
                            
                            .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
    	}else{
        	triggerProcessor.updateInvestigationOnComplaint()
                                .preventUpdateOnPrimaryInvestigaton();
    	}
    }
    else if(trigger.isBefore && trigger.isInsert){
        triggerProcessor = new SQX_Investigation.Bulkified(Trigger.new, Trigger.oldMap);
        triggerProcessor.onCreationDefaultAccountAndContact()
                        .onCreationSetPartDefectAndLotFromSoruce();
    } else if (trigger.isAfter && trigger.isInsert) {
        new SQX_Investigation.Bulkified(Trigger.new, Trigger.oldMap).createLinkedInvestigationAfterInvestigationIsCreated();
    }else if(trigger.isBefore && trigger.isDelete){
        triggerProcessor = new SQX_Investigation.Bulkified(Trigger.old, Trigger.newMap);
        triggerProcessor.preventDeletionOfLockedRecords();
    }
}