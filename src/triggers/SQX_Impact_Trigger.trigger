/**
* SQX_Impact objects class handles trigger
* 1. After Insert/Update: Adds requirements to Documents in the parent CO if necessary [addRequirementToCORelatedDocuments]
* 2. before delete: prevent deletion when parent record is locked [preventDeletionOnClosureAndVoid]
* 3. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Impact_Trigger on SQX_Impact__c (after insert, after update,before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
            	new SQX_Impact.Bulkified(trigger.New, trigger.oldMap)
    								.addRequirementToCORelatedDocuments();
        
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }else{
        if(trigger.isDelete){
                new SQX_Impact.Bulkified(trigger.old, trigger.newMap)
                                    .preventDeletionOnClosureAndVoid();
        }
    }
}