/**
* trigger for document review
* a. after insert/update changeControlledDocAsPerReviewDecision() : update controlled doc as per review decision in document review
* b. before insert/update: validate change order retiring a controlled document to have an action to retire that document[checkChangeOrderWithRetireDocumentAction()]
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Document_Review_Trigger on SQX_Document_Review__c (after insert, after update, before insert, before update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter ){
        if((trigger.isUpdate || trigger.isInsert)){
            new SQX_Document_Review.Bulkified(Trigger.new, Trigger.oldMap).changeControlledDocAsPerReviewDecision();
        }   
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
    else if (trigger.isBefore) {
        if (trigger.isInsert || trigger.isUpdate) {
            new SQX_Document_Review.Bulkified(Trigger.new, Trigger.oldMap).checkChangeOrderWithRetireDocumentAction();
        }
    }
}