/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Part_Trigger on SQX_Part__c (after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter && trigger.isUpdate){
        new SQX_Part.Bulkified(trigger.New, trigger.oldMap)
                    .percolateRiskAndActiveToSupplierPart()
                    
                    .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
    }    
     
}