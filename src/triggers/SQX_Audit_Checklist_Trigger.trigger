/**
* This trigger performs the following actions on Audit Checklist object:
* a. After Insert : Adds targets to audit checklist if they haven't already been using the UI [addTargetToAuditChecklist]
* b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Audit_Checklist_Trigger on SQX_Audit_Checklist__c (after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (Trigger.isAfter) {
        if (trigger.isInsert) {
         	    new SQX_Audit_Checklist.Bulkified(trigger.new, trigger.oldMap)
                    .addTargetToAuditChecklist();//Adds all the target to audit checklist 
        }
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}