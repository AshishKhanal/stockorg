/**
* The trigger performs the following workflow:
* a. Before Insert/Update : Checks whether the combination of Supplier Escalation, Finding and NC is unique [ensureCombinationIsUnique]
* b. Before Delete [preventEscalationReferenceCannotBeDeletedAfterEscalationIsVoidOrClosed]: prevents deletion of escalation reference if supplier escalation is closed or void
* c. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*    
* @author : Dil Bahadur Thapa
* @date : 10/05/2018
* @story :[SQX-6881]
*/
trigger SQX_Escalation_Reference_Trigger on SQX_Escalation_Reference__c (before insert, before update, before delete, after update) {
    
    // CQ trigger bypass logic
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        if(trigger.isInsert || trigger.isUpdate){
            new SQX_BulkifiedBase(trigger.New, trigger.oldMap).ensureCombinationIsUnique(
                new Schema.SObjectField[]{Schema.SQX_Escalation_Reference__c.SQX_Supplier_Escalation__c,
                    Schema.SQX_Escalation_Reference__c.SQX_Related_Nonconformance__c,
                    Schema.SQX_Escalation_Reference__c.SQX_Related_Finding__c,
                    Schema.SQX_Escalation_Reference__c.SQX_Related_Escalation__c,
                    Schema.SQX_Escalation_Reference__c.Other_Reference__c}, //the combination of fields
                    Schema.SQX_Escalation_Reference__c.Uniqueness_Constraint__c //the field that is unique
            );
        }else{
            new SQX_Escalation_Reference.Bulkified(Trigger.old, Trigger.newMap)
                .preventEscalationReferenceCannotBeDeletedAfterEscalationIsVoidOrClosed();
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}