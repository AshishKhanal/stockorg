/**
* trigger to perform actions for SD
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Supplier_Deviation_Trigger on SQX_Supplier_Deviation__c (before delete, after update, before update) {

    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    SQX_Supplier_Deviation.Bulkified processor;
    
    if(Trigger.isDelete) {
        processor = new SQX_Supplier_Deviation.Bulkified(trigger.old, trigger.newMap);
    } else {
        processor = new SQX_Supplier_Deviation.Bulkified(trigger.new, trigger.oldMap);
    }

    processor.execute();
    
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            processor.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}