/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Medwatch_Suspect_Product_Trigger on SQX_Medwatch_Suspect_Product__c (after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    SQX_Medwatch_Suspect_Product.SQX_Medwatch_Suspect_Product_Handler handler = new SQX_Medwatch_Suspect_Product.SQX_Medwatch_Suspect_Product_Handler(Trigger.new, Trigger.oldMap);

    handler.execute();

    if(Trigger.isAfter && Trigger.isUpdate) {
        handler.addModifiedLongTextFieldHistory(); // execute at last; captures modified long text field history
    }
}