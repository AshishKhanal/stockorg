/**
* The trigger performs the following workflows
* a. After Insert/Update : Completes all CQ Aware Tasks [completeRelatedCQAwareTasks]
* b. Before Update/Insert: Set the name to combinaton of document number and revision [setNameToCombinationOfNumberAndRev]
* c. Before Update: Create review task or closes review task based on review flag[createOrCompleteReviewTaskForTheUser]
*    SQX-1202 "Task is created on review queue date with due date as Next Review Date"
* d. Before Update: When the effective date or expiration date is less than or equals to today and its approval status 
*    is approved and its document status is pre-release then the document status should be current else the document status should be pre-expire
*    Also updates date on pre-release
*    [updateDocumentStatusAndDates]
* e. After Insert/Update: Activate requirements when controlled document is pre-released/released [activateRequirementsForReleasedControlledDocument]
* 
* g. After Insert/Update: Update doc release when doc is obsoleted not by a new revision [updateReleaseForObsoletedDocs]
* h. After Insert/Update: Archive Content Document When Obsolete [archiveContentDocWhenObsolete]
* i. After Insert/Update: Change the previous version to Obsolete once the new rev becomes current [changePreviousVersionToObsolete]
* j. After Insert/Update: Deactivate the requirements and set pending trainings to obsolete for obsolete controlled documents [deactivateRequirementsForObsoleteControlledDocument]
* k. After Insert/Update: Update related content's title and description to match with controlled document [updateRelatedContent]
* l. After Delete: Delete related content document when controlled document is deleted [deleteContentDocWhenDeleted]
* m. bedore insert/update : creates controlled document approval list for a controlled document from linked approval matrix [copyApprovalMatrixStepsWhenReferredToDocContainingApprovalMatrix]
* n. Before update: set approver fields for current approval step when controlled document is in approval and current approval step value has changed [setApproversForCurrentApprovalStep]
* o. Before update: checks for empty approver in each Controlled Document approval record and/or a Controlled Document step exceeding maximum number of approvers when submitting for approval [checkAnyEmptyApproverOrStepExceedingMaximumApproversWhenSubmittingForApproval]
* p. Before update: checks whether a controlled document is in sync before being submitted for approval [preventOutOfSyncFromGoingForApproval]
* q. After Update: move the content from private library to draft vault upon submission for approval [onSubmissionForApprovalMoveActiveContentToDraftLibrary]
* r. Before Update: On Pre-release add content document link so that trainee can view document in draft library and whole of draft library doesn't have to be shared [onPreReleaseAddTrainingLink]
* s. After Update: On release remove any previously added content document link so that trainee can't view obsolete content later on. [onReleaseRemoveTrainingLink]
* t. After Update: On release of controlled document create a distribution content [updateLatestDocObject]
* u. Before Update: On release remote the version of release content [onReleaseRemoveVersionInReleaseContent]
*                   Covers previous : f. After Insert/Update: Move Documents from draft library to release library [moveDocumentFromDraftToRelease]
* v. Before update: throw error when the distribution vault is changed to null when doc status is current [preventChangingDistributionVaultToNoneWhenDocIsReleased]
* w. After update : change the library when the distribution vault is changed [distributeContentOnVaultChange]
* x. Before Update: On change of secondary content mode take necessary actions [secondaryContentModeChanged]
* y. after update [updateRelatedRecordOwner]: sync owner of primary and secondary content documents when controlled document owner is changed
* zz. Before update: On Change prevent reverting status from higher to lower example Pre-release to approved, Current to approved [preventDocStatusFromReverting]
* aa. Before update: On Change prevent document from being obsoleted if it is being referred by active audit or audit program.  [validateObsolescence]
* ab. after update: updates the assoctiation of the doc with record type inspection criteria when the doc status is updated [preventDuplicationOfAssociation]
* ac. After Insert/Update: Updates the Personnel Document Job Function Due Date when Controlled Document Effective Date is changed [updateDueDateWhenEffectiveDateIsChanged]
* ad. Before update : throw error when the document is being sent for approval with draft assessment associated with with it or in it's requirement [preventDocFromGoinInApprovalWithDraftAssessment].
* ae. Before Update: checks if first approver is empty when submitted for approval [checkFirstApproverOnSubmitForApproval]
* af. Before Delete: delete the controlled doc's child recordss
* ag. After Insert/Update: update content details and its record type when Secondary Content is added in manaul secondary format [updateContentDetailsWhenSecondaryContentIsChanged]
* ah. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Controlled_Document_Trigger on SQX_Controlled_Document__c (before insert, before update, before delete, after insert, after update, after delete) {

    if(SQX_Controlled_Document.disableTrigger || SQX_Utilities.disableTriggerForDataImport())
        return;

    if(trigger.isBefore){
        if (trigger.isInsert || trigger.isUpdate) {
            SQX_Controlled_Document.Bulkified processor = new SQX_Controlled_Document.Bulkified(trigger.new, trigger.oldMap);
            
            processor.setNameToCombinationOfNumberAndRev(); //always set the name to be combination of number and rev
            
            if (trigger.isUpdate) {
                processor.createOrCompleteReviewTaskForTheUser() //create a review task for user on update
                        .updateDocumentStatusAndDates()
                        .setApproversForCurrentApprovalStep()
                        .checkFirstApproverOnSubmitForApproval()
                        .checkAnyEmptyApproverOrStepExceedingMaximumApproversWhenSubmittingForApproval()
                        .preventOutOfSyncFromGoingForApproval()
                        .onReleaseRemoveVersionInReleaseContent()
                        .preventChangingDistributionVaultToNoneWhenDocIsReleased()
                        .secondaryContentModeChanged()
                        .preventDocStatusFromReverting()
                        .validateObsolescence()
                        .onPreReleaseAddTrainingLink()
                        .preventDocFromGoinInApprovalWithDraftAssessment();
            }
        }
        else if(trigger.isDelete){
            new SQX_Controlled_Document.Bulkified(trigger.old, trigger.newMap).deleteChildRecords();
        }
    }
    else{
        if(Trigger.isInsert || trigger.isUpdate){
            SQX_Controlled_Document.Bulkified processor = new SQX_Controlled_Document.Bulkified(trigger.new, trigger.oldMap);
                        
            processor.copyApprovalMatrixStepsWhenReferredToDocContainingApprovalMatrix()
                    .activateRequirementsForReleasedControlledDocument() //all requirements of controlled doc should be activate in current status
                    .updateReleaseForObsoletedDocs() // update doc release on obsolete
                    .archiveContentDocWhenObsolete() //archive the content if obsoleted
                    .changePreviousVersionToObsolete() //ensure that only one version is current
                    .deactivateRequirementsForObsoleteControlledDocument() //when controlled doc is obsolete, deactivate all active requirements and make pending trainings obsolete
                    .updateRelatedContent() //update title and description of related latest version of content version on title update of controlled document
                    .onSubmissionForApprovalMoveActiveContentToDraftLibrary()
                    .onReleaseRemoveTrainingLink()
                    .updateLatestDocObject()
                    .distributeContentOnVaultChange()
                    .updateDueDateWhenEffectiveDateIsChanged()
                    .updateContentDetailsWhenSecondaryContentIsChanged()
                    .completeRelatedCQAwareTasks();//complete all related CQ Aware tasks

            if (trigger.isUpdate) {
                processor.updateRelatedRecordOwner()
                        .preventDuplicationOfAssociation()
                        
                        .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
            }
                    
        }
        else if(trigger.isDelete){
            //delete the controlled doc's content upon deletion
            new SQX_Controlled_Document.Bulkified(trigger.old, trigger.newMap).deleteContentDocWhenDeleted();
        }
    }

}