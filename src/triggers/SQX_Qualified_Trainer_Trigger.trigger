/*
 * This trigger performs following actions:
 * a. before insert/update : prevents duplication of Qualified Trainer [ensureCombinationIsUnique]
 * b. before insert/update/delete : prevent manipulation of Qualified Trainer when controlled document is in approval [preventManipulationOfQualifiedTrainerOnApproval]
 * c. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Qualified_Trainer_Trigger on SQX_Qualified_Trainer__c (before insert, before update, before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isBefore){
        if(Trigger.isUpdate || Trigger.isInsert){
            new SQX_Qualified_Trainer.Bulkified(trigger.new, trigger.oldMap)
                .preventManipulationOfQualifiedTrainerOnApproval()
                .ensureCombinationIsUnique(
                    new Schema.SObjectField[]{Schema.SQX_Qualified_Trainer__c.SQX_Controlled_Document__c, 
                        Schema.SQX_Qualified_Trainer__c.SQX_Personnel__c},
                    Schema.SQX_Qualified_Trainer__c.Uniqueness_Constraint__c
                );
        }else if(Trigger.isDelete){
            new SQX_Qualified_Trainer.Bulkified(trigger.old, trigger.newMap)
                .preventManipulationOfQualifiedTrainerOnApproval();
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}