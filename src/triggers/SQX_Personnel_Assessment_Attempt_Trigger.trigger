/**
 * This trigger performs the following :
 * a. After update : If attempt is successful, associates the personnel assessment record with its training record [associatePersonnelAssessmentWithTrainingRecordOnSuccessfulCompletion]
 * b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Personnel_Assessment_Attempt_Trigger on SQX_Personnel_Assessment_Attempt__c (after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            new SQX_Personnel_Assessment_Attempt.Bulkified(Trigger.new, Trigger.oldMap)
                                                .associatePersonnelAssessmentWithTrainingRecordOnSuccessfulCompletion()
                                                
                                                .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}