/**
* this trigger performs the following activities on Change Order Approval object:
* a. Before Insert/Update: It copies the step number as text to use in approval matrix approval logic [setStepAsTextValue]
* b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Change_Order_Approval_Trigger on SQX_Change_Order_Approval__c (before insert, before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (trigger.isBefore) {
        if (trigger.isInsert || trigger.isUpdate) {
            new SQX_Change_Order_Approval.Bulkified(trigger.new, trigger.oldMap).setStepAsTextValue();
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}