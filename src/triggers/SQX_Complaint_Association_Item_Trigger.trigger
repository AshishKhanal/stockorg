/**
 * Trigger for complaint association item
 * a. before delete : prevents deletion of primary association item [preventPrimaryAssociationItemDeletion]
 * b. before delete : prevents deletion of associated item if complaint is locked [preventDeletionOfLockedRecords]
 * c. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 * c. After Update [syncPrimaryAssociatedItem]: copy associated item id to complaint's primary associated item
 */
trigger SQX_Complaint_Association_Item_Trigger on SQX_Complaint_Associated_Item__c (before delete, after update) {

    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    if (Trigger.isBefore) {
        if(trigger.isDelete){
            new SQX_Complaint_Association_Item.Bulkified(trigger.old, trigger.newMap)
                .preventPrimaryAssociationItemDeletion()
                .preventDeletionOfLockedRecords();
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_Complaint_Association_Item.Bulkified(Trigger.new, Trigger.oldMap).syncPrimaryAssociatedItem();
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}