/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Impacted_Customer_Trigger on SQX_Impacted_Customer__c (before insert, before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        new SQX_BulkifiedBase(trigger.New, trigger.oldMap).ensureCombinationIsUnique(
                new Schema.SObjectField[]{Schema.SQX_Impacted_Customer__c.SQX_Finding__c, Schema.SQX_Impacted_Customer__c.SQX_Impacted_Account__c},
                Schema.SQX_Impacted_Customer__c.Uniqueness_Constraint__c
            );
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}