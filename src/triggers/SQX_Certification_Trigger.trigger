/**
 * trigger to perform actions for Supplier Document
 * a. Before Insert [syncCQAccountOwnerField]: syncs CQ Account Owner field with related account's CQ Owner field
 * b. Before Update [resetFlagsWhenExpirationInformationIsUpdated]: resets Process_Expired_Document__c and Process_Expiry_Notification__c flags when Expiration Info changes
 * c. After Update [setExpiredSupplierDocument]: sets Expired Supplier Document field in Account to true.
 * d. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Certification_Trigger on SQX_Certification__c (before insert, before update, after update) {
    
    // CQ trigger bypass logic
    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    if(trigger.isBefore)
    {
        if (trigger.isInsert) {
            new SQX_Supplier_Document.Bulkified(trigger.new, trigger.oldMap)
                                     .syncCQAccountOwnerField();
        }
        if (trigger.isUpdate) {
            new SQX_Supplier_Document.Bulkified(trigger.new, trigger.oldMap)
                                     .resetFlagsWhenExpirationInformationIsUpdated();
        }
    }
    else if(trigger.isAfter)
    {
        if (trigger.isUpdate) {
            new SQX_Supplier_Document.Bulkified(trigger.new, trigger.oldMap)
                                     .setExpiredSupplierDocument()
                                     
                                     .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}