/**
 * trigger to perform actions for NSI
 * a. after update : update onboarding step and create sf tasks for active steps [openOnboardingStepsWithActiveSteps]
 * b. Before Delete: Prevents deletion of existing NSI records [preventDeletionOfNSI].
 * c. Before update: prevents initiation of NSI without all the required details [preventInitiatingNSIWithInvalidOnboardingSteps].
 * d. after update : method to delete SF tasks for void NSI [deleteRelatedSFTasks]
 * e. Before update: set status when workflow status is complete [setNSIStatusFromWorkflowStatus].
 * f. Before update : method to assign ownership to user who takes ownership of the record [assignOwnershipToCurrentUser]
 * g. After update: transfers the information to an Account when NSI is closed [transferTheInformationWhenNSIClosed].
 * h. Before update : add supplier part and supplier service under linked account of nsi when nsi is closed with approved/conditionally approved result [createSupplierPartAndServices].
 * i. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_NSI_Trigger on SQX_New_Supplier_Introduction__c (before delete, after update, before update) {
    
    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    
    SQX_NSI.Bulkified processor;
    
    if(Trigger.isDelete) {
        processor = new SQX_NSI.Bulkified(trigger.old, trigger.newMap);
    } else {
        processor = new SQX_NSI.Bulkified(trigger.new, trigger.oldMap);
    }

    processor.execute();
    
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            processor.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}