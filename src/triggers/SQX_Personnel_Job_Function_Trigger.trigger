/**
* Tasks performed:
* a. before insert/update: sets uniqueness constraint value [setUniquenessConstraintValue()]
* b. before delete: prevents deletion on once activated job function [preventDeletionOnOnceActivatedRecord()]
* c. after insert/update: creates or updates training when activated [createOrUpdatePersonnelDocumentTrainingWhenActivated()]
* d. after update: updates training when deactivated [updatePersonnelDocumentTrainingWhenDeactivated()]
* e. before insert/update: sets activation/deactivation dates when activated/deactivated [updateActivationAndDeactivationDate()]
* f. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Personnel_Job_Function_Trigger on SQX_Personnel_Job_Function__c (before insert, before update, before delete, after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (trigger.isBefore) {
        
        if (trigger.isInsert || trigger.isUpdate) {
            new SQX_Personnel_Job_Function.Bulkified(trigger.new, trigger.oldMap)
                .updateActivationAndDeactivationDate()
                .setUniquenessConstraintValue();
        }
        
        if (trigger.isDelete) {
            new SQX_Personnel_Job_Function.Bulkified(trigger.old, trigger.newMap)
                .preventDeletionOnOnceActivatedRecord();
        }
    
    }
    else if (trigger.isAfter) {
        
        if (trigger.isInsert) {
            new SQX_Personnel_Job_Function.Bulkified(trigger.new, trigger.oldMap)
                .createOrUpdatePersonnelDocumentTrainingWhenActivated();
        }
        else if (trigger.isUpdate) {
            new SQX_Personnel_Job_Function.Bulkified(trigger.new, trigger.oldMap)
                .createOrUpdatePersonnelDocumentTrainingWhenActivated()
                .updatePersonnelDocumentTrainingWhenDeactivated()
                
                .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    
    }
    
}