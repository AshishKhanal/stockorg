/**
 * The following workflows are done
 * a. Before Delete: On deletion of sample tracking check whether parent record is locked and if locked then stop deletion. [preventDeletionOfLockedRecords]
 * b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Sample_Tracking_Trigger on compliancequest__SQX_Sample_Tracking__c (before delete, after update) {
    
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (Trigger.isBefore) {
        if (Trigger.isDelete) {
            new SQX_Sample_Tracking.Bulkified(Trigger.old, null).preventDeletionOfLockedRecords();
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }

}