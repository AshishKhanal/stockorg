/**
* Trigger to perform actions of Complaint Task
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
* b. Before delete [preventDeletionOfLockedRecords] prevent to delete locked records
*/
trigger SQX_Complaint_Task_Trigger on SQX_Complaint_Task__c (before insert, after insert, after update, after delete, after undelete, before delete, before update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    SQX_Steps_Trigger_Handler stepsTriggerHandler;
    if(Trigger.isDelete) {
        stepsTriggerHandler = new SQX_Steps_Trigger_Handler(Trigger.old, Trigger.newMap);
    } else {
        stepsTriggerHandler = new SQX_Steps_Trigger_Handler(Trigger.new, Trigger.oldMap);
    }

    stepsTriggerHandler.setParentAndChildFields(SQX_Complaint_Task__c.SQX_Complaint__c);
    SQX_Complaint_Task.Bulkified wrapperProcess = Trigger.isDelete ? new SQX_Complaint_Task.Bulkified(Trigger.old, Trigger.newMap) : new SQX_Complaint_Task.Bulkified(Trigger.new, Trigger.oldMap);
    wrapperProcess.execute();
    
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            wrapperProcess.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }else{
        if(trigger.isDelete){
            wrapperProcess.preventDeletionOfLockedRecords();
        }
    }
}