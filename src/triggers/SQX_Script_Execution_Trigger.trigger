/**
* This trigger handles the following business logs for decision tree object.
* a. Before Delete: On deletion of decision tree check whether parent record is locked and if locked then stop deletion. [preventDeletionOfLockedRecords] 
* b. After Insert : Complete all related decision tree task [completeComplaintTask]
* c. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Script_Execution_Trigger on SQX_Script_Execution__c (after insert, before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        if(trigger.isDelete){
            new SQX_Script_Execution.Bulkified(trigger.old, null).preventDeletionOfLockedRecords();
        }
    }
    else {
        if(trigger.isInsert){
            new SQX_Script_Execution.Bulkified(trigger.new, trigger.oldMap).completeComplaintTask();
        }
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}