/**
* This is a trigger for NC Impacted Product that performs the following operation
* a. Before insert/update: add uniqueness constraint on the object [ensureCombinationIsUnique]
* b. After update: Updates the disposition with new impacted part and lot number if impacted product is updated [updateDispositionsIfImpactedProductIsUpdated]
* c. After update : prevents defect defective quantity greater than impacted product lot quantity [preventDefectiveQtyGreaterThanLotQty]
* d. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_NC_Impacted_Product_Trigger on SQX_NC_Impacted_Product__c (before insert, before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        if(trigger.isUpdate || trigger.isInsert){
            new SQX_NC_Impacted_Product.Bulkified(trigger.New, trigger.oldMap)
                .ensureCombinationIsUnique(
                new Schema.SObjectField[]{  SQX_NC_Impacted_Product__c.SQX_Nonconformance__c, 
                                            SQX_NC_Impacted_Product__c.SQX_Impacted_Part__c, 
                                            SQX_NC_Impacted_Product__c.Lot_Number__c, 
                                            SQX_NC_Impacted_Product__c.Sublot_Number__c}, //added sublot number as mentioned in SQX-1640
                SQX_NC_Impacted_Product__c.Uniqueness_Constraint__c
            );
            //2014/10/20 modified to ensure that the combination of Finding, Part and Lot are unique.
            
            //WARNING:
            //This finding + part + lot combination is being used in disposition to find the impacted product quickly
            //using SOQL query. Please ensure that changes over here are reflected there too. BTW unit tests will fail
            //if you dont :-D

        }
    }
    else{
        if(trigger.isUpdate){
            new SQX_NC_Impacted_Product.Bulkified(trigger.New, trigger.oldMap)
                                    .updateDispositionsIfImpactedProductIsUpdated()
                                    .preventDefectiveQtyGreaterThanLotQty()
                                    
                                    .addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}