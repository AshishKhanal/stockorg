/**
* The trigger performs the following workflow:
* a. before Insert/Update : Checks whether the combination of NC and Capa is verified by uniqueness constraint or not
* ab. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*    
* @author : Paras Kumar Bishwakarma
* @date : 2016/7/4
* @story :[SQX-2114]
*/
trigger SQX_NC_CAPA_Trigger on SQX_NC_CAPA__c (before insert,before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        if(trigger.isInsert || trigger.isUpdate){
            new SQX_BulkifiedBase(trigger.New, trigger.oldMap).ensureCombinationIsUnique(
                new Schema.SObjectField[]{Schema.SQX_NC_CAPA__c.SQX_Nonconformance__c, Schema.SQX_NC_CAPA__c.SQX_CAPA__c}, //the combination of fields
                Schema.SQX_NC_CAPA__c.Uniqueness_Constraint__c //the field that is unique
            );
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}