/**
* @author Pradhanta Bhandari
* @date 2014/2/4
* @description single account trigger handler, that dispatches to respective code
* 
* Trigger methods:
* a. Before Insert/Update [ensureDisapprovedAccountAreInactive]: ensures that all disapproved account are inactive
* b. After Update [propagateApprovalStatus]: propagates approval status
*/
trigger SQX_Account_Trigger on Account (before insert, before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isAfter){
        if(trigger.isUpdate){
            new SQX_Account.Bulkified().propagateApprovalStatus(Trigger.new, Trigger.old);
        }
    }
    else if(trigger.isBefore){
        if(trigger.isInsert || trigger.isUpdate){
            new SQX_Account.Bulkified().ensureDisapprovedAccountAreInactive(Trigger.new);
        }
    }
    
}