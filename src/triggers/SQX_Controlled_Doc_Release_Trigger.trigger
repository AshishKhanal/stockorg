/**
* trigger to manage controlled document release object
* a. Before insert/update: Generate the related distribution content for the controlled document [distributeContentOnRelease]
* b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Controlled_Doc_Release_Trigger on SQX_Controlled_Document_Release__c (before insert, before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(trigger.isBefore){
        if(trigger.isInsert || trigger.isUpdate){
            new SQX_Controlled_Document_Release.Bulkified(trigger.new, trigger.oldMap).distributeContentOnRelease();
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}