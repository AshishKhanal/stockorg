/**
* Trigger methods:
* a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Result_Type_Value_Trigger on SQX_Result_Type_Value__c (before insert, before update, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
     if(trigger.isBefore){

        if(trigger.isInsert || trigger.isUpdate){
            new SQX_BulkifiedBase(trigger.New, trigger.oldMap).ensureCombinationIsUnique(
                       new Schema.SObjectField[]{Schema.SQX_Result_Type_Value__c.SQX_Result_Type__c, Schema.SQX_Result_Type_Value__c.Name}, //the combination of fields
                       Schema.SQX_Result_Type_Value__c.Uniqueness_Constraint__c //the field that is unique
       );
        }
     }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }

}