/**
* Trigger for MedDev object
*/
trigger SQX_MedDev_Trigger on SQX_MedDev__c (after insert, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    SQX_MedDev_eSubmitter.SQX_MedDev_Trigger_Handler handler = new SQX_MedDev_eSubmitter.SQX_MedDev_Trigger_Handler(Trigger.new, Trigger.oldMap);

    handler.execute();

    if(Trigger.isAfter && Trigger.isUpdate) {
        handler.addModifiedLongTextFieldHistory(); // execute at last; captures modified long text field history
    }
}