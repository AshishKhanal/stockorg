/*
 * This trigger performs following actions:
 * a. before insert/update/delete : prevents deletion of specification of the active controlled documents [preventModificationOfSpecification]
 * b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Specification_Trigger on SQX_Specification__c (before insert, before update, before delete, after update) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if(Trigger.isBefore){
        if(Trigger.isUpdate || Trigger.isInsert){
            new SQX_Specification.Bulkified(trigger.new, trigger.oldMap).preventModificationOfSpecification();
        }
        else if(Trigger.isDelete){
            new SQX_Specification.Bulkified(trigger.old, trigger.newMap).preventModificationOfSpecification();
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}