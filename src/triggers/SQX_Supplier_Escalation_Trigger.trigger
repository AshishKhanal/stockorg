/**
 *   Trigger for SQX_Supplier_Escalation__c object
 * a. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
 */
trigger SQX_Supplier_Escalation_Trigger on SQX_Supplier_Escalation__c (after update, before update, before delete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;

    SQX_Supplier_Escalation.Bulkified handler;
    if(Trigger.isDelete) {
        handler = new SQX_Supplier_Escalation.Bulkified(Trigger.old, Trigger.newMap);
    } else {
        handler = new SQX_Supplier_Escalation.Bulkified(Trigger.new, Trigger.oldMap);
    }
    handler.execute();
    
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            handler.addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
    }
}