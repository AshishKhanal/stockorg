/**
* trigger actions for finding object
* a. before insert/update [changeOwnershipToAssigneeWhenFindingIsInitiated]: transfers ownership to provided assignee user when finding is initiated
* b. After Update [addModifiedLongTextFieldHistory]: captures modified long text field history
*/
trigger SQX_Finding_Trigger on SQX_Finding__c (before insert, before update,
after insert, after update, after delete, after undelete) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    SQX_Finding.Bulkified bulkified = new SQX_Finding.Bulkified();
    
    if(trigger.isBefore){
        if( trigger.isInsert){
            bulkified.assignAuditPrimaryContactToAuditFinding(Trigger.new);
            bulkified.assignDefaultApprovers(Trigger.new);
            
            SQX_Finding.Bulkified paramBulkified = new SQX_Finding.Bulkified(trigger.new, trigger.oldMap);
            paramBulkified.changeOwnershipToAssigneeWhenFindingIsInitiated();
        }
        else if(trigger.isUpdate){
            //before update
            SQX_Finding.Bulkified paramBulkified = new SQX_Finding.Bulkified(trigger.new, trigger.oldMap);
            
            bulkified.assignDefaultApprovers(trigger.new);
            // transfer ownership to assignee when initiated before changing to another status eg. finding gets completed when there is no policy required
            paramBulkified.changeOwnershipToAssigneeWhenFindingIsInitiated();
            bulkified.onFindingMeetsAllRequirementsCreateTask(trigger.new, trigger.old);
            
            paramBulkified.setCAPALinkedStage();
            
        }
    }
    else if(trigger.isAfter){
        if(trigger.isUpdate){
            bulkified.shareFindingRecordWithAccountRole(trigger.new, trigger.old);
            // moved to capa
            //bulkified.assignTaskToInvestigator(trigger.new, trigger.old);
            //bulkified.closeRelatedTasksVoidUnsubmittedResponses_OnClosureOfFinding(Trigger.New,Trigger.Old);
            
            new SQX_BulkifiedBase(Trigger.new, Trigger.oldMap).addModifiedLongTextFieldHistory(); // call this long text history capturing method at last
        }
        else if(trigger.isInsert){
            bulkified.shareFindingRecordWithAccountRole(trigger.new, trigger.old);
            bulkified.shareAuditFindingWithAuditTeamMembers(trigger.new, trigger.old);
            //bulkified.assignTaskToInvestigator(trigger.new, trigger.old);
        }
        
        
        if(trigger.isUpdate || trigger.isInsert || trigger.isUnDelete || trigger.isDelete){
            bulkified.SQX_FindingsRollup(trigger.new, trigger.old);
        } 
    }
    
}