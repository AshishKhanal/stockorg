/**
* Trigger methods:
* a. Before Insert [setRecordIndex]: sets record index field value
* Note: addModifiedLongTextFieldHistory() is not added for this object since it is the history tracking object
*/
trigger SQX_Long_Text_History_Trigger on SQX_Long_Text_History__c (before insert) {
    if (SQX_Utilities.disableTriggerForDataImport())
        return;
    
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            new SQX_Long_Text_History.Bulkified(Trigger.new, Trigger.oldMap).setRecordIndex();
        }
    }
}