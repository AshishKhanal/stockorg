<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>CQ_Sample_Tracking</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>CQ_Sample_Tracking</compactLayoutAssignment>
    <compactLayouts>
        <fullName>CQ_Sample_Tracking</fullName>
        <fields>Name</fields>
        <fields>OwnerId</fields>
        <fields>SQX_Complaint__c</fields>
        <fields>SQX_Associated_Item__c</fields>
        <label>CQ Sample Tracking</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Tracking sample of complaints</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Activity__c</fullName>
        <description>Records type of activity done in sample tracking</description>
        <externalId>false</externalId>
        <label>Activity</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Receive</fullName>
                    <default>false</default>
                    <label>Receive</label>
                </value>
                <value>
                    <fullName>Send</fullName>
                    <default>false</default>
                    <label>Send</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Comment__c</fullName>
        <description>Records comment for sample tracking</description>
        <externalId>false</externalId>
        <label>Comment</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>From_Location__c</fullName>
        <description>Location from where sample is taken</description>
        <externalId>false</externalId>
        <label>From Location</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>CQ_Location</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>From_To_Location__c</fullName>
        <description>Combination of from and to location</description>
        <externalId>false</externalId>
        <formula>TEXT(From_Location__c )+ IF(ISPICKVAL( To_Location__c , &#39;&#39;), &#39;&#39;, &#39; / &#39; + TEXT(To_Location__c ))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>From/To Location</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lot__c</fullName>
        <description>Lot number of part</description>
        <externalId>false</externalId>
        <label>Lot/Serial Number</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Part_Lot__c</fullName>
        <description>Formula field combining part and lot</description>
        <externalId>false</externalId>
        <formula>IF(AND(ISBLANK(compliancequest__SQX_Part__c), ISBLANK(compliancequest__Lot__c)), 
        &#39;/&#39;, 
        compliancequest__SQX_Part__r.Name + IF(ISBLANK(compliancequest__Lot__c),&#39;&#39;,&#39;/&#39; + compliancequest__Lot__c) 
        )</formula>
        <label>Part / Lot</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Performed_By_Date__c</fullName>
        <description>Formula field to combine performed by and performed date</description>
        <externalId>false</externalId>
        <formula>Performed_By__c + &#39; / &#39;+  TEXT(Performed_Date__c)</formula>
        <label>Performed By/Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Performed_By__c</fullName>
        <description>Records name of the user who performs sample tracking</description>
        <externalId>false</externalId>
        <label>Performed By</label>
        <length>80</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Performed_Date__c</fullName>
        <description>Records date when sample is taken</description>
        <externalId>false</externalId>
        <label>Performed Date</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>SQX_Associated_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Associated item with sample tracking</description>
        <externalId>false</externalId>
        <label>Associated Item</label>
        <lookupFilter>
            <active>true</active>
            <description>Filter association item linked to this complaint</description>
            <errorMessage>Selected Associated Item does not belong to this complaint.</errorMessage>
            <filterItems>
                <field>SQX_Complaint_Associated_Item__c.SQX_Complaint__c</field>
                <operation>equals</operation>
                <valueField>$Source.SQX_Complaint__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>SQX_Complaint_Associated_Item__c</referenceTo>
        <relationshipLabel>Sample Trackings</relationshipLabel>
        <relationshipName>SQX_Sample_Trackings</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Complaint__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Complaint lookup</description>
        <externalId>false</externalId>
        <label>Complaint</label>
        <referenceTo>SQX_Complaint__c</referenceTo>
        <relationshipLabel>Sample Trackings</relationshipLabel>
        <relationshipName>SQX_Sample_Trackings</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Part__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Part related to complaint</description>
        <externalId>false</externalId>
        <label>Part</label>
        <referenceTo>SQX_Part__c</referenceTo>
        <relationshipLabel>Sample Trackings</relationshipLabel>
        <relationshipName>SQX_Sample_Trackings</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>To_Location__c</fullName>
        <description>Location to where sample is taken</description>
        <externalId>false</externalId>
        <label>To Location</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>CQ_Location</valueSetName>
        </valueSet>
    </fields>
    <label>Sample Tracking</label>
    <nameField>
        <description>Auto number for Sample Tracking record</description>
        <displayFormat>SN-{000000}</displayFormat>
        <label>Record Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Sample Trackings</pluralLabel>
    <searchLayouts/>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>Prevent_Modification_Of_Locked_Record</fullName>
        <active>true</active>
        <description>This rule prevents modification to locked Sample Tracking record</description>
        <errorConditionFormula>SQX_Complaint__r.Is_Locked__c</errorConditionFormula>
        <errorMessage>Record Status does not support the Action Performed.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_Performed_Date_to_be_future_date</fullName>
        <active>true</active>
        <description>Prevent Performed Date to be future date.</description>
        <errorConditionFormula>DATEVALUE( Performed_Date__c )  &gt;  TODAY()</errorConditionFormula>
        <errorMessage>Performed date cannot be future date.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
