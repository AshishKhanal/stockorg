<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stores training completion certificate of a personnel on a job function or a training program.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Completion_Date__c</fullName>
        <description>Stores date of certification i.e. completion of all training records on the job function by the personnel.</description>
        <externalId>false</externalId>
        <label>Completion Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Enrollment_Date__c</fullName>
        <description>Stores personnel job function activation date as enrollment date.</description>
        <externalId>false</externalId>
        <label>Enrollment Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SQX_Job_Function__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Stores related job function.</description>
        <externalId>false</externalId>
        <label>Job Function</label>
        <referenceTo>SQX_Job_Function__c</referenceTo>
        <relationshipLabel>Training Certificates</relationshipLabel>
        <relationshipName>SQX_Training_Certificates</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Personnel_Job_Function__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Stores related personnel job function.</description>
        <externalId>false</externalId>
        <label>Personnel Job Function</label>
        <referenceTo>SQX_Personnel_Job_Function__c</referenceTo>
        <relationshipLabel>Training Certificates</relationshipLabel>
        <relationshipName>SQX_Training_Certificates</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Personnel__c</fullName>
        <description>Stores related personnel or trainee.</description>
        <externalId>false</externalId>
        <label>Personnel</label>
        <referenceTo>SQX_Personnel__c</referenceTo>
        <relationshipLabel>Training Certificates</relationshipLabel>
        <relationshipName>SQX_Training_Certificates</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Title__c</fullName>
        <description>Stores job function name at the time of certification generation.</description>
        <externalId>false</externalId>
        <label>Title</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Training Certificate</label>
    <nameField>
        <description>Auto number used for uniquely identifying the Training Certificate</description>
        <displayFormat>TCF-{000000}</displayFormat>
        <label>Record Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Training Certificates</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
