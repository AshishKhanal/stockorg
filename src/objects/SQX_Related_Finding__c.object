<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object holds related finding for a containment. Usability for linking a single containment with multiple findings</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>SQX_Action__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>This field relates an action to finding.</description>
        <externalId>false</externalId>
        <label>Implementation</label>
        <referenceTo>SQX_Action__c</referenceTo>
        <relationshipLabel>Related Findings</relationshipLabel>
        <relationshipName>SQX_Related_Findings</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Containment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Containment relating to a finding</description>
        <externalId>false</externalId>
        <label>Containment</label>
        <referenceTo>SQX_Containment__c</referenceTo>
        <relationshipLabel>Related Findings</relationshipLabel>
        <relationshipName>SQX_Related_Findings</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Finding__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Relates Finding to containment</description>
        <externalId>false</externalId>
        <label>Finding</label>
        <referenceTo>SQX_Finding__c</referenceTo>
        <relationshipLabel>Related Findings</relationshipLabel>
        <relationshipName>SQX_Findings</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Investigation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field is used to related a investigation to multiple finding</description>
        <externalId>false</externalId>
        <label>Investigation</label>
        <referenceTo>SQX_Investigation__c</referenceTo>
        <relationshipLabel>Related Findings</relationshipLabel>
        <relationshipName>SQX_Related_Findings</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Related Finding</label>
    <nameField>
        <description>Auto number used for uniquely identifying the Related Finding</description>
        <displayFormat>RFIN-{000000}</displayFormat>
        <label>Related Finding Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Related Findings</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>CantSelectSameFindingAsParent</fullName>
        <active>true</active>
        <description>Related finding should select finding othper that the containment or investigation is created for</description>
        <errorConditionFormula>OR(SQX_Finding__c = SQX_Containment__r.SQX_Finding__c,SQX_Finding__c ==  SQX_Investigation__r.SQX_Finding__c )</errorConditionFormula>
        <errorDisplayField>SQX_Finding__c</errorDisplayField>
        <errorMessage>Please select finding other that parent record for related finding</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
