<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object used to track physical copies of document distributed</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Comment__c</fullName>
        <description>Field to record the comment</description>
        <externalId>false</externalId>
        <label>Comment</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Contact_Name__c</fullName>
        <description>Field to record the contact name</description>
        <externalId>false</externalId>
        <label>Contact Name</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Distributed_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Field to record distributed by user</description>
        <externalId>false</externalId>
        <label>Distributed By</label>
        <referenceTo>User</referenceTo>
        <relationshipName>SQX_Distribution_Trackings</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Distribution_Date__c</fullName>
        <description>Field is used to record the date of distribution</description>
        <externalId>false</externalId>
        <label>Distribution Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <description>Field to track location of distribution</description>
        <externalId>false</externalId>
        <label>Location</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Number_of_Copies__c</fullName>
        <description>Field to record number of copies distributed</description>
        <externalId>false</externalId>
        <label>Number of Copies</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SQX_Controlled_Document__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Parent record identifier</description>
        <externalId>false</externalId>
        <label>Controlled Document</label>
        <referenceTo>SQX_Controlled_Document__c</referenceTo>
        <relationshipLabel>Distribution Trackings</relationshipLabel>
        <relationshipName>SQX_Distribution_Trackings</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Distribution Tracking</label>
    <nameField>
        <description>Auto number used for uniquely identifying the Distribution Tracking</description>
        <displayFormat>DTK-{000000}</displayFormat>
        <label>Record Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Distribution Trackings</pluralLabel>
    <searchLayouts/>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>Copies_Cannot_Be_Negative</fullName>
        <active>true</active>
        <description>Number of Copies cannot be negative</description>
        <errorConditionFormula>Number_of_Copies__c  &lt; 0</errorConditionFormula>
        <errorMessage>Number of copies cannot be negative</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
