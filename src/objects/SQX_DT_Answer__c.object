<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object records the answer along with the question provided for a particular decision tree run.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <fields>
        <fullName>Answer_Sequence__c</fullName>
        <description>Used to record the Answer Sequence number.</description>
        <externalId>false</externalId>
        <label>Answer Sequence</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Answer_Value__c</fullName>
        <description>The actual value of the answer. Answer composes of text and value. Value should typically be unique identifier. For example Answer Text can be &apos;Yes, I can&apos; or &apos;Yes&apos; but the value can be simply be &apos;Y&apos;</description>
        <externalId>false</externalId>
        <label>Answer Value</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comment__c</fullName>
        <description>The comment provided by the user when answering a particular question</description>
        <externalId>false</externalId>
        <label>Comment</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Question_Text__c</fullName>
        <description>The actual question text that was asked to the user. This denormalizes but captures the actual question asked to the user, which is important.</description>
        <externalId>false</externalId>
        <label>Question Text</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>SQX_Answer_Option__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Holds the reference of Answer Option object.</description>
        <externalId>false</externalId>
        <label>Answer Option</label>
        <referenceTo>SQX_Answer_Option__c</referenceTo>
        <relationshipLabel>Decision Tree Answers</relationshipLabel>
        <relationshipName>SQX_Decision_Tree_Answers</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Decision_Tree__c</fullName>
        <description>The decision tree that was run to provide the answer.</description>
        <externalId>false</externalId>
        <label>Decision Tree</label>
        <referenceTo>SQX_Decision_Tree__c</referenceTo>
        <relationshipLabel>Decision Tree Answers</relationshipLabel>
        <relationshipName>SQX_Decision_Tree_Answers</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Decision Tree Answer</label>
    <nameField>
        <description>Captures the answer provided while running the decision tree</description>
        <label>Question Number</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Decision Tree Answers</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
