<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Records history of old values of long text area or rich text area field of a record when value is modified.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <fields>
        <fullName>Changed_On__c</fullName>
        <description>Modified date time of the long text field value.</description>
        <externalId>false</externalId>
        <inlineHelpText>Note: Long Text History may be captured earlier than Audit Trail when the field value was changed.</inlineHelpText>
        <label>Changed On</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Field_Name__c</fullName>
        <description>Full api name of the modified field of the related parent record id.</description>
        <externalId>false</externalId>
        <label>Field Name</label>
        <length>80</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Old_Value__c</fullName>
        <description>Holds old value of the long text area field when modified by user.</description>
        <externalId>false</externalId>
        <label>Old Value</label>
        <length>131072</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Parent_Record_Id__c</fullName>
        <description>Record id of the parent record that was modified.</description>
        <externalId>false</externalId>
        <label>Parent Record Id</label>
        <length>18</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Record_Index__c</fullName>
        <description>Internally used for indexing long text history records.</description>
        <externalId>true</externalId>
        <label>Record Index</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SQX_Changed_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>User who modified the field value of the parent record.</description>
        <externalId>false</externalId>
        <label>Changed By</label>
        <referenceTo>User</referenceTo>
        <relationshipName>SQX_Long_Text_History_Details</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Long Text History</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Parent_Record_Id__c</columns>
        <columns>Field_Name__c</columns>
        <columns>SQX_Changed_By__c</columns>
        <columns>Changed_On__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <description>Auto number field for Long Text History object</description>
        <displayFormat>LTH-{000000}</displayFormat>
        <label>Long Text History Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Long Text History</pluralLabel>
    <searchLayouts/>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>Changed_By_required</fullName>
        <active>true</active>
        <description>Prevent blank value in Changed By field.</description>
        <errorConditionFormula>ISBLANK(SQX_Changed_By__c)</errorConditionFormula>
        <errorDisplayField>SQX_Changed_By__c</errorDisplayField>
        <errorMessage>Changed By is required</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_Record_Modification</fullName>
        <active>true</active>
        <description>Prevents modification of history record once created.</description>
        <errorConditionFormula>NOT ISNEW()</errorConditionFormula>
        <errorMessage>Modification of history record is not allowed</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
