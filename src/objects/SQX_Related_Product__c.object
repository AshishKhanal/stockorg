<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object records the product related to the Product History Review.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Description__c</fullName>
        <description>This field records the description of the related product.</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Other_Reference_Number__c</fullName>
        <description>This field records the serial number of the related products</description>
        <externalId>false</externalId>
        <label>Other Reference Number</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reference_Type__c</fullName>
        <description>This field records the reference type of the related products.</description>
        <externalId>false</externalId>
        <label>Reference Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Deviation</fullName>
                    <default>false</default>
                    <label>Deviation</label>
                </value>
                <value>
                    <fullName>Inspection</fullName>
                    <default>false</default>
                    <label>Inspection</label>
                </value>
                <value>
                    <fullName>Change Order</fullName>
                    <default>false</default>
                    <label>Change Order</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SQX_CAPA__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field will record the capa related to related product</description>
        <externalId>false</externalId>
        <label>CAPA</label>
        <referenceTo>SQX_CAPA__c</referenceTo>
        <relationshipLabel>Related Products</relationshipLabel>
        <relationshipName>SQX_Related_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Nonconformance__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field will record the nc related to related product</description>
        <externalId>false</externalId>
        <label>Nonconformance</label>
        <referenceTo>SQX_Nonconformance__c</referenceTo>
        <relationshipLabel>Related Products</relationshipLabel>
        <relationshipName>SQX_Related_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Product_Component__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field records which part is related to the related products</description>
        <externalId>false</externalId>
        <label>Product/Component</label>
        <referenceTo>SQX_Part__c</referenceTo>
        <relationshipLabel>Related Products</relationshipLabel>
        <relationshipName>SQX_Related_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Product_History_Review__c</fullName>
        <description>This field records the which product history review is related to this related product.</description>
        <externalId>false</externalId>
        <label>Product History Review</label>
        <referenceTo>SQX_Product_History_Review__c</referenceTo>
        <relationshipLabel>Related Products</relationshipLabel>
        <relationshipName>SQX_Related_Products</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <label>Related Product</label>
    <nameField>
        <description>Auto number used for uniquely identifying the Related Product</description>
        <displayFormat>RPH-{000000}</displayFormat>
        <label>Related Product Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Related Products</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
