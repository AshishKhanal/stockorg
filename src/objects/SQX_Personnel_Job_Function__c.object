<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Records Personnel associated with a job function.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fieldSets>
        <fullName>New_Personnel_Job_Function</fullName>
        <description>New_Personnel_Job_Function vf page</description>
        <displayedFields>
            <field>SQX_Job_Function__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>true</isRequired>
        </displayedFields>
        <displayedFields>
            <field>SQX_Personnel__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>true</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Active__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>New Personnel Job Function</label>
    </fieldSets>
    <fieldSets>
        <fullName>Retrain_PersonnelJobFunction_Detail</fullName>
        <availableFields>
            <field>Activation_Date__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>CreatedById</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>CreatedDate</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Deactivation_Date__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>LastModifiedById</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>LastModifiedDate</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Name</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>SQX_Personnel__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <description>SQX_Retrain_PersonnelJobFunction vf page</description>
        <displayedFields>
            <field>Personnel_Name__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>SQX_Job_Function__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Active__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Retrain Personnel Job Function Detail</label>
    </fieldSets>
    <fields>
        <fullName>Activation_Date__c</fullName>
        <description>The date of activation of a personnel job function.</description>
        <externalId>false</externalId>
        <label>Activation Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>false</defaultValue>
        <description>It determines whether the personnel job function is active or not.</description>
        <externalId>false</externalId>
        <label>Active</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Current_Training_Program_Step__c</fullName>
        <description>Internally used for generating step-wise document training records for the related personnel on the related job function or training program.</description>
        <externalId>false</externalId>
        <label>Current Training Program Step</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Deactivation_Date__c</fullName>
        <description>The date of deactivation of a personnel job function.</description>
        <externalId>false</externalId>
        <label>Deactivation Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Is_PDJF_Generated__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Internal field; Used to track if SQX_7_0_PdjfGeneration batch class generated pdjf for the personnel</description>
        <externalId>false</externalId>
        <label>Is PDJF Generated</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Job_Function_Name__c</fullName>
        <description>Stores the name of job function.</description>
        <externalId>false</externalId>
        <formula>SQX_Job_Function__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsBlank</formulaTreatBlanksAs>
        <label>Job Function Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Overdue_Training_Count__c</fullName>
        <description>Internally used field to count overdue Personnel Document Training</description>
        <externalId>false</externalId>
        <label>Overdue Training Count</label>
        <summaryFilterItems>
            <field>SQX_Personnel_Document_Job_Function__c.Is_Overdue__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>SQX_Personnel_Document_Job_Function__c.SQX_Personnel_Job_Function__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Pending_Training_Count__c</fullName>
        <description>Internally used field to count Pending Document Training</description>
        <externalId>false</externalId>
        <label>Pending Training Count</label>
        <summaryFilterItems>
            <field>SQX_Personnel_Document_Job_Function__c.Is_Overdue__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>SQX_Personnel_Document_Job_Function__c.Training_Status__c</field>
            <operation>equals</operation>
            <value>Pending, Trainer Approval Pending</value>
        </summaryFilterItems>
        <summaryForeignKey>SQX_Personnel_Document_Job_Function__c.SQX_Personnel_Job_Function__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Personnel_Name__c</fullName>
        <description>Full name of the related personnel record.</description>
        <externalId>false</externalId>
        <formula>SQX_Personnel__r.Full_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Personnel Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Retrain_Comment__c</fullName>
        <description>User&apos;s comment for retrain action.</description>
        <externalId>false</externalId>
        <label>Retrain Comment</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>SQX_Job_Function__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Job function related to a personnel.</description>
        <externalId>false</externalId>
        <label>Job Function</label>
        <referenceTo>SQX_Job_Function__c</referenceTo>
        <relationshipLabel>Personnel Job Functions</relationshipLabel>
        <relationshipName>SQX_Personnel_Job_Functions</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Personnel__c</fullName>
        <description>Personnel related to a job function.</description>
        <externalId>false</externalId>
        <label>Personnel</label>
        <referenceTo>SQX_Personnel__c</referenceTo>
        <relationshipLabel>Personnel Job Functions</relationshipLabel>
        <relationshipName>SQX_Personnel_Job_Functions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Training_Status__c</fullName>
        <description>Overall Training status of Personnel Job Function</description>
        <externalId>false</externalId>
        <formula>IF( ( Active__c == false), &apos;NA&apos;,
IF( (  Overdue_Training_Count__c  &gt; 0) , &apos;Overdue&apos;, 
IF( ( Pending_Training_Count__c &gt; 0) , &apos;Pending&apos;, 
 &apos;Current&apos; 
)
) 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Training Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Unique_New_Or_Active_Constraint__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>It is internally used for ensuring that new or active record with the combination of Job Function and Personnel are unique.</description>
        <externalId>false</externalId>
        <label>Unique New Or Active Constraint</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Personnel Job Function</label>
    <nameField>
        <description>Auto number used for uniquely identifying the Personnel Job Function</description>
        <displayFormat>PJF-{000000}</displayFormat>
        <label>Personnel Job Function Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Personnel Job Functions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Prevent_activation_on_inactive_job</fullName>
        <active>true</active>
        <description>It prevents personnel job function to set active for an inactive job function.</description>
        <errorConditionFormula>AND(Active__c == true,
SQX_Job_Function__r.Active__c == false, NOT($Permission.SQXDataMigrationCustomPermission))</errorConditionFormula>
        <errorDisplayField>SQX_Job_Function__c</errorDisplayField>
        <errorMessage>Personnel Job Function cannot be active for an inactive Job Function.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_activation_on_inactive_personnel</fullName>
        <active>true</active>
        <description>It prevents personnel job function to set active for an inactive personnel.</description>
        <errorConditionFormula>Active__c == true
&amp;&amp;
SQX_Personnel__r.Active__c == false</errorConditionFormula>
        <errorDisplayField>SQX_Personnel__c</errorDisplayField>
        <errorMessage>You cannot activate Personnel Job Function for an inactive Personnel.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_modification_after_activation</fullName>
        <active>true</active>
        <description>It prevents modification of job function and/or Personnel values after the Personnel job function is activated.</description>
        <errorConditionFormula>NOT ISNEW()
&amp;&amp;
ISCHANGED( SQX_Job_Function__c )
&amp;&amp;
NOT ISNULL( Activation_Date__c )</errorConditionFormula>
        <errorDisplayField>SQX_Job_Function__c</errorDisplayField>
        <errorMessage>You cannot change Job Function related to an activated or deactivated record.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_reactivation</fullName>
        <active>true</active>
        <description>It prevents reactivating for a deactivated Personnel job function.</description>
        <errorConditionFormula>ISCHANGED( Active__c )
&amp;&amp;
Active__c == true
&amp;&amp;
NOT ISNULL( Deactivation_Date__c )</errorConditionFormula>
        <errorMessage>You cannot activate deactivated Personnel Job Function. Please create new Personnel Job Function to activate.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Retrain</fullName>
        <availability>online</availability>
        <description>Opens retrain personnel job function page.</description>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Retrain</masterLabel>
        <openType>sidebar</openType>
        <page>SQX_Retrain_PersonnelJobFunction</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
