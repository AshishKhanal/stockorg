<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Response holds information of the responses provided by user of the CQ system or Suppliers regarding the created NC/CAPA/Audit policies.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field records the account which is being responded.</description>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Responses</relationshipLabel>
        <relationshipName>Responses</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Action_Post_Approval_Required__c</fullName>
        <description>Internal usage field to decide whether response must be sent for post approval</description>
        <externalId>false</externalId>
        <formula>SQX_CAPA__r.Post_Approval_Required__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Action Post Approval Required</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Action_Pre_approval_Required__c</fullName>
        <description>Internal usage field that checks for pre-approval.</description>
        <externalId>false</externalId>
        <formula>SQX_CAPA__r.Pre_Approval_Required__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Action Pre-approval Required</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Approval_Status__c</fullName>
        <description>This fields records the Approval Status of the Response.</description>
        <externalId>false</externalId>
        <label>Approval Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>-</fullName>
                    <default>true</default>
                    <label>-</label>
                </value>
                <value>
                    <fullName>Pending</fullName>
                    <default>false</default>
                    <label>Pending</label>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                    <label>Approved</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
                <value>
                    <fullName>Recalled</fullName>
                    <default>false</default>
                    <label>Recalled</label>
                </value>
                <value>
                    <fullName>Partially Approved</fullName>
                    <default>false</default>
                    <label>Partially Approved</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>CAPA_Id__c</fullName>
        <description>This field returns the Id of the parent field that is associated with this response.Example: If it is a response for NC it returns the NC&apos;s Id. Similarly, if it is related to a CAPA then returns the CAPA&apos;s Id. 

Note: This field was originally used to return the CAPA Id hence API Name is CAPA_Id</description>
        <externalId>false</externalId>
        <formula>SQX_CAPA__c +  IF(NOT(ISBLANK(SQX_CAPA__c)) || NOT(ISBLANK(SQX_Nonconformance__c)),'',SQX_Finding__c) +  SQX_Nonconformance__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Parent Id</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CAPA_Name__c</fullName>
        <description>This field returns the name of the parent field that is associated with this response.Example: If it is a response for NC it returns the NC&apos;s name. Similarly, if it is related to a CAPA then returns the CAPA&apos;s name. Used mainly in email templates

Note: This field was originally used to return the CAPA name hence API Name is CAPA_Name</description>
        <externalId>false</externalId>
        <formula>SQX_CAPA__r.Name + IF(NOT(ISBLANK(SQX_CAPA__r.Name)) || NOT(ISBLANK(SQX_Nonconformance__r.Name)),'',SQX_Finding__r.Name) + SQX_Nonconformance__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Parent Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Finding_Description__c</fullName>
        <description>Description of the Response Finding.</description>
        <externalId>false</externalId>
        <label>Finding Description</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Finding_Type__c</fullName>
        <description>This field records which type of finding does the response has.</description>
        <externalId>false</externalId>
        <formula>SQX_Finding__r.RecordType.Name</formula>
        <label>Finding Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Needs_Disposition_Approval__c</fullName>
        <description>This field is indicates if disposition included in the response needs approval. It returns true if this response includes a disposition and needs approval in NC level</description>
        <externalId>false</externalId>
        <formula>SQX_Nonconformance__r.Disposition_Approval__c &amp;&amp;  Number_of_Dispositions__c &gt; 0</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Needs Disposition Approval</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Needs_Investigation_Approval__c</fullName>
        <description>[DEPRECATED] This field is indicates if investigation included in the response needs approval. It returns true if this response includes a investigation and needs approval in finding level</description>
        <externalId>false</externalId>
        <formula>false</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Needs Investigation Approval</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Number_of_Actions__c</fullName>
        <description>Internal Usage only field that will be used to decide whether to send a response for approval or not.</description>
        <externalId>false</externalId>
        <label>Number of Actions</label>
        <summaryFilterItems>
            <field>SQX_Response_Inclusion__c.Type__c</field>
            <operation>equals</operation>
            <value>Action</value>
        </summaryFilterItems>
        <summaryForeignKey>SQX_Response_Inclusion__c.SQX_Response__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Number_of_Containments__c</fullName>
        <description>The number of containments that is being included in the response</description>
        <externalId>false</externalId>
        <label>Number of Containments</label>
        <summaryFilterItems>
            <field>SQX_Response_Inclusion__c.Type__c</field>
            <operation>equals</operation>
            <value>Containment</value>
        </summaryFilterItems>
        <summaryForeignKey>SQX_Response_Inclusion__c.SQX_Response__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Number_of_Dispositions__c</fullName>
        <description>this field contains the number of dispositions included in this response.</description>
        <externalId>false</externalId>
        <label>Number of Dispositions</label>
        <summaryFilterItems>
            <field>SQX_Response_Inclusion__c.Type__c</field>
            <operation>equals</operation>
            <value>Disposition</value>
        </summaryFilterItems>
        <summaryForeignKey>SQX_Response_Inclusion__c.SQX_Response__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Number_of_Investigations__c</fullName>
        <description>Internal Usage field to get the total number of investigations submitted in the response, used to prevent user from directly publishing if approval is required.</description>
        <externalId>false</externalId>
        <label>Number of Investigations</label>
        <summaryFilterItems>
            <field>SQX_Response_Inclusion__c.Type__c</field>
            <operation>equals</operation>
            <value>Investigation</value>
        </summaryFilterItems>
        <summaryForeignKey>SQX_Response_Inclusion__c.SQX_Response__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Number_of_Rejected_Inclusion__c</fullName>
        <description>Internal usage field to see if any response inclusion has been individually rejected or not.</description>
        <externalId>false</externalId>
        <label>Number of Rejected Inclusion</label>
        <summaryFilterItems>
            <field>SQX_Response_Inclusion__c.Is_Approved__c</field>
            <operation>equals</operation>
            <value>0</value>
        </summaryFilterItems>
        <summaryForeignKey>SQX_Response_Inclusion__c.SQX_Response__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Published_Date__c</fullName>
        <description>This field is used to record the value when the response is completed.</description>
        <externalId>false</externalId>
        <label>Completed Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Requires_Additional_Response__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field indicates whether a finding has additional response or not. If it is true means Finding has additional response else not.</description>
        <externalId>false</externalId>
        <label>Requires Additional Response</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Requires_Disposition_Approval__c</fullName>
        <description>returns true if disposition requires approval. formula field to fetch the disposition approval from NC object.</description>
        <externalId>false</externalId>
        <formula>SQX_Nonconformance__r.Disposition_Approval__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Requires Disposition Approval</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Requires_Investigation_Approval__c</fullName>
        <description>Internal usage field to decide whether an investigation requires approval or not.</description>
        <externalId>false</externalId>
        <formula>OR(
  IF( ISBLANK( SQX_Audit_Response__c ), SQX_Finding__r.Investigation_Approval__c,
    SQX_Audit_Response__r.SQX_Audit__r.Requires_Investigation_Approval__c
  ),
  SQX_CAPA__r.Investigation_Approval__c,
  SQX_Nonconformance__r.Investigation_Approval__c
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Requires Investigation Approval</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Response_Summary__c</fullName>
        <description>This field holds audit response summary to be entered while submitting audit response</description>
        <externalId>false</externalId>
        <label>Response Summary</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>SQX_Audit_Response__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Relationship with audit response</description>
        <externalId>false</externalId>
        <label>Audit Response</label>
        <referenceTo>SQX_Audit_Response__c</referenceTo>
        <relationshipLabel>Finding Responses</relationshipLabel>
        <relationshipName>SQX_Finding_Responses</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_CAPA__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The CAPA for which the response is being submitted</description>
        <externalId>false</externalId>
        <label>CAPA</label>
        <referenceTo>SQX_CAPA__c</referenceTo>
        <relationshipLabel>Responses</relationshipLabel>
        <relationshipName>SQX_Responses</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Disposition_Approver__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field stores the user who will be responsible for approving the response if it includes a disposition.</description>
        <externalId>false</externalId>
        <label>Disposition Approver</label>
        <referenceTo>User</referenceTo>
        <relationshipName>SQX_Disposition_Responses</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Finding__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The finding for which this Response has been approved</description>
        <externalId>false</externalId>
        <label>Finding</label>
        <referenceTo>SQX_Finding__c</referenceTo>
        <relationshipLabel>Responses</relationshipLabel>
        <relationshipName>SQX_Responses</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Implementation_Approver__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field indicates the person responsible for approving the actions, pre and post implementation.</description>
        <externalId>false</externalId>
        <label>Implementation Approver</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Responses_Implementation_Approver</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Investigation_Approver__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>It specifies the Approver for an Investigation.</description>
        <externalId>false</externalId>
        <label>Investigation Approver</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Responses_Investigation_Approver</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Nonconformance__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The NC for which the response is being submitted</description>
        <externalId>false</externalId>
        <label>NC</label>
        <referenceTo>SQX_Nonconformance__c</referenceTo>
        <relationshipLabel>Responses</relationshipLabel>
        <relationshipName>SQX_Responses</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Response_Approver__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Users who approves the audit response</description>
        <externalId>false</externalId>
        <label>Response Approver</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Responses</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>This field hold data about status of response in various stages</description>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Draft</fullName>
                    <default>true</default>
                    <label>Draft</label>
                </value>
                <value>
                    <fullName>In Approval</fullName>
                    <default>false</default>
                    <label>In Approval</label>
                </value>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                    <label>Completed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Submitted_By__c</fullName>
        <description>The name of the logged in user submitting the response.</description>
        <externalId>false</externalId>
        <label>Submitted By</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Submitted_On__c</fullName>
        <description>Submission Date Time on publishing or sending for approval.</description>
        <externalId>false</externalId>
        <label>Submitted On</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <label>Response</label>
    <listViews>
        <fullName>All</fullName>
        <columns>OBJECT_ID</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <description>Auto number used for uniquely identifying the Finding Response</description>
        <displayFormat>RESP-{00000}</displayFormat>
        <label>Response Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Responses</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Audit_Implementation_Response</fullName>
        <active>true</active>
        <description>This is the list of response which has finding not required</description>
        <label>Audit Implementation Response</label>
        <picklistValues>
            <picklist>Approval_Status__c</picklist>
            <values>
                <fullName>-</fullName>
                <default>true</default>
                <description>The NC for which the response is being submitted</description>
            </values>
            <values>
                <fullName>Approved</fullName>
                <default>false</default>
                <description>The NC for which the response is being submitted</description>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>false</default>
                <description>The NC for which the response is being submitted</description>
            </values>
            <values>
                <fullName>Rejected</fullName>
                <default>false</default>
                <description>The NC for which the response is being submitted</description>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Completed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Draft</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>In Approval</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Response</fullName>
        <active>true</active>
        <description>This is the list of response which has finding required</description>
        <label>Response</label>
        <picklistValues>
            <picklist>Approval_Status__c</picklist>
            <values>
                <fullName>-</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Approved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Rejected</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Completed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Draft</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>In Approval</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <searchFilterFields>OBJECT_ID</searchFilterFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchResultsAdditionalFields>SQX_Finding__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Response_Summary__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <sharingReasons>
        <fullName>Managed_Sharing__c</fullName>
        <label>CQ Managed Sharing</label>
    </sharingReasons>
    <validationRules>
        <fullName>Ensure_Finding_Is_Required</fullName>
        <active>false</active>
        <description>This rule ensures that the finding is required for finding with response record type</description>
        <errorConditionFormula>AND( 
 RecordType.DeveloperName  = &apos;Response&apos;, 
ISNULL( SQX_Finding__c ) )</errorConditionFormula>
        <errorMessage>Finding is required for response with finding</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Finding_Should_Be_Open</fullName>
        <active>false</active>
        <description>This rule ensures that only findings that are open can be responded.</description>
        <errorConditionFormula>AND(NOT( RecordType.DeveloperName  = &apos;Audit_Implementation_Response&apos;),
IF( ISNEW(),
IF( ISPICKVAL( SQX_Finding__r.Status__c, &apos;Open&apos;), false, true),
IF(ISCHANGED(SQX_Finding__c), true, false)))</errorConditionFormula>
        <errorDisplayField>SQX_Finding__c</errorDisplayField>
        <errorMessage>Only findings that are open can be responded to.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>No_changes_can_be_made_once_published</fullName>
        <active>false</active>
        <description>This rule ensures that once a response is published, it can not be changed.</description>
        <errorConditionFormula>AND(ISNEW() == false, ISCHANGED(Status__c) = false,OR(ISPICKVAL(Status__c, &apos;Completed&apos;),ISPICKVAL(Status__c, &apos;Void&apos;)))</errorConditionFormula>
        <errorMessage>Once a response is published it can not be changed.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Response_Summary_Is_Provided</fullName>
        <active>true</active>
        <description>Response summary should be provided when it is linked with NC and CAPA, but is not needed when linked with Audit</description>
        <errorConditionFormula>IF(SQX_Audit_Response__c==null, LEN(TRIM(Response_Summary__c )) = 0, false)</errorConditionFormula>
        <errorDisplayField>Response_Summary__c</errorDisplayField>
        <errorMessage>Response Summary can not be empty</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Publish_Response</fullName>
        <availability>online</availability>
        <description>Button to launch the page to publish the finding response</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Publish</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>window.location=&apos;./apex/SQX_CommonActions?id={!SQX_Finding_Response__c.Id}&amp;action=publish&apos;</url>
    </webLinks>
</CustomObject>
