<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>CQ_Product_History_Review_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>CQ_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>CQ_Compact_Layout</fullName>
        <fields>Name</fields>
        <fields>SQX_Complaint__c</fields>
        <fields>OwnerId</fields>
        <fields>Pending_PHR_Checklist_Count__c</fields>
        <fields>Status__c</fields>
        <fields>Completed_On__c</fields>
        <label>CQ Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object holds information about product history review of complaints.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Completed_By__c</fullName>
        <description>The person responsible for managing the complaint.</description>
        <externalId>false</externalId>
        <label>Completed By</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Completed_On__c</fullName>
        <description>Records the date when the product history review was completed.</description>
        <externalId>false</externalId>
        <label>Completed On</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Pending_PHR_Checklist_Count__c</fullName>
        <description>This field will give the number of phr checklist to be answered.</description>
        <externalId>false</externalId>
        <label>Pending PHR Checklist Count</label>
        <summaryFilterItems>
            <field>SQX_Product_History_Answer__c.Answer__c</field>
            <operation>equals</operation>
            <value></value>
        </summaryFilterItems>
        <summaryForeignKey>SQX_Product_History_Answer__c.SQX_Product_History_Review__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Product_History_Review_Summary__c</fullName>
        <description>Summary of Product History Review</description>
        <externalId>false</externalId>
        <label>Summary</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Relevant_Observation__c</fullName>
        <description>Records observation for PHR</description>
        <externalId>false</externalId>
        <label>Relevant Observation</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Yes</fullName>
                    <default>false</default>
                    <label>Yes</label>
                </value>
                <value>
                    <fullName>No</fullName>
                    <default>false</default>
                    <label>No</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SQX_Complaint__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field records related complaint of product history review.</description>
        <externalId>false</externalId>
        <label>Complaint</label>
        <referenceTo>SQX_Complaint__c</referenceTo>
        <relationshipLabel>Product History Reviews</relationshipLabel>
        <relationshipName>SQX_Product_History_Reviews</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Investigation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Records investigation related to PHR</description>
        <externalId>false</externalId>
        <label>Investigation</label>
        <referenceTo>SQX_Investigation__c</referenceTo>
        <relationshipLabel>Product History Reviews</relationshipLabel>
        <relationshipName>SQX_Product_History_Reviews</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Task__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field records the task library that was being referred when creating the task. This field is for internal purpose only and helps void any report that have been generated by previous run of the decision tree.</description>
        <externalId>false</externalId>
        <label>Task</label>
        <referenceTo>SQX_Task__c</referenceTo>
        <relationshipLabel>Product History Reviews</relationshipLabel>
        <relationshipName>SQX_Product_History_Reviews</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>holds status of the product history review</description>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Draft</fullName>
                    <default>false</default>
                    <label>Draft</label>
                </value>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                    <label>Completed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Product History Review</label>
    <nameField>
        <description>Auto number used for uniquely identifying the product history review</description>
        <displayFormat>PHR-{000000}</displayFormat>
        <label>PHR Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Product History Reviews</pluralLabel>
    <searchLayouts/>
    <sharingModel>Read</sharingModel>
    <sharingReasons>
        <fullName>SQX_Shared__c</fullName>
        <label>CQ Managed Sharing</label>
    </sharingReasons>
    <validationRules>
        <fullName>Ensure_All_The_PHR_Cheklist_Are_Complete</fullName>
        <active>true</active>
        <description>This rule ensures that to complete phr all the phr checklist must be answered.</description>
        <errorConditionFormula>AND(  ISPICKVAL(Status__c, &apos;Completed&apos;) ,(Pending_PHR_Checklist_Count__c &gt; 0))</errorConditionFormula>
        <errorMessage>All the Product History Checklist must be answered to complete Product History Review.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Ensure_Completed_By_Is_Provided</fullName>
        <active>true</active>
        <description>This rule ensures that completed by is provided when PHR is complete</description>
        <errorConditionFormula>AND( 
ISPICKVAL( Status__c , &#39;Completed&#39;) , 
ISBLANK(  Completed_By__c )
)</errorConditionFormula>
        <errorDisplayField>Completed_By__c</errorDisplayField>
        <errorMessage>Completed By must be provided to complete the Product History Review.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Ensure_Summary_Is_Provided_When_Complete</fullName>
        <active>true</active>
        <description>This rule ensures that the product history review summary is provided when product history review is complete.</description>
        <errorConditionFormula>AND( 
ISPICKVAL( Status__c , &#39;Completed&#39;) , 
LEN(Product_History_Review_Summary__c ) = 0
)</errorConditionFormula>
        <errorDisplayField>Product_History_Review_Summary__c</errorDisplayField>
        <errorMessage>Summary must be provided to complete the Product History Review.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_Modification_Of_Locked_Record</fullName>
        <active>true</active>
        <description>This rule prevents modification to locked Product History Review record</description>
        <errorConditionFormula>SQX_Complaint__r.Is_Locked__c</errorConditionFormula>
        <errorMessage>Record Status does not support the Action Performed.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_Blank_Summary</fullName>
        <active>true</active>
        <description>Rule to ensure summary is not blank when relevant observation is yes.</description>
        <errorConditionFormula>AND(ISPICKVAL( Relevant_Observation__c , &apos;Yes&apos;),
LEN(Product_History_Review_Summary__c ) = 0)</errorConditionFormula>
        <errorDisplayField>Product_History_Review_Summary__c</errorDisplayField>
        <errorMessage>Summary is required.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>SQX_Create_PHR</fullName>
        <availability>online</availability>
        <description>Button to create new phr and redirect to detail screen.</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Create PHR</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>{! URLFOR(&#39;/apex/compliancequest__SQX_PHR_Create&#39;, null, [complaintId=SQX_Complaint__c.Id]) }</url>
    </webLinks>
</CustomObject>
