<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stores information about activities in CAPA records.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Activity_Code__c</fullName>
        <description>Capture  the action code for purpose of signature translation label</description>
        <externalId>false</externalId>
        <label>Activity Code</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Activity__c</fullName>
        <description>Translated labels</description>
        <externalId>false</externalId>
        <formula>CASE( Activity_Code__c, 
                &apos;save&apos;, $Label.SQX_PS_CAPA_Saving_the_record,
                &apos;close&apos;, $Label.SQX_PS_CAPA_Closing_the_record,
                &apos;initiate&apos;, $Label.SQX_PS_CAPA_Initiating_the_record,
                &apos;approve&apos;, $Label.SQX_PS_CAPA_Approving_Response,
                &apos;reject&apos;, $Label.SQX_PS_CAPA_Rejecting_Response,
                &apos;void&apos;, $Label.SQX_PS_CAPA_Voiding_the_record,
                &apos;submitresponse&apos;, $Label.SQX_PS_CAPA_Submitting_Response,
                &apos;recall&apos;, $Label.SQX_PS_CAPA_Recalling_Response,
                &apos;ownership&apos;, $Label.SQX_PS_Changing_Owner,
                &apos;changestage&apos;, $Label.SQX_PS_Changing_Stage,
                &apos;approvestage&apos;, $Label.SQX_PS_Approving_Record,
                &apos;rejectstage&apos;, $Label.SQX_PS_Rejecting_Record,
                &apos;recallstage&apos;, $Label.SQX_PS_Recalling_Record,
  Activity_Code__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Activity</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comment__c</fullName>
        <description>This shows the reason why record is changed.</description>
        <externalId>false</externalId>
        <label>Comment</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Modified_By__c</fullName>
        <description>One who modifies the related record.</description>
        <externalId>false</externalId>
        <label>Modified By</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>New_Stage__c</fullName>
        <description>Records stage when the record activity was added.</description>
        <externalId>false</externalId>
        <label>New Stage</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>-</fullName>
                    <default>false</default>
                    <label>-</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Old_Stage__c</fullName>
        <description>Records previous value of the stage</description>
        <externalId>false</externalId>
        <label>Old Stage</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>-</fullName>
                    <default>false</default>
                    <label>-</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Purpose_Of_Signature__c</fullName>
        <description>This field shows propose for changing the record.</description>
        <externalId>false</externalId>
        <label>Purpose Of Signature</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SQX_CAPA__c</fullName>
        <description>This refers to the related CAPA of the Record_Activity record.</description>
        <externalId>false</externalId>
        <label>CAPA</label>
        <referenceTo>SQX_CAPA__c</referenceTo>
        <relationshipLabel>CAPA Record Activities</relationshipLabel>
        <relationshipName>SQX_CAPA_Record_Activities</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Type_of_Activity__c</fullName>
        <description>Registers which action is performed</description>
        <externalId>false</externalId>
        <label>Type of Activity</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>CAPA Record Activity</label>
    <nameField>
        <description>Auto number used for uniquely identifying the CAPA record activity</description>
        <displayFormat>CRA-{000000}</displayFormat>
        <label>CAPA Record Activity Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>CAPA Record Activities</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
