<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object records the answers for the questions of the product history reviews.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Answer_Value__c</fullName>
        <description>This field will record the value for the answer text.</description>
        <externalId>false</externalId>
        <label>Answer Value</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Answer__c</fullName>
        <description>This field will store the answer for phr checklist.</description>
        <externalId>false</externalId>
        <label>Answer</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>This field copies the description of the question with task description.</description>
        <externalId>false</externalId>
        <formula>&apos;&apos;</formula>
        <label>Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Notes__c</fullName>
        <description>This field records the notes while answering the questions.</description>
        <externalId>false</externalId>
        <label>Notes</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Question__c</fullName>
        <description>In this field question will be copied from question object question text field.</description>
        <externalId>false</externalId>
        <label>Question</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Question_Number__c</fullName>
        <description>Internal field to track question number in the task.</description>
        <externalId>false</externalId>
        <formula>SQX_Question_With_Task__r.Name</formula>
        <label>Question Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Result_Type_Id__c</fullName>
        <description>Formula field used for internal purposes to quickly retrieve the id of the result type specified in selected Question with task</description>
        <externalId>false</externalId>
        <formula>SQX_Question_With_Task__r.SQX_Result_Type__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Result Type Id</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SQX_Product_History_Review__c</fullName>
        <description>This field records which product history is related to this answer.</description>
        <externalId>false</externalId>
        <label>Product History Review</label>
        <referenceTo>SQX_Product_History_Review__c</referenceTo>
        <relationshipLabel>Product History Answers</relationshipLabel>
        <relationshipName>SQX_Product_History_Answers</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>SQX_Question_With_Task__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field records which question is related to the product history review</description>
        <externalId>false</externalId>
        <label>Question With Task</label>
        <referenceTo>SQX_Task_Question__c</referenceTo>
        <relationshipLabel>Product History Answers</relationshipLabel>
        <relationshipName>SQX_Product_History_Answers</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Product History Checklist</label>
    <nameField>
        <description>Auto number used for uniquely identifying the Product History Answer</description>
        <displayFormat>PHA-{000000}</displayFormat>
        <label>Product History Checklist Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Product History Checklists</pluralLabel>
    <searchLayouts>
        <excludedStandardButtons>New</excludedStandardButtons>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
