<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Records training session of multiple personnel</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fieldSets>
        <fullName>Training_Session_Info</fullName>
        <availableFields>
            <field>CreatedById</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>CreatedDate</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Empty_Result_Count__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Is_Locked__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>LastModifiedById</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>LastModifiedDate</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Name</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <description>This field set is used in Training E-sig Sign-off page.</description>
        <displayedFields>
            <field>Title__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>SQX_Controlled_Document__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Primary_Instructor__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Start_Date__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>End_Date__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Duration__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Schedule__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Is_Online__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Session_Link__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Org_Division__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Org_Business_Unit__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Org_Region__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Org_Site__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Location__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>OwnerId</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Total_Trainees__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Roster_Batch_Job_Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Training Session Info</label>
    </fieldSets>
    <fields>
        <fullName>Duration__c</fullName>
        <description>Duration of the session</description>
        <externalId>false</externalId>
        <label>Duration</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Empty_Result_Count__c</fullName>
        <description>Count of number of empty roster&apos;s result field</description>
        <externalId>false</externalId>
        <label>Empty Result Count</label>
        <summaryFilterItems>
            <field>SQX_Training_Session_Roster__c.Result__c</field>
            <operation>equals</operation>
            <value></value>
        </summaryFilterItems>
        <summaryForeignKey>SQX_Training_Session_Roster__c.SQX_Training_Session__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <description>End date of the session</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Is_Locked__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Internal field; used to lock record when status is void or closed</description>
        <externalId>false</externalId>
        <label>Is Locked</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Online__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Records if the session is online</description>
        <externalId>false</externalId>
        <label>Is Online</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <description>Location information of the session</description>
        <externalId>false</externalId>
        <label>Location</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Org_Business_Unit__c</fullName>
        <description>This field records business unit for training session.</description>
        <externalId>false</externalId>
        <label>Org. Business Unit</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_Business_Unit</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Org_Division__c</fullName>
        <description>This field records division for training session.</description>
        <externalId>false</externalId>
        <label>Org. Division</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_Division</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Org_Region__c</fullName>
        <description>This field records region for training session.</description>
        <externalId>false</externalId>
        <label>Org. Region</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_Region</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Org_Site__c</fullName>
        <description>This field records site for training session.</description>
        <externalId>false</externalId>
        <label>Org. Site</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_Site</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Previous_Status__c</fullName>
        <description>Internal field; used to save previous status of training session</description>
        <externalId>false</externalId>
        <label>Previous_Status</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Primary_Instructor__c</fullName>
        <description>Name of the personnel as primary instructor of the session</description>
        <externalId>false</externalId>
        <label>Primary Instructor</label>
        <length>80</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Roster_Batch_Job_Status__c</fullName>
        <description>Status of session batch job that processes related rosters.</description>
        <externalId>false</externalId>
        <label>Roster Batch Job Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Document Training Generation Queued</fullName>
                    <default>false</default>
                    <label>Document Training Generation Queued</label>
                </value>
                <value>
                    <fullName>Document Training Generation Error</fullName>
                    <default>false</default>
                    <label>Document Training Generation Error</label>
                </value>
                <value>
                    <fullName>Document Training Completion Queued</fullName>
                    <default>false</default>
                    <label>Document Training Completion Queued</label>
                </value>
                <value>
                    <fullName>Document Training Completion Error</fullName>
                    <default>false</default>
                    <label>Document Training Completion Error</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Roster_Processing_Queued_On__c</fullName>
        <description>Internally used to record queued time to process roster when session is completed or closed.</description>
        <externalId>false</externalId>
        <label>Roster Processing Queued On</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>SQX_Controlled_Document__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Related controlled document.</description>
        <externalId>false</externalId>
        <label>Subject</label>
        <referenceTo>SQX_Controlled_Document__c</referenceTo>
        <relationshipLabel>Training Sessions</relationshipLabel>
        <relationshipName>SQX_Training_Sessions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Schedule__c</fullName>
        <description>Schedule information of the session</description>
        <externalId>false</externalId>
        <label>Schedule</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Session_Link__c</fullName>
        <description>Records URL for the online session</description>
        <externalId>false</externalId>
        <label>Session Link</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <description>Start date of the session</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Training Session&apos;s Status</description>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Draft</fullName>
                    <default>true</default>
                    <label>Draft</label>
                </value>
                <value>
                    <fullName>Open</fullName>
                    <default>false</default>
                    <label>Open</label>
                </value>
                <value>
                    <fullName>Complete</fullName>
                    <default>false</default>
                    <label>Complete</label>
                </value>
                <value>
                    <fullName>Closed</fullName>
                    <default>false</default>
                    <label>Closed</label>
                </value>
                <value>
                    <fullName>Void</fullName>
                    <default>false</default>
                    <label>Void</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Title__c</fullName>
        <description>Title of the session</description>
        <externalId>false</externalId>
        <label>Title</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Trainees__c</fullName>
        <description>Total number of trainees for the session</description>
        <externalId>false</externalId>
        <label>Total Trainees</label>
        <summaryForeignKey>SQX_Training_Session_Roster__c.SQX_Training_Session__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <label>Training Session</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>SQX_Controlled_Document__c</columns>
        <columns>Primary_Instructor__c</columns>
        <columns>Title__c</columns>
        <columns>Status__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <description>Auto number used for uniquely identifying the Training Session</description>
        <displayFormat>TSN-{000000}</displayFormat>
        <label>Record Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Training Sessions</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>SQX_Controlled_Document__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Title__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Start_Date__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>SQX_Controlled_Document__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Primary_Instructor__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Title__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Duration__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <searchResultsAdditionalFields>SQX_Controlled_Document__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Primary_Instructor__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Title__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>Cannot_Void_Closed_Training_Session</fullName>
        <active>true</active>
        <description>Closed training session cannot be voided</description>
        <errorConditionFormula>AND(  ISPICKVAL( Status__c , &apos;Void&apos;) , 
ISPICKVAL( PRIORVALUE( Status__c ) , &apos;Closed&apos;) )</errorConditionFormula>
        <errorMessage>Closed training session cannot be voided</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_Change_After_Record_Void_Close</fullName>
        <active>true</active>
        <description>This rule prevents changing locked record i.e. complete, closed or void state</description>
        <errorConditionFormula>AND(Is_Locked__c , NOT(ISCHANGED(Status__c)), NOT(ISCHANGED( OwnerId )), NOT ISCHANGED( Roster_Batch_Job_Status__c ), NOT ISCHANGED( Roster_Processing_Queued_On__c ))</errorConditionFormula>
        <errorMessage>Locked training Session cannot be modified</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Provide_Session_Link_if_Session_IsOnline</fullName>
        <active>true</active>
        <description>Session Link is required if Is Online is checked</description>
        <errorConditionFormula>AND(Is_Online__c,  ISBLANK( Session_Link__c ))</errorConditionFormula>
        <errorMessage>Session Link is required if Is Online is checked.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Referred_Ctrl_Doc_must_be_Active</fullName>
        <active>true</active>
        <description>Controlled Document must be in Pre-Release, Current or Pre-Expire in order to refer it.</description>
        <errorConditionFormula>AND( ISPICKVAL(Status__c, &apos;Open&apos;), NOT(ISBLANK( SQX_Controlled_Document__c )),NOT(ISPICKVAL(SQX_Controlled_Document__r.Document_Status__c,&apos;Pre-Release&apos;) || ISPICKVAL(SQX_Controlled_Document__r.Document_Status__c,&apos;Current&apos;)||
ISPICKVAL(SQX_Controlled_Document__r.Document_Status__c,&apos;Pre-Expire&apos;)))</errorConditionFormula>
        <errorMessage>Subject must be in Pre-Release, Current or Pre-Expire state when training session is opened</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Roster_Result_Before_Closing_Training</fullName>
        <active>true</active>
        <description>All training session roster items must have a result before a training session can be closed</description>
        <errorConditionFormula>AND( ISPICKVAL( Status__c , &apos;Complete&apos;) , Empty_Result_Count__c &gt; 0 )</errorConditionFormula>
        <errorMessage>All roster items must have a result when training session is completed</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Start_Date_Cant_Be_Greater_Than_End_Date</fullName>
        <active>true</active>
        <description>Start date cannot be greater than end date.</description>
        <errorConditionFormula>Start_Date__c  &gt;  End_Date__c</errorConditionFormula>
        <errorMessage>Start date cannot be greater than End date.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Training_Session_Cannot_Be_Closed</fullName>
        <active>true</active>
        <description>Training session can only be closed from complete state</description>
        <errorConditionFormula>AND( ISCHANGED(Status__c ) , ISPICKVAL ( Status__c , &apos;Closed&apos;) ,  NOT ( ISPICKVAL( PRIORVALUE(Status__c) , &apos;Complete&apos;) ) )</errorConditionFormula>
        <errorMessage>Training session can only be closed from complete state</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Close</fullName>
        <availability>online</availability>
        <description>custom button to close training session</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Close</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>{!URLFOR(&apos;/apex/compliancequest__SQX_Training_Session_E_Sig&apos;, null , [id= compliancequest__SQX_Training_Session__c.Id ,mode=&apos;Close&apos;])}</url>
    </webLinks>
    <webLinks>
        <fullName>Complete</fullName>
        <availability>online</availability>
        <description>Custom button to complete training session</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Complete</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>{!URLFOR(&apos;/apex/compliancequest__SQX_Training_Session_E_Sig&apos;, null , [id= compliancequest__SQX_Training_Session__c.Id ,mode=&apos;Complete&apos;])}</url>
    </webLinks>
    <webLinks>
        <fullName>Initiate</fullName>
        <availability>online</availability>
        <description>Initiate Training Session</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Initiate</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>{!URLFOR(&apos;/apex/compliancequest__SQX_Training_Session_E_Sig&apos;, null , [id= compliancequest__SQX_Training_Session__c.Id ,mode=&apos;Initiate&apos;])}</url>
    </webLinks>
    <webLinks>
        <fullName>Reopen</fullName>
        <availability>online</availability>
        <description>Custom button to reopen training session</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Reopen</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>{!URLFOR(&apos;/apex/compliancequest__SQX_Training_Session_E_Sig&apos;, null , [id= compliancequest__SQX_Training_Session__c.Id ,mode=&apos;Reopen&apos;])}</url>
    </webLinks>
    <webLinks>
        <fullName>Needs_Training_List</fullName>
        <availability>online</availability>
        <description>redirect to show the list of personnel requiring training signoff</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Needs Training List</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>{!URLFOR(&apos;/apex/compliancequest__SQX_TSR_Personnel_List&apos;, null , [id= compliancequest__SQX_Training_Session__c.Id])}</url>
    </webLinks>
    <webLinks>
        <fullName>Void</fullName>
        <availability>online</availability>
        <description>Custom button to void training session</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Void</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>{!URLFOR(&apos;/apex/compliancequest__SQX_Training_Session_E_Sig&apos;, null , [id= compliancequest__SQX_Training_Session__c.Id ,mode=&apos;Void&apos;])}</url>
    </webLinks>
</CustomObject>
