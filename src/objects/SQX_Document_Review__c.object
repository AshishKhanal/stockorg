<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>SQX_Document_Review_Editor</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <content>SQX_Document_Review_Editor</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object records the reviews added by the document owner. A review can be to Continue Use of a document, Revising a document or Retiring a document</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <fieldSets>
        <fullName>New_Document</fullName>
        <availableFields>
            <field>SQX_Obsolete_Document_Change_Order__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <description>SQX_Document_Review_Editor page</description>
        <displayedFields>
            <field>Performed_Date__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Performed_By__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Review_Comment__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>New Document</label>
    </fieldSets>
    <fields>
        <fullName>Age__c</fullName>
        <description>This field is need for Document management Report. This field is inserted as column in Completed Document Reviews by Days
Age (Review Completed Date - Review Target Date)</description>
        <externalId>false</externalId>
        <formula>Performed_Date__c - Target_Review_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Age</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Controlled_Document__c</fullName>
        <description>This field records the controlled document for the Document Review.</description>
        <externalId>false</externalId>
        <label>Controlled Document</label>
        <referenceTo>SQX_Controlled_Document__c</referenceTo>
        <relationshipLabel>Document Reviews</relationshipLabel>
        <relationshipName>Document_Reviews</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Expiration_Date__c</fullName>
        <description>This field is used to record expiration date for the controlled document.</description>
        <externalId>false</externalId>
        <label>Expiration Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Next_Review_Date__c</fullName>
        <description>The tentative next reviewal date for the document</description>
        <externalId>false</externalId>
        <label>Next Review Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Performed_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The user's name who performs the Document Review.</description>
        <externalId>false</externalId>
        <label>Performed By</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Document_Reviews</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Performed_Date_Internal__c</fullName>
        <description>Holds Performed Date field value and is used in report &quot;Completed Document Reviews by Month&quot;.</description>
        <externalId>false</externalId>
        <formula>Performed_Date__c</formula>
        <label>Performed Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Performed_Date__c</fullName>
        <defaultValue>Today()</defaultValue>
        <description>Records performed date of a Document Review.</description>
        <externalId>false</externalId>
        <label>Performed Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Review_Comment__c</fullName>
        <description>This field is used to add comments for Document Review .</description>
        <externalId>false</externalId>
        <label>Review Comment</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Review_Decision__c</fullName>
        <description>Describes the Decision of Document Review (Continue Use, Retire, Revise).</description>
        <externalId>false</externalId>
        <label>Review Decision</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Continue Use</fullName>
                    <default>false</default>
                    <label>Continue Use</label>
                </value>
                <value>
                    <fullName>Retire</fullName>
                    <default>false</default>
                    <label>Retire</label>
                </value>
                <value>
                    <fullName>Revise</fullName>
                    <default>false</default>
                    <label>Revise</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SQX_Obsolete_Document_Change_Order__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>This field records the change order that retired the related controlled document.</description>
        <externalId>false</externalId>
        <inlineHelpText>Change Order with action to retire the related controlled document</inlineHelpText>
        <label>Change Order</label>
        <referenceTo>SQX_Change_Order__c</referenceTo>
        <relationshipLabel>Obsolete Document Reviews</relationshipLabel>
        <relationshipName>SQX_Obsolete_Document_Reviews</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Target_Review_Date__c</fullName>
        <description>Targeted Date for revival date in the document.</description>
        <externalId>false</externalId>
        <label>Target Review Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Document Review</label>
    <nameField>
         <description>Auto number used for uniquely identifying the Doc Review</description>
        <displayFormat>DRVW-{000000}</displayFormat>
        <label>Document Review Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Document Reviews</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Doc_Already_In_Obsolescence_Approval</fullName>
        <active>true</active>
        <description>Controlled Document that is already in Obsolescence approval cannot resend for Obsolescence approval process.</description>
        <errorConditionFormula>AND( ISPICKVAL(Review_Decision__c,&apos;Retire&apos;), ISPICKVAL(Controlled_Document__r.Approval_Status__c,&apos;Obsolescence Approval&apos;))</errorConditionFormula>
        <errorMessage>Controlled Document has already been submitted for Obsolescence approval process.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Ensure_Expiration_Date_is_Entered</fullName>
        <active>true</active>
        <description>This rule is used to prevent user from saving document review when the expiration date is not entered and review decision is to retire.</description>
        <errorConditionFormula>AND( ISPICKVAL( Review_Decision__c , &apos;Retire&apos;) ,
 ISBLANK( Expiration_Date__c ) )</errorConditionFormula>
        <errorMessage>Expiration Date is required to retire the document.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Ensure_No_Next_Review_Date_In_Retire</fullName>
        <active>true</active>
        <description>Ensures that no next review date can be provided when retiring.</description>
        <errorConditionFormula>AND(
ISPICKVAL( Review_Decision__c , &apos;Retire&apos;),
NOT(ISBLANK(Next_Review_Date__c ))
)</errorConditionFormula>
        <errorDisplayField>Next_Review_Date__c</errorDisplayField>
        <errorMessage>Next Review Date can&apos;t be added when Retiring the controlled document.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Ensure_Review_In_Current_Only</fullName>
        <active>true</active>
        <description>This rule ensures that review can be added when doc is current only.</description>
        <errorConditionFormula>AND(
OR(ISNEW(),
   ISCHANGED(Controlled_Document__c)),
AND(NOT(ISPICKVAL(Controlled_Document__r.Document_Status__c, &apos;Pre-Expire&apos;)),
NOT(ISPICKVAL(Controlled_Document__r.Document_Status__c, &apos;Current&apos;))))</errorConditionFormula>
        <errorMessage>Review can only be added when document is Current or Pre-Expire.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Next_Review_Date_not_less_than_Performed</fullName>
        <active>true</active>
        <description>Do not next review date less than Performed Date</description>
        <errorConditionFormula>AND(OR(ISCHANGED( Next_Review_Date__c), ISNEW()), Next_Review_Date__c  &lt; Performed_Date__c )</errorConditionFormula>
        <errorDisplayField>Next_Review_Date__c</errorDisplayField>
        <errorMessage>Next Review Date can not be less than Performed Date.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_Future_Date_In_Performed_Date</fullName>
        <active>true</active>
        <description>Do not allow future date in performed date</description>
        <errorConditionFormula>AND(OR(ISCHANGED( Performed_Date__c ), ISNEW()), Performed_Date__c &gt; Today())</errorConditionFormula>
        <errorDisplayField>Performed_Date__c</errorDisplayField>
        <errorMessage>Performed Date can not be future date.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Review_Date_Is_Required_On_Continue_Use</fullName>
        <active>true</active>
        <description>When review decision is continue use then review date is required.</description>
        <errorConditionFormula>AND(
    ISPICKVAL(Review_Decision__c, &apos;Continue Use&apos;),  
    ISBLANK( Next_Review_Date__c )
)</errorConditionFormula>
        <errorMessage>Next Review Date is required when review decision is Continue Use.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>New_Document_Review</fullName>
        <availability>online</availability>
        <description>This button is used to send the required params to the document review VF page</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Document Review</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>{!URLFOR($Action.SQX_Document_Review__c.New, null, [controlleddocid =  SQX_Controlled_Document__c.Id, retURL = URLFOR($Action.SQX_Controlled_Document__c.View, SQX_Controlled_Document__c.Id) ])}</url>
    </webLinks>
</CustomObject>
