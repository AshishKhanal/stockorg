<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object stores the information about the Concomitant Medical Products and Therapy Dates</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>D11_Medical_Product__c</fullName>
        <description>This field records the Medical Product name</description>
        <externalId>false</externalId>
        <label>Medical Product</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>D11_Therapy_Date__c</fullName>
        <description>This field records the therapy date for Concomitant medical product</description>
        <externalId>false</externalId>
        <label>Therapy Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SQX_D11_Medical_Product_Null_Value__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field records null value record for D11 Concomitant medical product and therapy dates</description>
        <externalId>false</externalId>
        <label>D11 Medical Product Null Value</label>
        <referenceTo>SQX_Medwatch_D11_Medical_Prod_Null_Value__c</referenceTo>
        <relationshipLabel>D11. Concomitant medical products ....</relationshipLabel>
        <relationshipName>SQX_D11_Concomitant_Medical_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Medwatch__c</fullName>
        <description>The medwatch record for which this record stores details about Suspect Medical Devices</description>
        <externalId>false</externalId>
        <label>Medwatch</label>
        <referenceTo>SQX_Medwatch__c</referenceTo>
        <relationshipLabel>D11. Concomitant medical products ....</relationshipLabel>
        <relationshipName>SQX_D11_Concomitant_medical_products</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>D11. Concomitant medical product ....</label>
    <nameField>
        <description>Record number for Concomitant medical products and therapy dates</description>
        <displayFormat>CMP-{000000}</displayFormat>
        <label>Record Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>D11. Concomitant medical products ....</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Prevent_Edit_After_Successful_Submission</fullName>
        <active>true</active>
        <description>This rule prevents user from editing this record once the parent Medwatch record has been successfully acknowledged.</description>
        <errorConditionFormula>SQX_Medwatch__r.Is_Locked__c</errorConditionFormula>
        <errorMessage>Record locked. Cannot perform the desired action</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
