<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object holds picklist values for all category across objects</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field records whether the picklist value is active or not.</description>
        <externalId>false</externalId>
        <label>Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Category__c</fullName>
        <description>Category of a picklist value</description>
        <externalId>false</externalId>
        <label>Category</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Parent__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Parent Id of the picklist value. Equivalent to dependent picklist relation</description>
        <externalId>false</externalId>
        <label>Parent</label>
        <lookupFilter>
            <active>true</active>
            <description>Ensures that only active picklist values are provided.</description>
            <errorMessage>Selected Picklist Value is not active.</errorMessage>
            <filterItems>
                <field>SQX_Picklist_Value__c.Active__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>SQX_Picklist_Value__c</referenceTo>
        <relationshipLabel>Dependent Pickists</relationshipLabel>
        <relationshipName>SQX_Picklist_Values</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Value__c</fullName>
        <description>Contains value of a picklist value</description>
        <externalId>false</externalId>
        <label>Value</label>
        <length>80</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Picklist Value</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Value__c</columns>
        <columns>Category__c</columns>
        <columns>NAME</columns>
        <columns>Parent__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <description>The name field to identify picklist value</description>
        <label>Picklist Value Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Picklist Values</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Value__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Category__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Parent__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Category__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Parent__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Value__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Category__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Parent__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Value__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Category__c</searchFilterFields>
        <searchFilterFields>Parent__c</searchFilterFields>
        <searchFilterFields>Value__c</searchFilterFields>
        <searchResultsAdditionalFields>Value__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Category__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Parent__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
