<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Defects recorded with finding</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Actual__c</fullName>
        <description>This field records the actual condition of the defect. This is the description of the anomaly that has violated the specification.</description>
        <externalId>false</externalId>
        <label>Actual</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Cost__c</fullName>
        <defaultValue>0</defaultValue>
        <description>The cost incurred because of the defect</description>
        <externalId>false</externalId>
        <label>Cost of Defect</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Defect_Category__c</fullName>
        <description>The category which this defects belong to.</description>
        <externalId>false</externalId>
        <label>Defect Category</label>
        <length>120</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Defect_Code__c</fullName>
        <description>Defect code field that can be entered manually</description>
        <externalId>false</externalId>
        <inlineHelpText>Please add a defect code, if one can not be found using lookup. Note: If lookup is selected, it always overwrites the value in this field.</inlineHelpText>
        <label>Defect / Failure</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Defect_Description__c</fullName>
        <description>Description of as to what this defect is about.</description>
        <externalId>false</externalId>
        <label>Defect Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Defective_Quantity__c</fullName>
        <defaultValue>0</defaultValue>
        <description>The total number of items that were found to be defective</description>
        <externalId>false</externalId>
        <label>Defective Quantity</label>
        <precision>14</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lot_Ser_Number__c</fullName>
        <description>The lot serial number that was defective</description>
        <externalId>false</externalId>
        <label>Lot Number</label>
        <length>120</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Number_of_defects__c</fullName>
        <description>the number of defects present in the object</description>
        <externalId>false</externalId>
        <label>No. of Defects</label>
        <precision>6</precision>
        <required>true</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Part_Name__c</fullName>
        <description>The formula field for getting the related part name</description>
        <externalId>false</externalId>
        <formula>SQX_Part__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Part Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SQX_Defect_Code__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The defect code that is being referred by this defect</description>
        <externalId>false</externalId>
        <label>Defect Code</label>
        <lookupFilter>
            <active>true</active>
            <description>Ensures that defect code is active.</description>
            <filterItems>
                <field>SQX_Defect_Code__c.Active__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>SQX_Defect_Code__c</referenceTo>
        <relationshipLabel>Defects</relationshipLabel>
        <relationshipName>SQX_Defects</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Finding__c</fullName>
        <description>The finding to which this defect is related.</description>
        <externalId>false</externalId>
        <label>Finding</label>
        <referenceTo>SQX_Finding__c</referenceTo>
        <relationshipLabel>Defects</relationshipLabel>
        <relationshipName>SQX_Defects</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>SQX_Inspection_Detail__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Failed inspection detail responsible for creating defect.</description>
        <externalId>false</externalId>
        <label>Inspection Detail</label>
        <referenceTo>SQX_Inspection_Detail__c</referenceTo>
        <relationshipLabel>Defects</relationshipLabel>
        <relationshipName>SQX_Defects</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Part__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The part that is related to the defect</description>
        <externalId>false</externalId>
        <label>Part</label>
        <referenceTo>SQX_Part__c</referenceTo>
        <relationshipLabel>Defects</relationshipLabel>
        <relationshipName>SQX_Defects</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Severity__c</fullName>
        <description>The severity of the defect.</description>
        <externalId>false</externalId>
        <label>Defect Severity</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1 - Negligible</fullName>
                    <default>false</default>
                    <label>1 - Negligible</label>
                </value>
                <value>
                    <fullName>2 - Low</fullName>
                    <default>false</default>
                    <label>2 - Low</label>
                </value>
                <value>
                    <fullName>3 - Moderate</fullName>
                    <default>false</default>
                    <label>3 - Moderate</label>
                </value>
                <value>
                    <fullName>4 - Significant</fullName>
                    <default>false</default>
                    <label>4 - Significant</label>
                </value>
                <value>
                    <fullName>5 - Catastrophic</fullName>
                    <default>false</default>
                    <label>5 - Catastrophic</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Source__c</fullName>
        <description>This field will record the source of the defect</description>
        <externalId>false</externalId>
        <label>Source</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>As Investigated</fullName>
                    <default>false</default>
                    <label>As Investigated</label>
                </value>
                <value>
                    <fullName>As Reported</fullName>
                    <default>false</default>
                    <label>As Reported</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Specification__c</fullName>
        <description>The specification that was not met</description>
        <externalId>false</externalId>
        <label>Specification</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Sublot_Number__c</fullName>
        <description>This field will show sublot number of the defect parts</description>
        <externalId>false</externalId>
        <label>Sublot Number</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Finding Defect</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <description>Auto number used for uniquely identifying the Finding Defect</description>
        <displayFormat>DEF-{00000}</displayFormat>
        <label>Defect Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Finding Defects</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>SQX_Part__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Specification__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Actual__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>UPDATEDBY_USER</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>SQX_Part__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Specification__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Actual__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>UPDATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SQX_Part__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Specification__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Actual__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>UPDATEDBY_USER</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>SQX_Part__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Specification__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Actual__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>UPDATEDBY_USER</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>Cost_Cannot_Be_Negative</fullName>
        <active>true</active>
        <description>Restrict negative values for cost</description>
        <errorConditionFormula>Cost__c  &lt; 0</errorConditionFormula>
        <errorDisplayField>Cost__c</errorDisplayField>
        <errorMessage>Please enter positive value</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Defect_Code_is_required</fullName>
        <active>true</active>
        <description>Ensures that either defect category and code is provided.</description>
        <errorConditionFormula>AND(ISBLANK( SQX_Defect_Code__c ), OR(ISBLANK( Defect_Category__c), ISBLANK( Defect_Code__c)))</errorConditionFormula>
        <errorMessage>Please enter a valid defect code and category, using either the lookup or text fields.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Defective_Quantity_Cannot_Be_Negative</fullName>
        <active>true</active>
        <description>Restrict negative values for defective quantity</description>
        <errorConditionFormula>Defective_Quantity__c &lt; 0</errorConditionFormula>
        <errorDisplayField>Defective_Quantity__c</errorDisplayField>
        <errorMessage>Please enter positive value</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
