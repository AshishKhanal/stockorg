<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object records site impacted by the defects/ discrepancies.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Business_Unit__c</fullName>
        <description>It indicated the business unit of the impacted site.</description>
        <externalId>false</externalId>
        <label>Business Unit</label>
        <length>80</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Department__c</fullName>
        <description>It indicated the business unit of the impacted site.</description>
        <externalId>false</externalId>
        <label>Department</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Division__c</fullName>
        <description>It indicated the business unit of the impacted site.</description>
        <externalId>false</externalId>
        <label>Division</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Operation__c</fullName>
        <description>It indicated the operation of the impacted site.</description>
        <externalId>false</externalId>
        <label>Operation</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Production_Line__c</fullName>
        <description>It indicated the Production Line of the impacted site.</description>
        <externalId>false</externalId>
        <label>Production Line</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SQX_Finding__c</fullName>
        <description>It indicates the Finding to which the impacted site are associated with.</description>
        <externalId>false</externalId>
        <label>Finding</label>
        <referenceTo>SQX_Finding__c</referenceTo>
        <relationshipLabel>Impacted Sites</relationshipLabel>
        <relationshipName>SQX_Impacted_Sites</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Site__c</fullName>
        <description>It indicated the impacted site.</description>
        <externalId>false</externalId>
        <label>Site</label>
        <length>80</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Impacted Site</label>
    <nameField>
        <description>Auto number used for uniquely identifying the Impacted Site</description>
        <displayFormat>{000000000}</displayFormat>
        <label>Record Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Impacted Sites</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>SQX_Finding__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Business_Unit__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Department__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Division__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Operation__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Production_Line__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Site__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SQX_Finding__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Business_Unit__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Department__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Division__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Operation__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Production_Line__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Site__c</lookupPhoneDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Allow_adding_findings_not_closed</fullName>
        <active>true</active>
        <description>It ensures that a Finding cannot be added when it is closed.</description>
        <errorConditionFormula>ISPICKVAL(SQX_Finding__r.Status__c, &apos;Closed&apos;)</errorConditionFormula>
        <errorMessage>Finding is already closed.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Allow_adding_to_findings_open_or_draft</fullName>
        <active>false</active>
        <description>It ensures that a Impacted Site could only be added to Finding with states 'Open' or 'Draft'.</description>
        <errorConditionFormula>NOT(OR( ISPICKVAL(SQX_Finding__r.Status__c, &apos;Open&apos;),ISPICKVAL(SQX_Finding__r.Status__c, &apos;Draft&apos;)))</errorConditionFormula>
        <errorMessage>Finding is already completed or closed.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
