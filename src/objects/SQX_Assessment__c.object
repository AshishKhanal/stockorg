<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <content>SQX_Clone_Assessment</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>SQX_Assessment_Editor</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <content>SQX_Assessment_Editor</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object records all assessment setups for an organization.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fieldSets>
        <fullName>Assessment_Information</fullName>
        <availableFields>
            <field>Days_between_Retake__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Number_Of_Retakes_Allowed__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Randomize__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Show_Correct_Answers_On_Completion__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Total_Questions__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <description>used in the launch screen of the take assessment.</description>
        <displayedFields>
            <field>Name</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Description__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Total_Questions_To_Ask__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Passing_Percentage__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Assessment Information</label>
    </fieldSets>
    <fieldSets>
        <fullName>Assessment_Information_SCORM_Type</fullName>
        <availableFields>
            <field>Approval_Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>OwnerId</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <availableFields>
            <field>Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <description>used in the launch screen of the take assessment for SCORM record type.</description>
        <displayedFields>
            <field>Name</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Description__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Assessment Information SCORM Type</label>
    </fieldSets>
    <fieldSets>
        <fullName>Edit_Assessment_SCORM_Type</fullName>
        <availableFields>
            <field>Approval_Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </availableFields>
        <description>This fieldset is used when SCORM type assessment is edited.</description>
        <displayedFields>
            <field>Description__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Number_Of_Retakes_Allowed__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Days_between_Retake__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Edit Assessment - SCORM Type</label>
    </fieldSets>
    <fieldSets>
        <fullName>New_Assessment_CQ_Type</fullName>
        <description>Used when creating new assessment for CQ Record Type</description>
        <displayedFields>
            <field>Description__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Total_Questions_To_Ask__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Passing_Percentage__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Number_Of_Retakes_Allowed__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Days_between_Retake__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Randomize__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Show_Correct_Answers_On_Completion__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>New Assessment - CQ Type</label>
    </fieldSets>
    <fieldSets>
        <fullName>New_Assessment_SCORM_Type</fullName>
        <description>Used when creating new assessment for SCORM Record Type</description>
        <displayedFields>
            <field>Description__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Number_Of_Retakes_Allowed__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Days_between_Retake__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>New Assessment - SCORM Type</label>
    </fieldSets>
    <fields>
        <fullName>Approval_Status__c</fullName>
        <description>This fields records the Approval Status of the Assessment.</description>
        <externalId>false</externalId>
        <label>Approval Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>In Approval</fullName>
                    <default>false</default>
                    <label>In Approval</label>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                    <label>Approved</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Days_between_Retake__c</fullName>
        <defaultValue>0</defaultValue>
        <description>This fields records the value that specifies number of days users have to wait to retake the assessment from the previous assessment.</description>
        <externalId>false</externalId>
        <label>Days between Retake</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>This fields records Description of the created assessment.</description>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Is_Locked__c</fullName>
        <description>locks the assessment if status is either pre-release or current or pre-expire</description>
        <externalId>false</externalId>
        <formula>OR(
   ISPICKVAL(Status__c, &apos;Pre-Release&apos;),
   ISPICKVAL(Status__c, &apos;Current&apos;),
   ISPICKVAL(Status__c, &apos;Pre-Expire&apos;),
   ISPICKVAL(Status__c, &apos;Obsolete&apos;)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Is Locked</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Number_Of_Retakes_Allowed__c</fullName>
        <defaultValue>0</defaultValue>
        <description>This fields records the value that specifies that how many times a user can retakes the assessment.</description>
        <externalId>false</externalId>
        <label>Number Of Retakes Allowed</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Passing_Percentage__c</fullName>
        <defaultValue>0</defaultValue>
        <description>This fields records passing percentage of an assessment.</description>
        <externalId>false</externalId>
        <label>Passing Percentage</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Randomize__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field shows whether to ask question in random order.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, Assessment questions will be in random order.</inlineHelpText>
        <label>Randomize</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Scorm_Cloud_Content_Status__c</fullName>
        <description>DEPRECATED - Out of sync, if the assessment is not in sync with the content uploaded to the cloud.</description>
        <externalId>false</externalId>
        <label>SCORM Cloud Content Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Out of Sync</fullName>
                    <default>true</default>
                    <label>Out of Sync</label>
                </value>
                <value>
                    <fullName>In Sync</fullName>
                    <default>false</default>
                    <label>In Sync</label>
                </value>
                <value>
                    <fullName>Pending</fullName>
                    <default>false</default>
                    <label>Pending</label>
                </value>
                <value>
                    <fullName>Failed</fullName>
                    <default>false</default>
                    <label>Failed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Scorm_Content_Reference__c</fullName>
        <description>Holds the content document id of the SCORM assessment uploaded</description>
        <externalId>false</externalId>
        <label>SCORM Content</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Show_Correct_Answers_On_Completion__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field specifies that whether to show answers of asked questions on completion of assessment or not.</description>
        <externalId>false</externalId>
        <label>Show Correct Answers On Completion</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>This fields records Status of the Assessment.</description>
        <externalId>false</externalId>
        <label>Status</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Draft</fullName>
                    <default>true</default>
                    <label>Draft</label>
                </value>
                <value>
                    <fullName>Pre-Release</fullName>
                    <default>false</default>
                    <label>Pre-Release</label>
                </value>
                <value>
                    <fullName>Current</fullName>
                    <default>false</default>
                    <label>Current</label>
                </value>
                <value>
                    <fullName>Pre-Expire</fullName>
                    <default>false</default>
                    <label>Pre-Expire</label>
                </value>
                <value>
                    <fullName>Obsolete</fullName>
                    <default>false</default>
                    <label>Obsolete</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Total_Questions_To_Ask__c</fullName>
        <defaultValue>0</defaultValue>
        <description>This fields records total number of question which should be asked in the Assessment.</description>
        <externalId>false</externalId>
        <label>Total Questions To Ask</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Questions__c</fullName>
        <description>This fields records total number of the question in the given Assessment.</description>
        <externalId>false</externalId>
        <label>Total Questions</label>
        <summaryForeignKey>SQX_Assessment_Question__c.SQX_Assessment__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Uniqueness_Constraint__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>This field is an internal field used to enforce the uniqueness constraint among various field in the object.</description>
        <externalId>false</externalId>
        <label>Uniqueness Constraint</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Assessment</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <columns>Randomize__c</columns>
        <columns>Total_Questions__c</columns>
        <columns>Total_Questions_To_Ask__c</columns>
        <columns>Passing_Percentage__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <description>The name field to identify assessment in SF</description>
        <label>Assessment Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Assessments</pluralLabel>
    <recordTypes>
        <fullName>CQ_Assessment</fullName>
        <active>true</active>
        <description>CQ hosted assessment type</description>
        <label>CQ Assessment</label>
        <picklistValues>
            <picklist>Approval_Status__c</picklist>
            <values>
                <fullName>Approved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>In Approval</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Rejected</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Scorm_Cloud_Content_Status__c</picklist>
            <values>
                <fullName>In Sync</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Out of Sync</fullName>
                <default>true</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Current</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Draft</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Obsolete</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pre-Expire</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pre-Release</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>SCORM_Assessment</fullName>
        <active>true</active>
        <description>SCORM content as assessment</description>
        <label>SCORM Assessment</label>
        <picklistValues>
            <picklist>Approval_Status__c</picklist>
            <values>
                <fullName>Approved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>In Approval</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Rejected</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Scorm_Cloud_Content_Status__c</picklist>
            <values>
                <fullName>Failed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>In Sync</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Out of Sync</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Current</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Draft</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Obsolete</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pre-Expire</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pre-Release</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Randomize__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Total_Questions__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Total_Questions_To_Ask__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Passing_Percentage__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Show_Correct_Answers_On_Completion__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>Assessment_Can_Not_Back_to_Draft</fullName>
        <active>true</active>
        <description>Once the Status of Assessment is higher than draft it cannot be set back to draft.</description>
        <errorConditionFormula>AND(NOT(ISNEW()),ISPICKVAL(Status__c,&quot;Draft&quot;),NOT(ISPICKVAL(PRIORVALUE(Status__c),&quot;Draft&quot;)))</errorConditionFormula>
        <errorMessage>Assessment Status cannot set back to draft from other statuses.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Ensure_Field_Value_Limit_Not_Excedded</fullName>
        <active>true</active>
        <description>Total Question To Ask, Days between Retakes and Number of Retakes Allowed cannot have value less than zero.</description>
        <errorConditionFormula>OR(Total_Questions_To_Ask__c &lt; 0,
   Days_between_Retake__c &lt; 0,
   Number_Of_Retakes_Allowed__c &lt; 0)</errorConditionFormula>
        <errorMessage>Total Question To Ask, Days between Retakes and Number of Retakes Allowed cannot have value less than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Ensure_Passing_Percentage_Value_Is_Valid</fullName>
        <active>true</active>
        <description>Passing Percentage cannot have value less than zero and greater than 100.</description>
        <errorConditionFormula>OR(Passing_Percentage__c &lt; 0.0,
Passing_Percentage__c &gt; 1.0)</errorConditionFormula>
        <errorMessage>Passing Percentage cannot have value less than zero and greater than 100.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Ensure_that_content_is_in_sync</fullName>
        <active>false</active>
        <description>DEPRECATED - This rule ensures that for SCORM Assessments content is synchronized before changing the assessment status</description>
        <errorConditionFormula>AND(
RecordType.DeveloperName == &apos;SCORM_Assessment&apos;,
ISCHANGED(Status__c),
NOT(ISPICKVAL(Status__c, &apos;Draft&apos;)),
NOT(ISPICKVAL(Scorm_Cloud_Content_Status__c,&apos;In Sync&apos;)
))</errorConditionFormula>
        <errorMessage>Content not synchronized with SCORM Cloud Content. Please upload the content to SCORM player before changing the status.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Locked_Assessment_Cannot_Be_Altered</fullName>
        <active>true</active>
        <description>Only status of assessment is editable once it is locked.</description>
        <errorConditionFormula>AND(NOT($Permission.SQXDataMigrationCustomPermission),AND(
    PRIORVALUE(Is_Locked__c) == true,
    OR(
        ISCHANGED(Days_between_Retake__c) , 
        ISCHANGED(Description__c) ,
        ISCHANGED(Number_Of_Retakes_Allowed__c) ,
        ISCHANGED(Passing_Percentage__c) ,
        ISCHANGED(Randomize__c) ,
        ISCHANGED(Show_Correct_Answers_On_Completion__c) ,
        ISCHANGED(Total_Questions__c) ,
        ISCHANGED(Total_Questions_To_Ask__c) ,
        ISCHANGED(Name)
    )
))</errorConditionFormula>
        <errorMessage>Assessment is locked. Only status can be updated.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Passing_Percentage_Is_Required</fullName>
        <active>true</active>
        <description>Rule to validate that passing percentage has been set for CQ type Assessments</description>
        <errorConditionFormula>AND(RecordType.DeveloperName == &apos;CQ_Assessment&apos;, ISNULL( Passing_Percentage__c ))</errorConditionFormula>
        <errorDisplayField>Passing_Percentage__c</errorDisplayField>
        <errorMessage>Field cannot be blank</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Qn_2_Ask_Is_Less_or_Equal_2_Total_Qn</fullName>
        <active>true</active>
        <description>Question to ask should be less or equal than total question.</description>
        <errorConditionFormula>Total_Questions_To_Ask__c &gt;  Total_Questions__c</errorConditionFormula>
        <errorMessage>Total Questions To Ask should be less or equal to Total Questions.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Total_Questions_To_Ask_Is_Required</fullName>
        <active>true</active>
        <description>Rule to check if total questions to ask field has been set for CQ type Assessment</description>
        <errorConditionFormula>AND(RecordType.DeveloperName == &apos;CQ_Assessment&apos;, ISNULL(Total_Questions_To_Ask__c))</errorConditionFormula>
        <errorDisplayField>Total_Questions_To_Ask__c</errorDisplayField>
        <errorMessage>Field cannot be blank</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Preview_Assessment</fullName>
        <availability>online</availability>
        <description>This button is used to open preview assessment.</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Preview Assessment</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR(&quot;/apex/compliancequest__SQX_Assessment_Preview&quot;, null,
 [astId=SQX_Assessment__c.Id,mode=&apos;preview&apos; ])}</url>
    </webLinks>
</CustomObject>
