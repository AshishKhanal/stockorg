<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object stores null value information for Suspect Product record</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>C10_Event_Reappeared__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>C10. Event Reappear After Reintroduction</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C1_Approval_Number__c</fullName>
        <description>This field records the null value for Approval Number</description>
        <externalId>false</externalId>
        <label>Approval Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C1_Drug_Type__c</fullName>
        <description>This field records the null value for Drug Type</description>
        <externalId>false</externalId>
        <label>Drug Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C1_If_IND_Give_Protocol_Number__c</fullName>
        <description>This field records the null value for Protocol Number if the drug type was selected IND</description>
        <externalId>false</externalId>
        <label>If IND, Give Protocol #</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C1_Lot_Number__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>Lot #</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C1_Manufacturer_or_Compounder__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>Manufacturer/Compounder</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C1_NDC_Number_Unique_Id__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>NDC # or Unique ID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C1_Name_and_Strength__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>Name and Strength</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C2_Concomitant_Medical_Products__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>C2. Concomitant Medical Products</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C2_Therapy_Date__c</fullName>
        <description>This field records the null value for Therapy Date</description>
        <externalId>false</externalId>
        <label>Therapy Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C3_Dose_Unit__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>Dose Unit</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C3_Dose__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>Dose</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C3_Frequency_Unit__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>Frequency Unit</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C3_Frequency__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>Frequency</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C3_Number_of_separate_dosages__c</fullName>
        <description>This field records the null value for number of separate dosages</description>
        <externalId>false</externalId>
        <label>Number of separate dosages</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C3_Route_Used__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>Route Used</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C4_Therapy_Start_Date__c</fullName>
        <description>This field records the null value for Therapy Start Date</description>
        <externalId>false</externalId>
        <label>C4. Therapy Dates - Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C4_Therapy_Stop_Date__c</fullName>
        <description>This field records null value for Therapy Stop Date</description>
        <externalId>false</externalId>
        <label>Therapy Dates - Stop Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C5_Diagnosis_For_Use__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>C5. Diagnosis for Use</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C6_Is_the_Product_Compounded__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>C6. Is the Product Compounded?</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C7_Is_the_Product_Over_the_Counter__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>C7. Is the Product Over-the-Counter?</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C8_Expiration_Date__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>C8. Expiration Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>C9_Event_Abated__c</fullName>
        <description>This field records null values</description>
        <externalId>false</externalId>
        <label>C9. Event Abated After Use Stopped?</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>SQX_FDA_Null_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>SQX_Medwatch_Suspect_Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field records the child medwatch suspect product record</description>
        <externalId>false</externalId>
        <label>Suspect Product</label>
        <referenceTo>SQX_Medwatch_Suspect_Product__c</referenceTo>
        <relationshipLabel>Medwatch Suspect Product</relationshipLabel>
        <relationshipName>SQX_Medwatch_Suspect_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Suspect Product Null Value</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <description>This field holds the record number for Suspect Product Null value</description>
        <displayFormat>NV-{000000}</displayFormat>
        <label>Record Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Suspect Product Null Values</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Prevent_Edit_After_Successful_Submission</fullName>
        <active>true</active>
        <description>This rule prevents user from editing this record once the related Suspect Product record is locked after the parent medwatch record has been successfully acknowledged.</description>
        <errorConditionFormula>SQX_Medwatch_Suspect_Product__r.SQX_Medwatch__r.Is_Locked__c</errorConditionFormula>
        <errorMessage>Record locked. Cannot perform the desired action</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
