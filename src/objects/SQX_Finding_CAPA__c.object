<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Records links between Finding and CAPA</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>CAPA_Status__c</fullName>
        <description>Status of CAPA</description>
        <externalId>false</externalId>
        <formula>TEXT( SQX_CAPA__r.Status__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CAPA Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CAPA_Title__c</fullName>
        <description>Title of CAPA</description>
        <externalId>false</externalId>
        <formula>SQX_CAPA__r.Title__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CAPA Title</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comment__c</fullName>
        <description>Purpose of Reference</description>
        <externalId>false</externalId>
        <label>Comments</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Finding_Stage__c</fullName>
        <description>Stage of Finding</description>
        <externalId>false</externalId>
        <formula>TEXT( SQX_Finding__r.Stage__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Finding Stage</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Finding_Status__c</fullName>
        <description>Status of Finding</description>
        <externalId>false</externalId>
        <formula>TEXT(SQX_Finding__r.Status__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Finding Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Finding_Title__c</fullName>
        <description>Title of Finding</description>
        <externalId>false</externalId>
        <formula>SQX_Finding__r.Title__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Finding Title</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Resolves_by_CAPA__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Finding is Resolved by CAPA</description>
        <externalId>false</externalId>
        <label>Resolves by CAPA</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SQX_CAPA__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Refers to the CAPA</description>
        <externalId>false</externalId>
        <label>CAPA</label>
        <referenceTo>SQX_CAPA__c</referenceTo>
        <relationshipLabel>CAPA Findings</relationshipLabel>
        <relationshipName>SQX_Finding_CAPAs</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Finding__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Refers to Finding</description>
        <externalId>false</externalId>
        <label>Finding</label>
        <referenceTo>SQX_Finding__c</referenceTo>
        <relationshipLabel>CAPA Findings</relationshipLabel>
        <relationshipName>SQX_Finding_CAPAs</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Unique_Finding_Reference__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Unique Finding Reference From CAPA</description>
        <externalId>false</externalId>
        <label>Unique Finding Reference</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>CAPA Finding</label>
    <nameField>
     <description>Auto number used for uniquely identifying the Finding CAPA</description>
        <displayFormat>FCR-{000000}</displayFormat>
        <label>Record Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>CAPA Findings</pluralLabel>
    <searchLayouts/>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>Ineffective_closed_CAPA_cannot_be_linked</fullName>
        <active>true</active>
        <description>Ineffective closed Capa cannot be referenced to Finding</description>
        <errorConditionFormula>AND(
    OR(ISNEW(), ISCHANGED(SQX_CAPA__c)),
    ISPICKVAL(SQX_CAPA__r.Status__c, &apos;Closed&apos;),
    NOT ISPICKVAL(SQX_CAPA__r.Resolution__c, &apos;Effective&apos;)
)</errorConditionFormula>
        <errorMessage>Ineffective closed Capa cannot be referenced to Finding</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_Removal_of_Reference</fullName>
        <active>true</active>
        <description>Prevent removal of CAPA Finding Reference if Finding is in CAPA Link Approval Stage</description>
        <errorConditionFormula>AND(
         ISCHANGED( Resolves_by_CAPA__c ),
        Resolves_by_CAPA__c == false,
         ISPICKVAL( SQX_Finding__r.Stage__c , &apos;CAPA Link Approval&apos;) 
        )</errorConditionFormula>
        <errorMessage>Finding CAPA reference cannot be removed if Finding is in CAPA Link Approval Stage</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
