<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object to define the characteristics needed in the part, part family or process.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Characteristics__c</fullName>
        <description>Records the description of the part, part family or process specifications.</description>
        <externalId>false</externalId>
        <label>Characteristics</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Impact__c</fullName>
        <description>Records the impact of the part, part family or process that is to be inspected.</description>
        <externalId>false</externalId>
        <label>Impact</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Low</fullName>
                    <default>false</default>
                    <label>Low</label>
                </value>
                <value>
                    <fullName>Medium</fullName>
                    <default>false</default>
                    <label>Medium</label>
                </value>
                <value>
                    <fullName>High</fullName>
                    <default>false</default>
                    <label>High</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Inspection_Method__c</fullName>
        <description>Records the details of the inspection method to be used.</description>
        <externalId>false</externalId>
        <label>Inspection Method</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Lower_Spec_Limit__c</fullName>
        <description>Records the minimum requirement of the specification.</description>
        <externalId>false</externalId>
        <label>Lower Spec Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>6</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Measurement_Standard__c</fullName>
        <description>Records the standard in which measurement of part is done.</description>
        <externalId>false</externalId>
        <label>Measurement Standard</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SQX_Equipment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Records the equipment used  for inspection.</description>
        <externalId>false</externalId>
        <label>Equipment</label>
        <referenceTo>SQX_Equipment__c</referenceTo>
        <relationshipLabel>Specifications</relationshipLabel>
        <relationshipName>SQX_Specifications</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Inspection_Criteria__c</fullName>
        <description>Records the inspection criteria document related to the specifications.</description>
        <externalId>false</externalId>
        <label>Inspection Criteria</label>
        <referenceTo>SQX_Controlled_Document__c</referenceTo>
        <relationshipLabel>Specifications</relationshipLabel>
        <relationshipName>SQX_Specifications</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Specification__c</fullName>
        <description>Records the specification detail of the specification.</description>
        <externalId>false</externalId>
        <label>Specification</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Specification_Number__c</fullName>
        <description>Field to sort specification.</description>
        <externalId>false</externalId>
        <label>Specification Number</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Upper_Spec_Limit__c</fullName>
        <description>Records the highest limit to be tolerated.</description>
        <externalId>false</externalId>
        <label>Upper Spec Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>6</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Specification</label>
    <nameField>
        <description>Auto number used for uniquely identifying the Specification</description>
        <displayFormat>SPN-{000000}</displayFormat>
        <label>Record Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Specifications</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Ensure_Characteristics_is_entered</fullName>
        <active>true</active>
        <description>This rule ensures that the characteristics is entered</description>
        <errorConditionFormula>ISBLANK( Characteristics__c )</errorConditionFormula>
        <errorDisplayField>Characteristics__c</errorDisplayField>
        <errorMessage>Characteristics is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>USL_should_be_greater_than_LSL</fullName>
        <active>true</active>
        <description>This rule ensures that USL should be greater than LSL.</description>
        <errorConditionFormula>Upper_Spec_Limit__c  &lt;  Lower_Spec_Limit__c</errorConditionFormula>
        <errorMessage>Upper Spec Limit should be greater than or equal to Lower Spec Limit.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
