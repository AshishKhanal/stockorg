<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object stores information of the cause of defects or discrepancies.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Description__c</fullName>
        <description>It is the description of the Root Cause codes.</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <label>Root Cause Code</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Description__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <description>Identifies the name of root cause code</description>
        <label>Root Cause Code Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Root Cause Codes</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Description__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>UPDATEDBY_USER</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>UPDATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Description__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>UPDATEDBY_USER</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>Description__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>UPDATEDBY_USER</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>RootCauseCodeShouldBeUnique</fullName>
        <active>true</active>
        <description>This rule ensures that duplicate Root Cause Code Name is not created.</description>
        <errorConditionFormula>UPPER( VLOOKUP(
 $ObjectType.SQX_Root_Cause_Code__c.Fields.Name,
 $ObjectType.SQX_Root_Cause_Code__c.Fields.Name,
 Name))= UPPER(Name)
 &amp;&amp;
 ( ISNEW() || PRIORVALUE(Name) &lt;&gt; Name)</errorConditionFormula>
        <errorMessage>Root Cause Code Name is already present in the System.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
