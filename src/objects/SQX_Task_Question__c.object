<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override updated by Lightning App Builder during activation.</comment>
        <content>CQ_Question</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>CQ_Question_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>CQ_Question_Compact_Layout</fullName>
        <fields>Name</fields>
        <fields>CQ_Task_Name__c</fields>
        <fields>Answer_Type__c</fields>
        <label>CQ Question Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object stores the question for the Task record. Questions are used in objects such as Decision Tree and PHR. This object acts as a library for defining various Questions.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <fields>
        <fullName>Answer_Type__c</fullName>
        <description>Type of answer required for question</description>
        <externalId>false</externalId>
        <label>Answer Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Options - Radio Button</fullName>
                    <default>false</default>
                    <label>Options - Radio Button</label>
                </value>
                <value>
                    <fullName>Options - Picklist</fullName>
                    <default>false</default>
                    <label>Options - Picklist</label>
                </value>
                <value>
                    <fullName>Picklist Values</fullName>
                    <default>false</default>
                    <label>Picklist Values</label>
                </value>
                <value>
                    <fullName>Text Input</fullName>
                    <default>false</default>
                    <label>Text Input</label>
                </value>
                <value>
                    <fullName>Date Input</fullName>
                    <default>false</default>
                    <label>Date Input</label>
                </value>
                <value>
                    <fullName>DateTime Input</fullName>
                    <default>false</default>
                    <label>DateTime Input</label>
                </value>
                <value>
                    <fullName>Number Input</fullName>
                    <default>false</default>
                    <label>Number Input</label>
                </value>
                <value>
                    <fullName>Custom Component</fullName>
                    <default>false</default>
                    <label>Custom Component</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Allow_Comment__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Determines if comment is visible</description>
        <externalId>false</externalId>
        <label>Allow Comment</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>CQ_Task_Name__c</fullName>
        <description>CQ Task Name</description>
        <externalId>false</externalId>
        <formula>SQX_Task__r.Name</formula>
        <label>CQ Task Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Custom_Component_Name__c</fullName>
        <description>Name with namespace prefix of custom component to render to collect answer</description>
        <externalId>false</externalId>
        <inlineHelpText>Custom Lightning Component used to collect answer</inlineHelpText>
        <label>Custom Component Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Guide__c</fullName>
        <description>Instructions for the question</description>
        <externalId>false</externalId>
        <label>Guide</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>QuestionText__c</fullName>
        <description>This field stores the  actual question that is presented to the user.</description>
        <externalId>false</externalId>
        <label>Question Text</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Picklist_Category__c</fullName>
        <description>Category of picklist value to use for filter if answer type is picklist</description>
        <externalId>false</externalId>
        <inlineHelpText>Picklist category to user for filter if answer type is picklist</inlineHelpText>
        <label>Picklist Category</label>
        <length>120</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SQX_Next_Question__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Next question to be asked after this.</description>
        <externalId>false</externalId>
        <inlineHelpText>For answer type other than &quot;Options&quot; specify next question to ask. For options next question is determined by the answer chosen</inlineHelpText>
        <label>Next Question</label>
        <referenceTo>SQX_Task_Question__c</referenceTo>
        <relationshipLabel>Questions</relationshipLabel>
        <relationshipName>SQX_Questions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Result_Type__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The type of result that the question requires. It could be things like Yes/No etc. that has been provided in the Result Type library.</description>
        <externalId>false</externalId>
        <label>Result Type</label>
        <referenceTo>SQX_Result_Type__c</referenceTo>
        <relationshipLabel>Questions</relationshipLabel>
        <relationshipName>SQX_Questions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Task__c</fullName>
        <description>This field records the task for which this question is intended. The task could be of any kind such as decision tree task or PHR.</description>
        <externalId>false</externalId>
        <label>Task</label>
        <referenceTo>SQX_Task__c</referenceTo>
        <relationshipLabel>Questions</relationshipLabel>
        <relationshipName>SQX_Task_Question</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Question</label>
    <nameField>
        <description>The name field to identify Task Questions in SF</description>
        <label>Question Number</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Questions</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Dynamic_Question</fullName>
        <active>true</active>
        <description>This record type is for questions that are to be used in dynamic questionnaire, where the next question depends on the answer given by the user. Example. Decision Tree Based Questionnaire</description>
        <label>Dynamic Question</label>
        <picklistValues>
            <picklist>Answer_Type__c</picklist>
            <values>
                <fullName>Custom Component</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Date Input</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>DateTime Input</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Number Input</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Options - Picklist</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Options - Radio Button</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Picklist Values</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Text Input</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Simple_Question</fullName>
        <active>true</active>
        <description>This record type is for questions that are to be used in simple questionnaire, where sequence of question doesn&#39;t depend on the answer given by the user. Example. PHR Checklist</description>
        <label>Simple Question</label>
        <picklistValues>
            <picklist>Answer_Type__c</picklist>
            <values>
                <fullName>Custom Component</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Date Input</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>DateTime Input</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Number Input</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Options - Picklist</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Options - Radio Button</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Picklist Values</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Text Input</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>QuestionText__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>QuestionText__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>QuestionText__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchResultsAdditionalFields>QuestionText__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Custom_Cmp_Name_required_for_Custom_Cmp</fullName>
        <active>true</active>
        <description>Rule to ensure custom component name is required for Custom Component</description>
        <errorConditionFormula>AND(ISPICKVAL( Answer_Type__c ,&apos;Custom Component&apos; ), ISBLANK(  Custom_Component_Name__c ))</errorConditionFormula>
        <errorDisplayField>Custom_Component_Name__c</errorDisplayField>
        <errorMessage>Custom Component Name is required for Custom Component Answer Type</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>No_Next_Question_is_selected_for_Options</fullName>
        <active>true</active>
        <description>Rule to prevent selecting next question when answer type is Options</description>
        <errorConditionFormula>AND(
OR(
ISPICKVAL(Answer_Type__c, &apos;Options - Radio Button&apos;), 
ISPICKVAL(Answer_Type__c, &apos;Options - Picklist&apos;)), 
NOT(ISBLANK( SQX_Next_Question__c )))</errorConditionFormula>
        <errorDisplayField>SQX_Next_Question__c</errorDisplayField>
        <errorMessage>Next Question cannot be selected for Options Answer Type</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Picklist_Category_required_for_Values</fullName>
        <active>true</active>
        <description>Rule to ensure Picklist Category is required for Picklist Values</description>
        <errorConditionFormula>AND(ISPICKVAL( Answer_Type__c ,&apos;Picklist Values&apos; ), ISBLANK( Picklist_Category__c ))</errorConditionFormula>
        <errorDisplayField>Picklist_Category__c</errorDisplayField>
        <errorMessage>Picklist Category is required for Picklist Values Answer Type</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
