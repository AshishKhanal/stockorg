<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Audit report object records information related to reports generated/uploaded for a particular Audit. The attachment of the audit report object is of particular importance because it contains the actual generated/uploaded report. The report document contains the snapshot of the audit&apos;s information.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Approval_Status__c</fullName>
        <description>Approval Status of Audit Report</description>
        <externalId>false</externalId>
        <label>Approval Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>-</fullName>
                    <default>true</default>
                    <label>-</label>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                    <label>Approved</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SQX_Approver__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Approver for the audit report.</description>
        <externalId>false</externalId>
        <label>Approver</label>
        <referenceTo>User</referenceTo>
        <relationshipName>SQX_Audit_Reports</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Audit__c</fullName>
        <description>The audit that this Report belongs to.</description>
        <externalId>false</externalId>
        <label>Audit</label>
        <referenceTo>SQX_Audit__c</referenceTo>
        <relationshipLabel>Audit Reports</relationshipLabel>
        <relationshipName>SQX_Audit_Reports</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Status of the Audit Report</description>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Draft</fullName>
                    <default>true</default>
                    <label>Draft</label>
                </value>
                <value>
                    <fullName>Complete</fullName>
                    <default>false</default>
                    <label>Complete</label>
                </value>
                <value>
                    <fullName>In Approval</fullName>
                    <default>false</default>
                    <label>In Approval</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Audit Report</label>
    <nameField>
        <description>Auto number used for uniquely identifying the audit report</description>
        <displayFormat>ARP-{000000}</displayFormat>
        <label>Audit Report Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Audit Reports</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Audit_Report_cannot_be_created</fullName>
        <active>true</active>
        <description>When Audit status is not In Progress Audit Report cannot be added</description>
        <errorConditionFormula>/*
  Prevent creation of new audit report when
  a) IsNew() -&gt; new report is being created
  b) Audit.Status is not in progress
*/
AND(
  ISNEW()
, NOT(ISPICKVAL( SQX_Audit__r.Stage__c , &apos;In Progress&apos;))
)</errorConditionFormula>
        <errorMessage>Audit Report can only be created, when Audit is In Progress.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
