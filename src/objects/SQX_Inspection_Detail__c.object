<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Records the details of the inspection to be done in part/process.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Characteristics__c</fullName>
        <description>Records the characteristics copied from related criteria specifications.</description>
        <externalId>false</externalId>
        <label>Characteristics</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Comment__c</fullName>
        <description>Records the comment for the inspection detail.</description>
        <externalId>false</externalId>
        <label>Comment</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Defective_Quantity__c</fullName>
        <description>Records the number of parts that failed the inspection.</description>
        <externalId>false</externalId>
        <label>Defective Quantity</label>
        <precision>14</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Equipment_Calibrated__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Records if the used equipment is calibrated or not.</description>
        <externalId>false</externalId>
        <label>Equipment Calibrated ?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Immediate_Action__c</fullName>
        <description>Action required for the failed inspection detail</description>
        <externalId>false</externalId>
        <label>Immediate Action</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Impact__c</fullName>
        <description>Records the impact copied from related criteria specifications.</description>
        <externalId>false</externalId>
        <label>Impact</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Low</fullName>
                    <default>false</default>
                    <label>Low</label>
                </value>
                <value>
                    <fullName>Medium</fullName>
                    <default>false</default>
                    <label>Medium</label>
                </value>
                <value>
                    <fullName>High</fullName>
                    <default>false</default>
                    <label>High</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Inspection_Method__c</fullName>
        <description>Records the inspection method copied from related criteria specifications.</description>
        <externalId>false</externalId>
        <label>Inspection Method</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Lower_Spec_Limit__c</fullName>
        <description>Records the lower spec limit copied from related criteria specifications.</description>
        <externalId>false</externalId>
        <label>Lower Spec Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>6</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Measurement_Standard__c</fullName>
        <description>Records the measurement standard copied from related criteria specifications.</description>
        <externalId>false</externalId>
        <label>Measurement Standard</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Need_Followup__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Set to true if inspection detail fails.</description>
        <externalId>false</externalId>
        <label>Need Followup</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Number_of_Defects__c</fullName>
        <description>Records the number of defects parts found in the inspection.</description>
        <externalId>false</externalId>
        <label>Number of Defects</label>
        <precision>14</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Observation__c</fullName>
        <description>Observed value during inspection</description>
        <externalId>false</externalId>
        <label>Observation</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Observed_Value__c</fullName>
        <description>Records the value of the part observed at the time of inspection.</description>
        <externalId>false</externalId>
        <label>Observed Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>6</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Result__c</fullName>
        <description>Records the result of the inspection.</description>
        <externalId>false</externalId>
        <label>Result</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>true</sorted>
                <value>
                    <fullName>Fail</fullName>
                    <default>false</default>
                    <label>Fail</label>
                </value>
                <value>
                    <fullName>Pass</fullName>
                    <default>false</default>
                    <label>Pass</label>
                </value>
                <value>
                    <fullName>Skip</fullName>
                    <default>false</default>
                    <label>Skip</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SQX_Equipment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Records the Equipment  copied from related criteria specifications.</description>
        <externalId>false</externalId>
        <label>Equipment Used</label>
        <referenceTo>SQX_Equipment__c</referenceTo>
        <relationshipLabel>Inspection Details</relationshipLabel>
        <relationshipName>SQX_Inspection_Details</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SQX_Inspection__c</fullName>
        <description>Records the inspection which should have this inspection details.</description>
        <externalId>false</externalId>
        <label>Inspection</label>
        <referenceTo>SQX_Inspection__c</referenceTo>
        <relationshipLabel>Inspection Details</relationshipLabel>
        <relationshipName>SQX_Inspection_Details</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>SQX_Specification__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Records the specification from which its detail is copied.</description>
        <externalId>false</externalId>
        <label>Specification Id</label>
        <referenceTo>SQX_Specification__c</referenceTo>
        <relationshipLabel>Inspection Details</relationshipLabel>
        <relationshipName>SQX_Inspection_Details</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Sample_Size__c</fullName>
        <description>Records the sample that are inspected.</description>
        <externalId>false</externalId>
        <label>Sample Size</label>
        <precision>14</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Specification__c</fullName>
        <description>Records the specification copied from related criteria specifications.</description>
        <externalId>false</externalId>
        <label>Specification</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Upper_Spec_Limit__c</fullName>
        <description>Records the upper spec limit copied from related criteria specifications.</description>
        <externalId>false</externalId>
        <label>Upper Spec Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>6</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Inspection Detail</label>
    <nameField>
        <description>Auto number used for uniquely identifying the Inspection Detail</description>
        <displayFormat>IDN-{000000}</displayFormat>
        <label>Inspection Detail Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Inspection Details</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>Ensure_Characteristics_Is_Provided</fullName>
        <active>true</active>
        <description>This rule ensures that characteristics is provided while creating new inspection detail.</description>
        <errorConditionFormula>ISBLANK( Characteristics__c )</errorConditionFormula>
        <errorDisplayField>Characteristics__c</errorDisplayField>
        <errorMessage>Characteristics is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Ensure_Impact_Is_Provided</fullName>
        <active>true</active>
        <description>This rule ensures that impact is provided while creating new inspection detail.</description>
        <errorConditionFormula>ISBLANK( TEXT(Impact__c) )</errorConditionFormula>
        <errorDisplayField>Impact__c</errorDisplayField>
        <errorMessage>Impact is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_Changing_Transferred_Ins_Details</fullName>
        <active>true</active>
        <description>This rules prevents changing fields transferred from the specification to inspection details.</description>
        <errorConditionFormula>AND(NOT(ISBLANK( SQX_Specification__c )),
OR(
ISCHANGED(Impact__c),
ISCHANGED(Characteristics__c),
ISCHANGED(Inspection_Method__c),
ISCHANGED(Lower_Spec_Limit__c),
ISCHANGED(Measurement_Standard__c),
ISCHANGED(Specification__c),
ISCHANGED(Upper_Spec_Limit__c),
ISCHANGED(SQX_Specification__c )
)
)</errorConditionFormula>
        <errorMessage>Inspection Detail copied from matching inspection criteria&apos;s specification cannot be changed.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>USL_should_be_greater_than_LSL</fullName>
        <active>true</active>
        <description>This rule ensures that USL should be greater than LSL.</description>
        <errorConditionFormula>Upper_Spec_Limit__c &lt; Lower_Spec_Limit__c</errorConditionFormula>
        <errorMessage>Upper Spec Limit should be greater than or equal to Lower Spec Limit.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
