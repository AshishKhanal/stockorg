<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Holds information about Part supplied by supplier.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fieldSets>
        <fullName>Basic_Information</fullName>
        <description>In Account VF</description>
        <displayedFields>
            <field>Part__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Supplier_Reference_Part_Number__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Active__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Approved__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Basic Information</label>
    </fieldSets>
    <fieldSets>
        <fullName>Supplier_Admin_Basic_Information</fullName>
        <description>Basic information of Supplier</description>
        <displayedFields>
            <field>Part_Name__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Supplier_Reference_Part_Number__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Active__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Approved__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Supplier Admin Basic Information</label>
    </fieldSets>
    <fieldSets>
        <fullName>Supplier_Admin_Editable_Fields</fullName>
        <description>Account VF Page</description>
        <displayedFields>
            <field>Supplier_Reference_Part_Number__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Supplier Admin Editable Fields</label>
    </fieldSets>
    <fields>
        <fullName>Account__c</fullName>
        <description>One of the Account/Supplier which supplies the part.</description>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Supplier Parts</relationshipLabel>
        <relationshipName>Supplier_Parts</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>true</defaultValue>
        <description>This field,when checked, indicates that the corresponding Supplier Part is Active.
An active part can be added as a Supplier Part either as Active or Not Active. 
A non-active part can be added as a Supplier Part only as Not Active.</description>
        <externalId>false</externalId>
        <label>Active</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Approved__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field,when checked, indicates that the Part from the particular supplier is Approved.</description>
        <externalId>false</externalId>
        <label>Approved</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Part_Name__c</fullName>
        <description>Indicate the Part Name.</description>
        <externalId>false</externalId>
        <formula>Part__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Part Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Part_Number__c</fullName>
        <description>Cross reference to the part number of part, just to display it in the related list.</description>
        <externalId>false</externalId>
        <formula>Part__r.Part_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Part Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Part_Text__c</fullName>
        <description>where a user can enter a Part Name</description>
        <externalId>false</externalId>
        <label>Part Name</label>
        <length>120</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Part__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Indicates the Part.</description>
        <externalId>false</externalId>
        <label>Part</label>
        <referenceTo>SQX_Part__c</referenceTo>
        <relationshipLabel>Supplier Parts</relationshipLabel>
        <relationshipName>SQX_Supplier_Parts</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Reason_For_Change__c</fullName>
        <description>This field is used to keep track of the reason for changing value of any field of the object.</description>
        <externalId>false</externalId>
        <label>Reason For Change</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Supplier_Part_Family__c</fullName>
        <description>Indicates the Part Family Name.</description>
        <externalId>false</externalId>
        <formula>Part__r.Part_Family__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Part Family</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Supplier_Part_Risk_Level__c</fullName>
        <description>Indicates Risk level of a Part. 
It only takes values 1-5. 
1 indicates that the Part has highest Risk Level. 
5 indicates that the Part has lowest Risk Level.</description>
        <externalId>false</externalId>
        <label>Part Risk Level</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Supplier_Part_Type__c</fullName>
        <description>This field describes the category/type of a part such as manufacturing/assembly</description>
        <externalId>false</externalId>
        <formula>TEXT(Part__r.Part_Type__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Part Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Supplier_Reference_Part_Number__c</fullName>
        <description>It stores 18 character ID of Account concatenated with 18 character ID of Part.</description>
        <externalId>false</externalId>
        <label>Supplier Reference Part Number</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Uniqueness_Constraint__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>**Internal usage field to ensure that duplicate supplier part combination is not possible</description>
        <externalId>false</externalId>
        <label>Uniqueness Constraint</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Supplier Part</label>
    <nameField>
        <description>Auto number used for uniquely identifying the supplier part</description>
        <displayFormat>SP-{000000}</displayFormat>
        <label>Supplier Part Number</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Supplier Parts</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Part__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Supplier_Part_Risk_Level__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Supplier_Reference_Part_Number__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Approved__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Active__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Part__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Supplier_Part_Risk_Level__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Supplier_Reference_Part_Number__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Part_Name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Approved__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Active__c</lookupPhoneDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>It_cant_be_active_when_disapproved</fullName>
        <active>true</active>
        <description>A supplier part can not be activated without being approved.</description>
        <errorConditionFormula>IF( Approved__c == false,  Active__c , false)</errorConditionFormula>
        <errorMessage>A supplier part can not be activated without being approved.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Supplier_Is_Inactive_Cant_Be_Active</fullName>
        <active>true</active>
        <description>A supplier part cannot be active when the supplier is inactive.</description>
        <errorConditionFormula>IF(ISPICKVAL(Account__r.Status__c,&apos;Inactive&apos;), 
IF( Active__c = true, true, false), false)</errorConditionFormula>
        <errorDisplayField>Active__c</errorDisplayField>
        <errorMessage>A supplier part can only be active if the supplier is also active.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Supplier_Is_Not_Approved_Part_Cant_Be</fullName>
        <active>true</active>
        <description>When a supplier is not approved, the supplier-part cannot be approved.</description>
        <errorConditionFormula>IF(OR(ISPICKVAL( Account__r.Approval_Status__c , &apos;Approved&apos;),
ISPICKVAL( Account__r.Approval_Status__c , &apos;Conditionally Approved&apos;)),
false, IF( Approved__c , true, false))</errorConditionFormula>
        <errorDisplayField>Approved__c</errorDisplayField>
        <errorMessage>Part can not be approved, while related supplier is disapproved.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Validating_Supplier_Part_ACTIVE_Status</fullName>
        <active>true</active>
        <description>A Supplier Part cannot be ACTIVE when the corresponding Part is INACTIVE</description>
        <errorConditionFormula>(Part__r.Active__c  = FALSE)  &amp;&amp;  (Active__c = TRUE)</errorConditionFormula>
        <errorMessage>This Supplier Part  cannot be added as ACTIVE because the corresponding Part is INACTIVE. However it could be added as INACTIVE.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Check_Either_Part_Name_Or_Part</fullName>
        <active>true</active>
        <description>Either any one of Part Name or Parts.</description>
        <errorConditionFormula>AND(ISBLANK( Part_Text__c ),ISBLANK( Part__c )) || AND(NOT(ISBLANK( Part_Text__c )),NOT(ISBLANK( Part__c )))</errorConditionFormula>
        <errorMessage>Either Part or Part Name should be added.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
