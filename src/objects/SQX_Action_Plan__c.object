<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Action Plan records a plan to solve/fix/prevent issues discovered by investigation.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Approved__c</fullName>
        <defaultValue>false</defaultValue>
        <description>When checked, it indicates that the Action Plan has been approved by the customer.</description>
        <externalId>false</externalId>
        <label>Approved</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Completed_By__c</fullName>
        <description>Person responsible for completing</description>
        <externalId>false</externalId>
        <label>Completed By</label>
        <length>120</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Completed__c</fullName>
        <defaultValue>false</defaultValue>
        <description>When checked, it indicates that the Action Plan has already been implemented by the supplier.</description>
        <externalId>false</externalId>
        <label>Completed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Completion_Date__c</fullName>
        <description>It indicates the date on which action plan is implemented.</description>
        <externalId>false</externalId>
        <label>Completion Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Days_Required__c</fullName>
        <description>Records number of days to complete action created from the approved action plan.</description>
        <externalId>false</externalId>
        <label>Days Required</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Description of the plan</description>
        <externalId>false</externalId>
        <inlineHelpText>Please enter what you are planning to do to resolve the issue</inlineHelpText>
        <label>Description</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Due_Date__c</fullName>
        <description>It is the date by which the Action plan needs to be implemented.</description>
        <externalId>false</externalId>
        <label>Due Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Plan_Number__c</fullName>
        <defaultValue>&apos;1&apos;</defaultValue>
        <description>Provides a customer to provide a custom sequence number</description>
        <externalId>false</externalId>
        <label>Plan Number</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Plan_Type__c</fullName>
        <description>It refers to the type of Action. It could be corrective or preventive.</description>
        <externalId>false</externalId>
        <label>Plan Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Corrective</fullName>
                    <default>true</default>
                    <label>Corrective</label>
                </value>
                <value>
                    <fullName>Preventive</fullName>
                    <default>false</default>
                    <label>Preventive</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SQX_Investigation__c</fullName>
        <description>The investigation for which the plan is created.</description>
        <externalId>false</externalId>
        <inlineHelpText>Please select the investigation which the action plan is trying to fix/solve/prevent.</inlineHelpText>
        <label>Investigation</label>
        <referenceTo>SQX_Investigation__c</referenceTo>
        <relationshipLabel>Action Plans</relationshipLabel>
        <relationshipName>SQX_Action_Plans</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>SQX_User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field records the user to whom the task has been assigned. This user will be responsible for completing the task once it needs to be implemented. Additionally, if a plan is completed directly this field can be used for the purpose of recording the user who performed the task.</description>
        <externalId>false</externalId>
        <label>Assignee</label>
        <referenceTo>User</referenceTo>
        <relationshipName>SQX_Action_Plans</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Action Plan</label>
    <nameField>
        <description>Auto number used for uniquely identifying the action plan</description>
        <displayFormat>APN-{000000}</displayFormat>
        <label>Action Plan Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Action Plans</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Plan_Number__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Plan_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Approved__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Due_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>UPDATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Completed__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Plan_Number__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Plan_Type__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Description__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Approved__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Due_Date__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>UPDATEDBY_USER</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Completed__c</lookupPhoneDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>Completion_date_is_required_if_complete</fullName>
        <active>true</active>
        <description>It ensures that completion date is required when action is marked as completed.</description>
        <errorConditionFormula>IF(Completed__c, ISNULL( Completion_Date__c  ), false)</errorConditionFormula>
        <errorDisplayField>Completion_Date__c</errorDisplayField>
        <errorMessage>Completion date is required</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Due_date_is_required_only_in_incomplete</fullName>
        <active>true</active>
        <description>Rule to ensure days required or due date is added.</description>
        <errorConditionFormula>AND(ISNULL( Completion_Date__c ),  Completed__c == false,
ISNULL( Due_Date__c ), ISBLANK( Days_Required__c ) )</errorConditionFormula>
        <errorDisplayField>Due_Date__c</errorDisplayField>
        <errorMessage>Due date or Days Required is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Ensure_completion_date_is_in_complete</fullName>
        <active>true</active>
        <description>Ensures that a completion date can only be entered when action plan is complete</description>
        <errorConditionFormula>IF( Completed__c == false,  ISNULL(  Completion_Date__c ) == false, false)</errorConditionFormula>
        <errorDisplayField>Completion_Date__c</errorDisplayField>
        <errorMessage>Completion date can only be filled in when a action plan is completed</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Ensure_completion_date_is_not_in_future</fullName>
        <active>true</active>
        <description>It validates that a Action's completion date cannot be a future date.</description>
        <errorConditionFormula>Completion_Date__c &gt;  TODAY()</errorConditionFormula>
        <errorDisplayField>Completion_Date__c</errorDisplayField>
        <errorMessage>Completion date can not be a future date</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Prevent_negative_days_required</fullName>
        <active>true</active>
        <description>This rule ensures that negative days required are not entered</description>
        <errorConditionFormula>Days_Required__c &lt; 0</errorConditionFormula>
        <errorDisplayField>Days_Required__c</errorDisplayField>
        <errorMessage>Days Required cannot be less than zero.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
