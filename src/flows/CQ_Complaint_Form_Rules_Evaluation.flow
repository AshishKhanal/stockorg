<?xml version="1.0" encoding="UTF-8"?>
<Flow xmlns="http://soap.sforce.com/2006/04/metadata">
    <assignments>
        <name>Assign_Evaluate_Expression</name>
        <label>Assign Evaluate Expression</label>
        <locationX>180</locationX>
        <locationY>128</locationY>
        <assignmentItems>
            <assignToReference>EvaluationExpression</assignToReference>
            <operator>Assign</operator>
            <value>
                <elementReference>EvaluationExpressionTemplate</elementReference>
            </value>
        </assignmentItems>
    </assignments>
    <decisions>
        <name>Check_if_action_is_create</name>
        <label>Check if action is create</label>
        <locationX>180</locationX>
        <locationY>12</locationY>
        <defaultConnectorLabel>[Default Outcome]</defaultConnectorLabel>
        <rules>
            <name>Action_is_create</name>
            <conditionLogic>or</conditionLogic>
            <conditions>
                <leftValueReference>Action</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>create</stringValue>
                </rightValue>
            </conditions>
            <conditions>
                <leftValueReference>Action</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>edit</stringValue>
                </rightValue>
            </conditions>
            <connector>
                <targetReference>Assign_Evaluate_Expression</targetReference>
            </connector>
            <label>Action is create</label>
        </rules>
    </decisions>
    <description>Flow to evaluate form rules when complaint fields are changed</description>
    <interviewLabel>CQ Complaint - Form Rules Evaluation {!$Flow.CurrentDateTime}</interviewLabel>
    <label>CQ Complaint - Form Rules Evaluation</label>
    <processMetadataValues>
        <name>BuilderType</name>
        <value>
            <stringValue>LightningFlowBuilder</stringValue>
        </value>
    </processMetadataValues>
    <processType>AutoLaunchedFlow</processType>
    <startElementReference>Check_if_action_is_create</startElementReference>
    <status>Active</status>
    <textTemplates>
        <description>This text template holds evaluation expression.</description>
        <name>EvaluationExpressionTemplate</name>
        <text>[
    {
        &quot;field&quot;: &quot;compliancequest__SQX_Lot_Info__c&quot;,
        &quot;operator&quot;: &quot;changed&quot;,
        &quot;invoke&quot;: { &quot;ns&quot;: &quot;compliancequest&quot;, &quot;name&quot;:   &quot;CQ_Complaint_Set_Part_On_Lot_Info_Change&quot; }
    },
    {
        &quot;field&quot;:&quot;compliancequest__Patient_Involved__c&quot;,
        &quot;operator&quot;:&quot;eq&quot;,
        &quot;value&quot;:true,
        &quot;setRequired&quot;:[&quot;compliancequest__Patient_Gender__c&quot;,
                                &quot;compliancequest__Patient_Age__c&quot;,
                                &quot;compliancequest__Patient_Weight__c&quot;,
                                &quot;compliancequest__Unit_Of_Weight__c&quot;]
    },
    {
        &quot;field&quot;: &quot;compliancequest__SQX_Part__c&quot;,
        &quot;operator&quot;: &quot;changed&quot;,
        &quot;invoke&quot;: { &quot;ns&quot;: &quot;compliancequest&quot;, &quot;name&quot;:   &quot;CQ_Complaint_Set_Part_Family_when_Part_is_set&quot; }
    },
    {
        &quot;field&quot;: &quot;compliancequest__SQX_Part__c&quot;,
        &quot;operator&quot;: &quot;isnull&quot;,
        &quot;invoke&quot;: { &quot;ns&quot;: &quot;compliancequest&quot;, &quot;name&quot;:   &quot;CQ_Complaint_Set_Part_Family_when_Part_is_set&quot; }
    },
    {
        &quot;field&quot;: &quot;compliancequest__CAPA_Escalation__c&quot;,
        &quot;operator&quot;: &quot;eq&quot;,
        &quot;value&quot;:true,
        &quot;setRequired&quot;: [&quot;compliancequest__SQX_CAPA__c&quot;]
    },
    {
        &quot;field&quot;: &quot;compliancequest__Change_Control__c&quot;,
        &quot;operator&quot;: &quot;eq&quot;,
        &quot;value&quot;:true,
        &quot;setRequired&quot;: [&quot;compliancequest__Change_Control_Number__c&quot;]
    },
    {
        &quot;field&quot;: &quot;compliancequest__SQX_External_Contact__c&quot;,
        &quot;operator&quot;: &quot;changed&quot;,
        &quot;invoke&quot;: { &quot;ns&quot;: &quot;compliancequest&quot;, &quot;name&quot;:   &quot;CQ_Complaint_Set_Account_From_Contact&quot; }
    },
    {
        &quot;field&quot;: &quot;compliancequest__SQX_External_Contact__c&quot;,
        &quot;operator&quot;: &quot;isnull&quot;,
        &quot;invoke&quot;: { &quot;ns&quot;: &quot;compliancequest&quot;, &quot;name&quot;:   &quot;CQ_Complaint_Set_Account_From_Contact&quot; }
    }
]</text>
    </textTemplates>
    <variables>
        <description>this variable holds the action for which the flow is called.</description>
        <name>Action</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>false</isOutput>
    </variables>
    <variables>
        <description>Holds evaluate expression.</description>
        <name>EvaluationExpression</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>false</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>This variable holds array of sobject.</description>
        <name>InRecords</name>
        <dataType>SObject</dataType>
        <isCollection>true</isCollection>
        <isInput>true</isInput>
        <isOutput>false</isOutput>
        <objectType>SQX_Complaint__c</objectType>
    </variables>
    <variables>
        <description>Loop variables that holds Complaint record.</description>
        <name>LoopComplaintRecord</name>
        <dataType>SObject</dataType>
        <isCollection>false</isCollection>
        <isInput>false</isInput>
        <isOutput>false</isOutput>
        <objectType>SQX_Complaint__c</objectType>
    </variables>
    <variables>
        <description>Holds collection of sobject records.</description>
        <name>OutRecords</name>
        <dataType>SObject</dataType>
        <isCollection>true</isCollection>
        <isInput>false</isInput>
        <isOutput>true</isOutput>
        <objectType>SQX_Complaint__c</objectType>
    </variables>
</Flow>
