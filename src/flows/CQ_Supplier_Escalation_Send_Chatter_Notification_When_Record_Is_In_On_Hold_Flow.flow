<?xml version="1.0" encoding="UTF-8"?>
<Flow xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionCalls>
        <name>Post_To_Chatter_When_Record_Is_On_Hold</name>
        <label>Post To Chatter When Record Is On Hold</label>
        <locationX>697</locationX>
        <locationY>70</locationY>
        <actionName>chatterPost</actionName>
        <actionType>chatterPost</actionType>
        <inputParameters>
            <name>subjectNameOrId</name>
            <value>
                <elementReference>supplierEscalation.Id</elementReference>
            </value>
        </inputParameters>
        <inputParameters>
            <name>text</name>
            <value>
                <elementReference>Post_to_Chatter_When_Supplier_Escalation_is_in_On_Hold</elementReference>
            </value>
        </inputParameters>
    </actionCalls>
    <description>This flow will post message to chatter wheneer Supplier Escalation record is in On Hold.</description>
    <interviewLabel>CQ Supplier Escalation Send Chatter Notification When Record Is In On Hold Flow {!$Flow.CurrentDateTime}</interviewLabel>
    <label>CQ Supplier Escalation Send Chatter Notification When Record Is In On Hold Flow</label>
    <processType>AutoLaunchedFlow</processType>
    <recordLookups>
        <name>Read_Account_Name</name>
        <label>Read Account Name</label>
        <locationX>544</locationX>
        <locationY>71</locationY>
        <assignNullValuesIfNoRecordsFound>true</assignNullValuesIfNoRecordsFound>
        <connector>
            <targetReference>Post_To_Chatter_When_Record_Is_On_Hold</targetReference>
        </connector>
        <filters>
            <field>Id</field>
            <operator>EqualTo</operator>
            <value>
                <elementReference>supplierEscalation.SQX_Account__c</elementReference>
            </value>
        </filters>
        <object>Account</object>
        <outputAssignments>
            <assignToReference>accountName</assignToReference>
            <field>Name</field>
        </outputAssignments>
    </recordLookups>
    <recordLookups>
        <name>Read_Custom_Setting</name>
        <label>Read Custom Setting</label>
        <locationX>245</locationX>
        <locationY>77</locationY>
        <assignNullValuesIfNoRecordsFound>false</assignNullValuesIfNoRecordsFound>
        <connector>
            <targetReference>Read_Part</targetReference>
        </connector>
        <object>SQX_Custom_Settings_Public__c</object>
        <outputAssignments>
            <assignToReference>orgBaseUrl</assignToReference>
            <field>Org_Base_URL__c</field>
        </outputAssignments>
    </recordLookups>
    <recordLookups>
        <name>Read_Part</name>
        <label>Read Part</label>
        <locationX>398</locationX>
        <locationY>73</locationY>
        <assignNullValuesIfNoRecordsFound>true</assignNullValuesIfNoRecordsFound>
        <connector>
            <targetReference>Read_Account_Name</targetReference>
        </connector>
        <filters>
            <field>Id</field>
            <operator>EqualTo</operator>
            <value>
                <elementReference>supplierEscalation.SQX_Part__c</elementReference>
            </value>
        </filters>
        <object>SQX_Part__c</object>
        <outputAssignments>
            <assignToReference>partName</assignToReference>
            <field>Name</field>
        </outputAssignments>
    </recordLookups>
    <recordLookups>
        <name>Read_Supplier_Escalation</name>
        <label>Read Supplier Escalation</label>
        <locationX>85</locationX>
        <locationY>75</locationY>
        <assignNullValuesIfNoRecordsFound>true</assignNullValuesIfNoRecordsFound>
        <connector>
            <targetReference>Read_Custom_Setting</targetReference>
        </connector>
        <filters>
            <field>Id</field>
            <operator>EqualTo</operator>
            <value>
                <elementReference>supplierEscalationStep.SQX_Parent__c</elementReference>
            </value>
        </filters>
        <object>SQX_Supplier_Escalation__c</object>
        <outputAssignments>
            <assignToReference>supplierEscalation.Id</assignToReference>
            <field>Id</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>supplierEscalation.Name</assignToReference>
            <field>Name</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>supplierEscalation.SQX_Account__c</assignToReference>
            <field>SQX_Account__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>supplierEscalation.SQX_Part__c</assignToReference>
            <field>SQX_Part__c</field>
        </outputAssignments>
    </recordLookups>
    <startElementReference>Read_Supplier_Escalation</startElementReference>
    <status>Active</status>
    <textTemplates>
        <description>This text template holds the message that is posted to Chatter.</description>
        <name>Post_to_Chatter_When_Supplier_Escalation_is_in_On_Hold</name>
        <text>Supplier Escalation {!supplierEscalation.Name} is On Hold due to the following comment and needs your attention.
{!supplierEscalationStep.Comment__c}

Account Name: {!accountName}
Product: {!partName}

Click on the link below to access the record -
{!orgBaseUrl}{!supplierEscalation.Id}</text>
    </textTemplates>
    <variables>
        <description>holds name of account of supplier escalation</description>
        <name>accountName</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>false</isInput>
        <isOutput>false</isOutput>
    </variables>
    <variables>
        <description>holds base url of org.</description>
        <name>orgBaseUrl</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>false</isInput>
        <isOutput>false</isOutput>
    </variables>
    <variables>
        <description>holds part name.</description>
        <name>partName</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>false</isInput>
        <isOutput>false</isOutput>
    </variables>
    <variables>
        <description>holds supplier escalation record.</description>
        <name>supplierEscalation</name>
        <dataType>SObject</dataType>
        <isCollection>false</isCollection>
        <isInput>false</isInput>
        <isOutput>false</isOutput>
        <objectType>SQX_Supplier_Escalation__c</objectType>
    </variables>
    <variables>
        <description>holds supplier escalation step record</description>
        <name>supplierEscalationStep</name>
        <dataType>SObject</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>false</isOutput>
        <objectType>SQX_Supplier_Escalation_Step__c</objectType>
    </variables>
</Flow>
