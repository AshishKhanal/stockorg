<?xml version="1.0" encoding="UTF-8"?>
<Flow xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionCalls>
        <name>NavToComplaint</name>
        <label>NavToComplaint</label>
        <locationX>229</locationX>
        <locationY>331</locationY>
        <actionName>compliancequest:SQX_FlowAction_NavigateToRecord</actionName>
        <actionType>component</actionType>
        <inputParameters>
            <name>sObjId</name>
            <value>
                <elementReference>vComplaintId</elementReference>
            </value>
        </inputParameters>
    </actionCalls>
    <decisions>
        <name>Check_if_status_is_Open</name>
        <label>Check if status is Open</label>
        <locationX>65</locationX>
        <locationY>125</locationY>
        <defaultConnector>
            <targetReference>Show_Error_Message</targetReference>
        </defaultConnector>
        <defaultConnectorLabel>Status is not Open</defaultConnectorLabel>
        <rules>
            <name>Status_is_Open</name>
            <conditionLogic>and</conditionLogic>
            <conditions>
                <leftValueReference>ComplaintRecord.Status__c</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>Open</stringValue>
                </rightValue>
            </conditions>
            <connector>
                <targetReference>Choose_Decision_Tree_Task</targetReference>
            </connector>
            <label>Status is Open</label>
        </rules>
    </decisions>
    <description>This flow is used to execute decision tree task.</description>
    <dynamicChoiceSets>
        <name>SelectDecisionTrees</name>
        <dataType>String</dataType>
        <displayField>Name</displayField>
        <filters>
            <field>SQX_Complaint__c</field>
            <operator>EqualTo</operator>
            <value>
                <elementReference>vComplaintId</elementReference>
            </value>
        </filters>
        <filters>
            <field>Task_Type__c</field>
            <operator>EqualTo</operator>
            <value>
                <stringValue>Decision Tree</stringValue>
            </value>
        </filters>
        <object>SQX_Complaint_Task__c</object>
        <outputAssignments>
            <assignToReference>vTaskId</assignToReference>
            <field>SQX_Task__c</field>
        </outputAssignments>
        <sortField>CreatedDate</sortField>
        <sortOrder>Asc</sortOrder>
        <valueField>Name</valueField>
    </dynamicChoiceSets>
    <interviewLabel>CQ Execute Decision Tree {!$Flow.CurrentDateTime}</interviewLabel>
    <label>CQ Execute Decision Tree</label>
    <processType>Flow</processType>
    <recordLookups>
        <name>Query_Complaint_to_check_status</name>
        <label>Query Complaint to check status</label>
        <locationX>66</locationX>
        <locationY>21</locationY>
        <assignNullValuesIfNoRecordsFound>true</assignNullValuesIfNoRecordsFound>
        <connector>
            <targetReference>Check_if_status_is_Open</targetReference>
        </connector>
        <filters>
            <field>Id</field>
            <operator>EqualTo</operator>
            <value>
                <elementReference>vComplaintId</elementReference>
            </value>
        </filters>
        <object>SQX_Complaint__c</object>
        <outputReference>ComplaintRecord</outputReference>
        <queriedFields>Status__c</queriedFields>
    </recordLookups>
    <screens>
        <name>Choose_Decision_Tree_Task</name>
        <label>Choose Decision Tree Task</label>
        <locationX>65</locationX>
        <locationY>234</locationY>
        <allowBack>false</allowBack>
        <allowFinish>true</allowFinish>
        <allowPause>false</allowPause>
        <connector>
            <targetReference>Call_Execute_Script_Engine_for_Decision_Tree</targetReference>
        </connector>
        <fields>
            <name>Select_Decision_Tree_Task_to_run</name>
            <choiceReferences>SelectDecisionTrees</choiceReferences>
            <dataType>String</dataType>
            <fieldText>Select Decision Tree Task to run</fieldText>
            <fieldType>DropdownBox</fieldType>
            <isRequired>false</isRequired>
        </fields>
        <fields>
            <name>NavFooter</name>
            <extensionName>compliancequest:SQX_Flow_Navigation_Footer</extensionName>
            <fieldType>ComponentInstance</fieldType>
            <inputParameters>
                <name>customLabelOfButton</name>
                <value>
                    <stringValue>Run Decision Tree</stringValue>
                </value>
            </inputParameters>
            <isRequired>true</isRequired>
        </fields>
        <showFooter>false</showFooter>
        <showHeader>false</showHeader>
    </screens>
    <screens>
        <name>Show_Error_Message</name>
        <label>Show Error Message</label>
        <locationX>307</locationX>
        <locationY>130</locationY>
        <allowBack>false</allowBack>
        <allowFinish>true</allowFinish>
        <allowPause>true</allowPause>
        <fields>
            <name>ErrorMessage</name>
            <fieldText>&lt;DIV ALIGN=&quot;LEFT&quot;&gt;&lt;FONT FACE=&quot;Arial&quot; STYLE=&quot;font-size:12px&quot; COLOR=&quot;#FF0000&quot; LETTERSPACING=&quot;0&quot; KERNING=&quot;0&quot;&gt;{!StatusIsNotOpen}&lt;/FONT&gt;&lt;/DIV&gt;</fieldText>
            <fieldType>DisplayText</fieldType>
        </fields>
        <showFooter>true</showFooter>
        <showHeader>true</showHeader>
    </screens>
    <startElementReference>Query_Complaint_to_check_status</startElementReference>
    <status>Active</status>
    <subflows>
        <name>Call_Execute_Script_Engine_for_Decision_Tree</name>
        <label>Call Execute Script Engine for Decision Tree</label>
        <locationX>65</locationX>
        <locationY>327</locationY>
        <connector>
            <targetReference>NavToComplaint</targetReference>
        </connector>
        <flowName>CQ_Execute_Script</flowName>
        <inputAssignments>
            <name>vTaskId</name>
            <value>
                <elementReference>vTaskId</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <name>vComplaintId</name>
            <value>
                <elementReference>vComplaintId</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <name>ExecutionType</name>
            <value>
                <stringValue>Decision Tree</stringValue>
            </value>
        </inputAssignments>
    </subflows>
    <textTemplates>
        <description>This text template is used to show error message whenever user tries to run decision tree but status of complaint is not open.</description>
        <name>StatusIsNotOpen</name>
        <text>Decision Tree cannot be run because Complaint is not in Open status.</text>
    </textTemplates>
    <variables>
        <description>holds complaint record.</description>
        <name>ComplaintRecord</name>
        <dataType>SObject</dataType>
        <isCollection>false</isCollection>
        <isInput>false</isInput>
        <isOutput>false</isOutput>
        <objectType>SQX_Complaint__c</objectType>
    </variables>
    <variables>
        <description>Complaint Id to associate with the script</description>
        <name>vComplaintId</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>false</isOutput>
    </variables>
    <variables>
        <description>Holds id of task.</description>
        <name>vTaskId</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
</Flow>
